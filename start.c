#include "raklink.h"
#include "start.h"

#ifndef BSP_NOT_SUPPORT_WEAK
__RAKLINK_WEAK void app_start_init(void)
{
    /*
        week function
    */
}

__RAKLINK_WEAK void app_start_loop(uint32_t event, uint8_t *data, uint16_t data_len)
{
    /*
        week function
    */
}
#endif

extern void app_start_init(void);
extern void app_start_loop(uint32_t event, uint8_t *data, uint16_t data_len);

void start(void)
{
    static uint8_t start_init = 1;
    if ( start_init == 1 )
    {
        *(init_handle_t *)(emos.vsys->reg_init_handle) = &app_start_init;
        *(loop_handle_t *)(emos.vsys->reg_loop_handle) = &app_start_loop;
        emos.vsys->init();
        start_init = 0;
    }
    else
    {
        emos.vsys->run();
    }
}
