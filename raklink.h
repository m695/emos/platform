#ifndef __RAKLINK_H__
#define __RAKLINK_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <setjmp.h>
#include <math.h>

#define __RAKLINK_ENABLE_IRQ     
#define __RAKLINK_DISABLE_IRQ    
#define __RAKLINK_NOINLINE        __attribute__((noinline))
#define __RAKLINK_PACKED          __attribute__((packed))
#define __RAKLINK_UNUSED          __attribute__((unused))
#define __RAKLINK_USED            __attribute__((used))
#define __RAKLINK_OPTZ(X)         __attribute__((optimize(#X)))

#define __RAKLINK_WEAK            __attribute__((weak))
#define __RAKLINK_WEAKLINK(X)     __attribute__((weak,alias(#X)))
#define __RAKLINK_AT(X)           __attribute__((at(X)))
#define IENABLE                    1
#define IDISABLE                   0
#define ICHECK(X,Y)                 if(X==NULL) return Y;


#define UNUSED(X)                 (void)X

#define ARR2INT(X)                (X[0]<<0)|(X[1]<<8)|(X[2]<<16)|(X[3]<<24)
#define ARR2SHORT(X)              (X[0]<<0)|(X[1]<<8)
#define U16T_SWAP(X)              (((X&0x00FF)<<8)|((X&0xFF00)>>8))     
#define U32T_SWAP(X)              (((X&0x000000FF)<<24)|((X&0x0000FF00)<<8)|((X&0x00FF0000)>>8)|((X&0xFF000000)>>24))    

#define MASK_BYTE(X,S)            0xFF&(X>>(8*S))
#define SHORT_SWAP(M,L)           ((L<<8) + M)

#define ENABLE_CHECK(X)           ((X!=0) && (X!=1))

#define MSEC(X)                   (volatile uint32_t)(X*1000)

#define SHORT(A,B)                  ((B&0xff)<<8 | (A&0xff))
#define BIT(n)                      (1 << (n))
#define CKB1(REG,POS)               ((REG>>POS) & 1) 
#define CKB0(REG,POS)               (!((REG>>POS) & 1)) 
#define SETB(REG,POS)               (REG |= (1 << POS))
#define CLRB(REG,POS)               (REG &= (~(1 << POS)))
#define ORB(REG,POS)                (REG |= (1 << POS))
#define NOTB(REG,POS)               (REG &= (~(1 << POS)))
#define CFGB(REG,POS,VAR)           (VAR==1)?SETB(REG,POS):CLRB(REG,POS)

#define GPIO_MASK(X)              (X/100),(X%100)


#define COUNT_OF(x)               ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))
#define SIZE_OF(x)                sizeof(x)

#define __CURRENT_PATH(x)         #x
#define __RakPath(x)              __CURRENT_PATH(src/x)

/***********************************************************/

#define __EMOS_PACKED        __RAKLINK_PACKED
#define __EMOS_NOINLINE      __RAKLINK_NOINLINE
#define __EMOS_WEAK          __RAKLINK_WEAK
#define __EMOS_WEAKLINK      __RAKLINK_WEAKLINK(emos_dummy)

#include "boards/boards_def.h"
#include "kernel/kernel.h"
#include "kernel/emos/emos.h"



#ifdef __cplusplus
}
#endif

#endif
