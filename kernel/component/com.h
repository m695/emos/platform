#ifndef __COM__H__
#define __COM__H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../kernel.h"

#define com_source_id             (com_global.src_id)
#define com_mydev                 com.vsys->mydev
#define com_iprobe                (com_mydev()->iprobe)
#define com_sprobe                com.rak_probe
#define com_get_snsr(PID,SID,P)   com_sprobe(PID).snsr_get(SID, P)
#define com_get_probe(PID,P)      com_sprobe(PID).probe_get(P)
#define com_tlv_decode(SRC,LEN,CB)   com.units->tlv_decode(SRC,LEN,1,2,CB)

#define com_vsys_tgsend( TID, MSG, TAG, DAT, LEN )                 com_vsys_send_hook(TID, TAG, MSG, (uint8_t *)DAT, LEN)
#define com_vsys_send( TID, MSG, DAT, LEN )                        com_vsys_send_hook(TID, com_source_id, MSG, (uint8_t *)DAT, LEN)
#define com_vsys_mrsend( TID, MSG, DAT1, LEN1, DAT2, LEN2 )        com_vsys_mrsend_hook(TID, com_source_id, MSG, (uint8_t *)DAT1, LEN1, (uint8_t *)DAT2, LEN2)
#define com_vsys_sendend( TID, MSG, DAT, LEN )                     com_vsys_mrsend(TID, MSG, DAT, LEN ,"\0",1)
#ifndef com_vsys_sendprf
#define com_vsys_sendprf(TID,MSG,...) \
do { \
    uint8_t *this_com_evtmsg = emos_outext(__VA_ARGS__); \
    com_vsys_sendend( TID, MSG, (uint8_t *)this_com_evtmsg, strlen((char *)this_com_evtmsg)); \
} while (0)
#endif

#ifndef com_vsys_sendprfe
#define com_vsys_sendprfe(TID,MSG,...) \
do { \
    uint8_t *this_com_evtmsg = emos_outext(__VA_ARGS__); \
    com_vsys_send( TID, MSG, (uint8_t *)this_com_evtmsg, strlen((char *)this_com_evtmsg)); \
} while (0)
#endif
#define nbiot_printf(...) \
do { \
    uint8_t *this_com_evtmsg = emos_outext(__VA_ARGS__); \
    com.nbiot->enter.puts((uint8_t *)this_com_evtmsg, strlen((char *)this_com_evtmsg));\
} while (0)

#define com_malloc          com_malloc_hook//com.vsys->mem()->malloc
#define com_free            com_free_hook//com.vsys->mem()->free
#define com_copy            com_copy_hook//com.vsys->mem()->copy
#define com_task_wakeup( TID )               com.vsys->mbox()->sleep(TID, IDISABLE);
#define com_task_sleep( TID )                com.vsys->mbox()->sleep(TID, IENABLE);
#define com_task_timeout( TID,TIMEOUT )      com.vsys->mbox()->timeout(TID, TIMEOUT);
#define com_flash_bank0_read   com.flashmap->bk0_con_read
#define com_flash_bank0_readonly   com.flashmap->bk0_con_readonly
#define com_flash_bank0_write  com.flashmap->bk0_con_write
#define com_flash_bank1_read   com.flashmap->bk1_con_read
#define com_flash_bank1_write  com.flashmap->bk1_con_write
#define com_flash_bank2_read   com.flashmap->bk2_con_read
#define com_flash_bank2_write  com.flashmap->bk2_con_write
#define com_flash_bank2_readonly   com.flashmap->bk2_con_readonly
#define com_flash_bank4_read   com.flashmap->bk4_con_read
#define com_flash_bank4_readonly   com.flashmap->bk4_con_readonly
#define com_flash_bank4_write  com.flashmap->bk4_con_write

#define com_ipso_size          com.units->ipso_size

#define com_pri_timer_shot     emos_timer0_shot
#define com_pri_timer_start    emos_timer0_start
#define com_pri_timer_stop     emos_timer0_stop
#define com_sec_timer_shot     emos_timer1_shot
#define com_sec_timer_start    emos_timer1_start
#define com_sec_timer_stop     emos_timer1_stop

#define DBG_ALL(...)           emos.log->debug(EMOS_LOG_ALL,__VA_ARGS__)
#define DBG_ALLA(D,L)          emos.log->debugA(EMOS_LOG_ALL,D,L)

#define DBG_INFO(...)          emos.log->debug(EMOS_LOG_INFO,__VA_ARGS__)
#define DBG_INFOA(D,L)         emos.log->debugA(EMOS_LOG_INFO,D,L)

#define DBG_WIRE(...)          emos.log->debug(EMOS_LOG_EVT0,"[WI]:");emos.log->debug(EMOS_LOG_EVT0,__VA_ARGS__)
#define DBG_WIREA(D,L)         emos.log->debug(EMOS_LOG_EVT0,"[WI]:");emos.log->debugA(EMOS_LOG_EVT0,D,L)

#define DBG_BLE(...)           emos.log->debug(EMOS_LOG_EVT1,"[BLE]:");emos.log->debug(EMOS_LOG_EVT1,__VA_ARGS__)
#define DBG_BLEA(D,L)          emos.log->debug(EMOS_LOG_EVT1,"[BLE]:");emos.log->debugA(EMOS_LOG_EVT1,D,L)

#define DBG_NBIOT(...)         emos.log->debug(EMOS_LOG_EVT2,"[NB]:");emos.log->debug(EMOS_LOG_EVT2,__VA_ARGS__)
#define DBG_NBIOTA(D,L)        emos.log->debug(EMOS_LOG_EVT2,"[NB]:");emos.log->debugA(EMOS_LOG_EVT2,D,L)

#define DIR_MSG(...)           com.dirp->tarprnf(__VA_ARGS__)
#define SYS_INFO(...)          com_vsys_sendprfe( COM_TASK_EVENT, COM_EVENT_SYS_INFO, __VA_ARGS__)//com.dirp->sysprnf(__VA_ARGS__)
// #define BSP_INFO(...)          com_vsys_sendprfe( COM_TASK_EVENT, COM_EVENT_SYS_INFO, __VA_ARGS__)//com.dirp->sysprnf(__VA_ARGS__)
#define SYS_INFOA(D,L)         com_vsys_sendend( COM_TASK_EVENT, COM_EVENT_SYS_INFO, D, L);


#define EVT_MSG(X)             "\r\n+EVT:" X "\r\n"
#define DO_MSG(X)              "\r\n+TODO:" X "\r\n"
#define RET_MSG(X)             "" X "\r\n"

#define NBIOT_MSG(...) \
do { \
    uint8_t *this_emos_tmsg = emos_outext(__VA_ARGS__); \
    com_vsys_sendend( COM_TASK_EVENT, COM_EVENT_NBIOT_TRANS, (uint8_t *)this_emos_tmsg, strlen((char *)this_emos_tmsg)); \
} while (0)

#define EMOS_PROCESS_TYPE     emos_msg_type_t *msg_type = (emos_msg_type_t *)&(task_msg->msg_type); UNUSED(msg_type);
#define EMOS_PROCESS_PAYLOAD  uint8_t *msg_payload = (uint8_t *)(task_msg->payload); UNUSED(msg_payload);
#define EMOS_PROCESS_LEN      uint16_t msg_len = task_msg->payload_len; UNUSED(msg_len);
#define COM_PROCESS_NAME(X)   com_##X##_process   

#ifndef emos_process
#define emos_process(NAME, DOFUNC) \
void COM_PROCESS_NAME(NAME)(emos_task_msg_t *task_msg) \
{ \
    EMOS_PROCESS_TYPE \
    EMOS_PROCESS_PAYLOAD \
    EMOS_PROCESS_LEN \
    com_global.src_id = task_msg->src_id; \
    DOFUNC \
}
#endif

#define emos_extern(NAME) extern void COM_PROCESS_NAME(NAME)(emos_task_msg_t *task_msg)

/******************************************************************************/
/*                            HUB API                                         */
/******************************************************************************/
#define com_nbiot_power               com_nbiot_power_hook //com.nbiot->power
#define com_nbiot_upload              com_nbiot_upload_hook //com.nbiot->upload
#define com_nbiotapp                  com_nbiotapp_hook
#define com_http_request              com_http_request_hook
#define com_nbiot_apply               com_nbiot_apply_hook //com.nbiot->apply
#define com_cli_cmd                   com_cli_cmd_hook //com.cli->command

typedef enum
{
    USER_EVENT_OWI_HUB_RECV = 0x1,
    USER_EVENT_USB_URT_RECV,
    USER_EVENT_URT1_RECV,
    USER_EVENT_URT2_RECV,
    USER_EVENT_BLE_RECV,
    USER_EVENT_IIC_RECV,
    USER_EVENT_NBIOT_RECV,
    USER_EVENT_OWI_CRC_ERROR,
    USER_EVENT_TIMER_SHOT, 
    USER_EVENT_TIMER0_TRIG, 
    USER_EVENT_TIMER1_TRIG,
    USER_EVENT_ATCMD,
    USER_EVENT_CMD_TIMEOUT,
    USER_EVENT_INIT_DONE,
    USER_EVENT_LORA_JOINED,
    USER_EVENT_LORA_UNJOIN,
    USER_EVENT_REBOOT,
    USER_EVENT_GPIO_TRIG,
    USER_EVENT_START_PREPARE,
    USER_EVENT_SENSOR_CKDONE,
    USER_EVENT_CLI_RSP,
    
    USER_EVENT_DATAREQ,
} USER_EVENT_ID_E;


/******************************************************************************/
/*                            RAK Probe                                    */
/******************************************************************************/

typedef struct
{
    uint8_t len;
    uint8_t id;
    uint8_t type;
    uint8_t value[10];
} __EMOS_PACKED com_if_rak_probe_sensor_data_t;


typedef struct
{
    uint8_t id;
    uint8_t idx;
    struct
    {
        uint8_t id;
        uint8_t type;
        union 
        {
            uint16_t rule;
            struct 
            {
                uint16_t alert:1;
                uint16_t below:1;
                uint16_t above:1;
                uint16_t periodic:1;
                uint16_t between:1;
                uint16_t diff:1;
                uint16_t disable:1;
                uint16_t noswap:1;
            };
        };
    } ctrl;

    union 
    {
        uint16_t value;
        struct 
        {
            uint16_t init:1;
            uint16_t busy:1;
            uint16_t dchg:1;
            uint16_t newdat:1;
            uint16_t alert:1;
            uint16_t alert_last:1;
            uint16_t stop:1;
        };
    } sta;
    
    com_if_rak_probe_sensor_data_t data;

    struct
    {
        uint8_t above[10];
        uint8_t below[10];
    } thr;
    
    uint32_t intv;
    uint32_t current_intv;
} __EMOS_PACKED com_if_rak_probe_sensor_t;


typedef struct
{
    
    void *next;
    com_if_rak_probe_sensor_t info;
} __EMOS_PACKED com_if_rak_probe_sensor_node_t;

typedef struct
{
    uint8_t  apply;
    uint8_t  gps_signal;
    uint8_t  mode[10];
    uint8_t  ip[60];
    uint32_t port;
    uint8_t  auth_username[50];
    uint8_t  auth_password[50];
    uint8_t  client_id[24];
    uint8_t  sub_topic[50];
    uint8_t  pub_topic[50];
    uint8_t  temp1;
    uint8_t  temp2;
    uint8_t  http_url[50];
    uint8_t  temp3;
    uint32_t  server_auth;
    uint32_t  user_auth;
    uint32_t  http_method;
    uint32_t  http_url_len;
    uint8_t data_format;

} __EMOS_PACKED com_if_rak_nbiot_data_t;

typedef struct
{
    struct
    {
        void *next; 
    } snsr_root;
    
    
    struct
    {
        emos_uin32t expect;
        emos_uin32t current;
    } type_id;

    union 
    {
        uint8_t          value[18];
        struct
        {
            uint8_t bom[8];
            uint8_t fac;
            uint8_t year[2];
            uint8_t month[2];
            uint8_t number[5];
        };
    } sn;

    union 
    {
        uint8_t          value[8];
        struct
        {
            uint8_t year[2];
            uint8_t month[2];
            uint8_t number[4];
        };
    } ssn;


    uint8_t id;
    

    uint8_t hw_version;
    uint8_t sw_version[3];
    uint8_t model_name[20];
    uint8_t tag_id;
    uint8_t tag_id_en;
    union 
    {
        uint16_t value;
        struct
        {
            uint16_t stop:1;
            uint16_t real:1;
            uint16_t nocheck:1;
        };
        
    } sta;
    
    uint32_t intv;
} __EMOS_PACKED com_if_rak_probe_param_t;

typedef struct
{  
    void *next;
    com_if_rak_probe_param_t info;
} __EMOS_PACKED com_if_rak_probe_node_t;

typedef struct
{
    void (*new_probe)      ( void );
    uint8_t (*probe_count) ( void );
    uint8_t (*probe_get)   ( com_if_rak_probe_param_t ** );
    uint8_t (*probe_walk)  ( com_if_rak_probe_param_t ** );
    void (*probe_del)      ( void );
    void (*new_sensor)     ( uint8_t ,uint8_t);
    uint8_t (*snsr_count)  ( void );
    uint8_t (*snsr_walk)   ( uint8_t, com_if_rak_probe_sensor_t ** );
    uint8_t (*snsr_get)    ( uint8_t, com_if_rak_probe_sensor_t ** );
    uint8_t (*snsr_save)      ( uint8_t );
    uint8_t (*probe_save)     ( void );

    void (*snsr_del)      ( uint8_t );

    uint8_t (*loc_probe_get) ( com_if_rak_probe_param_t ** );
} __attribute__((aligned(4))) com_if_rak_probe_t;

/******************************************************************************/
/*                            RAK Sensor                                    */
/******************************************************************************/

typedef union
{
    uint16_t value;
    struct
    {
        uint8_t sku;
        uint8_t exist:1;
    };
} __EMOS_PACKED com_if_rak_sensor_sta_t;


typedef struct
{
    struct 
    {
        void * (*check)   ( void *adr );
        void * (*sleep)   ( void *adr );
        void * (*wakeup)  ( void *adr );
        void * (*warmup)  ( void *adr );
        void * (*process) ( void *adr );
        void * (*read)    ( void *adr );
    } i;
    
    uint8_t (*check)    ( void );
    uint8_t (*sleep)    ( void );
    uint8_t (*wakeup)   ( void );
    uint16_t (*warmup)   ( void );
    uint8_t (*process)   ( void );
    uint16_t (*read) ( uint8_t * );
    uint8_t (*reset)    ( void );
    uint8_t (*new_csmzed)    ( void );
    com_if_rak_sensor_sta_t *(*status) ( void );

} __EMOS_PACKED com_if_rak_sensor_t;

/******************************************************************************/
/*                            RAK Protocol                                    */
/******************************************************************************/

#define ATCMD_REQ   0
#define ATCMD_RSP   1

typedef union
{
    uint8_t value;
    struct
    {
        uint8_t iscloud:1;
    };
} __EMOS_PACKED com_if_rak_protocol_ctrl_t;

typedef struct
{
    com_if_rak_protocol_ctrl_t *ctrl;
    void (*req_puts)  ( uint8_t *, uint16_t );
    void (*rsp_puts)  ( uint8_t *, uint16_t );
    uint8_t (*get_snsrcnt) ( uint8_t );
    uint8_t (*get_snsrintv) ( uint8_t , uint8_t);
    uint8_t (*get_snsrrule) ( uint8_t , uint8_t);
    uint8_t (*get_snsrdata) ( uint8_t , uint8_t);
    uint8_t (*get_snsrhthr) ( uint8_t , uint8_t);
    uint8_t (*get_snsrlthr) ( uint8_t , uint8_t);

    
    uint8_t (*set_snsrrule) ( uint8_t , uint8_t, uint16_t);
    uint8_t (*set_snsrintv) ( uint8_t , uint8_t, uint32_t);
    uint8_t (*set_snsrhthr) ( uint8_t , uint8_t, uint8_t*, uint8_t);
    uint8_t (*set_snsrlthr) ( uint8_t , uint8_t, uint8_t*, uint8_t);

    uint8_t (*set_scmd_add) ( uint8_t , uint8_t*, uint8_t);
    uint8_t (*set_scmd_rest) ( uint8_t );
    uint8_t (*set_scmd_del) ( uint8_t );
    uint8_t (*get_scmd_list) ( uint8_t );
    uint8_t (*snd_scmd_build) ( uint8_t );


    uint8_t (*get_prbintv) ( uint8_t );
    uint8_t (*set_prbintv) ( uint8_t , uint32_t);
    uint8_t (*get_prbsmplintv) ( uint8_t );
    uint8_t (*set_prbsmplintv) ( uint8_t , uint32_t);
    uint8_t (*set_prbrest) ( uint8_t );
    uint8_t (*set_prbdfu) ( uint8_t );
    uint8_t (*set_prbdel) ( uint8_t );
    uint8_t (*set_prbreboot) ( uint8_t );
    uint8_t (*snd_provision) ( void );
    uint8_t (*snd_hello) ( void );
    uint8_t (*snd_snsrdata) (uint8_t, uint8_t *, uint16_t );
    uint8_t (*snd_prbdisp) (uint8_t, uint8_t *, uint16_t );
    uint8_t (*get_snsrinfo) (uint8_t , uint8_t );
} __EMOS_PACKED com_if_rak_protocol_t;

/******************************************************************************/
/*                            RAK TRANSPARENT                                        */
/******************************************************************************/

typedef struct
{
    uint8_t interface;
    uint8_t pid;
    uint8_t frm_type;
    uint8_t dir;
    uint8_t pay_type;
} __EMOS_PACKED com_if_rak_tp_t;

typedef struct
{
    uint8_t (*sndtp) ( uint8_t *, uint16_t );
    uint8_t (*sndbtp) ( com_if_rak_tp_t *, uint8_t *, uint16_t );
} __EMOS_PACKED com_if_rak_transparent_t;

/******************************************************************************/
/*                            RAK IOCONTROL                                      */
/******************************************************************************/
typedef union
{
    uint16_t value;
    struct
    {
        uint16_t use:1;
    };
} emos_if_ioc_ctrl_t;

typedef struct
{
    uint8_t (*sndioc) ( uint8_t , uint8_t *, uint32_t , uint8_t , uint8_t  ,uint8_t );
    uint8_t (*sndsioc) ( uint8_t , uint8_t *, uint32_t , uint8_t *, uint8_t  ,uint8_t );
    uint8_t (*bakioc) ( uint8_t , uint8_t *, uint32_t , uint8_t , uint8_t  ,uint8_t );
    uint8_t (*uploadioc) ( uint8_t , uint8_t *, uint32_t );
    emos_if_ioc_ctrl_t *(*uartlock) (void);
    uint8_t (*io_idname) ( uint8_t *, uint8_t * );
    uint8_t (*get_iobase) ( uint8_t , uint8_t , uint8_t * );
    uint8_t (*get_strbyipso) ( uint8_t , uint8_t * );
} __EMOS_PACKED com_if_rak_iocontrol_t;

/******************************************************************************/
/*                            RESOURCE MAP                                        */
/******************************************************************************/

typedef struct 
{
    union
    {
        uint8_t field_check[4];
        byte_t check;
    };
    
    uint32_t mapver;
    
    union
    {
        uint8_t field_factory[64];
        struct
        {
            union
            {
                uint8_t value[18];
                struct
                {
                    uint8_t bom[8];
                    uint8_t fac;
                    uint8_t year[2];
                    uint8_t month[2];
                    uint8_t number[5];
                } int_srnum;
            };

            union
            {
                uint8_t value[8];
                struct
                {
                    uint8_t year[2];
                    uint8_t month[2];
                    uint8_t number[4];
                };
            } ext_srnum;
            
            uint8_t model_name[16];
            union 
            {
                uint32_t value;
                struct 
                {
                    uint8_t byte[4];
                };
            } type_id;
            
            uint8_t sw_version[8];
            uint8_t hw_version;
        };
    };

    uint8_t sku[6];
    uint32_t probe_interval;
    uint32_t sampling_interval;
    uint8_t lora_autojoin;
    uint8_t lora_joincnt;
    uint8_t lora_joindly;
    uint8_t tag_id;
    uint8_t tag_id_en;
    uint8_t io485;
    uint8_t keep_id; 
    uint8_t lpwan_mode;

} __EMOS_PACKED com_if_rak_bank0_t;

typedef struct 
{
    union 
    {
        uint8_t f[COM_SNSR_NODE_SIZE];
        struct 
        {
            uint8_t sensor_type;
            uint16_t rule_flg;
            uint8_t hthr[10];
            uint8_t lthr[10]; //Threshold
            uint32_t interval;
        };
    };

#if (COM_SNSR_NODE_SIZE > 64)

#endif

#if (COM_SNSR_NODE_SIZE > 128)

#endif

} __EMOS_PACKED com_if_rak_bank1_page_t;

typedef struct
{
    com_if_rak_nbiot_data_t nbiot_info;
    
} __EMOS_PACKED com_if_rak_bank2_t;


typedef struct 
{
    com_if_rak_bank1_page_t rule_setting[RAK_SNSR_NODE_NUM];
} __EMOS_PACKED com_if_rak_bank1_t;

typedef struct 
{
    void    (*bk0_con_write)  ( uint8_t*, uint16_t );
    void    (*bk0_con_read)   ( uint8_t*, uint16_t );
    void    (*bk0_con_readonly)   ( uint8_t**, uint16_t );

    void    (*bk1_con_write)  ( uint32_t addr ,uint8_t*, uint16_t );
    void    (*bk1_con_read)   ( uint32_t addr ,uint8_t*, uint16_t );

    void    (*bk2_con_write)  ( uint8_t*, uint16_t );
    void    (*bk2_con_read)   ( uint8_t*, uint16_t );
    void    (*bk2_con_readonly)   ( uint8_t**, uint16_t );


    
    void    (*bk4_con_write)  ( uint32_t addr ,uint8_t*, uint16_t );
    void    (*bk4_con_read)   ( uint8_t*, uint16_t );
    void    (*bk4_con_readonly)   ( uint8_t**, uint16_t );

    void    (*rest_rdy)       ( void );
    void    (*rest)           ( void );
    void    (*bootmode)       ( void );
    void    (*bootmodeok)       ( void );

    
    uint8_t    (*dfu_field_write)     ( uint32_t idx ,uint8_t*, uint16_t );
    uint8_t    (*dfu_field_read)      ( uint32_t idx ,uint8_t** );  
    uint8_t    (*dfu_size_set)        ( uint32_t size);
    uint32_t   (*dfu_size_get)        ( void ); 
} __EMOS_PACKED com_if_flashmap_t;

/******************************************************************************/
/*                            UNITS                                        */
/******************************************************************************/

#define RAK_IPSO_DATA_TYPE_OFFSET   3200

typedef enum
{
    RAK_IOC_NO_IFACE  = 0,
    RAK_IOC_RS485     = 1,
    RAK_IOC_SDI12,
    RAK_IOC_RS232,
    RAK_IOC_CONTROL   = 9,
    RAK_IOC_UART_END  = 10,
    RAK_IOC_AIC       = 11,
    RAK_IOC_AIV,
    RAK_IOC_DI,
    RAK_IOC_DO,
} RAK_IOC_DEF_E;

typedef enum
{
    RAK_IPSO_DIGITAL_INPUT   = (3200 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_DIGITAL_OUTPUT  = (3201 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_ANALOG_INPUT    = (3202 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_ANALOG_INPUT_VOL   = (3203 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_ILLUM_SENSOR    = (3301 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_PRESENCE_SENSOR = (3302 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_TEMP_SENSOR     = (3303 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_HUMIDITY_SENSOR = (3304 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_HUMIDITY_PRO    = 0x70,
    RAK_IPSO_ACCELEROMETER   = (3313 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_BAROMETER       = (3315 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_GYROMETER       = (3334 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_GPS_LOCAL       = (3336 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_WIND            = (3390 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_WIND_DIR        = (3391 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_EC		         = (3392 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_PH		         = (3394 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_PYRANOMETER     = (3395 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_MODBUS	         = 0xF1,
    RAK_IPSO_SDI12	         = 0xF2,
    RAK_IPSO_RS232	         = 0xF3,

    RAK_IPSO_DEPTH           = (3319 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_POWER           = (3328 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_TILT            = (3329 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_DISTANCE        = (3330 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_DIRECTION       = (3332 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_RATE            = (3346 - RAK_IPSO_DATA_TYPE_OFFSET),
    RAK_IPSO_PUSHBTN         = (3347 - RAK_IPSO_DATA_TYPE_OFFSET),

} RAK_IPSO_DEF_E;

typedef uint8_t (*tlv_decode_cb ) ( uint16_t len, uint8_t *type, uint8_t *data );

typedef struct 
{
    uint8_t (*ipso_size)               ( uint8_t );   
    void (*tlv_decode)    ( uint8_t *, uint16_t , uint8_t , uint8_t , tlv_decode_cb  );   
} __EMOS_PACKED com_if_units_t;

/******************************************************************************/
/*                            RAK CLI                                        */
/******************************************************************************/

typedef struct
{
    union 
    {
        uint8_t value;
        struct
        {
            uint8_t ate_en:1;
            uint8_t wait_en:1;
        };
    };

    union 
    {
        uint16_t value;
        struct
        {
            uint8_t revet;
        };
    } waitfor;

    union 
    {
        uint16_t value;
        struct
        {
            uint8_t revet;
        };
    } waitcur;
    
    char input_cmd[64];
    uint32_t dir_id;
    uint16_t input_cmd_len;
    uint16_t input_cnt;
    uint16_t input_data_len;
    uint8_t input_data[CLI_BUFF_SIZE];
    uint16_t waitlog_len;
    uint8_t waitlog[TEMP_WIRELOG_BUFF];
    
} __EMOS_PACKED com_if_rak_cli_ctrl_t;

typedef struct 
{
    com_if_rak_cli_ctrl_t *ctrl;
    void (*command)(uint8_t *,uint16_t );
    void (*key)(uint8_t * ,uint16_t );
    void (*update) ( void );
} __EMOS_PACKED com_if_rak_cli_t;



/******************************************************************************/
/*                            MODBUS                                      */
/******************************************************************************/

typedef struct
{
    uint8_t (*req) (uint8_t *, uint8_t);
    uint8_t (*rsp) (uint8_t *, uint8_t);
} __EMOS_PACKED com_if_rak_modbus_t;

/******************************************************************************/
/*                            RAK_ADC                                      */
/******************************************************************************/

typedef struct
{
    uint8_t (*req) (uint8_t *, uint8_t);
    uint8_t (*rsp) (uint8_t *, uint8_t);
    uint8_t (*get_analog_cnt) (uint8_t);  
} __EMOS_PACKED com_if_rak_adc_t;

/******************************************************************************/
/*                            RAK_SDI12                                       */
/******************************************************************************/

typedef struct
{
    uint8_t (*req) (uint8_t *, uint8_t);
    uint8_t (*rsp) (uint8_t *, uint8_t); 
} __EMOS_PACKED com_if_rak_sdi12_t;

/******************************************************************************/
/*                            RAK_SDI12                                       */
/******************************************************************************/

typedef struct
{
    uint8_t (*req) (uint8_t *, uint8_t);
    uint8_t (*rsp) (uint8_t *, uint8_t); 
} __EMOS_PACKED com_if_rak_rs232_t;

/******************************************************************************/
/*                            RAK_DIO                                        */
/******************************************************************************/

typedef struct
{
    uint8_t (*req) (uint8_t *, uint8_t);
    uint8_t (*rsp) (uint8_t *, uint8_t); 
    uint8_t (*get_dio_cnt) (uint8_t); 
} __EMOS_PACKED com_if_rak_dio_t;

/******************************************************************************/
/*                            NBIOT                                           */
/******************************************************************************/


typedef struct
{
    union
    {
        uint16_t value;
        struct
        {
            uint16_t intr:1;
            uint16_t retry:1;
            uint16_t conned:1;
            uint16_t pwrhold:1;
            uint32_t nbt_en:1; //HW support NBIOT
            uint32_t nbt_state:1; //power on/off
            uint32_t nbt_connection_busy:1;
            uint32_t nbt_context:1; //activate or not 
            uint32_t nbt_apply:1; // nbiot enable application
            uint32_t nbt_upload_end:1;
            uint32_t nbt_conn_status:1;
            uint32_t nbt_test_mode:1; // Enable Test mode for NBIOT 
            uint32_t nbt_data_format:1; // Data format for uplink data
            uint32_t nbt_http_method:1;
        };
    };
} __EMOS_PACKED com_if_rak_nbiot_ctrl_t;

typedef struct
{
    com_if_rak_nbiot_ctrl_t *ctrl;
    struct
    {
        void (*key)(uint8_t * ,uint16_t );
        void (*puts)  ( uint8_t *, uint16_t );
        void (*printf)  ( char *, ... );
    } enter;

    struct
    {
        void (*key)(uint8_t * ,uint16_t );
        void (*command)(uint8_t *,uint16_t );
    } recv;
    
    void (*init) (void);   
    void (*power) (uint8_t);   
    void (*power_check) (void);   
    void (*network_check) (void);
    void (*sleep) (uint8_t);
    void (*send) (uint8_t *, uint8_t);
    void (*apply) (uint8_t);
    void (*upload) (uint8_t *, uint16_t);
    void (*server_auth) (uint8_t);
    void (*auth_cacert) (uint8_t *, uint16_t);
    void (*auth_clientcert) (uint8_t *, uint16_t);
    void (*auth_clientkey) (uint8_t *, uint16_t);
    void (*gps_enable) (uint8_t );
    void (*gps_data) ();
    void (*http_request) (uint8_t *, uint16_t);
    void (*ble_lock) (uint8_t );
    void (*local_param_reset) (void);
} __EMOS_PACKED com_if_rak_nbiot_t;


/******************************************************************************/
/*                            DIRECT P                                           */
/******************************************************************************/

typedef struct
{
    void (*selputs)    ( uint32_t ,const char * , uint16_t);
    void (*tarputs)    ( const char * , uint16_t);
    void (*tarprnf)  ( char *, ... );
    void (*sysputs)    ( const char * , uint16_t);
    void (*sysprnf)  ( char *, ... );
    void (*ruiprnf)  ( char *, ... );
} __EMOS_PACKED com_if_directp_t;

/******************************************************************************/
/*                            RADIO                                           */
/******************************************************************************/

typedef struct
{
   uint8_t lpwan_mode;
//    union
//    {
//        uint16_t value;
//        struct
//        {
//            uint16_t lpwan_mode:4;
//        };
//    };
} __EMOS_PACKED com_if_rak_radio_ctrl_t;


typedef struct
{
    com_if_rak_radio_ctrl_t *ctrl;

    void (*lpwan) (uint8_t);
    void (*swap_lora) (void);
    void (*swap_nbiot) (void);
    void (*enable) (uint8_t);
} __EMOS_PACKED com_if_rak_radio_t;

/******************************************************************************/
/*                            VSYS                                           */
/******************************************************************************/

typedef struct 
{
    uint8_t id;
    uint8_t last_id;
    uint8_t hw_version;
    uint8_t sw_version[3];
    uint8_t model_name[16];
    uint32_t sampling_interval;
    uint32_t current_intveral;
    uint8_t tag_id;
    uint8_t sequence;
    com_if_rak_probe_param_t * iprobe;

    struct 
    {
        uint32_t src_id;
    } keep;
    
    union
    {
        uint32_t value;
        struct 
        {
            uint32_t tag_en:1;
            uint32_t test_en:1;
            uint32_t selfts_rst:1;
            // uint32_t dfu_busy:1;
            uint32_t far_ack:1;
            uint32_t do_reboot:1;
        };
        
    } bitcfg;
    
    struct
    {
        emos_uin32t expect;
        emos_uin32t current;
    } type_id;
    
    union 
    {
        uint8_t value[18];
        struct
        {
            uint8_t bom[8];
            uint8_t fac;
            uint8_t year[2];
            uint8_t month[2];
            uint8_t number[5];
        };
    } sn;

    union 
    {
        uint8_t value[8];
        struct
        {
            uint8_t year[2];
            uint8_t month[2];
            uint8_t number[4];
        };
    } ssn;

} __EMOS_PACKED com_if_vsys_mydev_t;


typedef struct
{
    void (*init)     ( void );
    uint8_t (*run)      ( void );
    com_if_vsys_mydev_t * (*mydev) ( void );
    emos_if_malloc_t *  (*mem )   ( void );
    emos_if_msgbox_t *  (*mbox )   ( void );
} __EMOS_PACKED com_if_vsys_t;


/******************************************************************************/
/*                            RAK LDO                                            */
/******************************************************************************/

typedef struct
{
    struct 
    {
        void * (*v33) ( void * );
        void * (*v120) ( void * );
        void * (*v240 ) ( void * );
    } i;
    
    void (*v33 ) ( void );
    void (*v120 ) ( void );
    void (*v240 ) ( void );
    void (*vccprb_rest ) ( void );

} __EMOS_PACKED com_if_rak_ldo_t;

/******************************************************************************/

typedef struct
{
    uint32_t src_id;
} com_global_t;
extern com_global_t com_global;

/******************************************************************************/

typedef struct
{
    jmp_buf * svpnt;
    com_if_units_t         *units;
    com_if_vsys_t          *vsys;
    com_if_rak_cli_t       *cli;
    com_if_rak_probe_t     (*rak_probe)(uint8_t);
    com_if_rak_sensor_t    (*rak_sensor)(uint8_t);
    com_if_rak_protocol_t   *rpcl;
    com_if_flashmap_t       *flashmap;
    com_if_rak_nbiot_t     *nbiot;
    com_if_directp_t       *dirp;
    com_if_rak_ldo_t       *ldo;
    com_if_rak_radio_t     *radio;
} __EMOS_PACKED com_t;


extern const com_t com;
extern void com_event_role_process(uint16_t , uint8_t * , uint16_t );
extern void com_event_role_init(uint16_t , uint8_t * , uint16_t );



#ifdef __cplusplus
}
#endif

#endif

