#ifndef __COM_INC_HALL_SENSOR_H__
#define __COM_INC_HALL_SENSOR_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
    COM_TASK_HALLSNSNR_ACTIVE,
} COM_TASK_HALLSNSNR_ID_E;

#define PIN_HALL_SNSNR     004

emos_extern(mdl_hallsnsr);

#ifdef __cplusplus
}
#endif

#endif
