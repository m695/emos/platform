#ifndef __COM_INC_RAK_SENSOR_H__
#define __COM_INC_RAK_SENSOR_H__

#ifdef __cplusplus
extern "C" {
#endif


#define RAK_SNSR_NUM   16

typedef enum
{
    COM_VSYS_RAK_SNSR_START,
    COM_VSYS_RAK_SNSR_SYNC,
    COM_VSYS_RAK_SNSR_UPDATE,
} COM_VSYS_RAK_SNSR_ID_E;

typedef uint8_t (*def_snsr_act ) ( void );

typedef def_snsr_act com_snsr_init_t;
typedef def_snsr_act com_snsr_check_t;
typedef def_snsr_act com_snsr_sleep_t;
typedef def_snsr_act com_snsr_wakeup_t;
typedef uint16_t (*com_snsr_warmup_t ) ( void );
typedef def_snsr_act com_snsr_process_t;
typedef uint16_t (*com_snsr_read_t ) ( uint8_t * );

extern com_if_rak_sensor_t com_if_rak_sensor_func(uint8_t idx);

emos_extern(mdl_rak_sensor);

#ifdef __cplusplus
}
#endif

#endif

