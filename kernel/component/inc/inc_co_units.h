#ifndef __COM_INC_UNITS_H__
#define __COM_INC_UNITS_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*com_uart_vprintf_t ) ( const char *, va_list );

extern const com_if_units_t com_if_units;

#ifdef __cplusplus
}
#endif

#endif

