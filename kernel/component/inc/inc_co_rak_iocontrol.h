#ifndef __COM_INC_RAK_IOCONTROL_H__
#define __COM_INC_RAK_IOCONTROL_H__

#ifdef __cplusplus
extern "C" {
#endif

// protocol format end
//function code define same as atc+XXXXX
// typedef enum
// {
//     IOPASSTHRH,
//     IO_ADDPOLL,
//     IO_ENABLEPOLL,
//     IO_POLLTASK,
//     IO_CFG,
//     IO_DATA,
//     IO_POLLCNT,
//     IO_RMPOLL,
//     IOC_MAXCODE,
// } COM_TASK_RAK_IOC_FUNCCODE_L;

// //interface code define
// typedef enum
// {
//     IOC_NO_IFACE = 0,
//     IOC_RS485 = 1,
//     IOC_SDI12,
//     IOC_RS232,
//     IOC_UART_END = 10,
//     IOC_MAMETER = 11,
//     IOC_VOLMETER,
//     IOC_DI,
//     IOC_DO,
    
// } COM_TASK_RAK_IOC_IFACE_L;

// //IOCONTROL one wire frame type
// typedef enum
// {
//     IOA_SET = 1,
//     IOA_GET,
//     IOA_RSP,
// } COM_TASK_RAK_IOC_ACTION_L;

// //emos task type
// typedef enum
// {
//     COM_TASK_RAK_IOC_REQ,
//     COM_TASK_RAK_IOC_RSP
// } COM_TASK_RAK_IOC_ID_E;

// //for task state
// typedef enum
// {
//     FREE_GO,
//     STANDBY_GO,
//     GOING_GO,
//     TIMEOUT_GO,
//     WAITRECV_GO,
//     DONE_GO,
//     RETRY_GO,
// } REGOSTATE;

// //upload format
// typedef struct
// {
//     uint8_t id;
//     uint8_t type;
//     uint8_t datalen;
//     uint8_t data[];
// } __EMOS_PACKED rak_ioc_data_t;

// typedef struct
// {
//     uint8_t id;
//     uint8_t type;
//     uint8_t data[];
// } __EMOS_PACKED rak_ioc_value_t;
// //upload format end
// // protocol format = gen_frame + (passthrh or addpoll ....)
// typedef struct
// {
//     uint8_t iface;
//     uint8_t action;
//     uint8_t data[];
// } __EMOS_PACKED rak_ioc_gen_frame_t;
// typedef struct
// {
//     uint32_t timeout;
//     uint8_t cmd[];
// } __EMOS_PACKED rak_ioc_passthrh_frame_t;

// typedef struct
// {
//     uint8_t taskid;
//     uint32_t period;
//     uint32_t timeout;
//     uint8_t retry;
//     uint8_t cmd[];
// } __EMOS_PACKED rak_ioc_addpoll_frame_t;

// typedef struct
// {
//     uint8_t taskid;
//     uint8_t enable;
// } __EMOS_PACKED rak_ioc_enablepoll_frame_t;

// typedef struct
// {
//     uint8_t taskid;
// } __EMOS_PACKED rak_ioc_polltask_frame_t;

// typedef struct
// {
//     uint8_t entklen;
//     uint8_t enlist[];
// } __EMOS_PACKED rak_ioc_pollcnt_frame_t;

// typedef struct
// {
//     union 
//     {
//         uint8_t  value[12];
//         struct //for uart
//         {
//             uint32_t  baudrate;
//             uint8_t   databit;
//             uint8_t   stopbit;
//             uint8_t   parity; 
//         };
//         struct // for di AI current AI volt
//         {
//             uint8_t   ach;
//             uint32_t  period;
//             uint32_t  debouncing;
//         };
//         struct // for do
//         {
//             uint8_t  dch;
//             uint8_t  on_off;
//         };

//     } cfg;
// } __EMOS_PACKED rak_ioc_cfg_frame_t;

// typedef struct
// {
//     uint8_t   ach;
//     uint8_t   value;
// } __EMOS_PACKED rak_ioc_data_frame_t;





// extern void com_mdl_rak_iocontrol_process(emos_task_msg_t *task_msg);

#ifdef __cplusplus
}
#endif
#endif
