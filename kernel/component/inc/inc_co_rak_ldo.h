#ifndef __COM_INC_RAK_LDO_H__
#define __COM_INC_RAK_LDO_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef void ( *com_rak_ldo_v33_t ) ( void );
typedef void ( *com_rak_ldo_v120_t ) ( void );
typedef void ( *com_rak_ldo_v240_t ) ( void );
typedef void ( *com_rak_ldo_vccprb_rest_t ) ( void );

typedef enum
{
    COM_TASK_EVT_RAK_LDO_VCCPROBE_INIT,
    COM_TASK_EVT_RAK_LDO_V12_INIT,
    COM_TASK_EVT_RAK_LDO_VCCPROBE_RESET,
} COM_TASK_EVT_RAK_LDO_ID_E;

#ifdef __cplusplus
}
#endif

#endif

