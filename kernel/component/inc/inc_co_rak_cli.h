#ifndef __COM_INC_RAK_CLI_H__
#define __COM_INC_RAK_CLI_H__

#ifdef __cplusplus
extern "C" {
#endif


typedef struct 
{
    uint16_t wp;
    uint16_t rp;
    uint8_t buff[CLI_BUFF_SIZE];
} __EMOS_PACKED com_cli_buff_t;

typedef struct 
{
    char* header;
    uint32_t pcmd;
} __EMOS_PACKED com_cli_format_t;

typedef enum
{
    API_TASK_RAK_CLI_KEY,
    API_TASK_RAK_CLI_COMMAND,
} COM_TASK_RAK_CLI_ID_E;

typedef enum
{
    COM_TASK_RAK_ATCMD_AT = 0x1,
    COM_TASK_RAK_ATCMD_ATC,
    COM_TASK_RAK_ATCMD_TSC,
    COM_TASK_RAK_ATCMD_TS,
    COM_TASK_RAK_ATCMD_TP,
#ifdef EMOS_SUPPORT_RUI3_ATCELL
    COM_TASK_RAK_ATCMD_ATCELL,
#endif
    COM_TASK_RAK_ATCMD_NONE,
} COM_TASK_RAK_CLI_ATCCMD_E;

#define CLI_CK(X) *((char *)X)
#define CLI_CT(X) #X

#define xINT CLI_CT(A) // int
#define xHEX CLI_CT(B) // hex
#define xDEC CLI_CT(C) // 
#define xPRB CLI_CT(D) // probe id
#define xSTR CLI_CT(E) // string
#define xCHR CLI_CT(F) // char 
#define xBLN CLI_CT(G) // boolean
#define xQST CLI_CT(H) // get
#define xNUL CLI_CT(0)
#define xSNR CLI_CT(I) // snsr id
#define xENT CLI_CT(J) // input
#define xRSP CLI_CT(K) // wait

#define CLI_TEXT(...)   param->ret_string = emos_outext(__VA_ARGS__)

typedef struct
{
    const char *header;
    union
    {
        uint8_t value;
        struct
        {
            uint8_t is_qust:1;
            uint8_t is_afrwait:1;
        };
    } flag;
    uint8_t *ret_string;
    union 
    {
        uint16_t value;
        struct
        {
            uint8_t wire_frm_type;
            uint8_t wire_pay_type;
        };
    } waitfor;
    uint8_t *retdata;
    uint16_t retdata_len;
} __EMOS_PACKED com_mdl_rak_cli_param_t;

typedef struct
{
    const char * cmd;
    const char * field;
    uint8_t (*cmd_handle)( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv); 
} __EMOS_PACKED com_mdl_rak_cli_handle_t;

extern uint8_t *com_mdl_get_deveui( void );
extern uint8_t *com_mdl_get_appeui( void );
extern uint8_t *com_mdl_get_appkey( void );


extern uint8_t com_mdl_set_deveui( uint8_t * );
extern uint8_t com_mdl_set_appeui( uint8_t * );
extern uint8_t com_mdl_set_appkey( uint8_t * );

extern const com_mdl_rak_cli_handle_t ble_command_tbl[];
extern const com_mdl_rak_cli_handle_t dfu_command_tbl[];
extern const com_mdl_rak_cli_handle_t factory_command_tbl[];
extern const com_mdl_rak_cli_handle_t general_command_tbl[];
extern const com_mdl_rak_cli_handle_t lora_command_tbl[];
extern const com_mdl_rak_cli_handle_t nbiot_command_tbl[];
extern const com_mdl_rak_cli_handle_t nfc_command_tbl[];
extern const com_mdl_rak_cli_handle_t probe_command_tbl[];
extern const com_mdl_rak_cli_handle_t probeio_command_tbl[];
extern const com_mdl_rak_cli_handle_t radio_command_tbl[];

extern const com_if_rak_cli_t com_if_rak_cli;

emos_extern(mdl_rak_cli);

#ifdef __cplusplus
}
#endif

#endif

