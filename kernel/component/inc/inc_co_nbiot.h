#ifndef __COM_INC_RAK_NBIOT_H__
#define __COM_INC_RAK_NBIOT_H__

#ifdef __cplusplus
extern "C" {
#endif

#define NBIOT_BUFF_SIZE   128

typedef enum
{
    COM_NBIOT_CMD_ENTER,
    COM_NBIOT_CMD_RECV,
    COM_NBIOT_CMD_DIR_MAX,
} COM_NBIOT_CMD_DIR_E;

typedef enum
{
    COM_NBIOT_EVT_NONE,
    COM_NBIOT_EVT_QIOPEN,
    COM_NBIOT_EVT_CREG,
    COM_NBIOT_EVT_QIURC,
    COM_NBIOT_EVT_QMTCONN,
} COM_NBIOT_EVT_ID_E;

typedef enum
{
    COM_TASK_RAK_NBIOT_INIT,
    COM_TASK_RAK_NBIOT_POWER,
    COM_TASK_RAK_NBIOT_POWER_CHECK,
    COM_TASK_RAK_NBIOT_NETWORK_CHECK,
    COM_TASK_RAK_NBIOT_SLEEP,
    COM_TASK_RAK_NBIOT_CMD,
    COM_TASK_RAK_NBIOT_CMDTIMOUT,
    COM_TASK_RAK_NBIOT_RECV,
    COM_TASK_RAK_NBIOT_SCRIPT,
    COM_TASK_RAK_NBIOT_SEND,
    COM_TASK_RAK_NBIOT_APPLY_CONNECTION,
    COM_TASK_RAK_NBIOT_TCP_UPLOAD,
    COM_TASK_RAK_NBIOT_SERVER_AUTH,
    COM_TASK_RAK_NBIOT_AUTH_CACERT,
    COM_TASK_RAK_NBIOT_AUTH_CLIENT_CERT,
    COM_TASK_RAK_NBIOT_AUTH_CLIENT_KEY,
    COM_TASK_RAK_NBIOT_GPS_ENABLE,
    COM_TASK_RAK_NBIOT_GPS_DATA,
    COM_TASK_RAK_NBIOT_BLE_LOCK,
    COM_TASK_RAK_NBIOT_LOCAL_PARAM_RESET,
    COM_TASK_RAK_NBIOT_DUMMY,
} COM_TASK_RAK_NBIOT_ID_E;

typedef enum
{
    COM_NBIOT_TIMER_CREG_CHECK,
    COM_NBIOT_TIMER_SCRIPT_DELAY,
    COM_NBIOT_TIMER_USERCMD_DELAY,

} COM_NBIOT_TIMER_ID_E;

typedef enum
{
    COM_NBIOT_HTTP_GET = 0x0,
    COM_NBIOT_HTTP_POST,
    COM_NBIOT_HTTP_READ,
    COM_NBIOT_HTTP_URL,
} COM_NBIOT_HTTP_METHOD_E;


#define NBIOT_PWRKEY      103 //LED1
#define NBIOT_GPS_ENABLE  102 //IO2
#define NBIOT_DISABLE     28  //QSPI_DIO2
#define NBIOT_APREADY     2   //QSPI_DIO3
#define NBIOT_TX          20  //UART1_TX
#define NBIOT_RX          19  //UART1_RX
#define NBIOT_RI          24  //I2C2_SDA
#define NBIOT_DTR         25  //I2C2_SCL
#define NBIOT_TRIG        104 //LED2
#define NBIOT_DATA_LENGTH             1024

#define NBIOT_SCRIPT_PARAM_CNT   5

typedef struct 
{
    uint16_t wp;
    uint16_t rp;
    uint8_t buff[NBIOT_BUFF_SIZE];
} __EMOS_PACKED com_nbiot_buff_t;

typedef struct
{
    const char * cmd;
    const uint32_t delay;
    void (*cmd_handle)( uint8_t *, uint16_t );
} __EMOS_PACKED com_nbiot_format_t;

typedef struct
{
    const char * cmd;
    const uint32_t delay;
    uint8_t flag;
    uint32_t ** p[NBIOT_SCRIPT_PARAM_CNT];
} __EMOS_PACKED com_nbiot_script_t;

typedef void (*com_rak_nbiot_puts_t ) ( uint8_t *, uint16_t );





emos_extern(mdl_rak_nbiot);

extern const com_if_rak_nbiot_t com_if_rak_nbiot;

#ifdef __cplusplus
}
#endif

#endif
