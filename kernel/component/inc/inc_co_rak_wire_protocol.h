#ifndef __COM_INC_RAK_PROTOCOL_H__
#define __COM_INC_RAK_PROTOCOL_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*com_rak_protocl_puts_t ) ( uint8_t, uint8_t *, uint16_t );


#define DELIMTER          0x7E

#define WIRE_SEND_REQ    0
#define WIRE_SEND_RSP    1

typedef enum
{
    RAK_HUB_STEP_HEADER_START,
    RAK_HUB_STEP_HEADER,
    RAK_HUB_STEP_PAYLOAD,
    RAK_HUB_STEP_CRC,
} RAK_HUB_STEP_E;

typedef enum 
{
    RAK_HUB_FRM_TYPE_ECHO,
    RAK_HUB_FRM_TYPE_ATCMD,
    RAK_HUB_FRM_TYPE_SENSORHUB,
    RAK_HUB_FRM_TYPE_MAX,
} RAK_HUB_FRM_TYPE_E;

typedef enum 
{
    RAK_HUB_FLG_REQ,
    RAK_HUB_FLG_RSP,
    RAK_HUB_FLG_MAX,
} RAK_HUB_FLG_E;

typedef enum 
{
    RAK_PB_FRM_TYPE_WAKUP,
    RAK_PB_FRM_TYPE_PROVISION,
    RAK_PB_FRM_TYPE_PARAMSET,
    RAK_PB_FRM_TYPE_SENDAT,
    RAK_PB_FRM_TYPE_CONTROL,
    RAK_PB_FRM_TYPE_PARAMGET,
    RAK_PB_FRM_TYPE_ALERT,
    RAK_PB_FRM_TYPE_ERR,
    RAK_PB_FRM_TYPE_ACK,
    RAK_PB_FRM_TYPE_NACK,
    RAK_PB_FRM_TYPE_YMDM,
    RAK_PB_FRM_TYPE_IOC,
    RAK_PB_FRM_TYPE_MODBUS,
    RAK_PB_FRM_TYPE_SDI12,
    RAK_PB_FRM_TYPE_ADC,
    RAK_PB_FRM_TYPE_DIO,
    
    RAK_PB_FRM_TYPE_SCMD,
    
    RAK_PB_FRM_TYPE_DISP,
    RAK_PB_FRM_TYPE_MAX,
} RAK_PB_FRM_TYPE_E;

typedef enum 
{
    RAK_PB_PAY_TYPE_SCMD_ADD,
    RAK_PB_PAY_TYPE_SCMD_DEL,
    RAK_PB_PAY_TYPE_SCMD_REST,
    RAK_PB_PAY_TYPE_SCMD_LIST,
    RAK_PB_PAY_TYPE_SCMD_BUILD,
} RAK_PB_PAY_TYPE_SCMD_E;

typedef enum 
{
    RAK_PB_RULE_TYPE_ABOVE,
    RAK_PB_RULE_TYPE_BELOW,
    RAK_PB_RULE_TYPE_BETWEEN,
    RAK_PB_RULE_TYPE_PERIODIC,
} RAK_PB_RULE_TYPE_E;

typedef enum 
{
    RAK_PB_PAY_TPYE_VER,
    RAK_PB_PAY_TPYE_PARAMSET,
    RAK_PB_PAY_TPYE_SENDAT,
    RAK_PB_PAY_TPYE_CONTROL,
    RAK_PB_PAY_TPYE_MAX,
} RAK_PB_PAY_TPYE_E;

typedef enum 
{
    RAK_PB_PAY_TPYE_ALERT_CHG = 0x01,
} RAK_PB_PAY_TPYE_ALERT_E;



typedef enum
{
    RAK_PB_PAY_TPYE_SENDAT_DEF = 2,
    RAK_PB_PAY_TPYE_SENDAT_ACK,
} RAK_PB_PAY_TPYE_SENDAT_E;

typedef enum
{
    RAK_PB_PAY_TYPE_CONTROL_REBOOT = 0x1,
    RAK_PB_PAY_TYPE_CONTROL_DFU,
    RAK_PB_PAY_TYPE_CONTROL_RESTORE,
    RAK_PB_PAY_TYPE_CONTROL_REMOVE,
} RAK_PB_PAY_TPYE_CONTROL_E;

typedef enum
{
    RAK_PB_PAY_TYPE_PARAM_PRB_INTV = 0x1,
    RAK_PB_PAY_TYPE_PARAM_SNSR_INTV,
    RAK_PB_PAY_TYPE_PARAM_SNSR_RULE,
    RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR,
    RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR,
    RAK_PB_PAY_TYPE_PARAM_PRB_TAGID,
    RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN,
    RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE,
    RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE,
    /* new */
    RAK_PB_PAY_TYPE_PARAM_SNSR_CNT,
    RAK_PB_PAY_TYPE_PARAM_SNSR_DATA,
    RAK_PB_PAY_TYPE_PARAM_SMPL_INTV,
    RAK_PB_PAY_TYPE_PARAM_SNSR_INFO,
} RAK_PB_PAY_TPYE_PARAMGSET_E;

typedef union 
{
    uint16_t value;
    struct
    {
        uint8_t lsb;
        uint8_t msb;
    };
} mshort;

typedef struct 
{
    uint8_t               start_delimiter; //8bit
    mshort                length;
    RAK_HUB_FRM_TYPE_E    frame_type;      //8bit
    RAK_HUB_FLG_E         flag;            //8bit
    uint8_t               payload[];       //n bytes
} __EMOS_PACKED rak_hub_protocol_frame_t;

typedef struct 
{
    uint8_t               destlination;    //8bit
    uint8_t               source;          //8bit
    uint8_t               sequence;        //8bit
    union
    {
        uint8_t frame_type;              //8bit
        struct
        {
            uint8_t     fm_type:7;      
            uint8_t     fm_dir:1;
        };
    };
    
    uint8_t               payload_length;  //8bit
    uint8_t               payload_type;    //8bit
    uint8_t               payload[];       //n bytes
} __EMOS_PACKED rak_probe_protocol_frame_t;

typedef struct 
{
    uint8_t               hw_version;
    uint8_t               sw_version[3];
    union 
    {
        uint8_t          value[18];
        struct
        {
            uint8_t bom[8];
            uint8_t fac;
            uint8_t year[2];
            uint8_t month[2];
            uint8_t number[5];
        };
    } sn;
    
    uint8_t               probe_id;
    uint8_t               type_id;
    uint8_t               model_name[16];
    uint8_t               tag_id;
    uint8_t               tag_id_en;
    uint8_t               type_id2;
    uint8_t               reserved; //will set payload type
    uint8_t               sensor_number;
    uint8_t               sensor_type[];
} __EMOS_PACKED rak_probe_protocol_ie_version_t;

typedef struct 
{
    uint8_t               hw_version;
    uint8_t               sw_version[3];
    union 
    {
        uint8_t          value[18];
        struct
        {
            uint8_t bom[8];
            uint8_t fac;
            uint8_t year[2];
            uint8_t month[2];
            uint8_t number[5];
        };
    } sn;
    
    uint8_t               probe_id;
    uint8_t               tag_id;
    union
    {
        uint32_t          value;
        struct
        {
            uint8_t       bytes[4];
        };
    } type_id;
    
    union
    {
        uint16_t value;
        struct
        {
            uint16_t tag_en:1;
        };
        
    } bitcfg;
    
    uint8_t               model_name[20];
    uint8_t               reserved[4]; //will set payload type
    uint8_t               sensor_number;
    uint8_t               sensor_type[];
} __EMOS_PACKED rak_probe_protocol_ie_version3_t;

typedef struct
{
    uint8_t id;
    uint8_t type;
    uint16_t rule;
} __EMOS_PACKED rak_probe_protocol_ie_version_sensor_t;

typedef struct
{
    uint8_t id;
    uint8_t type;
    uint8_t data[10];
} __EMOS_PACKED rak_probe_protocol_ie_sensor_data_t;

typedef struct 
{
    uint32_t intv;
    uint8_t  rest;
    uint8_t  del;
    uint8_t  calib;
} __EMOS_PACKED rak_probe_protocol_ie_param_probe_t;

typedef struct 
{
    uint8_t id;
    uint8_t payload[];
} __EMOS_PACKED rak_probe_protocol_ie_param_sensor_h_t;

typedef struct 
{
    uint32_t intv;
    uint16_t rule;
    struct
    {
        uint8_t above[10];
        uint8_t below[10];
    } thr;
} __EMOS_PACKED rak_probe_protocol_ie_param_sensor_t;

typedef struct
{
        uint8_t value;
} __EMOS_PACKED rak_probe_protocol_ie_control_dfu_t;

typedef struct 
{
    uint8_t               value;
} __EMOS_PACKED rak_probe_protocol_ie_control_t;

typedef struct 
{
    uint8_t cmd;
    uint8_t crc;
} __EMOS_PACKED rak_probe_protocol_ie_ymodem_t;

typedef struct 
{
    uint8_t len;
    uint8_t data[];
} __EMOS_PACKED rak_probe_protocol_ie_param_sensor2_t;


typedef union
{
    rak_probe_protocol_ie_version_t         version;
    rak_probe_protocol_ie_sensor_data_t     sendat;
    rak_probe_protocol_ie_control_t         control;
} __EMOS_PACKED rak_probe_protocol_ie_t;

typedef struct 
{
    uint8_t wakeup;
    rak_hub_protocol_frame_t rui;
    rak_probe_protocol_frame_t prb;
} __EMOS_PACKED rak_probe_wire_protocol_header_t;

typedef struct 
{
    uint8_t wakeup;
    rak_hub_protocol_frame_t rui;
} __EMOS_PACKED rak_rui_wire_protocol_header_t;

typedef struct 
{
    rak_probe_wire_protocol_header_t header;
    union
    {
        rak_probe_protocol_ie_version_t prov_v1;
        rak_probe_protocol_ie_version3_t prov_v3;
        rak_probe_protocol_ie_param_probe_t prb_get;
        struct
        {
            rak_probe_protocol_ie_param_sensor_h_t snsr_header;
            rak_probe_protocol_ie_param_sensor_t snsr_get;
        }v1;
        
        struct
        {
            rak_probe_protocol_ie_param_sensor_h_t snsr_header;
            rak_probe_protocol_ie_param_sensor2_t snsr_get;
        }v2;


        rak_probe_protocol_ie_ymodem_t ymodem;

        
        struct
        {
            uint8_t revert;
            uint8_t payload[];
        };
    };
} __EMOS_PACKED rak_probe_wire_protocol_t;

typedef struct 
{
    rak_rui_wire_protocol_header_t header;
} __EMOS_PACKED rak_rui_wire_protocol_t;

typedef struct 
{
    uint8_t pid;
    uint8_t sid;
    uint8_t mid;
    uint8_t frm_type;
    uint8_t pay_type;
    uint16_t data_len;
    uint8_t data[];
} __EMOS_PACKED rak_probe_wire_send_param_t;


typedef enum
{
    COM_VSYS_RAK_PCL_CHECK,
    COM_VSYS_RAK_PCL_COMMAND,
    COM_VSYS_RAK_PCL_SENDREQ,
    COM_VSYS_RAK_PCL_SENDRSP,
    COM_VSYS_RUI_SENDREQ,
    COM_VSYS_RUI_SENDRSP,
} COM_TASK_RAK_PCL_ID_E;

#define X_STX 0x02
#define X_ACK 0x06
#define X_NAK 0x15
#define X_EOF 0x04
#define X_C   0x43
#define MAX_ERROR 20
#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)

typedef struct 
{
    uint8_t unused;
    uint8_t start;
    uint8_t block;
    uint8_t block_neg;
    uint8_t payload[1024];
    uint16_t crc;
} __EMOS_PACKED rak_probe_protocol_ie_ymodem_data_t;

typedef struct 
{
    struct
    {
        uint8_t *bin_addr;
        uint32_t bin_size;
    } profile;
    
    uint8_t skip_payload;
} __EMOS_PACKED rak_rak_wire_upgrade_t;


typedef enum 
{
    RAK_PB_PAY_TPYE_PRV_VER,
    RAK_PB_PAY_TPYE_PRV_VER2,
    RAK_PB_PAY_TPYE_PRV_HELLO,
    RAK_PB_PAY_TPYE_PRV_VER3,
    RAK_PB_PAY_TPYE_PRV_VER4,
} RAK_PB_PAY_TPYE_PRV_E;

emos_extern(mdl_rak_wire_protocol);

extern const com_if_rak_protocol_t com_if_rak_protocol;

#ifdef __cplusplus
}
#endif

#endif

