#ifndef __COM_INC_RAK_PROBE_H__
#define __COM_INC_RAK_PROBE_H__

#ifdef __cplusplus
extern "C" {
#endif

extern com_if_rak_probe_t com_if_rak_probe_func(uint8_t idx);

#ifdef __cplusplus
}
#endif

#endif

