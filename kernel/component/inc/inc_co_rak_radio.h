#ifndef __COM_INC_RAK_RADIO_H__
#define __COM_INC_RAK_RADIO_H__

#ifdef __cplusplus
extern "C" {
#endif

extern const com_if_rak_radio_t com_if_rak_radio;

#define RADIO_SWITCH_CTRL    026
#define RADIO_SWAP_CTRL      101

#define RADIO_SWITCH_EN()   emos.gpio->o_dir_output(GPIO_MASK(RADIO_SWITCH_CTRL)); \
                            emos.gpio->o_phigh(GPIO_MASK(RADIO_SWITCH_CTRL))

#define RADIO_SWITCH_DIS()  emos.gpio->o_dir_output(GPIO_MASK(RADIO_SWITCH_CTRL)); \
                            emos.gpio->o_plow(GPIO_MASK(RADIO_SWITCH_CTRL))

#define RADIO_SWAP_LORA()   emos.gpio->o_dir_output(GPIO_MASK(RADIO_SWAP_CTRL)); \
                            emos.gpio->o_plow(GPIO_MASK(RADIO_SWAP_CTRL))

#define RADIO_SWAP_LTE()    emos.gpio->o_dir_output(GPIO_MASK(RADIO_SWAP_CTRL)); \
                            emos.gpio->o_phigh(GPIO_MASK(RADIO_SWAP_CTRL))

typedef enum
{
    COM_TASK_RAK_RADIO_LPWAN_SWITCH,
    COM_TASK_RAK_RADIO_SWAP_TO_LORA,
    COM_TASK_RAK_RADIO_SWAP_TO_NBIOT,
    COM_TASK_RAK_RADIO_ENABLE,

} COM_TASK_RAK_RADIO_ID_E;

#ifdef __cplusplus
}
#endif

#endif
