#ifndef __COM_INC_EXAMPLE_H__
#define __COM_INC_EXAMPLE_H__

#ifdef __cplusplus
extern "C" {
#endif


typedef enum
{
    COM_TASK_EVT_EXAMPLE_TODO1,
    COM_TASK_EVT_EXAMPLE_TODO2,
} COM_TASK_EVT_EXAMPLE_ID_E;

#ifdef __cplusplus
}
#endif

#endif

