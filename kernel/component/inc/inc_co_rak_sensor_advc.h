#ifndef __COM_INC_RAK_IOCTRLER_H__
#define __COM_INC_RAK_IOCTRLER_H__

#ifdef __cplusplus
extern "C" {
#endif

#define cNONE    '*'
#define cWRITE   'W'
#define cREAD    'R'
#define cINFO    'I'
#define cCLEAN   'C'
#define cMUL     'M'
#define cDIV     'D'
#define cOUTPUT  'O'
#define cVERIFY  'Z'
#define cEND     'E'
#define cSTART   'S'
#define cPAUSE   'P'
#define cVOLT    'V'
#define cAND     '&'
#define cLEFT    '<'
#define cRIGHT   '>'
#define cSWAP    'T'
#define cBACKUP  'B'
#define cUNDO    'U'


typedef enum
{
    IOCTRLER_IOTYPE_UART0 = 1,
    IOCTRLER_IOTYPE_UART1,
    IOCTRLER_IOTYPE_UART2,
    IOCTRLER_IOTYPE_IIC0,
    IOCTRLER_IOTYPE_IIC1,
    IOCTRLER_IOTYPE_IIC2,
    IOCTRLER_IOTYPE_SDI12,
    IOCTRLER_IOTYPE_DI0,
    IOCTRLER_IOTYPE_DI1,
    IOCTRLER_IOTYPE_DO0,
    IOCTRLER_IOTYPE_DO1,
    IOCTRLER_IOTYPE_RS485,
} RAK_IOCTRLER_IOTYPE_E;

typedef struct
{
    char * acsii;
    uint8_t id;
} __EMOS_PACKED mdl_rak_ioctrler_acsii_with_id_t;

typedef enum
{
    COM_TASK_EVT_RAK_SNSR_ADVC_TODO1,
    COM_TASK_EVT_RAK_SNSR_ADVC_DUMPMAP,
    COM_TASK_EVT_RAK_SNSR_ADVC_DUMPNEXT,
} COM_TASK_EVT_RAK_SNSR_ADVC_ID_E;


extern const mdl_rak_ioctrler_acsii_with_id_t ioctrler_iotype_tbl[];
extern uint8_t ioctrler_iotype_tbl_cnt;
extern const mdl_rak_ioctrler_acsii_with_id_t ioctrler_cmdtype_tbl[];
extern uint8_t ioctrler_cmdtype_tbl_cnt;

emos_extern(mdl_rak_sensor_advc);

#ifdef __cplusplus
}
#endif

#endif

