#ifndef __COM_INC_SERVICE_H__
#define __COM_INC_SERVICE_H__

#ifdef __cplusplus
extern "C" {
#endif



typedef enum
{
    COM_TASK_SERV_AFTER_TIME_SHOT,
    COM_TASK_SERV_SYNC,
    COM_TASK_SERV_SEND,
} COM_TASK_SERV_ID_E;

emos_extern(mdl_service);

#ifdef __cplusplus
}
#endif

#endif
