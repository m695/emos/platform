#include "../kernel.h"
#include "com.h"

#include "inc/inc_co_units.h"
#include "inc/inc_co_rak_cli.h"

com_global_t com_global;

extern const com_if_vsys_t      com_if_vsys;
extern const com_if_flashmap_t   com_if_flashmap;
extern const com_if_rak_protocol_t com_if_rak_protocol;
extern const com_if_directp_t       com_if_directp;

extern com_if_rak_sensor_t com_if_rak_sensor_func(uint8_t idx);
extern com_if_rak_probe_t com_if_rak_probe_func(uint8_t idx);
extern com_if_rak_modbus_t com_if_rak_modbus;
extern com_if_rak_nbiot_t com_if_rak_nbiot;
extern com_if_rak_radio_t com_if_rak_radio;
extern com_if_rak_transparent_t com_if_rak_transparent;
extern com_if_rak_adc_t com_if_rak_adc;
extern com_if_rak_sdi12_t com_if_rak_sdi12;
extern com_if_rak_sdi12_t com_if_rak_rs232;
extern com_if_rak_dio_t com_if_rak_dio;
extern com_if_rak_iocontrol_t com_if_rak_iocontrol;
extern com_if_rak_ldo_t com_if_rak_ldo;

static jmp_buf jmpbuffer;

const com_t com = 
{ 
    .svpnt        = &jmpbuffer,
    .units        = (com_if_units_t *)&com_if_units,
    .vsys         = (com_if_vsys_t *)&com_if_vsys,
    .cli          = (com_if_rak_cli_t *)&com_if_rak_cli,
    .rak_sensor   = com_if_rak_sensor_func,
    .rak_probe    = com_if_rak_probe_func,
    .flashmap     = (com_if_flashmap_t *)&com_if_flashmap,
    .rpcl         = (com_if_rak_protocol_t *)&com_if_rak_protocol,
    .nbiot        = (com_if_rak_nbiot_t *)&com_if_rak_nbiot,
    .dirp         = (com_if_directp_t *)&com_if_directp,
    .ldo          = (com_if_rak_ldo_t *)&com_if_rak_ldo,
    .radio        = (com_if_rak_radio_t *)&com_if_rak_radio,
};

