#include "../../kernel.h"
#include "../inc/inc_co_service.h"
#include "../inc/inc_co_nbiot.h"

typedef struct 
{
    uint8_t pid;
    uint8_t buf[10];
} srv_data_t;

static void do_service_sync(uint8_t pid, uint8_t *msg_payload,uint32_t msg_len)
{
    if (msg_len == 0)
    {
        return;
    }

    /* send data to hub*/
    com.rpcl->snd_snsrdata(pid,msg_payload,msg_len);
}

static void do_service_send(uint8_t pid, uint8_t *msg_payload,uint32_t msg_len)
{
    if (msg_len == 0)
    {
        return;
    }

#ifdef EMOS_SUPPORT_WIRE_HOST

#ifdef EMOS_SUPPORT_LORA
    for (size_t i = 0; i < msg_len; )
    {
        do
        {
            uint8_t *full_data = &msg_payload[i];
            uint8_t snsr_id    = full_data[0];
            uint8_t ipso_id    = full_data[1];
            uint8_t ipso_size  = com.units->ipso_size(ipso_id);
            uint8_t *snsr_data = &full_data[2];
            uint8_t full_data_len = (ipso_size + sizeof(snsr_id) + sizeof(ipso_id));

            /* data partition*/
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_LORAWAN_HEAP,full_data, full_data_len);

            i += full_data_len;
        } while (0);
    }
    
    // com_vsys_send( COM_TASK_EVENT, COM_EVENT_LORAWAN_HEAP,"88888888", 8);
    com_if_rak_probe_param_t * probe_info;
    com.rak_probe(pid).probe_get(&probe_info);
    
    uint8_t *title;
    uint8_t title_len = 3;
    title = emos.units->memcat(NULL, 0, (uint8_t *)probe_info->ssn.value, title_len);
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_LORAWAN_TITLE,title, title_len);
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_LORAWAN_SEND,&pid, 1);
#endif
    
 #ifdef EMOS_SUPPORT_NBIOT
    if (com.radio->ctrl->lpwan_mode == 2)
     {
         if (com.nbiot->ctrl->nbt_apply)
         {
             com_vsys_mrsend( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_TCP_UPLOAD, &pid, 1, msg_payload, msg_len);
         }
     }
 #endif
#endif
}

static uint32_t do_next_time_slot( uint8_t probe_id )
{
    
    com_if_rak_probe_param_t * probe_info;
    com_if_rak_probe_sensor_t * sensor_info;
    com_if_vsys_mydev_t * mydev = com.vsys->mydev();
    uint8_t ret;
    uint8_t snsr_cnt = 0;
    uint32_t current_intv = (-1);

    srv_data_t this_data;
    memset(&this_data, 0, sizeof(this_data));

    uint8_t data_len;
    
    com_get_probe(probe_id,&probe_info);

    this_data.pid = probe_id;
    snsr_cnt = com_sprobe(probe_id).snsr_count();
    data_len = 0;

    for (uint8_t i = 0; i < snsr_cnt; i++)
    {
        uint8_t is_trig = 0;
        uint8_t is_alert = 0;
        UNUSED(is_alert);
        ret = com_sprobe(probe_id).snsr_walk(i,&sensor_info);

        if (ret)
        {
            goto FUNC_EXIT;
        }

        if ( sensor_info->current_intv > mydev->current_intveral )
        {
            sensor_info->current_intv -= mydev->current_intveral;
            
            goto EXIT_LOOP;
        }  

        if( sensor_info->sta.init )
        {
            sensor_info->current_intv = RAK_PROTOCOL_DFUDELAY;
            goto EXIT_LOOP;
        }
        else
        {
            sensor_info->current_intv = sensor_info->intv;
        }
        
        do
        {
            int32_t cur_thr;
            int32_t cmp_thr;
            memcpy(&cur_thr,sensor_info->data.value,sensor_info->data.len);
            is_alert = sensor_info->ctrl.alert;

            /* if is real probe, by pass data flush*/
            if ( probe_info->sta.real == 0)
            {
                if ( sensor_info->sta.newdat == 0)
                {
                    break;
                }

                sensor_info->sta.newdat = 0;
            }
            /* end */

            if(sensor_info->ctrl.above)
            {
                memcpy(&cmp_thr,sensor_info->thr.above,sensor_info->data.len);
                if (cur_thr > cmp_thr)
                {
                    is_trig = 1;
                    break;
                }
            }

            if(sensor_info->ctrl.below)
            {
                memcpy(&cmp_thr,sensor_info->thr.below,sensor_info->data.len);
                if (cur_thr < cmp_thr)
                {
                    is_trig = 1;
                    break;
                }
            }

            if(sensor_info->ctrl.between)
            {
                memcpy(&cmp_thr,sensor_info->thr.below,sensor_info->data.len);
                if (cur_thr > cmp_thr)
                {
                    memcpy(&cmp_thr,sensor_info->thr.above,sensor_info->data.len);
                    if (cur_thr < cmp_thr)
                    {
                        is_trig = 1;
                        break;
                    }
                }
            }
            
            is_alert = 0;

            if(sensor_info->ctrl.diff)
            {
                if( sensor_info->sta.dchg )
                {
                    sensor_info->sta.dchg = 0;
                    is_trig = 1;
                    
                    break;
                }
            }

            if (sensor_info->ctrl.periodic)
            {
                is_trig = 1;
                break;
            }
        } while (0);
        
    EXIT_LOOP:
        
        if(current_intv > sensor_info->current_intv)
        {
            current_intv = sensor_info->current_intv;
        }

        if( is_trig )
        {
            this_data.buf[data_len + 0] = sensor_info->data.id;
            this_data.buf[data_len + 1] = sensor_info->data.type;

            uint8_t *pp = &(this_data.buf[data_len + 2]);

            memcpy(pp, sensor_info->data.value, sensor_info->data.len);
            if (sensor_info->ctrl.noswap != IENABLE) 
            {
                emos_memswap(pp,sensor_info->data.len);
            }
            
            data_len += (sensor_info->data.len + 2);
        }
    }
    
    if (data_len == 0)
    {
        goto FUNC_EXIT;
    }
    
    com_vsys_send( COM_TASK_SERV, COM_TASK_SERV_SYNC,&this_data, (data_len + sizeof(this_data.pid)));
    
FUNC_EXIT:
    return current_intv;
}


static void do_after_time_shot()
{
    com_if_vsys_mydev_t * mydev = com.vsys->mydev();
    uint32_t new_nxtime;

    do
    {  
        uint32_t current_interval;
        uint32_t get_interval = RAK_PROTOCOL_DFUDELAY;
        
        current_interval = (-1);
        com_if_rak_probe_param_t * probe_info;
        uint8_t probe_cnt = com_sprobe(0).probe_count();
        

        for (uint8_t prb_id = 0; prb_id < probe_cnt; prb_id++)
        {
            com_sprobe(prb_id).probe_walk(&probe_info);

            if (probe_info->sta.real == IDISABLE)
            {
                continue;
            }

            get_interval = do_next_time_slot(probe_info->id);
            
            if( get_interval < current_interval )
            {
                current_interval = get_interval;
            }
        }

        if ( mydev->bitcfg.far_ack )
        {
            mydev->current_intveral = 1;
        }
        else
        {
            if (current_interval == -1)
            {
                mydev->current_intveral = DEF_INTERVAL;
            }
            else
            {
                mydev->current_intveral = current_interval;
            }
        }
        
        if( probe_cnt > 0)
        {
            if (mydev->current_intveral > mydev->sampling_interval)
            {
                mydev->current_intveral = mydev->sampling_interval;
            }
        }
    } while (0);
    

    switch (mydev->id)
    {
        case PROBE_ID_DEF:
            mydev->current_intveral = 2;
            break;
        
        default:
            break;
    }
    
#ifdef EMOS_SUPPORT_WIRE_HOST
    if ( mydev->bitcfg.far_ack )
    {
        new_nxtime = SYNC_FAR_TIME * 2;
    }
    else
#endif
    {
        new_nxtime = MSEC(mydev->current_intveral);
    }

    com_vsys_send( COM_TASK_EVENT, COM_EVENT_PRI_TIMER_SHOT_NEW, &new_nxtime, sizeof(new_nxtime));
    
}

emos_process( mdl_service, 
{
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            break;
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                case COM_TASK_SERV_AFTER_TIME_SHOT:
                    do_after_time_shot();
                    break;
                
                case COM_TASK_SERV_SYNC:
                    do_service_sync(msg_payload[0], &msg_payload[1], msg_len-1);
                    break;

                case COM_TASK_SERV_SEND:
                    do_service_send(msg_payload[0], &msg_payload[1], msg_len-1);
                    break;
                default:
                    break;
            }
            break;
    }
})
