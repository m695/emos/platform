#ifndef __COM_MDL_GPIOMAP_H__
#define __COM_MDL_GPIOMAP_H__

#ifdef __cplusplus
extern "C" {
#endif


/**********************************************/

#define IO_PROBE_EN    0

#if defined(RUI2560_HUB)  //RUI2560_HUB
#undef IO_PROBE_EN
#define IO_PROBE_EN    21
#endif //IO_PROBE_EN

#if defined(RUI2460_HUB)  //RUI2460_HUB
#undef IO_PROBE_EN
#define IO_PROBE_EN    102
#endif //IO_PROBE_EN

/**********************************************/

#define IO_3V3_SW      0
#define IO_5V_SW       0
#define IO_12V_SW      0
#define IO_24V_SW      0

#if defined(STML07X2560_IO)  //STML07X2560_IO
#define IO_3V3_SW      100
#define IO_5V_SW       102
#define IO_12V_SW      112
#define IO_24V_SW      101
#endif

// #if defined(STML07X2460_IO)  //STML07X2460_IO
// #define IO_3V3_SW      100
// #define IO_5V_SW       102
// #define IO_12V_SW      112
// #define IO_24V_SW      101
// #endif

#ifdef __cplusplus
}
#endif

#endif

