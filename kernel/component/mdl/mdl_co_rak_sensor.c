#include "../../kernel.h"
#include "../inc/inc_co_rak_sensor.h"

#define THIS_SNSR_NUM  RAK_SNSR_NUM

static void task_config_tiemout(uint32_t dly_time)
{
    // emos.vsys->ctrl->psm_en = IDISABLE;
    com_task_timeout(COM_TASK_RAK_SENSOR, MSEC(dly_time));
}

static void com_mdl_rak_sensor_justdo( void )
{
    com_if_rak_sensor_t ptr;
    uint8_t ret = EMOS_OK;
    
    for(uint8_t i=0; i<THIS_SNSR_NUM; i++)
    {
        ptr = com.rak_sensor(i);
        ptr.read(NULL);
        ptr.sleep();
        ptr.process();
    }

    // emos.vsys->ctrl->psm_en = emos.vsys->ctrl->psm_def;
}

static void com_mdl_rak_sensor_update( uint8_t * data, uint16_t len)
{
#ifdef EMOS_SUPPORT_RAKSNSR
    com_if_rak_sensor_t ptr;
    uint16_t warmup_dly = 0;
    uint8_t ret = EMOS_OK;


    for(uint8_t i=0; i<THIS_SNSR_NUM; i++)
    {
        ptr = com.rak_sensor(i);
        ptr.wakeup();
        uint16_t val = ptr.warmup();
        warmup_dly = (val > warmup_dly)?val:warmup_dly;
    }

    if ( warmup_dly == 0)
    {
        for(uint8_t i=0; i<THIS_SNSR_NUM; i++)
        {
            ptr = com.rak_sensor(i);
            ptr.read(NULL);
            ptr.sleep();
            ptr.process();
        }
    }
    else
    {
        task_config_tiemout( warmup_dly);
    }
#endif
}

static void com_mdl_rak_sensor_sync( uint8_t * data, uint16_t len)
{
#ifdef EMOS_SUPPORT_RAKSNSR
    com_if_rak_sensor_t ptr;
    uint16_t warmup_dly = 0;

    for(uint8_t i=0; i<THIS_SNSR_NUM; i++)
    {
        ptr = com.rak_sensor(i);
        ptr.wakeup();
        uint16_t val = ptr.warmup();
        warmup_dly = (val > warmup_dly)?val:warmup_dly;
    }

    if ( warmup_dly == 0)
    {
        for(uint8_t i=0; i<THIS_SNSR_NUM; i++)
        {
            ptr = com.rak_sensor(i);
            ptr.read(NULL);
            ptr.sleep();
            ptr.process();
        }
    }
    else
    {
        task_config_tiemout( warmup_dly);
    }
    
#endif
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SNSR_SYNCDONE, NULL, 0);
}

static __RAKLINK_USED void com_mdl_rak_sensor_start( uint8_t * data, uint16_t len)
{
#ifdef EMOS_SUPPORT_RAKSNSR
    uint8_t cnt = 0;
#if 0
    uint8_t ret;
    com_if_rak_bank0_t eee;
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
    com_mydev()->type.value = 0;
    for(uint8_t i=0; i<THIS_SNSR_NUM; i++)
    {
        ptr = com.rak_sensor(i);
        ret = ptr.check();
        if (eee.type_id.value == (('M')+('A'<<8)+('0'<<16)+('1'<<24)))
        {
            ret = ptr.read(NULL);
        }
        if ( ret == EMOS_OK )
        {
            com_mydev()->type.value = eee.type_id.value;
        }
        else if (ret == 1) // dummy_func fix return 
        {
            continue;
        }
        else 
        {
            com_mydev()->type.value = 0;
        }
        ptr.sleep();

    }
#endif

    com_sprobe(0).loc_probe_get(&com_iprobe);
    if ( com_iprobe != NULL )
    {
        
        do
        {
            com_if_rak_sensor_t snsr_drv;
            
            if (com_iprobe->sta.nocheck == IENABLE)
            {
                
                for(uint8_t i=0; i<THIS_SNSR_NUM; i++)
                {
                    snsr_drv = com.rak_sensor(i);
                    snsr_drv.check();
                    snsr_drv.sleep();
                }
                memcpy(&(com_iprobe->type_id.current),&(com_iprobe->type_id.expect), sizeof(uint32_t));
                break;
            }
            else
            {
                com_iprobe->type_id.current.value = 0;
                for(uint8_t i=0; i<THIS_SNSR_NUM; i++)
                {
                    snsr_drv = com.rak_sensor(i);
                    if ( snsr_drv.check() == EMOS_OK )
                    {
                        com_iprobe->type_id.current.bytes[cnt++] = snsr_drv.status()->sku;
                    }
                    snsr_drv.sleep();
                }
            }
        } while (0);
    }
#endif
    
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SNSR_CKDONE, NULL, 0);
}

static void com_mdl_rak_sensor_init( void )
{
#ifdef EMOS_SUPPORT_RAKSNSR
    com_if_rak_sensor_t ptr;
    for(uint8_t i=0; i<THIS_SNSR_NUM; i++)
    {
        ptr = com.rak_sensor(i);
        ptr.reset();
    }
#endif
}

emos_process( mdl_rak_sensor, 
{
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            com_mdl_rak_sensor_init();
            break;
        case TASK_MODE_TIMEOUT:
            com_mdl_rak_sensor_justdo();
            break;
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                case COM_VSYS_RAK_SNSR_START:
                    com_mdl_rak_sensor_start(msg_payload, msg_len);
                    break;
                case COM_VSYS_RAK_SNSR_SYNC:
                    com_mdl_rak_sensor_sync(msg_payload, msg_len);
                    break;
                case COM_VSYS_RAK_SNSR_UPDATE:
                    com_mdl_rak_sensor_update(msg_payload, msg_len);
                    break;
                default:
                    break;
            }
            break;
    }
})
