#include "../../kernel.h"
#include "mdl_gpiomap.h"

#ifdef EMOS_SUPPORT_RAK_LDO

#include "../inc/inc_co_rak_ldo.h"


#define THIS_DELAY     1


static void do_com_mdl_rak_ldo_33v( void )
{
    emos.gpio->o_plow(GPIO_MASK(IO_3V3_SW)); 
    emos.gpio->o_plow(GPIO_MASK(IO_5V_SW));
    emos.gpio->o_plow(GPIO_MASK(IO_12V_SW)); 
    emos.gpio->o_plow(GPIO_MASK(IO_24V_SW));
}

static void do_com_mdl_rak_ldo_120v( void )
{
    emos.gpio->o_phigh(GPIO_MASK(IO_3V3_SW)); 
    emos.gpio->o_phigh(GPIO_MASK(IO_5V_SW));
    emos.gpio->o_phigh(GPIO_MASK(IO_12V_SW)); 
    emos.gpio->o_plow(GPIO_MASK(IO_24V_SW));
}

static void do_com_mdl_rak_ldo_240v( void )
{
    emos.gpio->o_phigh(GPIO_MASK(IO_3V3_SW)); 
    emos.gpio->o_phigh(GPIO_MASK(IO_5V_SW));
    emos.gpio->o_phigh(GPIO_MASK(IO_12V_SW)); 
    emos.gpio->o_phigh(GPIO_MASK(IO_24V_SW));
}

static void com_mdl_rak_ldo_v12_init( void )
{
    com.ldo->i.v33( (void *)do_com_mdl_rak_ldo_33v );
    com.ldo->i.v120( (void *)do_com_mdl_rak_ldo_120v );
    com.ldo->i.v240( (void *)do_com_mdl_rak_ldo_240v );

    emos.gpio->o_dir_output(GPIO_MASK(IO_3V3_SW)); 
    emos.gpio->o_dir_output(GPIO_MASK(IO_5V_SW));
    emos.gpio->o_dir_output(GPIO_MASK(IO_12V_SW)); 
    emos.gpio->o_dir_output(GPIO_MASK(IO_24V_SW));
    
    emos.gpio->o_plow(GPIO_MASK(IO_3V3_SW)); 
    emos.gpio->o_plow(GPIO_MASK(IO_5V_SW));
    emos.gpio->o_plow(GPIO_MASK(IO_12V_SW)); 
    emos.gpio->o_plow(GPIO_MASK(IO_24V_SW));
}

static void com_mdl_rak_ldo_vccprobe_pwroff( void )
{
    com_if_vsys_mydev_t * mydev = com_mydev();
    emos.gpio->o_dir_output(GPIO_MASK(IO_PROBE_EN));
    if( mydev->hw_version == 'D')
    {
        emos.gpio->o_phigh(GPIO_MASK(IO_PROBE_EN));
    }
    else
    {
        emos.gpio->o_plow(GPIO_MASK(IO_PROBE_EN));
    }
}

static void com_mdl_rak_ldo_vccprobe_pwron( void )
{
    com_if_vsys_mydev_t * mydev = com_mydev();
    emos.gpio->o_dir_output(GPIO_MASK(IO_PROBE_EN));
    if( mydev->hw_version == 'D')
    {
        emos.gpio->o_plow(GPIO_MASK(IO_PROBE_EN));
    }
    else
    {
        emos.gpio->o_phigh(GPIO_MASK(IO_PROBE_EN));
    }
}

static void com_mdl_rak_ldo_vccprobe_init( void )
{
    emos.gpio->o_dir_output(GPIO_MASK(IO_PROBE_EN));

    com_mdl_rak_ldo_vccprobe_pwroff();

    /* wait time for power on*/
    com_task_timeout(COM_TASK_RAK_LDO, MSEC(THIS_DELAY));
}


emos_process( mdl_rak_ldo, 
{
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            /* do nothing*/
            break;

        case TASK_MODE_POLLING:
            break;

        case TASK_MODE_TIMEOUT:
            com_mdl_rak_ldo_vccprobe_pwron();
            com_vsys_send(COM_TASK_EVENT,COM_EVENT_SYS_PROBE_PWRON,NULL,0);
            break;
            
        case TASK_MODE_SLEEP:
            break;

        case TASK_MODE_WAKEUP:
            break;
            
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                case COM_TASK_EVT_RAK_LDO_VCCPROBE_RESET:
                case COM_TASK_EVT_RAK_LDO_VCCPROBE_INIT:
                    com_mdl_rak_ldo_vccprobe_init();
                    break;
                case COM_TASK_EVT_RAK_LDO_V12_INIT:
                    com_mdl_rak_ldo_v12_init();
                    break;
                default:
                    break;
            }
            break;
    }
})
#else

#ifdef BSP_NOT_SUPPORT_WEAK
void com_mdl_rak_ldo_process(emos_task_msg_t *task_msg)
{ 
}
#endif //BSP_NOT_SUPPORT_WEAK

#endif //EMOS_SUPPORT_HALL
