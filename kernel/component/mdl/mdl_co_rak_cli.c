#include "../../kernel.h"

#ifdef EMOS_SUPPORT_CLI

#include "../inc/inc_co_rak_cli.h"

typedef com_mdl_rak_cli_handle_t cli_handle_t;

static uint8_t this_cli_buff[CLI_BUFF_SIZE];

static const void* cli_command_lst[] =
{
    &dfu_command_tbl,
    &general_command_tbl,
    &factory_command_tbl,
#ifdef EMOS_SUPPORT_WIRE_HOST
    &probe_command_tbl,
    &probeio_command_tbl,
#endif

#ifdef EMOS_SUPPORT_BLE
    &ble_command_tbl,
#endif

#ifdef EMOS_SUPPORT_NFC
    &nfc_command_tbl,
#endif

#ifdef EMOS_SUPPORT_LORA
    &lora_command_tbl,
#endif

#ifdef EMOS_SUPPORT_NBIOT
    &nbiot_command_tbl,
#endif
    &radio_command_tbl,
};

static void com_mdl_at_command(uint8_t *data ,uint8_t len)
{
    uint8_t ret = EMOS_OK;
    /* patch command */
    if(emos_strcmp((char *)data, "ATE", 0) == 0)
    {
        com.cli->ctrl->ate_en = !com.cli->ctrl->ate_en;
        goto PATCH_EXIT;
    }

    if(emos_strcmp((char *)data, "ATZ!", 0) == 0)
    {
        emos.units->reboot();
        goto PATCH_EXIT;
    }

    if(emos_strcmp((char *)data, "ATZ", 0) == 0)
    {
        com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_REBOOT, NULL, 0);
        goto PATCH_EXIT;
    }
    
    if(emos_strcmp((char *)data, "ATR", 0) == 0)
    {
        com.flashmap->rest_rdy();
    }

    com_vsys_send( COM_TASK_EVENT, COM_EVENT_BSP2ATCMD,(uint8_t *)data, len);
    return;

PATCH_EXIT:
    SYS_INFO(RET_MSG("%s"),emos.errcode[ret]);
}

static void com_mdl_ts_command(uint8_t *data ,uint16_t len)
{
    char **pp;
    pp = (char **)emos_split( (char *)data, "+", NULL);
    com_vsys_sendprf( COM_TASK_EVENT, COM_EVENT_WIRP_RUI_TRANS,"atc+%s",pp[1]);
}

static void com_mdl_atcell_command(uint8_t *data ,uint16_t len)
{
#ifdef EMOS_SUPPORT_NBIOT
    char *pp;
    pp = (char *)strsep( &data, "+=");
    com_vsys_sendprf( COM_TASK_EVENT, COM_EVENT_NBIOT_CMD,"at+%s\r\n\032\0",data);
#endif
}

static void com_mdl_tsc_command(uint8_t *data ,uint16_t len)
{
    char **pp;
    pp = (char **)emos_split( (char *)data, "+", NULL);
    com_vsys_sendprf( COM_TASK_EVENT, COM_EVENT_WIRP_RUI_TRANS,"atc+%s",pp[1]);
}

static void com_mdl_tp_command(uint8_t *data ,uint16_t len)
{
#ifdef EMOS_SUPPORT_TRANSPRENT
    com.tp->sndtp(data, len);
#endif
}

static void com_mdl_atc_command(uint8_t *data ,uint16_t len)
{
    uint8_t i;
    uint8_t j;
    char **pp;
    cli_handle_t ** grp_handle;
    cli_handle_t *tar_handle;
    uint16_t pp_cnt;
    char atcheader[20];
    uint8_t ret = EMOS_CMD_NOT_FOUND;
    com_mdl_rak_cli_param_t cli_param;
    memset(&cli_param, 0, sizeof(com_mdl_rak_cli_param_t));

    pp = emos_split_limit( (char *)data, " +=;[](){}", (uint16_t *)&pp_cnt);

    uint8_t list_cnt = COUNT_OF(cli_command_lst);
    for(j=0; j<list_cnt; j++)
    {
        grp_handle = (cli_handle_t**)cli_command_lst[j];
        
        i=0;
        
        while(grp_handle[i]!=NULL)
        {
            tar_handle=(cli_handle_t *)&grp_handle[i];
            uint8_t ret = emos_strcmp( pp[1], (char *)tar_handle->cmd, 0);

            if( ret == 0 ) 
            {
                ret = 1;
                goto FUNC_BREAK;
            }
            i++;
        }
    }
    
    SYS_INFO(RET_MSG("%s"),emos.errcode[ret]);
    goto FUNC_EXIT;
FUNC_BREAK:

    memset( atcheader, 0, sizeof(atcheader));
    snprintf( atcheader, sizeof(atcheader), "%s+%s","ATC",tar_handle->cmd );

    if(pp_cnt > 2)
    {
        uint16_t num;
        uint16_t this_cmd_len = strlen(pp[2]);
        memset( this_cli_buff, 0, sizeof(this_cli_buff) );
        memcpy( this_cli_buff, pp[2], this_cmd_len );

        char ** ss = emos_split((char *)this_cli_buff,":",(uint16_t *)&num);

        cli_param.header = atcheader;
        uint32_t out_num;
        uint16_t retc = 0;
        uint16_t cnum = 0;

        for (uint16_t ck=0,idx=0; ck < strlen(tar_handle->field) && idx<num; ck++,idx++)
        {
            retc = 0;
            uint16_t getsize = strlen(ss[idx]);


            if (tar_handle->field[ck] == CLI_CK(xNUL))
            {
                break;
            }

            for(uint16_t i=0; i<getsize ;i++)
            {
                if(ss[idx][i] == '?')
                {
                    retc = i+1;
                    break;
                }
            }

            if( retc != 0 && cli_param.flag.is_qust == 0 )
            {
                if(tar_handle->field[ck+1] == CLI_CK(xQST) ||
                   tar_handle->field[ck+0] == CLI_CK(xQST))
                {
                    if (retc == getsize)
                    {
                        ss[idx][retc-1] = '\0';
                        cli_param.flag.is_qust = 1;
                    }
                }

                if (tar_handle->field[ck] == CLI_CK(xQST))
                {
                    ck++;
                }
            }

            if (tar_handle->field[ck] == CLI_CK(xENT))
            {
                retc = emos_atoi(&ss[idx][0],&out_num);
                if ( retc != 0)
                {
                    ret = EMOS_PARAM_ERROR;
                    goto FUNC_RETN;
                }
                
                if (com.cli->ctrl->input_cmd_len == 0)
                {
                    
                    com.cli->ctrl->input_cnt = out_num;
                    com.cli->ctrl->input_data_len = out_num;
                    com.cli->ctrl->input_cmd_len = strlen((char *)data) + 1;
                    
                    memcpy(com.cli->ctrl->input_cmd,data,com.cli->ctrl->input_cmd_len);
                    com.cli->ctrl->input_cmd[com.cli->ctrl->input_cmd_len - 1] = '\0';
                    ret = EMOS_CLI_CONTINUE;
                    goto FUNC_RETN;
                }
                else
                {
                    com.cli->ctrl->input_cnt = 0;
                    com.cli->ctrl->input_data_len = 0;
                    com.cli->ctrl->input_cmd_len = 0;
                    memset(com.cli->ctrl->input_cmd , 0, sizeof(com.cli->ctrl->input_cmd));
                    
                    ss[idx] = (char *)&this_cli_buff[this_cmd_len];
                    this_cmd_len+=sizeof(uint32_t);
                    memset( ss[idx], 0, getsize);
                    memcpy( ss[idx], &out_num, sizeof(uint32_t));
                }
            }

            if (tar_handle->field[ck] == CLI_CK(xQST))
            {
                if ( ck == 0 && cli_param.flag.is_qust == 0)
                {
                    ret = EMOS_PARAM_ERROR;
                }
                
                ck++;
            }

            if (tar_handle->field[ck] == CLI_CK(xINT))
            {
                retc = emos_atoi(&ss[idx][0],&out_num);
                if ( retc != 0)
                {
                    ret = EMOS_PARAM_ERROR;
                    goto FUNC_RETN;
                }

                ss[idx] = (char *)&this_cli_buff[this_cmd_len];
                this_cmd_len+=sizeof(uint32_t);
                memset( ss[idx], 0, getsize);
                memcpy( ss[idx], &out_num, sizeof(uint32_t));
                
            }
            
            if (tar_handle->field[ck] == CLI_CK(xHEX))
            {
                // SYS_INFO("xH");
            }

            if (tar_handle->field[ck] == CLI_CK(xDEC))
            {
                // SYS_INFO("xD");
            }

            if (tar_handle->field[ck] == CLI_CK(xPRB))
            {
                retc = emos_atoi(&ss[idx][0],&out_num);
                if ( retc != 0 || PROBE_ID_CHECK(out_num))
                {
                    ret = EMOS_PARAM_ERROR;
                    goto FUNC_RETN;
                }

                memset( ss[idx], 0, strlen(ss[idx]));
                ss[idx][0] = out_num;
            }

            if (tar_handle->field[ck] == CLI_CK(xSNR))
            {
                retc = emos_atoi(&ss[idx][0],&out_num);
                if ( retc != 0 || (out_num > 0xFF))
                {
                    ret = EMOS_PARAM_ERROR;
                    goto FUNC_RETN;
                }

                memset( ss[idx], 0, strlen(ss[idx]));
                ss[idx][0] = out_num;
            }

            if (tar_handle->field[ck] == CLI_CK(xSTR))
            {
                ret = EMOS_OK;
            }

            if (tar_handle->field[ck] == CLI_CK(xCHR))
            {
                if ( retc != 0 || (strlen(ss[idx]) > 1))
                {
                    ret = EMOS_PARAM_ERROR;
                    goto FUNC_RETN;
                }
            }
            
            if (tar_handle->field[ck] == CLI_CK(xBLN))
            {
                retc = emos_atoi(&ss[idx][0],&out_num);
                if ( retc != 0 || (ENABLE_CHECK(out_num)))
                {
                    ret = EMOS_PARAM_ERROR;
                    goto FUNC_RETN;
                }

                memset( ss[idx], 0, strlen(ss[idx]));
                ss[idx][0] = out_num;
            }
            
            cnum++;
        }
        
        if(cnum != num)
        {
            ret = EMOS_PARAM_ERROR;
        }

        if(ret == EMOS_PARAM_ERROR)
        {
            goto FUNC_RETN;
        }

        cli_param.ret_string = NULL;
        
        cli_param.flag.is_afrwait = com.cli->ctrl->wait_en;
        cli_param.retdata_len = com.cli->ctrl->waitlog_len;
        cli_param.retdata = (com.cli->ctrl->waitlog);
        ret = tar_handle->cmd_handle(&cli_param, num, ss);
        if (ret == EMOS_CLI_WAIT)
        {
            /* backup cli command, and goto sleep task*/
            com.cli->ctrl->wait_en = 1;
            com.cli->ctrl->waitcur.value = 0;
            com.cli->ctrl->dir_id = com_source_id;
            com_task_sleep(COM_TASK_RAK_CLI);
            com.cli->ctrl->input_cmd_len = strlen((char *)data) + 1;
            memcpy(com.cli->ctrl->input_cmd,data,com.cli->ctrl->input_cmd_len);
            com.cli->ctrl->input_cmd[com.cli->ctrl->input_cmd_len - 1] = '\0';
            com.cli->ctrl->waitfor.value = cli_param.waitfor.value;

            return;
        }
        
        if ( cli_param.flag.is_afrwait == IENABLE )
        {
            /* done after wakeup */
            com.cli->ctrl->wait_en = IDISABLE;
            memset(com.cli->ctrl->input_cmd , 0, sizeof(com.cli->ctrl->input_cmd));
        }

        if (cli_param.ret_string != NULL)
        {
            do
            {
                uint8_t * pchar = emos.units->memcat(NULL,0,(char *)cli_param.ret_string,strlen((char *)cli_param.ret_string));
                DIR_MSG(RET_MSG("%s=%s"),atcheader,pchar);
                com_vsys_send( COM_USER_EVENT, USER_EVENT_CLI_RSP, pchar, strlen(pchar));
            } while (0);
        }
    }

FUNC_RETN:
    switch (ret)
    {
        case EMOS_CLI_CONTINUE:
            DIR_MSG("\r\n%s",emos.errcode[ret]);
            break;
        case EMOS_CLI_WAIT:
            break;
        default:
            DIR_MSG(RET_MSG("%s"),emos.errcode[ret]);
            break;
    }
FUNC_EXIT:
    return;
}

emos_process( mdl_rak_cli, 
{
    static uint32_t current_time = 0;
    static uint32_t retry_time = 0;
    uint32_t now_time = emos_gettick();

    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            com.cli->ctrl->ate_en = 0;
            break;
            
        case TASK_MODE_POLLING:
            if ( com.cli->ctrl->wait_en == IENABLE )
            {
                if (com.cli->ctrl->waitfor.value == com.cli->ctrl->waitcur.value)
                {
                    com.cli->ctrl->waitcur.value = 0;
                    com_task_wakeup(COM_TASK_RAK_CLI);
                    com_source_id = com.cli->ctrl->dir_id;
                    com_mdl_atc_command((uint8_t *)com.cli->ctrl->input_cmd, com.cli->ctrl->input_cmd_len);
                }
            }
            break;
        
        case TASK_MODE_WAKEUP:
            current_time = 0;
            retry_time = 0;
            break;

        case TASK_MODE_SLEEP:
            do
            {
                if (current_time == 0)
                {
                    current_time = now_time;
                    retry_time = now_time;
                }
                
                if ((now_time - retry_time) < MSEC(EMOS_CLI_RETRY))
                {
                    return;
                }
                retry_time = now_time;

                if ((now_time - current_time) < MSEC(EMOS_CLI_TIMEOUT))
                {
                    com.cli->ctrl->wait_en = IDISABLE;
                    com_source_id = com.cli->ctrl->dir_id;
                    com_mdl_atc_command((uint8_t *)com.cli->ctrl->input_cmd, com.cli->ctrl->input_cmd_len);
                }
                else
                {
                    com_task_wakeup(COM_TASK_RAK_CLI);
                    
                    com.cli->ctrl->wait_en = IDISABLE;
                    com.cli->ctrl->waitcur.value = 0;
                    com_source_id = com.cli->ctrl->dir_id;
                    DIR_MSG(RET_MSG("%s"),emos.errcode[EMOS_TIMEOUT_ERROR]);
                }
            } while (0);
            break;
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                case COM_TASK_RAK_ATCMD_AT:
                    com_mdl_at_command(msg_payload, msg_len);
                    break;
                case COM_TASK_RAK_ATCMD_ATC:
                    com_mdl_atc_command(msg_payload, msg_len);
                    break;
                case COM_TASK_RAK_ATCMD_TSC:
                    com_mdl_tsc_command(msg_payload, msg_len);
                    break;
                case COM_TASK_RAK_ATCMD_TS:
                    com_mdl_ts_command(msg_payload, msg_len);
                    break;
                case COM_TASK_RAK_ATCMD_TP:
                    com_mdl_tp_command(msg_payload, msg_len);
                    break;
                #ifdef EMOS_SUPPORT_RUI3_ATCELL
                case COM_TASK_RAK_ATCMD_ATCELL:
                    com_mdl_atcell_command(msg_payload, msg_len);
                    break;
                #endif
            }
            break;
    }
})
#endif



