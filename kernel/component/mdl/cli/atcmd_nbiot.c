#include "mdl_co_cli.h"

static uint8_t command_atc_nbiot_dtm(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com.nbiot->ctrl->nbt_test_mode = argv[0][0];

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_send(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com.nbiot->upload(argv[0], strlen(argv[0]));

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_apply(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    if (param->flag.is_qust)
    {
        CLI_TEXT("%d", com.nbiot->ctrl->nbt_apply);
    }
    else
    {
        com.nbiot->apply(argv[0][0]);
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_info(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    if (param->flag.is_qust) 
    {
        DBG_INFO("Mode = %s\r\nIP=%s\r\nPort=%d\r\n", eee.nbiot_info.mode, eee.nbiot_info.ip, eee.nbiot_info.port);
        DBG_INFO("Sub Topic = %s\r\nPub Topic = %s\r\n", eee.nbiot_info.sub_topic, eee.nbiot_info.pub_topic);
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_power(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{

    com.nbiot->power(argv[0][0]);

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_sleep(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{

    com.nbiot->sleep(argv[0][0]);

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_app_mode(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%s", eee.nbiot_info.mode);
    }
    else
    {
        memset((uint8_t *)&(eee.nbiot_info.mode), 0, sizeof(eee.nbiot_info.mode));
        memcpy((uint8_t *)&(eee.nbiot_info.mode), argv[0], strlen(argv[0]));
        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_srvr_ip(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    
    if (param->flag.is_qust)
    {
        CLI_TEXT("%s", eee.nbiot_info.ip);
    }
    else
    {
        memset((uint8_t *)&(eee.nbiot_info.ip), 0, sizeof(eee.nbiot_info.ip));
        memcpy((uint8_t *)&(eee.nbiot_info.ip), argv[0], strlen(argv[0]));
        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_srvr_port(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%d", eee.nbiot_info.port);
    }
    else
    {
        uint32_t num = 0;
        emos_atoi(argv[0],&num);
        eee.nbiot_info.port = num;
        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_srvr_hbeat(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    return EMOS_OK;
}

static uint8_t command_atc_nbiot_srvr_keep_alive(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    return EMOS_OK;
}

static uint8_t command_atc_nbiot_srvr_hbeat_content(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    return EMOS_OK;
}

static uint8_t command_atc_nbiot_srvr_data_format(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%d", eee.nbiot_info.data_format);
    }
    else
    {
        eee.nbiot_info.data_format = argv[0][0];
        com.nbiot->ctrl->nbt_data_format = argv[0][0];
        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_srvr_conn_status(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    if (param->flag.is_qust)
    {
        DBG_INFO("NBIOT Connection status = %d\r\n", com.nbiot->ctrl->nbt_conn_status);
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_mqtt_client_id(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%s", eee.nbiot_info.client_id);
    }
    else
    {
        memcpy((uint8_t *)&(eee.nbiot_info.client_id), argv[0], sizeof(eee.nbiot_info.client_id));
        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_mqtt_subscribe(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%s", eee.nbiot_info.sub_topic);
    }
    else
    {
        memset((uint8_t *)&(eee.nbiot_info.sub_topic), 0, sizeof(eee.nbiot_info.sub_topic));

        memcpy((uint8_t *)&(eee.nbiot_info.sub_topic), argv[0], strlen(argv[0]));

        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_mqtt_publish(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%s", eee.nbiot_info.pub_topic);
    }
    else
    {
        memset((uint8_t *)&(eee.nbiot_info.pub_topic), 0, sizeof(eee.nbiot_info.pub_topic));

        memcpy((uint8_t *)&(eee.nbiot_info.pub_topic), argv[0], strlen(argv[0]));

        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_server_auth(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%d", eee.nbiot_info.server_auth);
    }
    else
    {
        eee.nbiot_info.server_auth = argv[0][0];

        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

        //WistoolBox workaround
        DBG_INFO("AT+QSSLCFG=\"ignorelocaltime\",2,1\r\n");

        //com.nbiot->server_auth(argv[0][0]);
        com.nbiot->local_param_reset();
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_user_auth(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%d", eee.nbiot_info.user_auth);
    }
    else
    {
        eee.nbiot_info.user_auth = argv[0][0];

        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_auth_username(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    
    if (param->flag.is_qust)
    {
        CLI_TEXT("%s", eee.nbiot_info.auth_username);
    }
    else
    {
        memset((uint8_t *)&(eee.nbiot_info.auth_username), 0, sizeof(eee.nbiot_info.auth_username));
        memcpy((uint8_t *)&(eee.nbiot_info.auth_username), argv[0], strlen(argv[0]));
        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_auth_password(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    
    if (param->flag.is_qust)
    {
        CLI_TEXT("%s", eee.nbiot_info.auth_password);
    }
    else
    {
        memset((uint8_t *)&(eee.nbiot_info.auth_password), 0, sizeof(eee.nbiot_info.auth_password));
        memcpy((uint8_t *)&(eee.nbiot_info.auth_password), argv[0], strlen(argv[0]));
        com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    }

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_auth_cacert(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com.nbiot->auth_cacert((void *)argv[2], strlen((void *)argv[2]));

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_auth_client_cert(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com.nbiot->auth_clientcert((void *)argv[2], strlen((void *)argv[2]));

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_auth_client_key(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com.nbiot->auth_clientkey((void *)argv[2], strlen((void *)argv[2]));

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_gps_enable(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com.nbiot->gps_enable(argv[0][0]);

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_gps_data(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    if (param->flag.is_qust)
        com.nbiot->gps_data();

    return EMOS_OK;
}

static uint8_t command_atc_nbiot_http_request(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t p_data[100];
    memset(p_data, 0, 100);

    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    eee.nbiot_info.http_method = argv[0][0];
    com.nbiot->ctrl->nbt_http_method = argv[0][0];

    memset((uint8_t *)&(eee.nbiot_info.http_url), 0, sizeof(eee.nbiot_info.http_url));
    memcpy((void *)p_data, argv[1], strlen(argv[1]));
    strcat((void *)p_data, ":");
    strcat((void *)p_data, argv[2]);
    memcpy((void *)&(eee.nbiot_info.http_url), p_data, strlen((void *)p_data));
    
    eee.nbiot_info.http_url_len = strlen((void *)p_data);

    com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    return EMOS_OK;
}

const cli_handle_t nbiot_command_tbl[] =
{
    {"NBIOT_DTM" , xINT xNUL xNUL xNUL, command_atc_nbiot_dtm},
    
    {"NBIOT_SEND", xSTR xNUL xNUL xNUL, command_atc_nbiot_send},
    
    {"NBIOT_INFO" , xQST xNUL xNUL xNUL, command_atc_nbiot_info},
    {"NBIOT_APPLY", xINT xQST xNUL xNUL, command_atc_nbiot_apply},
    {"NBIOT_POWER", xINT xQST xNUL xNUL, command_atc_nbiot_power},
    {"NBIOT_SLEEP", xINT xNUL xNUL xNUL, command_atc_nbiot_sleep},

    {"NBIOTAPP"   , xSTR xQST xNUL xNUL, command_atc_nbiot_app_mode},
    {"SRVRIP"     , xSTR xQST xNUL xNUL, command_atc_nbiot_srvr_ip},
    {"SRVRPORT"   , xSTR xQST xNUL xNUL, command_atc_nbiot_srvr_port},
    {"SRVRHBEAT"  , xINT xQST xNUL xNUL, command_atc_nbiot_srvr_hbeat},
    {"SRVRKEEPALIVE", xSTR xQST xNUL xNUL, command_atc_nbiot_srvr_keep_alive},
    {"SRVRHBEATCNXT", xSTR xQST xNUL xNUL, command_atc_nbiot_srvr_hbeat_content},
    {"DATAENCAP"  , xINT xQST xNUL xNUL, command_atc_nbiot_srvr_data_format},
    {"SRVRCON"    , xQST xNUL xNUL xNUL, command_atc_nbiot_srvr_conn_status},
    {"MQTTCLIENTID", xSTR xQST xNUL xNUL, command_atc_nbiot_mqtt_client_id},
    {"MQTTSUB"    , xSTR xQST xNUL xNUL, command_atc_nbiot_mqtt_subscribe},
    {"MQTTPUB"    , xSTR xQST xNUL xNUL, command_atc_nbiot_mqtt_publish},
    {"SRVRAUTH"   , xINT xQST xNUL xNUL, command_atc_nbiot_server_auth},
    {"USERAUTH"   , xINT xQST xNUL xNUL, command_atc_nbiot_user_auth},
    {"AUTH_USER"  , xSTR xQST xNUL xNUL, command_atc_nbiot_auth_username},
    {"AUTH_PWD"   , xSTR xQST xNUL xNUL, command_atc_nbiot_auth_password},
    {"AUTH_CACERT",xINT xINT xSTR xNUL, command_atc_nbiot_auth_cacert},
    {"AUTH_CLIENTCERT",xINT xINT xSTR xNUL, command_atc_nbiot_auth_client_cert},
    {"AUTH_CLIENTKEY" ,xINT xINT xSTR xNUL, command_atc_nbiot_auth_client_key},
    {"GPS_ENABLE" , xINT xNUL xNUL xNUL, command_atc_nbiot_gps_enable},
    {"GPS_DATA"   , xQST xNUL xNUL xNUL, command_atc_nbiot_gps_data},
    {"HTTP_REQUEST" , xINT xSTR xSTR xNUL, command_atc_nbiot_http_request},
    {NULL},
};

