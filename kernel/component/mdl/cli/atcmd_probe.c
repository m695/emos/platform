#include "mdl_co_cli.h"


static uint8_t command_atc_hub_info( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    char hw_version = (char )com_mydev()->hw_version;
    com_if_vsys_mydev_t * mydev = com_mydev();
    uint8_t sw_version0 = mydev->sw_version[0];
    uint8_t sw_version1 = mydev->sw_version[1];
    uint8_t sw_version2 = mydev->sw_version[2];
    char deveui[8];
    emos.lora->get_deveui((void *)deveui);
    char *model = (char *)mydev->model_name;
    uint8_t *ssn = (uint8_t *)mydev->ssn.value;
    UNUSED(ssn);
    uint8_t *sn = (uint8_t *)mydev->sn.value;
    char type[5];
    uint8_t j=0;
    for (uint8_t i = 0; i < (sizeof(uint32_t)); i++)
    {
        if (mydev->type_id.current.bytes[i] != 0)
        {
            type[j] = mydev->type_id.current.bytes[i];
            j++;
        }
    }
    type[j] = '\0';
    CLI_TEXT("V%c:V%d.%d.%d:%02x%02x%02x%02x%02x%02x%02x%02x:%s:%.18s:%.4s",
                            hw_version,sw_version0,sw_version1,sw_version2,
                            deveui[0],deveui[1],deveui[2],deveui[3],deveui[4],deveui[5],deveui[6],deveui[7],
                            model,sn,type);
    return EMOS_OK;
}


static uint8_t command_atc_probe_info( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{  
    uint8_t ret;
    com_if_rak_probe_param_t * probe_info;

    uint32_t probe_id = (uint32_t)argv[0][0];
    
    ret = com_get_probe(probe_id,&probe_info);
    if( ret != EMOS_OK)
    {
        return EMOS_PARAM_ERROR;
    }
    
    if (probe_info->sta.stop == IENABLE)
    {
        return EMOS_PARAM_ERROR;
    }
    
    char type[5];
    uint8_t j=0;
    for (uint8_t i = 0; i < (sizeof(type) - 1); i++)
    {
        if (probe_info->type_id.current.bytes[i] != 0)
        {
            type[j] = probe_info->type_id.current.bytes[i];
            j++;
        }
    }
    type[j] = '\0';

    CLI_TEXT("%d:V%c:V%d.%d.%d:%s:%02x%02x%x:%s", probe_info->id,
                                                probe_info->hw_version,
                                                probe_info->sw_version[0],probe_info->sw_version[1],probe_info->sw_version[2],
                                                probe_info->model_name,
                                                probe_info->ssn.value[0],
                                                probe_info->ssn.value[1],
                                                probe_info->ssn.value[2]&0xF,
                                                type
                                                );
    return EMOS_OK;
}

static uint8_t command_atc_probe_cnt(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_probe_param_t * probe_info;
    uint8_t probe_cnt = com_sprobe(0).probe_count();
    uint8_t probe_lesscnt = 0;

    char cat_str[10];
    uint16_t tmp_len = probe_cnt * sizeof(":%3d") + 1;
    char * tmp = com_malloc(tmp_len);
    memset(tmp,0,tmp_len);
    for (uint8_t i = 0; i < probe_cnt; i++)
    {
        com_sprobe(i).probe_walk(&probe_info);
        if (probe_info->sta.stop == IENABLE)
        {
            probe_lesscnt++;
            continue;
        }
        
        snprintf(cat_str, sizeof(cat_str), ":%d", probe_info->id);
        strcat(tmp, cat_str);
    }
    probe_cnt = probe_cnt - probe_lesscnt;
    CLI_TEXT("%d%s", probe_cnt,tmp);
    com_free(tmp);

    return EMOS_OK;
}

static uint8_t command_atc_probe_state( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret;
    com_if_rak_probe_param_t * probe_info;

    uint32_t probe_id = (uint32_t)argv[0][0];
    
    ret = com_get_probe(probe_id,&probe_info);
    if( ret != EMOS_OK)
    {
        return EMOS_PARAM_ERROR;
    }

    CLI_TEXT("%d:%s", probe_info->id,(probe_info->sta.stop)?"STOP":"ACTIVE");
    return EMOS_OK;
}

static uint8_t command_atc_probe_sampling(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *sampling_intv = (uint32_t *)&argv[1][0];

    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    if ( param->flag.is_afrwait == IDISABLE)
    {
        
        if( param->flag.is_qust )
        {
            com.rpcl->get_prbsmplintv(probe_id);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMGET,RAK_PB_PAY_TYPE_PARAM_SMPL_INTV);
        }
        else
        {
            com.rpcl->set_prbsmplintv(probe_id,*sampling_intv);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMSET,RAK_PB_PAY_TYPE_PARAM_SMPL_INTV);
        }
        return EMOS_CLI_WAIT;
    }

    uint8_t *retdata = param->retdata;
    uint16_t retdata_len = param->retdata_len;

    UNUSED(retdata);
    UNUSED(retdata_len);

    if( param->flag.is_qust )
    {
        do
        {
            uint32_t *this_intv = (void *)retdata;
            CLI_TEXT("%d:%d",probe_id,*this_intv);
        } while (0);
    }

    return EMOS_OK;
}

static uint8_t command_atc_probe_intv(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *probe_intv = (uint32_t *)&argv[1][0];

    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    if ( param->flag.is_afrwait == IDISABLE)
    {
        
        if( param->flag.is_qust )
        {
            com.rpcl->get_prbintv(probe_id);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMGET,RAK_PB_PAY_TYPE_PARAM_PRB_INTV);
        }
        else
        {
            com.rpcl->set_prbintv(probe_id,*probe_intv);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMSET,RAK_PB_PAY_TYPE_PARAM_PRB_INTV);
        }
        return EMOS_CLI_WAIT;
    }

    uint8_t *retdata = param->retdata;
    uint16_t retdata_len = param->retdata_len;

    UNUSED(retdata);
    UNUSED(retdata_len);

    if( param->flag.is_qust )
    {
        do
        {
            uint32_t *this_intv = (void *)retdata;
            CLI_TEXT("%d:%d",probe_id,*this_intv);
        } while (0);
    }

    return EMOS_OK;
}

static uint8_t command_atc_probe_onof(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com.ldo->vccprb_rest();
    return EMOS_OK;
}

static uint8_t command_atc_probe_rest(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    uint32_t probe_id = (uint32_t)argv[0][0];

    ret = com_get_probe(probe_id,&probe_info);
    if( ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }
    
    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->set_prbrest(probe_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_CONTROL,RAK_PB_PAY_TYPE_CONTROL_RESTORE);
        return EMOS_CLI_WAIT;
    }

    com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROBE_RESET_DONE, (uint8_t *)&probe_id, 1);
    return EMOS_OK;
}

static uint8_t command_atc_probe_dfu(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    uint32_t probe_id = (uint32_t)argv[0][0];

    ret = com_get_probe(probe_id,&probe_info);
    if( ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }
    
    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->set_prbdfu(probe_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_CONTROL,RAK_PB_PAY_TYPE_CONTROL_DFU);
        return EMOS_CLI_WAIT;
    }

    com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROBE_DFU_PUSH, (uint8_t *)&probe_id, 1);
    return EMOS_OK;
}

static uint8_t command_atc_probe_del(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    uint32_t probe_id = (uint32_t)argv[0][0];

    ret = com_get_probe(probe_id,&probe_info);
    if( ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }
    
    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->set_prbdel(probe_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_CONTROL,RAK_PB_PAY_TYPE_CONTROL_REMOVE);
        return EMOS_CLI_WAIT;
    }

    com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROBE_DELETE_DONE, (uint8_t *)&probe_id, 1);
    return EMOS_OK;
}

static uint8_t command_atc_probe_reboot(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    uint32_t probe_id = (uint32_t)argv[0][0];

    ret = com_get_probe(probe_id,&probe_info);
    if( ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }
    
    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->set_prbreboot(probe_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_CONTROL,RAK_PB_PAY_TYPE_CONTROL_REBOOT);
        return EMOS_CLI_WAIT;
    }
    
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROBE_REBOOT_DONE, (uint8_t *)&probe_id, 1);
    return EMOS_OK;
}

static uint8_t command_atc_sensor_cnt(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    uint32_t probe_id = (uint32_t)argv[0][0];
    
    ret = com_get_probe(probe_id,&probe_info);
    if( ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }
    
    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->get_snsrcnt(probe_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMGET,RAK_PB_PAY_TYPE_PARAM_SNSR_CNT);
        return EMOS_CLI_WAIT;
    }

    uint8_t *retdata = param->retdata;
    uint16_t retdata_len = param->retdata_len;
    uint16_t string_len = retdata_len * 4 + 1;
    char * this_stirng = com_malloc(string_len);
    
    UNUSED(retdata);
    UNUSED(retdata_len);

    for (uint8_t i = 0; i < retdata_len; i++)
    {
        uint8_t tmp_string[8];
        snprintf((void *)tmp_string, sizeof(tmp_string), ":%d", retdata[i]);
        strcat((void *)this_stirng, (void *)tmp_string);
    }
    
    CLI_TEXT("%d:%d%s",probe_id,retdata_len,this_stirng);
    com_free(this_stirng);
    return ret;
}

static uint8_t command_atc_sensor_intv(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t snsr_id = (uint32_t)argv[1][0];
    uint32_t *snsr_intv = (uint32_t *)&argv[2][0];

    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    if ( param->flag.is_afrwait == IDISABLE)
    {
        
        if( param->flag.is_qust )
        {
            com.rpcl->get_snsrintv(probe_id, snsr_id);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMGET,RAK_PB_PAY_TYPE_PARAM_SNSR_INTV);
        }
        else
        {
            com.rpcl->set_snsrintv(probe_id,snsr_id,*snsr_intv);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMSET,RAK_PB_PAY_TYPE_PARAM_SNSR_INTV);
        }
        return EMOS_CLI_WAIT;
    }

    uint8_t *retdata = param->retdata;
    uint16_t retdata_len = param->retdata_len;

    UNUSED(retdata);
    UNUSED(retdata_len);

    if( param->flag.is_qust )
    {
        do
        {
            uint32_t *this_intv = (void *)retdata;
            CLI_TEXT("%d:%d:%d",probe_id,snsr_id,*this_intv);
        } while (0);
    }

    return EMOS_OK;
}


static uint8_t command_atc_sensor_rule(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;

    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t snsr_id  = (uint32_t)argv[1][0];
    uint16_t *snsr_rule = (uint16_t *)&argv[2][0];

    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    if ( param->flag.is_afrwait == IDISABLE)
    {
        
        if( param->flag.is_qust )
        {
            com.rpcl->get_snsrrule(probe_id, snsr_id);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMGET,RAK_PB_PAY_TYPE_PARAM_SNSR_RULE);
        }
        else
        {
            com.rpcl->set_snsrrule(probe_id,snsr_id,*snsr_rule);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMSET,RAK_PB_PAY_TYPE_PARAM_SNSR_RULE);
        }
        return EMOS_CLI_WAIT;
    }

    uint8_t *retdata = param->retdata;
    uint16_t retdata_len = param->retdata_len;

    UNUSED(retdata);
    UNUSED(retdata_len);

    if( param->flag.is_qust )
    {
        do
        {
            uint16_t *this_rule = (void *)retdata;
            CLI_TEXT("%d:%d:%d",probe_id,snsr_id,*this_rule);
        } while (0);
    }
    
    return EMOS_OK;
}

static uint8_t command_atc_sensor_data(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;

    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t snsr_id = (uint32_t)argv[1][0];
    
    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }
    
    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->get_snsrdata(probe_id, snsr_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMGET,RAK_PB_PAY_TYPE_PARAM_SNSR_DATA);
        return EMOS_CLI_WAIT;
    }

    uint8_t *retdata = param->retdata;
    uint16_t retdata_len = param->retdata_len;

    UNUSED(retdata);
    UNUSED(retdata_len);

    com_if_rak_probe_sensor_data_t * pdata = (com_if_rak_probe_sensor_data_t *)param->retdata;

    char * this_stirng = com_malloc(pdata->len + 1);
    for (uint8_t i = 0; i < pdata->len; i++)
    {
        uint8_t tmp_string[8];
        snprintf((void *)tmp_string, sizeof(tmp_string), "%02x", pdata->value[i]);
        strcat((void *)this_stirng, (void *)tmp_string);
    }

    CLI_TEXT("%d:%d:%02x:%s",probe_id,snsr_id,pdata->type,this_stirng);
    com_free(this_stirng);
    return ret;
}

static uint8_t command_atc_sensor_hthr(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;

    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t snsr_id = (uint32_t)argv[1][0];
    uint32_t *thr  = (uint32_t*)&argv[2][0];

    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    
    if ( param->flag.is_afrwait == IDISABLE)
    {
        
        if( param->flag.is_qust )
        {
            com.rpcl->get_snsrhthr(probe_id, snsr_id);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMGET,RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR);
        }
        else
        {
            com.rpcl->set_snsrhthr(probe_id,snsr_id,(uint8_t *)thr,sizeof(uint32_t));
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMSET,RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR);
        }
        return EMOS_CLI_WAIT;
    }


    uint8_t *retdata = param->retdata;
    uint16_t retdata_len = param->retdata_len;

    UNUSED(retdata);
    UNUSED(retdata_len);

    if( param->flag.is_qust )
    {
        int32_t *cur_thr = (void *)retdata;
        CLI_TEXT("%d:%d:%d",probe_id,snsr_id,*cur_thr);
    }
    return EMOS_OK;
}

static uint8_t command_atc_sensor_lthr(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;

    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t snsr_id = (uint32_t)argv[1][0];
    uint32_t *thr  = (uint32_t*)&argv[2][0];

    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    
    if ( param->flag.is_afrwait == IDISABLE)
    {
        
        if( param->flag.is_qust )
        {
            com.rpcl->get_snsrlthr(probe_id, snsr_id);
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMGET,RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR);
        }
        else
        {
            com.rpcl->set_snsrlthr(probe_id,snsr_id,(uint8_t *)thr,sizeof(uint32_t));
            param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMSET,RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR);
        }
        return EMOS_CLI_WAIT;
    }

    uint8_t *retdata = param->retdata;
    uint16_t retdata_len = param->retdata_len;

    UNUSED(retdata);
    UNUSED(retdata_len);

    if( param->flag.is_qust )
    {
        int32_t *cur_thr = (void *)retdata;
        CLI_TEXT("%d:%d:%d",probe_id,snsr_id,*cur_thr);
    }
    return EMOS_OK;
}

static uint8_t command_atc_sensor_info(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t snsr_id = (uint32_t)argv[1][0];
    
    ret = com_get_probe(probe_id,&probe_info);
    if( ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    if( param->flag.is_qust == IDISABLE)
    {
        return EMOS_PARAM_ERROR;
    }
    
    if ( param->flag.is_afrwait == IDISABLE)
    {
        
        com.rpcl->get_snsrinfo(probe_id, snsr_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_PARAMGET,RAK_PB_PAY_TYPE_PARAM_SNSR_INFO);
    
        return EMOS_CLI_WAIT;
    }
    
    uint8_t *retdata = param->retdata;
    uint16_t retdata_len = param->retdata_len;

    UNUSED(retdata);
    UNUSED(retdata_len);

    uint8_t *this_info = retdata;
    CLI_TEXT("%d:%d:%02x",probe_id,snsr_id,*this_info);

    return EMOS_OK;
}


#ifdef EMOS_SUPPORT_RAKSNSR_ADVC
#include "../../inc/inc_co_rak_sensor_advc.h"

static uint8_t command_atc_scmd_add(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    
    uint32_t probe_id = (uint32_t)argv[0][0];
    char *c_iotype = argv[1];
    uint8_t i_iotype = 0;
    char *c_cmdtype = argv[2];
    uint8_t i_cmdtype = 0;
    char *c_intype = argv[3];
    uint8_t i_intype = 0;
    char *c_cmd = argv[4];
    uint8_t cnt;

    if ( argc != 5 )
    {
        return EMOS_PARAM_ERROR;
    }
    
    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    cnt = ioctrler_iotype_tbl_cnt;
    for (size_t i = 0; i < cnt; i++,ret=EMOS_PARAM_ERROR )
    {
        if (emos_strcmp(ioctrler_iotype_tbl[i].acsii,c_iotype, 0) == EMOS_OK)
        {
            i_iotype = ioctrler_iotype_tbl[i].id;
            ret = EMOS_OK;
            break;
        }
    }
    
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    cnt = ioctrler_cmdtype_tbl_cnt;
    for (size_t i = 0; i < cnt; i++,ret=EMOS_PARAM_ERROR )
    {
        if (emos_strcmp(ioctrler_cmdtype_tbl[i].acsii,c_cmdtype, 0) == EMOS_OK)
        {
            i_cmdtype = ioctrler_cmdtype_tbl[i].id;
            ret = EMOS_OK;
            break;
        }
    }
    
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    #define SELECT_STR  0
    #define SELECT_HEX  1
    const char * input_type[] = 
    {
        [0] = "STR", 
        [1] = "HEX",
    };

    for (size_t i = 0; i < COUNT_OF(input_type); i++,ret=EMOS_PARAM_ERROR )
    {
        if (emos_strcmp(c_intype,input_type[i], 0) == EMOS_OK)
        {
            i_intype = i;
            ret = EMOS_OK;
            break;
        }
    }

    if ( param->flag.is_afrwait == IDISABLE)
    {
        uint16_t tlen = 0;// = (strlen(c_cmd) + 1)/2;
        uint8_t tmp_buf_len = sizeof(i_iotype) + sizeof(i_cmdtype) + 1 /*c_cmd len*/;
        uint8_t tmp_buf[512];
        uint8_t idx = 0;
        tmp_buf[idx++] = i_cmdtype;
        tmp_buf[idx++] = i_iotype;
        
        switch (i_intype)
        {
            default:
            case SELECT_STR:
                tlen = strlen(c_cmd);
                memcpy(&tmp_buf[3],c_cmd,strlen(c_cmd));
                break;
            case SELECT_HEX:
                tlen = (strlen(c_cmd) + 1)/2;
                emos.units->atoh((char *)&tmp_buf[3],c_cmd,strlen(c_cmd));
                break;
        }
        tmp_buf[2] = tlen;
        tmp_buf_len += tlen;
        com.rpcl->set_scmd_add(probe_id,tmp_buf,tmp_buf_len);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_SCMD,RAK_PB_PAY_TYPE_SCMD_ADD);
        return EMOS_CLI_WAIT;
    }

    return EMOS_OK;
}


static uint8_t command_atc_scmd_del(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    
    uint32_t probe_id = (uint32_t)argv[0][0];
    
    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->set_scmd_del(probe_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_SCMD,RAK_PB_PAY_TYPE_SCMD_DEL);
        return EMOS_CLI_WAIT;
    }

    return EMOS_OK;
}

static uint8_t command_atc_scmd_list(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    
    uint32_t probe_id = (uint32_t)argv[0][0];
    
    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->get_scmd_list(probe_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_SCMD,RAK_PB_PAY_TYPE_SCMD_LIST);
        return EMOS_CLI_WAIT;
    }

    return EMOS_OK;
}

static uint8_t command_atc_scmd_rest(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    
    uint32_t probe_id = (uint32_t)argv[0][0];
    
    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->set_scmd_rest(probe_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_SCMD,RAK_PB_PAY_TYPE_SCMD_REST);
        return EMOS_CLI_WAIT;
    }

    return EMOS_OK;
}

static uint8_t command_atc_scmd_build(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_OK;
    com_if_rak_probe_param_t * probe_info;
    
    uint32_t probe_id = (uint32_t)argv[0][0];
    
    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK )
    {
        return EMOS_PARAM_ERROR;
    }

    if ( param->flag.is_afrwait == IDISABLE)
    {
        com.rpcl->snd_scmd_build(probe_id);
        param->waitfor.value = SHORT(RAK_PB_FRM_TYPE_SCMD,RAK_PB_PAY_TYPE_SCMD_BUILD);
        return EMOS_CLI_WAIT;
    }

    return EMOS_OK;
}

#endif

const cli_handle_t probe_command_tbl[] =
{
    {"PRD_INFO"   , xQST xNUL xNUL xNUL, command_atc_hub_info},
    {"IO_INFO"    , xPRB xQST xNUL xNUL, command_atc_probe_info},
    {"HUB_INFO"   , xQST xNUL xNUL xNUL, command_atc_hub_info},
    {"PRB_INFO"   , xPRB xQST xNUL xNUL, command_atc_probe_info},
    {"PRB_CNT"    , xQST xNUL xNUL xNUL, command_atc_probe_cnt},
    {"PRB_BOOT"   , xPRB xNUL xNUL xNUL, command_atc_probe_reboot},
    {"PRB_DEL"    , xPRB xNUL xNUL xNUL, command_atc_probe_del},
    {"PRB_DFU"    , xPRB xNUL xNUL xNUL, command_atc_probe_dfu},
    {"PRB_STATE"  , xPRB xQST xNUL xNUL, command_atc_probe_state},
    {"PRB_INTV"   , xPRB xQST xINT xNUL, command_atc_probe_intv},
    {"PRB_SMPL"   , xPRB xQST xINT xNUL, command_atc_probe_sampling},
    {"PRB_REST"   , xPRB xNUL xNUL xNUL, command_atc_probe_rest},
    {"PRB_ONOF"   , xBLN xNUL xNUL xNUL, command_atc_probe_onof},
    {"SNSR_CNT"   , xPRB xQST xNUL xNUL, command_atc_sensor_cnt},
    {"SNSR_INFO"  , xPRB xSNR xQST xNUL, command_atc_sensor_info},
    {"SNSR_INTV"  , xPRB xSNR xQST xINT, command_atc_sensor_intv},
    {"SNSR_RULE"  , xPRB xSNR xQST xINT, command_atc_sensor_rule},
    {"SNSR_DATA"  , xPRB xSNR xQST xNUL, command_atc_sensor_data},
    {"SNSR_HTHR"  , xPRB xSNR xQST xINT, command_atc_sensor_hthr},
    {"SNSR_LTHR"  , xPRB xSNR xQST xINT, command_atc_sensor_lthr},
#ifdef EMOS_SUPPORT_RAKSNSR_ADVC
    {"SCMD_ADD"   , xPRB xSTR xSTR xSTR xSTR, command_atc_scmd_add},
    {"SCMD_DEL"   , xPRB xNUL xNUL xNUL, command_atc_scmd_del},
    {"SCMD_LIST"  , xPRB xNUL xNUL xNUL, command_atc_scmd_list},
    {"SCMD_REST"  , xPRB xNUL xNUL xNUL, command_atc_scmd_rest},
    {"SCMD_BULD"  , xPRB xNUL xNUL xNUL, command_atc_scmd_build},
#endif
    {NULL},
};
