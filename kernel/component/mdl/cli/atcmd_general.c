#include "mdl_co_cli.h"

static uint8_t command_atc_test( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint32_t *snsr_id = (uint32_t*)&argv[1][0];
    uint32_t *snsr_intv = (uint32_t *)&argv[2][0];

    UNUSED(snsr_id);
    UNUSED(snsr_intv);

    return EMOS_OK;
}

static uint8_t command_atc_status( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    
    // com_if_vsys_mydev_t * mydev = com_mydev();
    SYS_INFO("psm:[%02x]%02x\r\n",emos.vsys->ctrl->psm_def, emos.vsys->ctrl->psm_en);

    return EMOS_OK;
}

static uint8_t command_sleep_ctrl( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    int32_t act = (int16_t)argv[0][0];

    emos.vsys->ctrl->psm_def = act;
    emos.vsys->ctrl->psm_en = act;
    
    return EMOS_OK;
}

static uint8_t command_gpio_ctrl( com_mdl_rak_cli_param_t *param , uint8_t argc, char **argv)
{
    uint32_t dir;

    int32_t io = (int16_t)argv[0][0];
    int32_t act = (int16_t)argv[1][0];
    

    dir = io / 100;
    io  = io % 100;

    emos.gpio->o_dir_output(dir,io);
    if (act > 0)
    {
        emos.gpio->o_phigh(dir,io); // 1.01
    }
    else
    {
        emos.gpio->o_plow(dir,io); // 1.01
    }

    return EMOS_OK;
}

static uint8_t command_atc_reset(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com.flashmap->rest_rdy();
    return EMOS_OK;
}

static uint8_t command_atc_testmode(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    int32_t act = (int16_t)argv[0][0];
    uint8_t log_level = act;
    emos.log->ctrl->level = log_level;
    return EMOS_OK;
}

static uint8_t command_atc_debug(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    int32_t act = (int16_t)argv[0][0];
    uint8_t log_level = act;

    emos.log->ctrl->level = log_level;
    return EMOS_OK;
}

static uint8_t command_atc_reboot(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_REBOOT, NULL, 0);
    return EMOS_OK;
}

static uint8_t command_echo(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint32_t probe_id = (uint32_t)argv[0][0];

    if (probe_id == com_mydev()->id)
    {
        CLI_TEXT("hi,echo");
    }
    
    return EMOS_NONE_OK;
}

const cli_handle_t general_command_tbl[] =
{
    {"STATUS"     , xBLN xQST xNUL xNUL, command_atc_status},
    {"TEST"       , xSTR xINT xNUL xNUL, command_atc_test},
    {"SLEEP"      , xBLN xNUL xNUL xNUL, command_sleep_ctrl},
    {"GPIO"       , xINT xBLN xNUL xNUL, command_gpio_ctrl},
    {"REBOOT"     , xBLN xNUL xNUL xNUL, command_atc_reboot},
    {"ECHO"       , xPRB xNUL xNUL xNUL, command_echo},
    {"RESET"      , xBLN xNUL xNUL xNUL, command_atc_reset},
    {"TSMODE"     , xINT xNUL xNUL xNUL, command_atc_testmode},
    {"DEBUG"      , xINT xNUL xNUL xNUL, command_atc_debug},
    {NULL},
};
