#include "mdl_co_cli.h"



static uint8_t command_atc_lora_count(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    if( param->flag.is_qust )
    {
        CLI_TEXT("%d",emos.lorawan->ctrl->send_cnt);
    }
    else
    {
        emos.lorawan->ctrl->send_cnt = 0;
    }

    return EMOS_OK;
}


static uint8_t command_atc_lora_test(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint32_t *config  = (uint32_t*)&argv[0][0];
    DBG_INFO("+EVT: LoRa test frequency: %d\r\n",*config);
    emos.lora->test(*config);
    return EMOS_OK;
}

static uint8_t command_atc_lora_reset(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    emos.lora->reset();

    return EMOS_OK;
}

static uint8_t command_atc_lora_kreset(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    emos.lora->kreset();

    return EMOS_OK;
}

static uint8_t command_atc_appeui(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_FORMAT_ERROR;
    
    if( param->flag.is_qust )
    {
        char appeui[8];
        emos.lora->get_appeui((void *)appeui);
        CLI_TEXT("%02X%02X%02X%02X%02X%02X%02X%02X",appeui[0],appeui[1],appeui[2],appeui[3],appeui[4],appeui[5],appeui[6],appeui[7] );
        ret = EMOS_OK;
    }
    else
    {
        uint16_t str_len = strlen(argv[0]);
        if( str_len != 16)
        {
            return EMOS_PARAM_ERROR;
        }

        uint8_t tmp[8];
        emos_atoh((void *)tmp,(void *)argv[0],sizeof(tmp));
        ret = emos.lora->set_appeui(tmp);//com_mdl_set_appeui(tmp);
    }

    return ret;
}

static uint8_t command_atc_band(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    
    uint8_t new_band = (uint32_t)argv[0][0];
    uint8_t ret = EMOS_FORMAT_ERROR;
    int16_t devband = 0;
    if( param->flag.is_qust )
    {
        devband = emos.lora->get_band();
        CLI_TEXT("%d",devband );
        ret = EMOS_OK;
    }
    else
    {
        ret = emos.lora->set_band(new_band);
        if (ret == false)
        {
            return EMOS_PARAM_ERROR;
        }
    
        ret = EMOS_OK;
    }
    return ret;
}

static uint8_t command_atc_deveui(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_FORMAT_ERROR;

    if( param->flag.is_qust )
    {
        char deveui[8];
        emos.lora->get_appeui((void *)deveui);
        CLI_TEXT("%s=%02X%02X%02X%02X%02X%02X%02X%02X",deveui[0],deveui[1],deveui[2],deveui[3],deveui[4],deveui[5],deveui[6],deveui[7] );
        ret = EMOS_OK;
    }
    else
    {
        uint16_t str_len = strlen(argv[0]);
        if( str_len != 16)
        {
            return EMOS_PARAM_ERROR;
        }
        
        uint8_t tmp[8];
        emos_atoh((void *)tmp,(void *)argv[0],sizeof(tmp));
        ret = emos.lora->set_deveui(tmp);//com_mdl_set_appeui(tmp);
    }
    return ret;
}

static uint8_t command_atc_appkey(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_FORMAT_ERROR;

    if( param->flag.is_qust )
    {
        uint8_t appeky[16];// = com_mdl_get_appkey();
        emos.lora->get_appkey(appeky);
        CLI_TEXT("%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                                             appeky[0],appeky[1],appeky[2],appeky[3],appeky[4],appeky[5],appeky[6],appeky[7]
                                             ,appeky[8],appeky[9],appeky[10],appeky[11],appeky[12],appeky[13],appeky[14],appeky[15]  );
        ret = EMOS_OK;
    }
    else
    {
        
        uint16_t str_len = strlen(argv[0]);
        if( str_len != 32)
        {
            return EMOS_PARAM_ERROR;
        }
        uint8_t tmp[16];
        emos_atoh((void *)tmp,(void *)argv[0],sizeof(tmp));
        ret = emos.lora->set_appkey(tmp);//com_mdl_set_appeui(tmp);
    }
    return ret;
}


const cli_handle_t lora_command_tbl[] =
{
    {"LORA_CNT"   , xBLN xQST xNUL xNUL, command_atc_lora_count},
    {"LORA_RESET" , xBLN xNUL xNUL xNUL, command_atc_lora_reset},
    {"LORA_KRESET", xBLN xNUL xNUL xNUL, command_atc_lora_kreset},
    {"LORA_TEST"  , xINT xNUL xNUL xNUL, command_atc_lora_test},
    {"APPEUI"     , xQST xSTR xNUL xNUL, command_atc_appeui},
    {"APPKEY"     , xQST xSTR xNUL xNUL, command_atc_appkey},
    {"DEVEUI"     , xQST xSTR xNUL xNUL, command_atc_deveui},
    {"BAND"       , xINT xQST xNUL xNUL, command_atc_band},
    {NULL},
};

