#include "mdl_co_cli.h"

static uint8_t command_atc_btadv( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{

    char *getstr = argv[0];

    if (strlen(getstr) > 10)
    {
        return EMOS_PARAM_ERROR;
    }
    
    emos.ble->adv_name(getstr,strlen(getstr));
    emos.ble->adv_start();
    return EMOS_OK;
}

static uint8_t command_atc_btmac( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    CLI_TEXT("%s",emos.ble->getmac());
    return EMOS_OK;
}

static uint8_t command_atc_ble_test(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    emos.ble->test();
    return EMOS_OK;
}

const cli_handle_t ble_command_tbl[] =
{
    {"BTADV"      , xSTR xNUL xNUL xNUL, command_atc_btadv},
    {"BTMAC"      , xQST xNUL xNUL xNUL, command_atc_btmac},
    {"BLE_TEST"   , xQST xNUL xNUL xNUL, command_atc_ble_test},
    {NULL},
};

