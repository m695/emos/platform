#ifndef __COM_INC_UNITS_H__
#define __COM_INC_UNITS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../../../kernel.h" 
#include "../../inc/inc_co_rak_cli.h" 
#include "../../inc/inc_co_rak_iocontrol.h" 
#include "../../inc/inc_co_rak_wire_protocol.h"

typedef com_mdl_rak_cli_handle_t cli_handle_t;

#ifdef __cplusplus
}
#endif

#endif
