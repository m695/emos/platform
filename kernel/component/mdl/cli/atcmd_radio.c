#include "mdl_co_cli.h"

static uint8_t command_atc_lpwan_mode_switch(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{

    uint8_t ret = EMOS_PARAM_ERROR;

    com_if_rak_bank0_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank0_t));
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    if( param->flag.is_qust )
    {
        CLI_TEXT("%d", eee.lpwan_mode);

        ret = EMOS_OK;
    }
    else
    {
        uint8_t mode = argv[0][0];
        if(mode == 2 && com.nbiot->ctrl->nbt_en != 1)
        {
            return EMOS_MODE_NO_SUPPORT; 
        }
        eee.lpwan_mode = (uint8_t)mode;
        com_flash_bank0_write((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
        com.radio->lpwan(mode);
        ret = EMOS_OK;
    }

    return ret;

}

const cli_handle_t radio_command_tbl[] =
{
    {"LPWAN"      , xINT xQST xNUL xNUL, command_atc_lpwan_mode_switch},
    {NULL},
};
