#include "mdl_co_cli.h"

static uint8_t command_tsin_serialnumber( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_FORMAT_ERROR;
    com_if_rak_bank0_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank0_t));
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    if ( param->flag.is_qust )
    {
        CLI_TEXT("%.8s%c%.2s%.2s%.5s",eee.int_srnum.bom,eee.int_srnum.fac,eee.int_srnum.year,eee.int_srnum.month,eee.int_srnum.number);
        ret = EMOS_OK;
    }
    else
    {
        uint16_t str_len = strlen(argv[0]);
        if( str_len != (sizeof(eee.int_srnum) + 1))
        {
            goto FUNC_EXIT;
        }
        
        if (argv[0][str_len - 1] == '!')
        {
            memcpy((uint8_t *)&(eee.int_srnum), argv[0], sizeof(eee.int_srnum));
            com_flash_bank0_write((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
            ret = EMOS_OK;
        }
    }
    

FUNC_EXIT:
    return ret;
}

static uint8_t command_tsin_shortserialnumber( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_FORMAT_ERROR;
    com_if_rak_bank0_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank0_t));
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%02x%02x%x",eee.ext_srnum.value[0] ,eee.ext_srnum.value[1] ,eee.ext_srnum.value[2]);
        ret = EMOS_OK;
    }
    else
    {
        uint16_t str_len = strlen(argv[0]);
        if( str_len != 5+1)
        {
            goto FUNC_EXIT;
        }
        
        if (argv[0][str_len - 1] == '!')
        {
            uint8_t tmp[3];
            emos_atoh((char *)tmp,(char *)argv[0],sizeof(tmp));
            tmp[2] = ((tmp[2]>>4) & 0x0F);
            memcpy(eee.ext_srnum.value,tmp,3);
            com_flash_bank0_write((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
            ret = EMOS_OK;
        }
    }
    

FUNC_EXIT:
    return ret;
}

static uint8_t command_tsin_hwver( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_FORMAT_ERROR;
    com_if_rak_bank0_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank0_t));
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%c",eee.hw_version);
        
        ret = EMOS_OK;
    }
    else
    {
        uint16_t str_len = strlen(argv[0]);
        if( str_len != sizeof(eee.hw_version)+1)
        {
            goto FUNC_EXIT;
        }
        
        if (argv[0][str_len - 1] == '!')
        {
            eee.hw_version = argv[0][0];
            com_flash_bank0_write((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
            ret = EMOS_OK;
        }
    }
    
FUNC_EXIT:
    return ret;
}

static uint8_t command_tsin_devtype( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret = EMOS_FORMAT_ERROR;
    com_if_rak_bank0_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank0_t));
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    if (param->flag.is_qust)
    {
        CLI_TEXT("%.4s",(char *)(eee.type_id.byte));
        ret = EMOS_OK;
    }
    else
    {
        uint16_t str_len = strlen(argv[0]);
        if( (str_len > sizeof(eee.type_id) + 1/*"!"*/) || (str_len < 1/*"!"*/))
        {
            goto FUNC_EXIT;
        }
        
        if (argv[0][str_len - 1] == '!')
        {
            for (uint8_t i = 0; i < sizeof(eee.type_id.byte); i++)
            {
                if ( i < (str_len - 1) )
                {
                    eee.type_id.byte[i] = argv[0][i];
                }
                else
                {
                    eee.type_id.byte[i] = '\0';
                }
            }
        }
        else
        {
            goto FUNC_EXIT;
        }

        com_flash_bank0_write((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
        ret = EMOS_OK;
    }
    
FUNC_EXIT:
    return ret;
}

static uint8_t command_tsin_command( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
#ifdef EMOS_SUPPORT_TESTER
    // DBG_INFO("%s=%s\r\n",header,argv[0]);
    CLI_TEXT("%s",argv[0]);
#else
    // com.rpcl->prfrsp("%s=%s\r\n",header,argv[0]);
    CLI_TEXT("%s",argv[0]);
#endif
    return EMOS_OK;
}

static uint8_t command_tsin_checktype( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com_if_rak_bank0_t eee;
    UNUSED(eee);
    com_if_vsys_mydev_t * mydev = com_mydev();
    UNUSED(mydev);

#ifndef EMOS_SUPPORT_WIRE_HOST
    char *result = "FAIL";
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    uint32_t check_type_id = 0;
    uint8_t id[5]= {0};
    uint8_t eeeid[5] = {0};
    check_type_id = mydev->type_id.current.value;
    uint8_t *pp1 = (uint8_t *)&(eee.type_id.value);
    uint8_t *pp2 = (uint8_t *)&check_type_id;
    memcpy(id, pp2, 4);
    memcpy(eeeid, pp1, 4);
    if ((eee.type_id.value == check_type_id) || U32T_SWAP(eee.type_id.value) == check_type_id)
    {
        result = "PASS";
    }
    CLI_TEXT("%s:%s\r\n+EVT:TEST %s",eeeid,id,result);
    
    
    return EMOS_OK;
#else
    return EMOS_FORMAT_ERROR;
#endif
}

static uint8_t command_atc_dut_info(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
#ifdef EMOS_SUPPORT_WIRE_HOST
    com_if_vsys_mydev_t * mydev = com_mydev();
    char hw_version = (char )mydev->hw_version;
    uint8_t sw_version0 = mydev->sw_version[0];
    uint8_t sw_version1 = mydev->sw_version[1];
    uint8_t sw_version2 = mydev->sw_version[2];
    uint8_t deveui[8];
    
    emos.lora->get_deveui(deveui);
    char *model = (char *)mydev->model_name;
    uint8_t *ssn = (uint8_t *)mydev->ssn.value;
    UNUSED(ssn);
    uint8_t *sn = (uint8_t *)mydev->sn.value;
    char type[5];
    uint8_t j=0;
    for (uint8_t i = 0; i < (sizeof(uint32_t)); i++)
    {
        if (com_iprobe->type_id.current.bytes[i] != 0)
        {
            type[j] = com_iprobe->type_id.current.bytes[i];//mydev->type.bytes[i];
            j++;
        }
    }
    type[j] = '\0';
    CLI_TEXT("V%c:V%d.%d.%d:%02x%02x%02x%02x%02x%02x%02x%02x:%s:%.18s:%.4s",
                            hw_version,sw_version0,sw_version1,sw_version2,
                            deveui[0],deveui[1],deveui[2],deveui[3],deveui[4],deveui[5],deveui[6],deveui[7],
                            model,sn,type);
#endif


#ifdef EMOS_SUPPORT_SLAVE
    com_if_vsys_mydev_t * mydev = com_mydev();
    char hw_version = (char )mydev->hw_version;
    uint8_t sw_version0 = mydev->sw_version[0];
    uint8_t sw_version1 = mydev->sw_version[1];
    uint8_t sw_version2 = mydev->sw_version[2];
    char *model = (char *)mydev->model_name;

    uint8_t *ssn = (uint8_t *)mydev->ssn.value;
    uint8_t *sn = (uint8_t *)mydev->sn.value;
    char type[3];
    uint8_t j=0;
    for (uint8_t i = 0; i < (sizeof(type) - 1); i++)
    {
        if (mydev->type_id.current.bytes[i] != 0)
        {
            type[j] = mydev->type_id.current.bytes[i];
            j++;
        }
    }
    type[j] = '\0';
    CLI_TEXT("V%c:V%d.%d.%d:%s:%.18s:%02x%02x%x:%.2s",
                            hw_version,sw_version0,sw_version1,sw_version2,
                            model,sn,ssn[0],ssn[1],ssn[2]&0xf,type);
#endif
    return EMOS_OK;
}

const cli_handle_t factory_command_tbl[] =
{
    {"TSIN_SN"    , xQST xSTR xNUL xNUL, command_tsin_serialnumber},
    {"TSIN_SSN"   , xQST xSTR xNUL xNUL, command_tsin_shortserialnumber},
    {"TSIN_HWVER" , xQST xSTR xNUL xNUL, command_tsin_hwver},
    {"TSIN_DTYPE" , xQST xSTR xNUL xNUL, command_tsin_devtype},
    {"TSIN_CKTYPE", xQST xNUL xNUL xNUL, command_tsin_checktype},
    {"TSIN_CMD"   , xSTR xNUL xNUL xNUL, command_tsin_command},
    {"DUT_INFO"   , xQST xNUL xNUL xNUL, command_atc_dut_info},
    {NULL},
};

