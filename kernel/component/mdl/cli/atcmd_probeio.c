#include "mdl_co_cli.h"

#if 0
static uint8_t command_atc_io_rule(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret;
    com_if_rak_probe_param_t * probe_info;
    com_if_rak_probe_sensor_t * sensor_info;

    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    uint32_t snsr_id  = (uint32_t)argv[2][0];
    uint32_t snsr_rule = (uint32_t)argv[3][0];

    uint8_t  snsr_base = 0;
    uint8_t iface = IOC_NO_IFACE;

    // com.rak_ioc->io_idname(&iface, (uint8_t *)interface);
    // com.rak_ioc->get_iobase(probe_id, iface, &snsr_base);
    snsr_base = snsr_base + snsr_id -1;
    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK)
    {
        return EMOS_PARAM_ERROR;
    }

    ret = com_get_snsr(probe_id,snsr_base, &sensor_info);
    if ( ret )
    {
        return EMOS_PARAM_ERROR;
    }

    if( param->flag.is_qust )
    {
        CLI_TEXT("%d:%s:%d:%d",probe_id,interface,snsr_id,sensor_info->ctrl.rule);
    }
    else
    {
        do
        {
            if (sensor_info->ctrl.rule == snsr_rule)
            {
                break;
            }

            sensor_info->update.rule = 1;
            sensor_info->ctrl.rule = snsr_rule;
        } while (0);
    }
    return EMOS_OK;
}
static uint8_t command_atc_io_hthr(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret;
    com_if_rak_probe_param_t * probe_info;
    com_if_rak_probe_sensor_t * sensor_info;

    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    uint32_t snsr_id  = (uint32_t)argv[2][0];
    uint32_t *thr  = (uint32_t*)&argv[3][0];
    // uint16_t snsr_data_size = 0;

    uint8_t  snsr_base = 0;
    uint8_t iface = IOC_NO_IFACE;

    // com.rak_ioc->io_idname(&iface, (uint8_t *)interface);
    // com.rak_ioc->get_iobase(probe_id, iface, &snsr_base);
    snsr_base = snsr_base + snsr_id -1;
    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK)
    {
        return EMOS_PARAM_ERROR;
    }

    ret = com_get_snsr(probe_id,snsr_base, &sensor_info);
    if ( ret )
    {
        return EMOS_PARAM_ERROR;
    }

    if( param->flag.is_qust )
    {
        int32_t *thr = (int32_t *)sensor_info->thr.above;
        CLI_TEXT("%d:%s:%d:%d",probe_id,interface,snsr_id,*thr);
    }
    else
    {
        do
        {
            sensor_info->update.thr_above = 1;
            memcpy(sensor_info->thr.above, thr, sizeof(uint32_t));
        } while (0);
    }
    return EMOS_OK;
}

static uint8_t command_atc_io_lthr(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
     uint8_t ret;
    com_if_rak_probe_param_t * probe_info;
    com_if_rak_probe_sensor_t * sensor_info;

    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    uint32_t snsr_id  = (uint32_t)argv[2][0];
    uint32_t *thr  = (uint32_t*)&argv[3][0];
    // uint16_t snsr_data_size = 0;

    uint8_t  snsr_base = 0;
    uint8_t iface = IOC_NO_IFACE;

    // com.rak_ioc->io_idname(&iface, (uint8_t *)interface);
    // com.rak_ioc->get_iobase(probe_id, iface, &snsr_base);
    snsr_base = snsr_base + snsr_id -1;
    ret = com_get_probe(probe_id,&probe_info);
    if (ret != EMOS_OK)
    {
        return EMOS_PARAM_ERROR;
    }

    ret = com_get_snsr(probe_id,snsr_base, &sensor_info);
    if ( ret )
    {
        return EMOS_PARAM_ERROR;
    }

    if( param->flag.is_qust )
    {
        int32_t *thr = (int32_t *)sensor_info->thr.below;
        CLI_TEXT("%d:%s:%d:%d",probe_id,interface,snsr_id,*thr);
    }
    else
    {
        do
        {
            sensor_info->update.thr_below = 1;
            memcpy(sensor_info->thr.below, thr, sizeof(uint32_t));
        } while (0);
    }
    return EMOS_OK;
}
static uint8_t command_io_data(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret;
    // rak_ioc_data_frame_t io_data;
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    uint32_t channel = (uint32_t)argv[2][0];
    uint32_t value = (uint32_t)argv[3][0];
    UNUSED(value);
    uint8_t iface = IOC_NO_IFACE;
    // com.rak_ioc->io_idname(&iface, (uint8_t *)interface);
    if((iface > IOC_UART_END) && (param->flag.is_qust))
    {
        // com_if_rak_probe_param_t * probe_info;
        com_if_rak_probe_sensor_t * snsr_info;
        uint8_t  snsr_base = 0;
        // com.rak_ioc->get_iobase(probe_id, iface, &snsr_base);
        
        snsr_base = snsr_base + channel -1;
        ret = com_get_snsr(probe_id,snsr_base, &snsr_info);
        if( ret )
        {
            return EMOS_PARAM_ERROR;
        }
        // ret = com.rpcl->snddata(probe_id, &snsr_base,NULL,0);
        uint8_t *data = snsr_info->data.value;
        uint8_t data_buf[100];
        memset(data_buf , 0, sizeof(data_buf));
        for (uint8_t i = 0; i < snsr_info->data.len; i++)
        {
            snprintf(data_buf + (i*2),sizeof(data_buf),"%02x",data[ snsr_info->data.len - (i + 1) ]);
        }

        CLI_TEXT("%d:%s:%d:%02x:%s",probe_id,interface,channel,snsr_info->data.type,data_buf);
    }

    return EMOS_OK;
}
static uint8_t command_io_cfg(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret;
    rak_ioc_cfg_frame_t hconf;
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    uint32_t *baudrate = (uint32_t *)&argv[2][0];
    uint32_t *databit = (uint32_t *)&argv[3][0];
    uint32_t *stopbit = (uint32_t *)&argv[4][0];
    uint32_t *parity = (uint32_t *)&argv[5][0];
    uint32_t ibaudrate = 0;
    uint32_t idatabit = 0;
    uint32_t istopbit = 0;
    uint32_t iparity = 0;
    uint8_t iface = IOC_NO_IFACE;
    do
    {
        // com.rak_ioc->io_idname(&iface, (uint8_t*)interface);
        emos_atoi((char*)baudrate,&ibaudrate);
        emos_atoi((char*)databit,&idatabit);
        emos_atoi((char*)stopbit,&istopbit);
        emos_atoi((char*)parity,&iparity);
        if (iface < IOC_UART_END) 
        {
            hconf.cfg.baudrate = ibaudrate;
            hconf.cfg.databit = (uint8_t)idatabit;
            hconf.cfg.stopbit = (uint8_t)istopbit;
            hconf.cfg.parity = (uint8_t)iparity;

            if (param->flag.is_qust)
            {
                // com.rak_ioc->sndsioc(probe_id, (uint8_t*)&hconf, sizeof(rak_ioc_cfg_frame_t), (uint8_t*)interface, IO_CFG ,IOA_GET);
            }
            else
            {
                // com.rak_ioc->sndsioc(probe_id, (uint8_t*)&hconf, sizeof(rak_ioc_cfg_frame_t), (uint8_t*)interface, IO_CFG ,IOA_SET);
            }            
        }
        else if (iface == IOC_DO)
        {
            uint32_t channel = 0;
            uint32_t on_off = 0;
            emos_atoi((char*)baudrate,&channel);
            emos_atoi((char*)databit,&on_off);         
            do
            {
                hconf.cfg.dch = channel;
                hconf.cfg.on_off = on_off;
            } while (0);

            if (param->flag.is_qust)
            {
                // com.rak_ioc->sndsioc(probe_id, (uint8_t*)&hconf, sizeof(rak_ioc_cfg_frame_t), (uint8_t*)interface, IO_CFG ,IOA_GET);
            }
            else
            {
                // com.rak_ioc->sndsioc(probe_id, (uint8_t*)&hconf, sizeof(rak_ioc_cfg_frame_t), (uint8_t*)interface, IO_CFG ,IOA_SET);
            }
        }
        else if(iface > IOC_UART_END) 
        {            
            uint32_t channel = 0;
            uint32_t period = 0;
            uint8_t  snsr_base = 0;
            com_if_rak_probe_param_t * probe_info;
            com_if_rak_probe_sensor_t * sensor_info;
            emos_atoi((char *)baudrate,&channel);
            emos_atoi((char *)databit,&period);
            /*
            hconf.cfg.ach = channel;
            hconf.cfg.period = period;
            */
            // com.rak_ioc->get_iobase(probe_id, iface, &snsr_base);
            DBG_INFO("snsr_base %d\r\n", snsr_base);
            ret = com_get_probe(probe_id,&probe_info);
            if (ret != EMOS_OK)
            {
                return EMOS_PARAM_ERROR;
            }

            ret = com_get_snsr(probe_id,channel + snsr_base -1, &sensor_info);
            if ( ret )
            {
                return EMOS_PARAM_ERROR;
            }

            if( param->flag.is_qust )
            {
                CLI_TEXT("%d:%s:%d:%d",probe_id,interface,channel,sensor_info->intv);
            }
            else
            {
                do
                {
                    if (sensor_info->intv == period)
                    {
                        break;
                    }

                    sensor_info->update.sintv = 1;
                    sensor_info->intv = period;
                    sensor_info->current_intv = period;
                } while (0);
            }
        }
        
        

    } while (0);


    return EMOS_OK;
}


static uint8_t command_io_polltask(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    // uint8_t ret;
    uint8_t buff[255]={0};
    uint32_t bufflen=0;
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    uint32_t taskid = (uint32_t)argv[2][0];

    do
    {
        rak_ioc_polltask_frame_t * enablepoll;
        enablepoll = (rak_ioc_polltask_frame_t *)buff;
        enablepoll->taskid = taskid;
        bufflen = sizeof(rak_ioc_polltask_frame_t);
    } while (0);    
    if (param->flag.is_qust)
    {
        // com.rak_ioc->sndsioc(probe_id, buff, bufflen, (uint8_t *)interface, IO_POLLTASK ,IOA_GET);
    }
    else
    {
        // com.rak_ioc->sndsioc(probe_id, buff, bufflen, (uint8_t *)interface, IO_POLLTASK ,IOA_SET);
    }
    return EMOS_OK;
}
static uint8_t command_io_pollcnt(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    // uint8_t ret;
    uint8_t buff[255]={0};
    uint32_t bufflen=1;
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    if (param->flag.is_qust)
    {
        // com.rak_ioc->sndsioc(probe_id, buff, bufflen, (uint8_t *)interface, IO_POLLCNT ,IOA_GET);
    }

    return EMOS_OK;
}
static uint8_t command_io_rmpoll(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    // uint8_t ret;
    uint8_t buff[32]={0};
    uint32_t bufflen=1;
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    uint32_t taskid = (uint32_t)argv[2][0];

    if (param->flag.is_qust)
    {
        return EMOS_OK;
    }
    else 
    {
        buff[0] = taskid;
        // com.rak_ioc->sndsioc(probe_id, buff, bufflen, (uint8_t *)interface, IO_RMPOLL ,IOA_SET);
    }

    return EMOS_OK;
}
static uint8_t command_io_enablepoll(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    // uint8_t ret;
    uint8_t buff[255]={0};
    uint32_t bufflen=0;
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    uint32_t taskid = (uint32_t)argv[2][0];
    uint32_t enable = (uint32_t)argv[3][0];
    do
    {
        rak_ioc_enablepoll_frame_t * enablepoll;
        enablepoll = (rak_ioc_enablepoll_frame_t *)buff;
        enablepoll->taskid = taskid;
        enablepoll->enable = enable;
        bufflen = sizeof(rak_ioc_enablepoll_frame_t);
    } while (0);    
    if (param->flag.is_qust)
    {
        // com.rak_ioc->sndsioc(probe_id, buff, bufflen, (uint8_t *)interface, IO_ENABLEPOLL ,IOA_GET);
    }
    else
    {
        // com.rak_ioc->sndsioc(probe_id, buff, bufflen, (uint8_t *)interface, IO_ENABLEPOLL ,IOA_SET);
    }
    return EMOS_OK;
}

static uint8_t command_io_addpoll(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    // uint8_t ret;
    uint8_t buff[255]={0};
    uint32_t bufflen=0;
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *interface = (uint32_t*)&argv[1][0];
    uint32_t taskid = (uint32_t)argv[2][0];
    uint32_t *cmd = (uint32_t *)&argv[3][0];
    uint32_t *period = (uint32_t *)&argv[4][0];
    uint32_t *timeout = (uint32_t *)&argv[5][0];
    uint32_t retry = (uint32_t)argv[6][0];
    uint8_t iface = IOC_NO_IFACE;
    uint8_t cmdlen = 0;
    uint32_t iperiod = 0;
    uint32_t itimeout = 0;
    // uint32_t iretry = 0;
    do
    {
        rak_ioc_addpoll_frame_t * polltask;
        polltask = (rak_ioc_addpoll_frame_t *)buff;

        emos_atoi((char *)period,&iperiod);       
        emos_atoi((char *)timeout,&itimeout);
  
        polltask->taskid = taskid;
        polltask->period = iperiod;
        polltask->timeout = itimeout;
        polltask->retry = retry;
        //DBG_INFO("cmd %s, %d", cmd, strlen(cmd));
        // com.rak_ioc->io_idname(&iface, (uint8_t *)interface);
        if (iface == IOC_RS485)
        {
            emos.units->atoh(polltask->cmd, cmd, strlen(cmd));
            cmdlen = strlen((char *)cmd) /2;
        }
        else 
        {
            memcpy(polltask->cmd, cmd, strlen((char *)cmd));
            cmdlen = strlen((char *)cmd);
        }
        //memcpy(polltask->cmd, cmd, strlen(cmd));
        bufflen = sizeof(rak_ioc_addpoll_frame_t) + cmdlen;
    } while (0);    
    {
        // com.rak_ioc->sndsioc(probe_id, buff, bufflen, (uint8_t *)interface, IO_ADDPOLL ,IOA_SET);
    }
    return EMOS_OK;
}

static uint8_t command_io_passthrh(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    // uint8_t ret;
    uint8_t buff[255]={0};
    uint32_t bufflen=0;
    uint32_t probe_id = (uint32_t)argv[0][0];
    uint32_t *str_if = (uint32_t *)&argv[1][0];
    uint32_t *request = (uint32_t *)&argv[2][0];
    uint32_t *timeout = (uint32_t*)&argv[3][0];
    uint32_t itimeout = 0;
    uint8_t iface = IOC_NO_IFACE;
    uint8_t cmdlen = 0;
    do
    {
        rak_ioc_passthrh_frame_t * pass;
        pass = (rak_ioc_passthrh_frame_t *)buff;
        if (argc > 2) 
        {
            emos_atoi((char *)timeout,&itimeout);
        }
        pass->timeout = itimeout;
        // com.rak_ioc->io_idname(&iface, (uint8_t *)str_if);
        if (iface == IOC_RS485)
        {
            emos.units->atoh(pass->cmd, request, strlen(request));
            cmdlen = strlen(request) /2;
        }
        else 
        {
            memcpy(pass->cmd, request, strlen(request));
            cmdlen = strlen(request);
        }
        bufflen = sizeof(rak_ioc_passthrh_frame_t) + cmdlen;
    } while (0);    
    if (param->flag.is_qust)
    {
        // com.rak_ioc->sndsioc(probe_id, buff, bufflen, (uint8_t *)str_if, IOPASSTHRH ,IOA_GET);
    }
    else
    {
        // com.rak_ioc->sndsioc(probe_id, buff, bufflen, (uint8_t *)str_if, IOPASSTHRH ,IOA_SET);
    }
    return EMOS_OK;
}

//END  IOC

// PROBE IO INFO
static uint8_t command_atc_io_cnt(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t ret;
    com_if_rak_probe_param_t * probe_info;
    com_if_rak_probe_sensor_t * sensor_info;
    uint32_t probe_id = (uint32_t)argv[0][0];
        
    ret = com_get_probe(probe_id,&probe_info);
    if( ret != EMOS_OK)
    {
        return EMOS_PARAM_ERROR;
    }
        
    uint8_t snsr_cnt = 0;//probe_info->snsr_cnt;

    char cat_str[13];
    uint8_t sface[10];
    uint16_t tmp_len = snsr_cnt * (10 + 3) +1;
    char * tmp = com_malloc(tmp_len);
    uint8_t ipso_type=0xFF;
    uint8_t if_cnt=0;
    memset(tmp,0,tmp_len);
    if (snsr_cnt >0)
    {

        for (uint8_t i = 0; i < snsr_cnt; i++)
        {
            com_sprobe(probe_id).snsr_walk(i,&sensor_info);
            if (sensor_info->ctrl.type != ipso_type && ipso_type!=0xFF)
            {
                memset(sface, 0, sizeof(sface));
                // com.rak_ioc->get_strbyipso(ipso_type, sface);
                snprintf(cat_str, sizeof(cat_str), ":%s:%d",sface, if_cnt);
                strcat(tmp, cat_str);
                ipso_type = sensor_info->ctrl.type;
                if_cnt = 1;
            }
            else 
            {
                ipso_type = sensor_info->ctrl.type;
                if_cnt ++;
            }

        }
        // com.rak_ioc->get_strbyipso(ipso_type, sface);
        snprintf(cat_str, sizeof(cat_str), "%s:%d",sface, if_cnt);
        strcat(tmp, cat_str);
    }
    CLI_TEXT("%d%s",probe_id,tmp);
    com_free(tmp);
    return EMOS_OK;
}

#ifdef EMOS_SUPPORT_RS485
static uint8_t command_ts_rs485(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t testbuff[64] = {0};
    uint8_t testpatten[] = {0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39};
    emos.rs485(0)->write(testpatten,sizeof(testpatten));
    if (emos.rs485(0)->read(testbuff, sizeof(testbuff), 1000) > 0)
    {
        if (!memcmp(testbuff, testpatten, sizeof(testpatten)))
        {
            CLI_TEXT("PASS");
            return EMOS_OK;
        }
    }
    CLI_TEXT("FAIL");
    return EMOS_OK;
}
#endif
#ifdef EMOS_SUPPORT_SDI_12
static uint8_t command_ts_sdi12(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t testbuff[64] = {0};
    uint8_t testpatten[] = {0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39};
    emos.sdi12(0)->write(testpatten,sizeof(testpatten));
    if (emos.sdi12(0)->read(testbuff, sizeof(testbuff), 1000) > 0)
    {
        if (!memcmp(testbuff, testpatten, sizeof(testpatten)))
        {
            CLI_TEXT("PASS");
            return EMOS_OK;
        }
    }
    CLI_TEXT("FAIL");
    return EMOS_OK;
}
#endif
#ifdef EMOS_SUPPORT_RS232_IO
static uint8_t command_ts_rs232(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint8_t testbuff[64] = {0};
    uint8_t testpatten[] = {0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39};
    emos.rs232(0)->write(testpatten,sizeof(testpatten));
    if (emos.rs232(0)->read(testbuff, sizeof(testbuff), 1000) > 0)
    {
        if (!memcmp(testbuff, testpatten, sizeof(testpatten)))
        {
            CLI_TEXT("PASS");
            return EMOS_OK;
        }
    }
    CLI_TEXT("FAIL");
    return EMOS_OK;
}
#endif
#ifdef EMOS_SUPPORT_MA_METER
static uint8_t command_ts_adc(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    int32_t sum = 0;
    int32_t inum = (int16_t)argv[0][0];

    sum = emos.adc(inum).read();
    if (sum > 79)
        sum = (((sum - 78) +  (sum / 50)) * 100 * 20 / 4096);

    CLI_TEXT("%d:%d", inum, sum);

    return EMOS_OK;
}
#endif
static uint8_t command_rgpio_ctrl(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint32_t dir;
    uint8_t value = 0;
    int32_t io = (int16_t)argv[0][0];
    
    dir = io / 100;
    io  = io % 100;

    emos.gpio->o_cfg_pull(dir,io,1);
    //emos.gpio->o_dir_input(dir,io,0);
    value = emos.gpio->o_get(dir,io);
    CLI_TEXT("%d:%d",(dir*100+io), value);

    return EMOS_OK;
}

const cli_handle_t probeio_command_tbl[] =
{
    {"IO_CNT"     , xPRB xQST xNUL xNUL       , command_atc_io_cnt},
    {"IO_PASSTHRH", xPRB xSTR xQST xSTR xSTR  , command_io_passthrh},
    {"IO_ADDPOLL" , xPRB xSTR xSNR xSTR xSTR xSTR xSNR , command_io_addpoll},
    {"IO_ENABLEPOLL", xPRB xSTR xSNR xQST xSNR, command_io_enablepoll},
    {"IO_POLLTASK", xPRB xSTR xQST xSNR       , command_io_polltask},
    {"IO_CFG"     , xPRB xSTR xQST xSTR xSTR xSTR xSTR  , command_io_cfg},
    {"IO_DATA"    , xPRB xSTR xSNR xQST xSNR  , command_io_data},
    {"IO_RULE"    , xPRB xSTR xSNR xQST xINT  , command_atc_io_rule},
    {"IO_HTHR"    , xPRB xSTR xSNR xQST xINT  , command_atc_io_hthr},
    {"IO_LTHR"    , xPRB xSTR xSNR xQST xINT  , command_atc_io_lthr},
    {"IO_POLLCNT" , xPRB xSTR xQST xNUL       , command_io_pollcnt},
    {"IO_RMPOLL"  , xPRB xSTR xSNR xNUL       , command_io_rmpoll},
    
#ifdef EMOS_SUPPORT_RS485
    {"TS_RS485"   , xQST xNUL xNUL xNUL, command_ts_rs485},
#endif
#ifdef EMOS_SUPPORT_SDI_12
    {"TS_SDI12"   , xQST xNUL xNUL xNUL, command_ts_sdi12},
#endif
#ifdef EMOS_SUPPORT_RS232_IO
    {"TS_RS232"   , xQST xNUL xNUL xNUL, command_ts_rs232},
#endif
#ifdef EMOS_SUPPORT_MA_METER
    {"TS_ADC"     , xINT xQST xNUL xNUL, command_ts_adc},
#endif
    {"RGPIO"      , xINT xQST xNUL xNUL, command_rgpio_ctrl},
    {NULL},
};

#else

const cli_handle_t probeio_command_tbl[] =
{
    {NULL},
};

#endif

