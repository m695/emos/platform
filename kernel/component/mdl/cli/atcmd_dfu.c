#include "mdl_co_cli.h"

#ifdef EMOS_SUPPORT_TFILE
static uint8_t command_atc_dfufile( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint32_t *file_idx = (uint32_t*)&argv[0][0];
    uint32_t *file_count = (uint32_t *)&argv[1][0];
    
    com.flashmap->dfu_field_write(*file_idx,com.cli->ctrl->input_data,*file_count);

#if 0
    uint8_t *pdata;
    com.flashmap->dfu_field_read(*file_idx,&pdata);
    DBG_INFOA(pdata,1024);
#endif
    
    return EMOS_OK;
}
#endif

#ifdef EMOS_SUPPORT_TFILE
static uint8_t command_atc_dfusize( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    uint32_t *file_size = (uint32_t*)&argv[0][0];
    uint32_t rfile_size = 0;

    if ( param->flag.is_qust )
    {
        rfile_size = com.flashmap->dfu_size_get();
        CLI_TEXT("%d",rfile_size );
    }
    else
    {
        com.flashmap->dfu_size_set(*file_size);
    }

    return EMOS_OK;
}
#endif


#ifdef EMOS_SUPPORT_TFILE
static uint8_t command_atc_dfucrc32( com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{

    uint32_t rfile_size = com.flashmap->dfu_size_get();
    uint8_t *pdata;
    uint32_t crc32;

    com.flashmap->dfu_field_read(1,&pdata);
    crc32 = emos.units->builtin.crc32(pdata,rfile_size);
    
    CLI_TEXT("%x",crc32 );
    
    return EMOS_OK;
}
#endif

static uint8_t command_kdfu_ctrl(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    com.flashmap->bootmode();
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_REBOOT, NULL, 0);
    return EMOS_OK;
}

const cli_handle_t dfu_command_tbl[] =
{
#ifdef EMOS_SUPPORT_SLAVE
    {"KDFU"       , xBLN xNUL xNUL xNUL, command_kdfu_ctrl},
#endif

#ifdef EMOS_SUPPORT_TFILE
    {"DFUFILE"    , xINT xENT xNUL xNUL, command_atc_dfufile},
    {"DFUSIZE"    , xINT xQST xNUL xNUL, command_atc_dfusize},
    {"DFUFCHK"    , xQST xNUL xNUL xNUL, command_atc_dfucrc32},
#endif
    {NULL},
};

