#include "mdl_co_cli.h"

static uint8_t command_atc_nfc_test(com_mdl_rak_cli_param_t *param, uint8_t argc, char **argv)
{
    emos.nfc->test();
    return EMOS_OK;
}

const cli_handle_t nfc_command_tbl[] =
{
    {"NFC_TEST"   , xQST xNUL xNUL xNUL, command_atc_nfc_test},
    {NULL},
};

