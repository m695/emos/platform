#include "../../kernel.h"
#include "../inc/inc_co_rak_radio.h"

#ifdef EMOS_SUPPORT_RADIO

static void com_mdl_rak_radio_init( void )
{
    for (uint8_t i = 0; i < (sizeof(uint32_t)); i++)
    {
        if (com_mydev()->type_id.current.bytes[i] != 0)
        {
            if (com_mydev()->type_id.current.bytes[i] == 'B')
            {
                com.nbiot->ctrl->nbt_en = 1;
            }
            else
            {
                com.nbiot->ctrl->nbt_en = 0;
            }
        }
    }

    com_if_rak_bank0_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank0_t));
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    if (eee.lpwan_mode != 1 && eee.lpwan_mode != 2)
    {
        eee.lpwan_mode = 1; //Default to lora mode
        com_flash_bank0_write((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
    }
    com.radio->lpwan(eee.lpwan_mode);
}

static void com_mdl_rak_radio_lpwan_switch(uint8_t type)
{
    switch ( type )
    {
        case 1:
            do{
#ifdef EMOS_SUPPORT_NBIOT
            com.nbiot->power_check();
#endif      
            com.radio->ctrl->lpwan_mode = type;
            RADIO_SWAP_LORA();
            } while(0);
            break;
        case 2:
            if(com.nbiot->ctrl->nbt_en == 1)
            {
                com.radio->ctrl->lpwan_mode = type;
                RADIO_SWAP_LTE();
#ifdef EMOS_SUPPORT_NBIOT
                com.nbiot->power(1);
#endif                  

            }
            else
            {
               return EMOS_MODE_NO_SUPPORT;
            }   
            break;
        case 3:
            break;
        case 4:
            break;
        default:
            return EMOS_PARAM_ERROR;
    }       
    
    return EMOS_OK;
}

static void com_mdl_rak_radio_swap_to_lora( void )
{
    com_if_rak_bank0_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank0_t));
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    if (eee.lpwan_mode == 2)
    {
        return;
    }

    RADIO_SWAP_LORA();
}

static void com_mdl_rak_radio_swap_to_nbiot( void )
{
    com_if_rak_bank0_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank0_t));
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    if (eee.lpwan_mode == 1)
    {
        return;
    }

    RADIO_SWAP_LTE();
}

//Control the power of LoRaWan or NBIOT antenna
static void com_mdl_rak_radio_enable(uint8_t value)
{
    //Disable all antenna
    if (value  == 0)
    {
        RADIO_SWITCH_DIS();
        RADIO_SWAP_LORA();
    }
    //Enable antenna
    else if (value == 1)
    {
        if (com.radio->ctrl->lpwan_mode == 1)
        {
            RADIO_SWAP_LORA();
        }
        else if (com.radio->ctrl->lpwan_mode  == 2)
        {
            RADIO_SWAP_LTE();
        }
        RADIO_SWITCH_EN();
    }
}


emos_process( mdl_rak_radio,
{
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            com_mdl_rak_radio_init();
            break;
        case TASK_MODE_POLLING:
            break;
        case TASK_MODE_TIMEOUT:
            break;
        case TASK_MODE_SLEEP:
            break;
        case TASK_MODE_WAKEUP:
            break;
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                case COM_TASK_RAK_RADIO_LPWAN_SWITCH:
                    com_mdl_rak_radio_lpwan_switch(task_msg->payload[0]);
                    break;
                case COM_TASK_RAK_RADIO_SWAP_TO_LORA:
                    com_mdl_rak_radio_swap_to_lora();
                    break;
                 case COM_TASK_RAK_RADIO_SWAP_TO_NBIOT:
                    com_mdl_rak_radio_swap_to_nbiot();
                    break;
                 case COM_TASK_RAK_RADIO_ENABLE:
                    com_mdl_rak_radio_enable(task_msg->payload[0]);
                    break;
                 default:
                    break;
            }
            break;
    }
})

#else

#ifdef BSP_NOT_SUPPORT_WEAK
void com_mdl_rak_radio_process(emos_task_msg_t *task_msg)
{ 
}
#endif //BSP_NOT_SUPPORT_WEAK

#endif //EMOS_SUPPORT_RADIO
