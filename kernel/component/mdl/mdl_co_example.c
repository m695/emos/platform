#include "../../kernel.h"

// #define THIS_EXAMPLE 

#ifdef THIS_EXAMPLE

#include "../inc/inc_co_example.h"

emos_process( mdl_example, 
{
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            // com_task_timeout(COM_TASK_SERV, MSEC(10));
            break;

        case TASK_MODE_POLLING:
            break;

        case TASK_MODE_TIMEOUT:
            DBG_INFO("timeout\r\n");
            com_task_timeout(COM_TASK_SERV, MSEC(10));
            break;
            
        case TASK_MODE_SLEEP:
            com_task_wakeup(COM_TASK_SERV);
            break;

        case TASK_MODE_WAKEUP:
            com_task_sleep(COM_TASK_SERV);
            break;
            
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                case COM_TASK_EVT_EXAMPLE_TODO1:
                    break;
                case COM_TASK_EVT_EXAMPLE_TODO2:
                    break;
                default:
                    break;
            }
    }
})
#else

#ifdef BSP_NOT_SUPPORT_WEAK
void com_mdl_example_process(emos_task_msg_t *task_msg)
{ 
}
#endif //BSP_NOT_SUPPORT_WEAK

#endif //EMOS_SUPPORT_HALL
