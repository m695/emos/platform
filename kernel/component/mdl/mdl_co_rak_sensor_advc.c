#include "../../kernel.h"

#ifdef EMOS_SUPPORT_RAKSNSR_ADVC

#include "../inc/inc_co_rak_sensor_advc.h"

#define TASK_CTRL_ID   8

static uint8_t do_dummy_func( void )
{
    return EMOS_OK;
}

/*******************************************************************/

// static uint8_t do_rs485_wakeup( void )
// {
//     DBG_INFO("%s\r\n",__func__);
//     return EMOS_OK;
// }

// static uint8_t do_rs485_read( void )
// {
//     DBG_INFO("%s\r\n",__func__);
//     return EMOS_OK;
// }

static uint8_t next_slot = IDISABLE;

const mdl_rak_ioctrler_acsii_with_id_t ioctrler_iotype_tbl[] = 
{
    {.acsii="uart0"  ,.id=IOCTRLER_IOTYPE_UART0 },
    {.acsii="uart1"  ,.id=IOCTRLER_IOTYPE_UART1 },
    {.acsii="uart2"  ,.id=IOCTRLER_IOTYPE_UART2 },
    {.acsii="iic0"   ,.id=IOCTRLER_IOTYPE_IIC0 },
    {.acsii="iic1"   ,.id=IOCTRLER_IOTYPE_IIC1 },
    {.acsii="iic2"   ,.id=IOCTRLER_IOTYPE_IIC2 },
    {.acsii="sdi12"  ,.id=IOCTRLER_IOTYPE_SDI12 },
    {.acsii="di0"    ,.id=IOCTRLER_IOTYPE_DI0 },
    {.acsii="di1"    ,.id=IOCTRLER_IOTYPE_DI1 },
    {.acsii="do0"    ,.id=IOCTRLER_IOTYPE_DO0 },
    {.acsii="do1"    ,.id=IOCTRLER_IOTYPE_DO1 },
    {.acsii="rs485"  ,.id=IOCTRLER_IOTYPE_RS485 },
    
    NULL,
};
uint8_t ioctrler_iotype_tbl_cnt = COUNT_OF(ioctrler_iotype_tbl);

const mdl_rak_ioctrler_acsii_with_id_t ioctrler_cmdtype_tbl[] = 
{
    {.acsii="W",.id=cWRITE},
    {.acsii="R",.id=cREAD},
    {.acsii="I",.id=cINFO},
    {.acsii="C",.id=cCLEAN},
    {.acsii="M",.id=cMUL},
    {.acsii="D",.id=cDIV},
    {.acsii="O",.id=cOUTPUT},
    {.acsii="Z",.id=cVERIFY},
    {.acsii="E",.id=cEND},
    {.acsii="S",.id=cSTART},
    {.acsii="P",.id=cPAUSE},
    {.acsii="V",.id=cVOLT},
    {.acsii="T",.id=cSWAP},
    {.acsii="&",.id=cAND},
    {.acsii="<",.id=cLEFT},
    {.acsii=">",.id=cRIGHT},
    {.acsii="B",.id=cBACKUP},
    {.acsii="U",.id=cUNDO},
    NULL,
};
uint8_t ioctrler_cmdtype_tbl_cnt = COUNT_OF(ioctrler_cmdtype_tbl);

#if 0
#define GGGROUP(X) X

#define LPS22HB_CTRL_REG1      0x10U
#define LPS22HB_I2C_ADD     (0b1011100<<1)
#define LPS22HB_CTRL_REG2      0x11U
#define LPS22HB_WHO_AM_I       0x0FU
#define LPS22HB_STATUS         0x27U
#define LPS22HB_PRESS_OUT_XL   0x28U
#define LPS22HB_TEMP_OUT_L     0x2BU


typedef enum
{
  LPS22HB_POWER_DOWN  = 0,
  LPS22HB_ODR_1_Hz    = 1,
  LPS22HB_ODR_10_Hz   = 2,
  LPS22HB_ODR_25_Hz   = 3,
  LPS22HB_ODR_50_Hz   = 4,
  LPS22HB_ODR_75_Hz   = 5,
} lps22hb_odr_t;



#define LIS3DH_OUT_ADC1_L            0x08U
#define LIS3DH_OUT_ADC1_H            0x09U
#define LIS3DH_OUT_ADC2_L            0x0AU
#define LIS3DH_OUT_ADC2_H            0x0BU
#define LIS3DH_OUT_ADC3_L            0x0CU
#define LIS3DH_OUT_ADC3_H            0x0DU
#define LIS3DH_WHO_AM_I              0x0FU

#define LIS3DH_CTRL_REG0             0x1EU
#define LIS3DH_CTRL_REG1             0x20U
#define LIS3DH_CTRL_REG2             0x21U
#define LIS3DH_CTRL_REG4             0x23U
#define LIS3DH_CTRL_REG5             0x24U
#define LIS3DH_CTRL_REG6             0x25U
#define LIS3DH_STATUS_REG_AUX        0x07U

#define LIS3DH_OUT_X_L              0x28U
#define LIS3DH_OUT_X_H              0x29U
#define LIS3DH_OUT_Y_L              0x2AU
#define LIS3DH_OUT_Y_H              0x2BU
#define LIS3DH_OUT_Z_L              0x2CU
#define LIS3DH_OUT_Z_H              0x2DU
#define LIS3DH_FIFO_CTRL_REG        0x2EU
#define LIS3DH_INT2_THS             0x36U
#define LIS3DH_INT2_DURATION        0x37U
#define LIS3DH_REFERENCE            0x26U
#define LIS3DH_INT2_CFG             0x34U
#define LIS3DH_TEMP_CFG_REG         0x1FU




#define RAK1901_ID_TEMP  0x01
#define RAK1901_ID_HUMP  0x02
#define RAK1902_ID_TEMP  0x03
#define RAK1902_ID_BARO  0x04
#define RAK1904_ID_TEMP  0x05
#define RAK1904_ID_ACC   0x06


#define LIS3DH_I2C_ADDR               (0b0011000<<1)

#define IOTYPE_IIC    1


static const uint8_t test_cmd[] = 
{

#ifdef EMOS_SUPPORT_DYMSNSR
#if 1
    /* read slave id */
    cCLEAN,IOTYPE_IIC,0,
    cWRITE,IOTYPE_IIC,2,LPS22HB_I2C_ADD,LPS22HB_WHO_AM_I, /* continue */ cREAD,IOTYPE_IIC,2,LPS22HB_I2C_ADD, 0,
    cSTART,IOTYPE_IIC,0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
    cVERIFY,IOTYPE_IIC,1,0xB1,
    /* read reg1 */
#if 0
    cWRITE,IOTYPE_IIC,2,LPS22HB_I2C_ADD,LPS22HB_CTRL_REG1, /* continue */ cREAD,IOTYPE_IIC,2,LPS22HB_I2C_ADD, 0,
#endif
    cWRITE,IOTYPE_IIC,3,LPS22HB_I2C_ADD,LPS22HB_CTRL_REG1, 0x10, /* config wakeup */

    /* read reg2 */
#if 0
    cWRITE,IOTYPE_IIC,2,LPS22HB_I2C_ADD,LPS22HB_CTRL_REG2, /* continue */ cREAD,IOTYPE_IIC,2,LPS22HB_I2C_ADD, 0,
#endif
    cWRITE,IOTYPE_IIC,3,LPS22HB_I2C_ADD,LPS22HB_CTRL_REG2, 0x11, /* config one shot */

    /* read data start, first read status */
    cWRITE,IOTYPE_IIC,2,LPS22HB_I2C_ADD,LPS22HB_STATUS, /* continue */ cREAD,IOTYPE_IIC,2,LPS22HB_I2C_ADD, 0,

    cPAUSE,IOTYPE_IIC,1,20,
    /* read current data */
    cCLEAN,IOTYPE_IIC,0,
    cWRITE,IOTYPE_IIC,2,LPS22HB_I2C_ADD,LPS22HB_PRESS_OUT_XL, /* continue */ cREAD,IOTYPE_IIC,4,LPS22HB_I2C_ADD, 0, 0, 0,
    cMUL,IOTYPE_IIC,2,0x0,0x1, /* n=n*0x100 */
    // cINFO,IOTYPE_IIC,0,   /* for debug */
    cDIV,IOTYPE_IIC,3,0x0,0x0,0x10, /* n=n/0x100000 */
    // cINFO,IOTYPE_IIC,0,   /* for debug */
    cMUL,IOTYPE_IIC,1,0xA, /* n=n*10 */
    cINFO,IOTYPE_IIC,0,   /* for debug */
    cOUTPUT,IOTYPE_IIC,2,RAK1902_ID_BARO, RAK_IPSO_BAROMETER,

    /* read current data */
    cCLEAN,IOTYPE_IIC,0,
    cWRITE,IOTYPE_IIC,2,LPS22HB_I2C_ADD,LPS22HB_TEMP_OUT_L, /* continue */ cREAD,IOTYPE_IIC,3,LPS22HB_I2C_ADD, 0, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
    cDIV,IOTYPE_IIC,1,0xA, /* n=n/10 */
    cINFO,IOTYPE_IIC,0,   /* for debug */
    cOUTPUT,IOTYPE_IIC,2,RAK1902_ID_TEMP, RAK_IPSO_TEMP_SENSOR,

    /* read reg1 */
#if 0
    cWRITE,IOTYPE_IIC,2,LPS22HB_I2C_ADD,LPS22HB_CTRL_REG1, /* continue */ cREAD,IOTYPE_IIC,2,LPS22HB_I2C_ADD, 0,
#endif

    cWRITE,IOTYPE_IIC,3,LPS22HB_I2C_ADD,LPS22HB_CTRL_REG1, 0x00,/* config sleep*/

    cEND,IOTYPE_IIC, 0,
#endif

    /*******************************************************/
#if 1
    /* read slave id */
    cCLEAN,IOTYPE_IIC,0,
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_WHO_AM_I | 0x80,
    cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG0, 0x90, /* config connected */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG4 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // IOTYPE_IIC,cINFO,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG4, 0x80, /* Enable Block Data Update. */

    
#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG1 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG1, 0x5F, /* Set Output Data Rate to 1Hz. */

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG4, 0x80 | 0x00, /* Set full scale to 2g. */
    
#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_TEMP_CFG_REG | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif
    
    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_TEMP_CFG_REG, 0, 
    
    cPAUSE,IOTYPE_IIC,1,10,
    /* read current data */
    cCLEAN,IOTYPE_IIC,0,
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_OUT_X_L | 0x80, /* continue */ cREAD,IOTYPE_IIC,7,LIS3DH_I2C_ADDR, 0, 0, 0, 0, 0, 0,
    cINFO,IOTYPE_IIC,0,   /* for debug */
    cOUTPUT,IOTYPE_IIC,2,RAK1904_ID_ACC, RAK_IPSO_ACCELEROMETER,


#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG2 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG2, 0x0A, /* High-pass filter enabled on interrupt activity 2 */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG6 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG6, 0x20, /* Enable AOI1 on int1 pin */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG5 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG5, 0x02, /* Interrupt 2 pin latched */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG4 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG4, 0x08, /* Set full scale to 2 g */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_INT2_THS | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_INT2_THS, 0x10, /* Set interrupt threshold to 0x10 -> 250 mg */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_INT2_DURATION | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_INT2_DURATION, 0x0, /* Set no time duration */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_REFERENCE | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif
    
    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_REFERENCE, 0x0,  /* Dummy read to force the HP filter to current acceleration value. */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_INT2_CFG | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_INT2_CFG, 0x2A,   /* Configure wake-up interrupt event on all axis */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG1 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG1, 0x5F,   /* Set device in continuous mode with 12 bit resol. */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG4 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG4, 0x80,   /* Set device in continuous mode with 12 bit resol. */

#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG4 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG4, 0x80,   /* Set device in continuous mode with 12 bit resol. */


#if 0
    cWRITE,IOTYPE_IIC,2,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG1 | 0x80, /* continue */ cREAD,IOTYPE_IIC,2,LIS3DH_I2C_ADDR, 0,
    // cINFO,IOTYPE_IIC,0,   /* for debug */
#endif

    cWRITE,IOTYPE_IIC,3,LIS3DH_I2C_ADDR,LIS3DH_CTRL_REG1, 0x2F,   /* Set device in continuous mode with 12 bit resol. */
#endif
#else

    0xff,
#endif
};
#endif

static uint16_t scmd_count = 0;
static uint16_t scmd_index = 0;
static uint8_t find_tlv_disp( uint16_t len, uint8_t *type, uint8_t *data )
{
    uint8_t tmp_buf[255];
    uint8_t *ptmp = tmp_buf;

    if ( scmd_count != scmd_index)
    {
        scmd_count++;
        return EMOS_OK;
    }
    

    ptmp += sprintf((void*)ptmp, "%03d=", scmd_index++);
    for (size_t i = 0; i < ioctrler_cmdtype_tbl_cnt ; i++ )
    {
        if (ioctrler_cmdtype_tbl[i].id == type[0])
        {
            ptmp += sprintf((void*)ptmp, "%s,", ioctrler_cmdtype_tbl[i].acsii);
            break;
        }
    }

    for (size_t i = 0; i < ioctrler_iotype_tbl_cnt ; i++ )
    {
        if (ioctrler_iotype_tbl[i].id == type[1])
        {
            ptmp += sprintf((void*)ptmp, "%s,", ioctrler_iotype_tbl[i].acsii);
            break;
        }
    }

    for (size_t i = 0; i < len; i++)
    {
        ptmp += sprintf((void*)ptmp, "%02X,", data[i]);
    }

    ptmp += sprintf((void*)ptmp, "\r\n");
    SYS_INFOA(tmp_buf,strlen((char *)tmp_buf));

    com_if_vsys_mydev_t * mydev = com_mydev();
    com.rpcl->snd_prbdisp(mydev->id,tmp_buf,strlen((char *)tmp_buf));
    
    com_vsys_send( COM_TASK_RAK_SENSOR_ADVC, COM_TASK_EVT_RAK_SNSR_ADVC_DUMPNEXT, NULL,0);
    return EMOS_ERROR;
}

static uint8_t backup_buff[16];
static uint8_t tlv_dbuff[16];
static uint8_t math_buff[16];

static uint8_t test_tlv_cb( uint16_t len, uint8_t *type, uint8_t *data )
{
    #define cmd_type       type[0]
    #define io_type        type[1]
    #define regaddr_8bytes data[0]
    #define fm_snsrid      data[0]
    #define fm_snsrtype    data[1]
    
    uint8_t *fdata = &data[0];
    uint16_t fdata_len = len;
    uint8_t *cdata = &data[1];
    uint16_t cdata_len = 0;

    if (len != 0)
    {
        cdata_len = len - sizeof(regaddr_8bytes);
    }
    
    if ( next_slot == IENABLE )
    {
        if ( (cmd_type == cEND) || (cmd_type == cSTART) )
        {
            next_slot = IDISABLE;
        }
        goto EXIT_FUNC;
    }
    
    switch (cmd_type)
    {
        case cVOLT:
            switch (*fdata)
            {
                case 120:
                    com.ldo->v120();
                    break;
                case 240:
                    com.ldo->v240();
                    break;
                default:
                case 33:
                case 0:
                    com.ldo->v33();
                    break;
            }
            break;
        case cCLEAN:
            memset(tlv_dbuff , 0, sizeof(tlv_dbuff));
            break;
        case cVERIFY:
            if (memcmp(tlv_dbuff,fdata,len) != 0)
            {
                next_slot = IENABLE;
            }

            break;
        case cPAUSE:
            do
            {
                memset( math_buff, 0, sizeof(math_buff));
                memcpy( math_buff, fdata, len);
                uint64_t *target_dly = (uint64_t *)math_buff;
                emos_delay(*target_dly);
            }while(0);
            break;
        case cBACKUP:
            memcpy( backup_buff, tlv_dbuff, sizeof(tlv_dbuff));
            break;
        case cUNDO:
            memcpy( tlv_dbuff, backup_buff, sizeof(tlv_dbuff));
            break;
        case cSWAP:
        case cLEFT:
        case cRIGHT:
        case cAND:
        case cMUL:
        case cDIV:
            do
            {
                memset( math_buff, 0, sizeof(math_buff));
                memcpy( math_buff, fdata, len);
                uint64_t *target_math = (uint64_t *)math_buff;
                uint64_t *target_data = (uint64_t *)tlv_dbuff;
                switch(cmd_type)
                {
                    case cSWAP:
                        if ( *target_math > sizeof(tlv_dbuff) )
                        {
                            break;
                        }

                        emos.units->memswap(tlv_dbuff,*target_math);
                        break;
                    case cLEFT:
                        if ( *target_math > sizeof(tlv_dbuff) )
                        {
                            break;
                        }
                        
                        memmove(&tlv_dbuff[0], &tlv_dbuff[*target_math], (sizeof(tlv_dbuff) - *target_math) - 1);
                        break;
                    case cRIGHT:
                        break;
                    case cAND:
                        for (size_t i = 0; i < sizeof(math_buff); i++)
                        {
                            tlv_dbuff[i] &= math_buff[i];
                        }
                        break;
                    case cMUL:
                        *target_data *= *target_math;
                        break;
                    case cDIV:
                        *target_data /= *target_math;
                        break;
                }
            }while(0);
            break;

        case cINFO:
            DBG_INFO("(HEX)tlv read:");
            DBG_INFOA((void*)tlv_dbuff,sizeof(tlv_dbuff));
            break;
        case cREAD:
            switch (io_type)
            {
                case IOCTRLER_IOTYPE_RS485:
                    emos.rs485(0)->read((void*)tlv_dbuff,fdata_len, 500 /* timeout */);
                    break;
                case IOCTRLER_IOTYPE_UART0:
                    emos.uart(2).recv((void*)tlv_dbuff,fdata_len,500);
                    break;
                case IOCTRLER_IOTYPE_UART1:
                    emos.uart(2).recv((void*)tlv_dbuff,fdata_len,500);
                    break;
                case IOCTRLER_IOTYPE_UART2:
                    emos.uart(2).recv((void*)tlv_dbuff,fdata_len,500);
                    break;
                case IOCTRLER_IOTYPE_IIC0:
                    emos.iic(0)->sread(regaddr_8bytes,(void*)tlv_dbuff,cdata_len);
                    break;
                case IOCTRLER_IOTYPE_SDI12:
                    emos.sdi12(0)->read((void*)tlv_dbuff,fdata_len,500);
                    break;
                case IOCTRLER_IOTYPE_DI0:
                    tlv_dbuff[0] = emos.digital(2).read();
                    break;
                case IOCTRLER_IOTYPE_DI1:
                    tlv_dbuff[0] = emos.digital(2).read();
                    break;
                case IOCTRLER_IOTYPE_DO0:
                    tlv_dbuff[0] = emos.digital(0).read();
                    break;
                case IOCTRLER_IOTYPE_DO1:
                    tlv_dbuff[0] = emos.digital(1).read();
                    break;
                default:
                    break;
            }
            break;
        case cWRITE:
            switch (io_type)
            {
                case IOCTRLER_IOTYPE_RS485:
                    emos.rs485(0)->write((void*)fdata,fdata_len);
                    break;
                case IOCTRLER_IOTYPE_UART0:
                    emos.uart(2).puts((void*)fdata,fdata_len);
                    break;
                case IOCTRLER_IOTYPE_UART1:
                    emos.uart(2).puts((void*)fdata,fdata_len);
                    break;
                case IOCTRLER_IOTYPE_UART2:
                    emos.uart(2).puts((void*)fdata,fdata_len);
                    break;
                case IOCTRLER_IOTYPE_IIC0:
                    emos.iic(0)->swrite(regaddr_8bytes,(void*)cdata,cdata_len);
                    break;
                case IOCTRLER_IOTYPE_SDI12:
                    emos.sdi12(0)->write((void*)fdata,fdata_len);
                    break;
                case IOCTRLER_IOTYPE_DI0:
                    break;
                case IOCTRLER_IOTYPE_DI1:
                    break;
                case IOCTRLER_IOTYPE_DO0:
                    emos.digital(0).write(fdata[0]);
                    break;
                case IOCTRLER_IOTYPE_DO1:
                    emos.digital(1).write(fdata[0]);
                    break;
                default:
                    break;
            }
            break;
        case cOUTPUT:
            do
            {
                uint8_t this_pid = com_iprobe->id;
                com_if_rak_probe_sensor_t *snsr_info = NULL;
                if ( com_sprobe(this_pid).snsr_get(fm_snsrid,&snsr_info) != EMOS_OK )
                {
                    com_sprobe(this_pid).new_sensor(fm_snsrid, fm_snsrtype);
                    com_sprobe(this_pid).snsr_get(fm_snsrid,&snsr_info);
                    SYS_INFO(EVT_MSG("ADD_SNSR:%02x %02x %02x"),fm_snsrid,fm_snsrtype,snsr_info->data.len);
                }
                else
                {
                    snsr_info->sta.stop = IDISABLE;
                }

                
                switch (io_type)
                {
                    case IOCTRLER_IOTYPE_RS485:
                        memcpy(snsr_info->data.value, (uint8_t*)tlv_dbuff, snsr_info->data.len);
                        break;

                    case IOCTRLER_IOTYPE_IIC0:
                        memcpy(snsr_info->data.value, (uint8_t*)tlv_dbuff, snsr_info->data.len);
                        break;

                    case IOCTRLER_IOTYPE_SDI12:
                        do
                        {
                            uint32_t gnum = 0;
                            emos_atoi((void*)&tlv_dbuff[2],&gnum);
                            memcpy((void*)snsr_info->data.value, (void*)&gnum, snsr_info->data.len);
                        } while (0);

                        break;
                    case IOCTRLER_IOTYPE_DO1:
                    case IOCTRLER_IOTYPE_DO0:
                    case IOCTRLER_IOTYPE_DI1:
                    case IOCTRLER_IOTYPE_DI0:
                        memcpy(snsr_info->data.value, (uint8_t*)tlv_dbuff, 1);
                        break;
                    case IOCTRLER_IOTYPE_UART0:
                    case IOCTRLER_IOTYPE_UART1:
                    case IOCTRLER_IOTYPE_UART2:
                        memcpy(snsr_info->data.value, (uint8_t*)tlv_dbuff, snsr_info->data.len);
                        break;
                    default:
                        break;
                }
            } while (0);
            break;
        default:
            break;
    }
EXIT_FUNC:
    return EMOS_OK;
}

static uint8_t do_ctrler_process( void )
{
    next_slot = IDISABLE;
    
    #define PAGE_SIZE 1000
    uint8_t *ptmp;
    com_flash_bank4_readonly(&ptmp,PAGE_SIZE);

    com_tlv_decode(ptmp, PAGE_SIZE,test_tlv_cb );
    return EMOS_OK;
}

static void com_mdl_rak_sensor_advc_init()
{
    uint8_t this_id = TASK_CTRL_ID;
    com.rak_sensor(this_id).i.check((void *)do_dummy_func);
    // com.rak_sensor(func_id).i.sleep((void *)do_dummy_func);
    // com.rak_sensor(func_id).i.wakeup((void *)do_dummy_func);
    // com.rak_sensor(func_id).i.read((void *)do_dummy_func);
    com.rak_sensor(this_id).i.process((void *)do_ctrler_process);
}

emos_process( mdl_rak_sensor_advc, 
{
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            com_mdl_rak_sensor_advc_init();
            break;

        case TASK_MODE_POLLING:
            break;

        case TASK_MODE_TIMEOUT:
            break;
            
        case TASK_MODE_SLEEP:
            break;

        case TASK_MODE_WAKEUP:
            break;
            
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                case COM_TASK_EVT_RAK_SNSR_ADVC_TODO1:
                    break;
                case COM_TASK_EVT_RAK_SNSR_ADVC_DUMPMAP:
                    do
                    {
                        uint8_t *ptmp;
                        com_flash_bank4_readonly(&ptmp,PAGE_SIZE);
                        scmd_index = 0;
                        scmd_count = 0;
                        com_tlv_decode(ptmp, PAGE_SIZE, find_tlv_disp );
                    } while (0);
                    break;
                case COM_TASK_EVT_RAK_SNSR_ADVC_DUMPNEXT:
                    do
                    {
                        uint8_t *ptmp;
                        com_flash_bank4_readonly(&ptmp,PAGE_SIZE);
                        scmd_count = 0;
                        com_tlv_decode(ptmp, PAGE_SIZE, find_tlv_disp );
                    } while (0);
                    break;
                default:
                    break;
            }
    }
})
#else

#ifdef BSP_NOT_SUPPORT_WEAK
void com_mdl_rak_sensor_advc_process(emos_task_msg_t *task_msg)
{ 
}
#endif //BSP_NOT_SUPPORT_WEAK

#endif //EMOS_SUPPORT_HALL
