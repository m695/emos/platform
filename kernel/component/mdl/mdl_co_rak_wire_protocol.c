#include "../../kernel.h"
#include "../inc/inc_co_rak_wire_protocol.h"

typedef struct 
{
    void (*rxreq)(uint8_t *msg, uint16_t len); 
    void (*rxrsp)(uint8_t *msg, uint16_t len); 
    
} __EMOS_PACKED frame_command_entry_t;

typedef struct 
{
    uint16_t (*txreq)(rak_probe_wire_protocol_t ** ,uint16_t, uint8_t, uint8_t, uint8_t *, uint8_t *,uint16_t ,uint8_t *); 
    uint16_t (*txrsp)(rak_probe_wire_protocol_t ** ,uint16_t, uint8_t, uint8_t, uint8_t *, uint8_t *,uint16_t ,uint8_t *); 
} __EMOS_PACKED frame_send_entry_t;

typedef struct
{
    uint8_t msg[TEMP_WIRELOG_BUFF];
    uint8_t len;
} this_handshake_box_t;

typedef struct 
{
    uint8_t ptype;
    uint8_t datalen;
} format_snsr_cfg_size_t;

static void this_recv_dummy(uint8_t*m, uint16_t l){ };
static const frame_command_entry_t recv_cmd_entry[];
static const frame_send_entry_t send_cmd_entry[];

this_handshake_box_t hdshk_box;
static uint8_t keep_sequence = 1;

#define WAKEUP_BYTE   0xff
static const uint8_t wakeup_byte = WAKEUP_BYTE;

static uint16_t this_send_dummy( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid, uint8_t *pay_type ,uint8_t *data,uint16_t data_len,uint8_t *isc){return 0;};
static uint8_t mdl_co_rak_wire_protocol_send(rak_probe_wire_protocol_t *src, 
                                            uint16_t msg_len, 
                                            uint8_t dir, 
                                            uint8_t mid,
                                            uint8_t tid, 
                                            uint8_t sid, 
                                            uint8_t frm_type, 
                                            uint8_t pay_type, 
                                            uint8_t *data,
                                            uint16_t data_len);

static __RAKLINK_UNUSED uint16_t mdl_co_rak_wire_protocol_send_req_bynew(uint8_t mid,uint8_t pid, uint8_t sid, uint8_t frm_type, uint8_t pay_type)
{
    return mdl_co_rak_wire_protocol_send(NULL,0,WIRE_SEND_REQ,mid,pid,sid,frm_type,pay_type,NULL,0);
}

static __RAKLINK_UNUSED uint16_t mdl_co_rak_wire_protocol_send_rsp_bynew(uint8_t mid,uint8_t pid, uint8_t sid, uint8_t frm_type, uint8_t pay_type)
{
    return mdl_co_rak_wire_protocol_send(NULL,0,WIRE_SEND_RSP,mid,pid,sid,frm_type,pay_type,NULL,0);
}

static __RAKLINK_UNUSED uint16_t mdl_co_rak_wire_protocol_send_req_withsrc(uint8_t *src, uint16_t src_len, uint8_t mid,uint8_t pid, uint8_t sid, uint8_t frm_type, uint8_t pay_type)
{
    return mdl_co_rak_wire_protocol_send((rak_probe_wire_protocol_t *)src,src_len,WIRE_SEND_REQ,mid,pid,sid,frm_type,pay_type,NULL,0);
}

static __RAKLINK_UNUSED uint16_t mdl_co_rak_wire_protocol_send_rsp_withsrc(uint8_t *src, uint16_t src_len, uint8_t mid,uint8_t tid, uint8_t sid, uint8_t frm_type, uint8_t pay_type)
{
    return mdl_co_rak_wire_protocol_send((rak_probe_wire_protocol_t *)src,src_len,WIRE_SEND_RSP,mid,tid,sid,frm_type,pay_type,NULL,0);
}

static __RAKLINK_UNUSED uint16_t mdl_co_rak_wire_protocol_send_req_withinp(uint8_t mid,uint8_t pid, uint8_t sid, uint8_t frm_type, uint8_t pay_type, uint8_t *data,uint16_t data_len)
{
    return mdl_co_rak_wire_protocol_send(NULL,0,WIRE_SEND_REQ,mid,pid,sid,frm_type,pay_type,data,data_len);
}

static __RAKLINK_UNUSED uint16_t mdl_co_rak_wire_protocol_send_rsp_withinp(uint8_t mid,uint8_t pid, uint8_t sid, uint8_t frm_type, uint8_t pay_type, uint8_t *data,uint16_t data_len)
{
    return mdl_co_rak_wire_protocol_send(NULL,0,WIRE_SEND_RSP,mid,pid,sid,frm_type,pay_type,data,data_len);
}

static __RAKLINK_UNUSED uint8_t mdl_co_rak_chk_probe( com_if_rak_probe_param_t ** in_probe_info )
{
#ifdef EMOS_SUPPORT_PROBE_SIMULATION
    com_if_vsys_mydev_t * mydev = com_mydev();
    if(mydev->id == PROBE_ID_HUB)
    {
        if ( com_iprobe != NULL )
        {
            *in_probe_info = com_iprobe;
            return EMOS_OK;
        }
    }
#endif
    return EMOS_FAIL;
}

#define mdl_co_rak_chk_probe_cb(P,X)  do{ if (mdl_co_rak_chk_probe(P) == EMOS_OK) { X } } while (0);


static const format_snsr_cfg_size_t format_snsr_cfg_size[] =
{
    {.ptype=RAK_PB_PAY_TYPE_PARAM_PRB_INTV  ,.datalen=sizeof(uint32_t) },
    {.ptype=RAK_PB_PAY_TYPE_PARAM_SNSR_CNT  ,.datalen=0 },
    {.ptype=RAK_PB_PAY_TYPE_PARAM_SNSR_INFO ,.datalen=sizeof(uint8_t)  },
    {.ptype=RAK_PB_PAY_TYPE_PARAM_SNSR_INTV ,.datalen=sizeof(uint32_t) },
    {.ptype=RAK_PB_PAY_TYPE_PARAM_SNSR_RULE ,.datalen=sizeof(uint16_t) },
    {.ptype=RAK_PB_PAY_TYPE_PARAM_SNSR_DATA ,.datalen=sizeof(com_if_rak_probe_sensor_data_t) },
    {.ptype=RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR ,.datalen=10 },
    {.ptype=RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR ,.datalen=10 },
    {.ptype=RAK_PB_PAY_TYPE_PARAM_SMPL_INTV ,.datalen=sizeof(uint32_t) },
};

static __RAKLINK_UNUSED uint32_t function_get_paramlen( uint8_t isptype )
{
    uint8_t cnt = COUNT_OF(format_snsr_cfg_size);
    for (size_t i = 0; i < cnt; i++)
    {
        if (format_snsr_cfg_size[i].ptype == isptype )
        {
            return format_snsr_cfg_size[i].datalen;
        }
    }
    return 0;
}

static void recv_control_req( uint8_t * data, uint16_t len)
{
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);
    uint8_t ftype = wp->header.prb.frame_type;
    uint8_t ptype = wp->header.prb.payload_type;
    uint8_t plen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t target_id = PROBE_ID_DEF;
    uint8_t my_id = 0;
    uint8_t snsr_id = 0;

    if (len < plen)
    {
        return;
    }

    switch (ptype)
    {
        case RAK_PB_PAY_TYPE_CONTROL_REBOOT:
        case RAK_PB_PAY_TYPE_CONTROL_DFU:
            break;
        case RAK_PB_PAY_TYPE_CONTROL_RESTORE:
        case RAK_PB_PAY_TYPE_CONTROL_REMOVE:
            break;
        default:
            return;
    }

    target_id = wp->header.prb.source;
    my_id = wp->header.prb.destlination;
    mdl_co_rak_wire_protocol_send_rsp_withsrc(data,len,my_id,target_id,snsr_id,ftype,ptype);
}

static void recv_paramset_req( uint8_t * data, uint16_t len)
{
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);
    uint8_t ftype = wp->header.prb.frame_type;
    uint8_t ptype = wp->header.prb.payload_type;
    uint8_t plen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t target_id = PROBE_ID_DEF;
    uint8_t my_id = 0;
    uint8_t snsr_id = 0;

    if (len < plen)
    {
        return;
    }

    switch (ptype)
    {
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            snsr_id = wp->v2.snsr_header.id;
            break;
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
            break;
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
            break;
        default:
            return;
    }

    target_id = wp->header.prb.source;
    my_id = wp->header.prb.destlination;
    mdl_co_rak_wire_protocol_send_rsp_withsrc(data,len,my_id,target_id,snsr_id,ftype,ptype);
}

static void recv_paramget_req( uint8_t * data, uint16_t len)
{
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);
    uint8_t ftype = wp->header.prb.frame_type;
    uint8_t ptype = wp->header.prb.payload_type;
    uint8_t plen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t target_id = PROBE_ID_DEF;
    uint8_t my_id = 0;
    uint8_t snsr_id = 0;

    if (len < plen)
    {
        return;
    }

    switch (ptype)
    {
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            snsr_id = wp->v2.snsr_header.id;
            break;
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
            break;
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
            break;
        default:
            return;
    }

    target_id = wp->header.prb.source;
    my_id = wp->header.prb.destlination;
    mdl_co_rak_wire_protocol_send_rsp_withsrc(data,len,my_id,target_id,snsr_id,ftype,ptype);
}

static void recv_paramget_rsp( uint8_t * data, uint16_t len)
{
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);

    uint8_t paylen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;

    if (len < paylen)
    {
        return;
    }

    paylen = 0;
    switch (wp->header.prb.payload_type)
    {
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            paylen += sizeof(rak_probe_protocol_ie_param_sensor_h_t);
            paylen += sizeof(rak_probe_protocol_ie_param_sensor2_t);
            break;
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
            // paylen += sizeof(rak_probe_protocol_ie_param_probe_t);
            break;
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
            // paylen += sizeof(rak_probe_protocol_ie_param_sensor_t) + sizeof(rak_probe_protocol_ie_param_sensor_h_t);
            break;
        default:
            return;
    }

    if ( ! (wp->header.prb.payload_length > paylen))
    {
        return;
    }

    switch (wp->header.prb.payload_type)
    {
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            do
            {
                memset(&hdshk_box, 0, sizeof(this_handshake_box_t));
                memcpy(hdshk_box.msg,wp->v2.snsr_get.data,wp->v2.snsr_get.len);
                hdshk_box.len = wp->v2.snsr_get.len;
            } while (0);
            
            break;
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
            break;
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
            break;
        default:
            return;
    }
}

static void recv_ymodem_req( uint8_t * data, uint16_t len)
{

#ifdef EMOS_SUPPORT_YMODEM
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);
    uint8_t ftype = wp->header.prb.frame_type;
    uint8_t ptype = wp->header.prb.payload_type;
    uint8_t plen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t target_id = PROBE_ID_DEF;
    uint8_t my_id = 0;
    uint8_t snsr_id = 0;

    if (len < plen)
    {
        return;
    }
    
    target_id = wp->header.prb.source;
    my_id = wp->header.prb.destlination;

    mdl_co_rak_wire_protocol_send_rsp_withsrc(data,len,my_id,target_id,0,ftype,ptype);
#endif

}

static void recv_sensordat_req( uint8_t * data, uint16_t len)
{
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);
    uint8_t ftype = wp->header.prb.frame_type;
    uint8_t ptype = wp->header.prb.payload_type;
    uint8_t plen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t target_id = PROBE_ID_DEF;
    uint8_t my_id = 0;

    if (len < plen)
    {
        return;
    }

    target_id = wp->header.prb.source;
    my_id = wp->header.prb.destlination;
    mdl_co_rak_wire_protocol_send_rsp_withsrc(data,len,my_id,target_id,0,ftype,ptype);
}

static void recv_provision_req( uint8_t * data, uint16_t len)
{
#ifdef EMOS_SUPPORT_WIRE_HOST
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);
    uint8_t ftype = wp->header.prb.frame_type;
    uint8_t ptype = wp->header.prb.payload_type;
    uint8_t plen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t target_id = PROBE_ID_DEF;
    uint8_t my_id = 0;

    switch (ptype)
    {
        case RAK_PB_PAY_TPYE_PRV_VER:
        case RAK_PB_PAY_TPYE_PRV_VER2:
            plen += sizeof(rak_probe_protocol_ie_version_t);
            break;
        case RAK_PB_PAY_TPYE_PRV_VER3:
            plen += sizeof(rak_probe_protocol_ie_version3_t);
            break;
        case RAK_PB_PAY_TPYE_PRV_VER4:
        case RAK_PB_PAY_TPYE_PRV_HELLO:
        default:
            return;
    }
    
    if (len < plen)
    {
        return;
    }
    
    target_id = wp->header.prb.source;
    my_id = wp->header.prb.destlination;
    mdl_co_rak_wire_protocol_send_rsp_withsrc(data,len,my_id,target_id,0,ftype,ptype);
#endif
}

static void recv_provision_rsp( uint8_t * data, uint16_t len)
{
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);
    uint8_t ptype = wp->header.prb.payload_type;
    uint8_t plen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    com_if_vsys_mydev_t * mydev = com_mydev();
    uint8_t *pssn;
    
    switch (ptype)
    {
        case RAK_PB_PAY_TPYE_PRV_VER:
        case RAK_PB_PAY_TPYE_PRV_VER2:
            plen += sizeof(rak_probe_protocol_ie_version_t);
            break;
        case RAK_PB_PAY_TPYE_PRV_VER3:
            plen += sizeof(rak_probe_protocol_ie_version3_t);
            break;
        case RAK_PB_PAY_TPYE_PRV_HELLO:
        case RAK_PB_PAY_TPYE_PRV_VER4:
        default:
            return;
    }
    
    if (len < plen)
    {
        return;
    }

    uint8_t probe_id = PROBE_ID_DEF;

    switch (ptype)
    {
        case RAK_PB_PAY_TPYE_PRV_VER:
        case RAK_PB_PAY_TPYE_PRV_VER2:
            probe_id = wp->prov_v1.probe_id;
            pssn = wp->prov_v1.sn.value;
            break;
        case RAK_PB_PAY_TPYE_PRV_HELLO:
            return;
        case RAK_PB_PAY_TPYE_PRV_VER3:
            probe_id = wp->prov_v3.probe_id;
            pssn = wp->prov_v3.sn.value;
            break;
        case RAK_PB_PAY_TPYE_PRV_VER4:
        default:
            return;
    }

    if( mydev->id != PROBE_ID_DEF )
    {
        return;
    }

    if(memcmp(mydev->ssn.value, pssn, sizeof(mydev->ssn.value)) != EMOS_OK)
    {
        return;
    }

#ifdef EMOS_SUPPORT_SLAVE
    
    if ( com_iprobe != NULL )
    {
        com_iprobe->id = probe_id;
    }


    mydev->id = probe_id;
    mydev->last_id = probe_id;
    mydev->sequence = 0;

    com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROBE_PARAM_SAVE, &probe_id, sizeof(probe_id));

#endif
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROVISION_DONE, NULL, 0);
}

static uint16_t send_control_req( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
#ifdef EMOS_SUPPORT_WIRE_HOST
    rak_probe_wire_protocol_t *wp = NULL;
    com_if_rak_probe_sensor_t * snsr_info;
    com_if_rak_probe_param_t * probe_info;
    uint16_t plen = 0;
    uint8_t probe_id = 0;
    uint8_t probe_cnt = 0;
    uint8_t snsr_cnt = 0;

    plen = data_len;
    tlen += plen;
    
    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;

    memcpy(wp->header.prb.payload, data, data_len);
    wp->header.prb.payload_length = plen;
#else
    *isc = EMOS_FAIL;
#endif
    return tlen;
}

static uint16_t send_control_rsp( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp = NULL;
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    com_if_rak_probe_param_t * probe_info;
    uint16_t plen = 0;
    uint8_t ret = 0;

    plen = data_len;
    tlen += plen;
    
    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;

    memcpy(wp->header.prb.payload, data, data_len);
    wp->header.prb.payload_length = plen;
    
    ret = com_get_probe(pid,&probe_info);
    if ( ret != EMOS_OK )
    {
        *isc = EMOS_FAIL;
        goto EXIT_FUNC;
    }

    if (probe_info->sta.real != IENABLE)
    {
        *isc = EMOS_FAIL;
        goto EXIT_FUNC;
    }
    
    switch (*pay_type)
    {
        case RAK_PB_PAY_TYPE_CONTROL_REBOOT:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_TARGET_REBOOT, &pid, 1);
            break;
        case RAK_PB_PAY_TYPE_CONTROL_DFU:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_TARGET_DFU, &pid, 1);
            break;
        case RAK_PB_PAY_TYPE_CONTROL_RESTORE:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_TARGET_REST, &pid, 1);
            break;
        case RAK_PB_PAY_TYPE_CONTROL_REMOVE:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_TARGET_KILL, &pid, 1);
            break;
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }
EXIT_FUNC:
    return tlen;
}

static uint16_t send_snsrdata_req( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp = NULL;
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint16_t plen = 0;

    plen = data_len;
    tlen += plen;
    
    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;

    memcpy(wp->header.prb.payload, data, data_len);
    wp->header.prb.payload_length = plen;

    return tlen;
}

static uint16_t send_ymodem_rsp( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
#ifdef EMOS_SUPPORT_YMODEM
    rak_probe_wire_protocol_t *wp;
    wp = *msg;
    rak_probe_protocol_ie_ymodem_data_t *ydata;
    static uint16_t plen = 0;
    
    static uint8_t ret,cnt,next,skip_payload,errors=0; 
    static char  spf[16];
    static char * filename = "a.bin";
    static uint16_t packet_size = 0;
    static uint32_t filesize;
    static uint8_t * bin;
    uint8_t cmd_type = wp->ymodem.cmd;

    typedef struct
    {
        uint8_t active;
        uint8_t start;
        uint8_t block;
        uint8_t block_neg;
    } ymodem_flag_t;
    static ymodem_flag_t yflag;

    enum
    {
        YMDM_PAYID_PROBE,
        YMDM_PAYID_PROBEIO,
    } YMDM_PAYID_E;

    switch(cmd_type)
    {
        case X_ACK:
            skip_payload = 0;
            break;
        case X_NAK:
            skip_payload = 1;

            if(yflag.active == IENABLE)
            {
                yflag.block++;
                break;
            }

        case X_C:
            skip_payload = 1;
            yflag.active = IENABLE;
            plen = sizeof(rak_probe_protocol_ie_ymodem_data_t) + 1;

            
            switch (*pay_type)
            {
                case YMDM_PAYID_PROBEIO:
                case YMDM_PAYID_PROBE:
                    do
                    {
                        filesize = com.flashmap->dfu_size_get();
                        com.flashmap->dfu_field_read(1,&bin);
                    } while (0);
                    
                    break;
                default:
                    *isc = EMOS_FAIL;
                    goto EXIT_FUNC;
            }

            yflag.start = X_STX;
            break;
        default:
            skip_payload = 1;
            break;
    }

    tlen += plen;
    
    if (yflag.active == IDISABLE)
    {
        *isc = EMOS_FAIL;
        goto EXIT_FUNC;
    }

    *msg = com_malloc(tlen);
    wp = *msg;
    ydata = (rak_probe_protocol_ie_ymodem_data_t *)wp->header.prb.payload;
    
    if(cmd_type == X_C)
    {
        ydata->unused = 0xee;
        yflag.block = 0x00;
        strncpy((char *) ydata->payload, filename, sizeof(ydata->payload));
        strcpy( (char *) ydata->payload + strlen(filename) + 1 , spf ); 
    }

    if(!!filesize)
    {
        if(!skip_payload)
        {
            packet_size = min(filesize, sizeof(ydata->payload));
            memcpy(ydata->payload, bin, packet_size);
            memset(ydata->payload + packet_size, 0x1A, sizeof(ydata->payload) - packet_size);
        }

        if (!skip_payload) 
        {
            yflag.block++;// = 0x00;
            filesize -= packet_size;
            bin += packet_size;
        }

        sprintf(spf, "%ld", filesize);
        SYS_INFO("filesize: %d\r\n",filesize);
    }
    else
    {
        yflag.start = X_EOF;
        yflag.block++;

        yflag.active = IDISABLE;
        // memset((void *)&yflag, 0, sizeof(ymodem_flag_t));
    }

    ydata->start = yflag.start;
    ydata->block = yflag.block;
    ydata->crc = U16T_SWAP(emos.units->builtin.crc16(ydata->payload, sizeof(ydata->payload)));
    ydata->block_neg = 0xff - ydata->block;

    *pay_type = RAK_PB_PAY_TPYE_CONTROL;
    wp->header.prb.payload_length = plen;

EXIT_FUNC:
#else
    *isc = EMOS_FAIL;
#endif
    return tlen;
}

static uint16_t send_snsrdata_rsp( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp;
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t tid = PROBE_ID_DEF;

    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;
    tid = wp->header.prb.source;
    com_vsys_mrsend( COM_TASK_EVENT, COM_EVENT_SNSR_DATREQ,&tid,sizeof(tid), wp->header.prb.payload, wp->header.prb.payload_length);

    memset(wp->header.prb.payload, 0, wp->header.prb.payload_length);

    wp->header.prb.payload_length = 0;
    return tlen;
}

static uint16_t send_paramset_rsp( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp = NULL;
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint16_t plen = 0;
    uint16_t this_len = 0;
    com_if_rak_probe_sensor_t *snsr_info;
    com_if_rak_probe_param_t * probe_info;
    uint8_t ret = 0;
    
    switch (*pay_type)
    {
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            this_len = function_get_paramlen(*pay_type);
            plen += sizeof(rak_probe_protocol_ie_param_sensor_h_t);
            plen += sizeof(rak_probe_protocol_ie_param_sensor2_t);
            plen += this_len;
            break;
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }

    tlen += plen;

    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;

    com_get_probe(pid,&probe_info);
    if ( ret == EMOS_OK )
    {
        if (probe_info->sta.real != IENABLE)
        {
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
        }
    }
    
    sid = wp->v2.snsr_header.id;
    com_get_snsr(pid,sid,&snsr_info);
    if ( ret != EMOS_OK )
    {
        switch (*pay_type)
        {
            case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
            case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
                break;
            
            default:
                *isc = EMOS_FAIL;
                goto EXIT_FUNC;
        }
    }

    uint8_t *pdata = wp->v2.snsr_get.data;
    uint8_t pdata_len = wp->v2.snsr_get.len;
    do
    {
        
        if (keep_sequence == wp->header.prb.sequence)
        {
            break;
        }

        switch (*pay_type)
        {
            // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
            // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
            case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
                do
                {
                    uint32_t new_intv;
                    memcpy(&(new_intv),pdata,pdata_len);
                    com_vsys_mrsend( COM_TASK_EVENT, COM_EVENT_SYS_TARGET_INTV, &pid, 1, pdata, pdata_len);
                } while (0);
                goto PRB_SAVE;

            case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
                memcpy(&(probe_info->intv),pdata,pdata_len);
                goto PRB_SAVE;

            PRB_SAVE:
                com_vsys_send( COM_TASK_EVENT, COM_EVENT_NEXT_WAKEUP, NULL, 0);
                com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROBE_PARAM_SAVE, &pid, sizeof(pid));
                
                break;

            case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
                memcpy(&(snsr_info->ctrl.rule),pdata,pdata_len);
                goto SNSR_SAVE;

            case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
                memcpy(&(snsr_info->intv),pdata,pdata_len);
                com_vsys_send( COM_TASK_EVENT, COM_EVENT_NEXT_WAKEUP, NULL, 0);
                goto SNSR_SAVE;

            case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
                memcpy(snsr_info->thr.above,pdata,pdata_len);
                goto SNSR_SAVE;

            case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
                memcpy(snsr_info->thr.below,pdata,pdata_len);
                goto SNSR_SAVE;
            
            SNSR_SAVE:
                com_vsys_mrsend( COM_TASK_EVENT, COM_EVENT_SENSOR_PARAM_SAVE, &pid, sizeof(pid), &sid, sizeof(sid));
                break;
            case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
            case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
            case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
            case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
            case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
            default:
                *isc = EMOS_FAIL;
                goto EXIT_FUNC;
        }
    } while (0);
    
    wp->header.prb.payload_length = plen;
EXIT_FUNC:

    return tlen;
}

static uint16_t send_paramset_req( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
#ifdef EMOS_SUPPORT_WIRE_HOST
    rak_probe_wire_protocol_t *wp = NULL;
    uint16_t plen = 0;
    uint16_t this_len = 0;
    
    switch (*pay_type)
    {
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            this_len = function_get_paramlen(*pay_type);

            if (data_len < this_len)
            {
                goto BAD_CASE;
            }
            
            plen += sizeof(rak_probe_protocol_ie_param_sensor_h_t);
            plen += sizeof(rak_probe_protocol_ie_param_sensor2_t);
            plen += this_len;
            break;
        BAD_CASE:
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }

    tlen += plen;

    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;

    wp->v2.snsr_header.id = sid;
    switch (*pay_type)
    {
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            wp->v2.snsr_get.len = this_len;
            memcpy(wp->v2.snsr_get.data, data, this_len);
            break;
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }
    wp->header.prb.payload_length = plen;
EXIT_FUNC:
#else
    *isc = EMOS_FAIL;
#endif
    return tlen;
}

static uint16_t send_paramget_rsp( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp = NULL;
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    com_if_rak_probe_sensor_t *snsr_info;
    com_if_rak_probe_param_t * probe_info;
    UNUSED(probe_info);
    UNUSED(snsr_info);
    uint8_t snsr_cnt = 0;
    uint8_t this_pid;
    uint8_t this_sid = sid;
    uint8_t this_len = 0;
    uint16_t paylen = 0;

    if ( com_iprobe != NULL )
    {
        this_pid = com_iprobe->id;
        snsr_cnt = com_sprobe(this_pid).snsr_count();
    }
    else
    {
        *isc = EMOS_FAIL;
        goto EXIT_FUNC;
    }
    
    // DBG_INFO("-->%02x\r\n",sid);

    switch (*pay_type)
    {
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
            this_len = snsr_cnt;
            goto LAST_SWITCH;
        
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            this_len = function_get_paramlen(*pay_type);

        LAST_SWITCH:
            paylen += sizeof(rak_probe_protocol_ie_param_sensor_h_t);
            paylen += sizeof(rak_probe_protocol_ie_param_sensor2_t);
            paylen += this_len;
            break;
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
            paylen += sizeof(rak_probe_protocol_ie_param_probe_t);
            break;
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
            paylen += sizeof(rak_probe_protocol_ie_param_sensor_t) + sizeof(rak_probe_protocol_ie_param_sensor_h_t);
            break;
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }

    tlen += paylen;
    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    
    wp = *msg;
    
    switch (*pay_type)
    {
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
            break;
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
            break;
        
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
            do
            {
                com_get_probe(this_pid,&probe_info);
                wp->v2.snsr_get.len = this_len;
                memcpy(wp->v2.snsr_get.data,&(probe_info->intv),wp->v2.snsr_get.len);
            } while (0);
            
            break;

        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
            do
            {
                com_if_vsys_mydev_t * mydev = com_mydev();
                wp->v2.snsr_get.len = this_len;
                memcpy(wp->v2.snsr_get.data,&(mydev->sampling_interval),wp->v2.snsr_get.len);
            }while(0);
            break;

        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
            do
            {
                wp->v2.snsr_get.len = this_len;
                for (size_t i = 0; i < snsr_cnt; i++)
                {
                    com_sprobe(this_pid).snsr_walk(i,&snsr_info);

                    if (snsr_info->sta.stop == IENABLE)
                    {
                        wp->v2.snsr_get.len--;
                        continue;
                    }
                    
                    wp->v2.snsr_get.data[i] = snsr_info->id;
                }
            }while(0);
            
            break;
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
            do
            {
                if(com_get_snsr(this_pid,this_sid,&snsr_info) != EMOS_OK)
                {
                    goto BAD_PARAM;
                }
                
                wp->v2.snsr_get.len = this_len;
                memcpy(wp->v2.snsr_get.data,&(snsr_info->ctrl.type),wp->v2.snsr_get.len);
                
            }while(0);
            break;

        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
            
            do
            {
                if(com_get_snsr(this_pid,this_sid,&snsr_info) != EMOS_OK)
                {
                    goto BAD_PARAM;
                }

                if(snsr_info->sta.stop == IENABLE )
                {
                    goto BAD_PARAM;
                }

                wp->v2.snsr_get.len = this_len;
                memcpy(wp->v2.snsr_get.data,&(snsr_info->intv),wp->v2.snsr_get.len);
                
            }while(0);
            break;
        
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
            
            do
            {
                if(com_get_snsr(this_pid,this_sid,&snsr_info) != EMOS_OK)
                {
                    goto BAD_PARAM;
                }

                wp->v2.snsr_get.len = this_len;
                memcpy(wp->v2.snsr_get.data,&(snsr_info->ctrl.rule),wp->v2.snsr_get.len);
                
            }while(0);
            break;
        
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:

            do
            {
                if(com_get_snsr(this_pid,this_sid,&snsr_info) != EMOS_OK)
                {
                    goto BAD_PARAM;
                }
                
                wp->v2.snsr_get.len = this_len;
                memcpy(wp->v2.snsr_get.data,&(snsr_info->data),wp->v2.snsr_get.len);

                /* data swap for little endian*/
                com_if_rak_probe_sensor_data_t *pdata = (com_if_rak_probe_sensor_data_t *)(wp->v2.snsr_get.data);
                emos_memswap(pdata->value,snsr_info->data.len);
                
            }while(0);
            break;

        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
            
            do
            {
                if(com_get_snsr(this_pid,this_sid,&snsr_info) != EMOS_OK)
                {
                    goto BAD_PARAM;
                }

                wp->v2.snsr_get.len = (snsr_info->data.len)<<1;
                memcpy(wp->v2.snsr_get.data,(snsr_info->thr.above), wp->v2.snsr_get.len);
                
            }while(0);

            break;
        
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            
            do
            {
                if(com_get_snsr(this_pid,this_sid,&snsr_info) != EMOS_OK)
                {
                    goto BAD_PARAM;
                }

                wp->v2.snsr_get.len = (snsr_info->data.len)<<1;
                memcpy(wp->v2.snsr_get.data,(snsr_info->thr.below), wp->v2.snsr_get.len);
            }while(0);
            
            break;
        BAD_PARAM:
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }
    wp->header.prb.payload_length = paylen;
EXIT_FUNC:
    return tlen;
}

static uint16_t send_paramget_req( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
#ifdef EMOS_SUPPORT_WIRE_HOST
    rak_probe_wire_protocol_t *wp = NULL;
    uint16_t plen = 0;
    switch (*pay_type)
    {
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            plen += sizeof(rak_probe_protocol_ie_param_sensor_h_t);
            plen += sizeof(rak_probe_protocol_ie_param_sensor2_t);
            break;
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
            plen += sizeof(rak_probe_protocol_ie_param_probe_t);
            break;
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
            plen += sizeof(rak_probe_protocol_ie_param_sensor_t) + sizeof(rak_probe_protocol_ie_param_sensor_h_t);
            break;
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }
    tlen += plen;
    
    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;

    wp->v2.snsr_header.id = sid;
    switch (*pay_type)
    {
        case RAK_PB_PAY_TYPE_PARAM_PRB_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SMPL_INTV:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGID:
        // case RAK_PB_PAY_TYPE_PARAM_PRB_TAGEN:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_RULE:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INTV:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_CNT:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_INFO:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_DATA:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR:
        case RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR:
            wp->v2.snsr_get.len = 0;
            break;
        case RAK_PB_PAY_TYPE_PARAM_PRB_UPDATE:
            break;
        case RAK_PB_PAY_TYPE_PARAM_SNSR_UPDATE:
            break;
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }
    wp->header.prb.payload_length = plen;
EXIT_FUNC:

#else
    *isc = EMOS_FAIL;
#endif
    return tlen;
}

static uint16_t send_provision_req( rak_probe_wire_protocol_t **msg , uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp = NULL;
    com_if_vsys_mydev_t * mydev = com_mydev();
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint16_t plen = 0;
    wp = *msg;
    switch (*pay_type)
    {
        case RAK_PB_PAY_TPYE_PRV_VER:
        case RAK_PB_PAY_TPYE_PRV_VER2:
            plen += sizeof(rak_probe_protocol_ie_version_t);
            break;
        case RAK_PB_PAY_TPYE_PRV_HELLO:
        case RAK_PB_PAY_TPYE_PRV_VER3:
            plen += sizeof(rak_probe_protocol_ie_version3_t);
            break;
        case RAK_PB_PAY_TPYE_PRV_VER4:
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }
    tlen += plen;

    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;

    switch (*pay_type)
    {
        case RAK_PB_PAY_TPYE_PRV_VER:
        case RAK_PB_PAY_TPYE_PRV_VER2:
            wp->prov_v1.hw_version    = mydev->hw_version;
            wp->prov_v1.probe_id      = mydev->last_id;
            wp->prov_v1.sw_version[0] = mydev->sw_version[0];
            wp->prov_v1.sw_version[1] = mydev->sw_version[1];
            wp->prov_v1.sw_version[2] = mydev->sw_version[2];
            memcpy(wp->prov_v1.sn.value, mydev->ssn.value, sizeof(mydev->ssn.value));
            memcpy(wp->prov_v1.model_name,mydev->model_name,sizeof(mydev->model_name));
            memcpy(&(wp->prov_v1.type_id),mydev->iprobe->type_id.current.bytes,1);
            break;
        case RAK_PB_PAY_TPYE_PRV_HELLO:
            break;
        case RAK_PB_PAY_TPYE_PRV_VER3:
            wp->prov_v3.hw_version    = mydev->hw_version;
            wp->prov_v3.probe_id      = mydev->last_id;
            wp->prov_v3.sw_version[0] = mydev->sw_version[0];
            wp->prov_v3.sw_version[1] = mydev->sw_version[1];
            wp->prov_v3.sw_version[2] = mydev->sw_version[2];
            memcpy(wp->prov_v3.sn.value, mydev->ssn.value, sizeof(mydev->ssn.value));
            memcpy(wp->prov_v3.model_name,mydev->model_name,sizeof(mydev->model_name));
            memcpy(wp->prov_v3.type_id.bytes,mydev->iprobe->type_id.current.bytes,4);
            break;
        case RAK_PB_PAY_TPYE_PRV_VER4:
        default:
            *isc = EMOS_FAIL;
            goto EXIT_FUNC;
    }
    wp->header.prb.payload_length = plen;
EXIT_FUNC:

    return tlen;
}

static __RAKLINK_UNUSED uint8_t func_inquery_probeid(uint8_t probe_id, uint8_t *sn, uint16_t sn_len)
{
    com_if_rak_probe_param_t * probe_info;
    uint8_t this_probe_id = probe_id;
    uint8_t ret = EMOS_OK;
    uint8_t probe_cnt = com_sprobe(0).probe_count();

    for(uint8_t i=0; i<probe_cnt; i++)
    {
        com_sprobe(i).probe_walk(&probe_info);
        if(memcmp(probe_info->sn.value, sn, sn_len) == 0)
        {
            this_probe_id = probe_info->id;
            goto EXIT_FUNC;
        }
    }

    if( this_probe_id == 0 )
    {
        this_probe_id = 1;
    }

    ret = com_get_probe(this_probe_id,&probe_info);
    if ( ret == EMOS_OK )
    {
        this_probe_id = PROBE_ID_DEF;
    }

    if (this_probe_id == PROBE_ID_DEF)
    {
        this_probe_id = 1;
    }

    for(uint8_t pid = this_probe_id; pid < PROBE_ID_LIMIT ; pid++)
    {
        
        com_if_rak_probe_param_t * tmp;
        ret = com_get_probe(pid,&tmp);
        if (ret)
        {
            this_probe_id = pid;
            break;
        }
        
    }

    if ( this_probe_id < PROBE_ID_LIMIT )
    {
        com_sprobe(this_probe_id).new_probe();
        // ret = com_sprobe(this_probe_id).probe_get(&probe_info);
    }
    else
    {
        this_probe_id = PROBE_ID_NOID;
        // ver->probe_id = this_probe_id;
        // goto FUNC_EXIT;
    }
EXIT_FUNC:

    if (this_probe_id == 0)
    {
        this_probe_id = PROBE_ID_NOID;
    }
    
    return this_probe_id;
}

static uint16_t send_provision_rsp( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
#ifdef EMOS_SUPPORT_WIRE_HOST
    rak_probe_wire_protocol_t *wp = NULL;
    com_if_rak_probe_param_t * probe_info;
    uint16_t plen = 0;
    uint8_t ret = 0;

    wp = *msg;
    switch (*pay_type)
    {
        case RAK_PB_PAY_TPYE_PRV_VER:
        case RAK_PB_PAY_TPYE_PRV_VER2:
            plen += sizeof(rak_probe_protocol_ie_version_t);
            break;
        case RAK_PB_PAY_TPYE_PRV_HELLO:
            goto EXIT_FUNC;
        case RAK_PB_PAY_TPYE_PRV_VER3:
            plen += sizeof(rak_probe_protocol_ie_version3_t);
            break;
        case RAK_PB_PAY_TPYE_PRV_VER4:
        default:
            goto EXIT_FUNC;
    }
    tlen += plen;

    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
        wp = *msg;
    }

    uint8_t this_probe_id = 0;

    uint8_t this_sn[30];
    uint8_t this_sn_len = 0;
    switch (*pay_type)
    {
        case RAK_PB_PAY_TPYE_PRV_VER:
        case RAK_PB_PAY_TPYE_PRV_VER2:
            do
            {
                rak_probe_protocol_ie_version_t * pver;
                pver = &(wp->prov_v1);
                
                this_probe_id=pver->probe_id;
                this_sn_len = sizeof(pver->sn.value);
                memcpy(this_sn,pver->sn.value,this_sn_len);
                this_probe_id = func_inquery_probeid(this_probe_id,this_sn,this_sn_len);                
                pver->probe_id=this_probe_id;
                com_get_probe(this_probe_id,&probe_info);
                memcpy(probe_info->model_name ,pver->model_name ,sizeof(probe_info->model_name));
                memcpy(probe_info->sn.value   ,pver->sn.value   ,sizeof(probe_info->sn.value));
                memcpy(probe_info->ssn.value  ,pver->sn.value   ,sizeof(probe_info->ssn.value));
                memcpy(probe_info->sw_version ,pver->sw_version ,sizeof(probe_info->sw_version));
                memcpy(probe_info->type_id.current.bytes,&(pver->type_id),1);
                
                probe_info->hw_version = pver->hw_version;
                probe_info->id = this_probe_id;
            } while (0);

            break;
        case RAK_PB_PAY_TPYE_PRV_VER3:
            do
            {
                rak_probe_protocol_ie_version3_t * pver;
                pver = &(wp->prov_v3);

                this_probe_id=pver->probe_id;
                this_sn_len = sizeof(pver->sn.value);
                memcpy(this_sn,pver->sn.value,this_sn_len);
                this_probe_id = func_inquery_probeid(this_probe_id,this_sn,this_sn_len);
                pver->probe_id=this_probe_id;
                com_get_probe(this_probe_id,&probe_info);
                memcpy(probe_info->model_name ,pver->model_name  ,sizeof(probe_info->model_name));
                memcpy(probe_info->sn.value   ,pver->sn.value    ,sizeof(probe_info->sn.value));
                memcpy(probe_info->ssn.value  ,pver->sn.value    ,sizeof(probe_info->ssn.value));
                memcpy(probe_info->sw_version ,pver->sw_version  ,sizeof(probe_info->sw_version));
                memcpy(probe_info->type_id.current.bytes,pver->type_id.bytes,4);

                probe_info->hw_version = pver->hw_version;
                probe_info->id = this_probe_id;

            } while (0);
            break;
        case RAK_PB_PAY_TPYE_PRV_HELLO:
        case RAK_PB_PAY_TPYE_PRV_VER4:
        default:
            *isc = EMOS_FAIL;
            break;
    }
    probe_info->sta.stop = IDISABLE;
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROBE_ONLINE, &this_probe_id, sizeof(this_probe_id));
    wp->header.prb.payload_length = plen;
    return tlen;
EXIT_FUNC:
#else
    *isc = EMOS_FAIL;
#endif
    return tlen;
}

static uint16_t send_scmd_req( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp = NULL;
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint16_t plen = 0;

    plen = data_len;
    tlen += plen;
    
    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;

    memcpy(wp->header.prb.payload, data, data_len);
    wp->header.prb.payload_length = plen;

    return tlen;
}

static void recv_scmd_req( uint8_t * data, uint16_t len)
{
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);
    uint8_t ftype = wp->header.prb.frame_type;
    uint8_t ptype = wp->header.prb.payload_type;
    uint8_t plen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t target_id = PROBE_ID_DEF;
    uint8_t my_id = 0;
    uint8_t snsr_id = 0;

    if (len < plen)
    {
        return;
    }

    target_id = wp->header.prb.source;
    my_id = wp->header.prb.destlination;

    mdl_co_rak_wire_protocol_send_rsp_withsrc(data,len,my_id,target_id,snsr_id,ftype,ptype);
}

static void recv_disp_req( uint8_t * data, uint16_t len)
{
    rak_probe_wire_protocol_t *wp = (rak_probe_wire_protocol_t *)(data);
    uint8_t ftype = wp->header.prb.frame_type;
    uint8_t ptype = wp->header.prb.payload_type;
    uint8_t plen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t target_id = PROBE_ID_DEF;
    uint8_t my_id = 0;
    uint8_t snsr_id = 0;

    if (len < plen)
    {
        return;
    }

    target_id = wp->header.prb.source;
    my_id = wp->header.prb.destlination;

    mdl_co_rak_wire_protocol_send_rsp_withsrc(data,len,my_id,target_id,snsr_id,ftype,ptype);
}

static uint16_t send_disp_req( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp = NULL;
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;


    uint16_t plen = 0;

    plen = data_len;
    tlen += plen;
    
    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;

    memcpy(wp->header.prb.payload, data, data_len);
    wp->header.prb.payload_length = plen;

    return tlen;
}

static uint16_t send_disp_rsp( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp;
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t probe_id = PROBE_ID_DEF;
    UNUSED(probe_id);

    uint8_t tid = PROBE_ID_DEF;
    UNUSED(tid);

    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;
    tid = wp->header.prb.source;

    SYS_INFOA(wp->header.prb.payload, wp->header.prb.payload_length);

    memset(wp->header.prb.payload, 0, wp->header.prb.payload_length);

    wp->header.prb.payload_length = 0;
    return tlen;
}

static uint16_t scmd_offset = 0;
static uint16_t lastest_tlv_len = 0;
static uint8_t find_tlv_cb( uint16_t len, uint8_t *type, uint8_t *data )
{
    lastest_tlv_len = (len + 3);
    scmd_offset+=(len + 3);
    
    return EMOS_OK;
}


static uint16_t send_scmd_rsp( rak_probe_wire_protocol_t **msg ,uint16_t msg_len, uint8_t pid, uint8_t sid,uint8_t *pay_type,uint8_t *data,uint16_t data_len,uint8_t *isc)
{
    rak_probe_wire_protocol_t *wp;
    volatile uint16_t tlen = sizeof(rak_probe_wire_protocol_header_t) + 1/*crc*/;
    uint8_t probe_id = PROBE_ID_DEF;
    UNUSED(probe_id);

    uint8_t tid = PROBE_ID_DEF;
    UNUSED(tid);

    #define PAGE_SIZE  1000

    if (*msg == NULL)
    {
        *msg = com_malloc(tlen);
    }
    else
    {
        *msg = com_copy(tlen,*msg,msg_len);
    }
    wp = *msg;
    tid = wp->header.prb.source;

    uint8_t *pdata = wp->header.prb.payload;
    uint8_t pdata_len = wp->header.prb.payload_length;
    do
    {
        if (keep_sequence == wp->header.prb.sequence)
        {
            break;
        }
    
        switch (*pay_type)
        {
            case RAK_PB_PAY_TYPE_SCMD_BUILD:
                com_vsys_send( COM_TASK_EVENT, COM_EVENT_SNSR_SYNCSTART, NULL, 0);
                
                break;
            case RAK_PB_PAY_TYPE_SCMD_ADD:

                do
                {
                    uint8_t *ptmp;
                    com_flash_bank4_readonly(&ptmp,PAGE_SIZE);

                    scmd_offset = 0;
                    com_tlv_decode(ptmp, PAGE_SIZE, find_tlv_cb );

                    com_flash_bank4_write(scmd_offset,pdata,pdata_len);
                    uint8_t end = 0;
                    com_flash_bank4_write(pdata_len + scmd_offset ,&end,1);

                } while (0);
                
                break;
            
            case RAK_PB_PAY_TYPE_SCMD_DEL:

                do
                {
                    uint8_t *ptmp;
                    com_flash_bank4_readonly(&ptmp,PAGE_SIZE);

                    scmd_offset = 0;
                    com_tlv_decode(ptmp, PAGE_SIZE, find_tlv_cb );
                    uint8_t end = 0;
                    com_flash_bank4_write( scmd_offset - lastest_tlv_len ,&end,1);
                    
                } while (0);

                break;
            
            case RAK_PB_PAY_TYPE_SCMD_LIST:
                
                com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_SCMD_DUMP, NULL,0);

                break;
            
            case RAK_PB_PAY_TYPE_SCMD_REST:
                do
                {
                    uint8_t tmp = 0;
                    com_flash_bank4_write(0, &tmp,sizeof(tmp));
                    
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_SNSR_REST, NULL,0);
                } while (0);
                
                break;
            default:
                *isc = EMOS_FAIL;
                goto EXIT_FUNC;
        }
    } while (0);


    memset(wp->header.prb.payload, 0, wp->header.prb.payload_length);
    
EXIT_FUNC:
    wp->header.prb.payload_length = 0;
    return tlen;
}

static void com_mdl_rak_wire_protocol_check(uint8_t *msg_data, uint16_t msg_data_len)
{    
    rak_hub_protocol_frame_t *hub_packet = NULL;
    
    uint8_t hub_header_size = sizeof(rak_hub_protocol_frame_t);
    uint8_t offset = 0;
FUNC_RESET:

    for( offset=0; offset<msg_data_len; offset++)
    {
        if(msg_data[offset] == DELIMTER)
        {
            hub_packet = (rak_hub_protocol_frame_t *)(msg_data + offset);
            break;
        }
    }
    
    if (hub_packet == NULL)
    {
        return;
    }
    
    do
    {
        if (hub_packet->start_delimiter != DELIMTER)
        {
            break;
        }

        if (msg_data_len < hub_header_size)
        {
            break;
        }
        
        uint8_t recv_len = SHORT_SWAP(hub_packet->length.msb,hub_packet->length.lsb);
        uint8_t cur_chsum = msg_data[hub_header_size + recv_len + offset];
        uint8_t chsum = 0;
        chsum += emos_popcount(hub_packet->frame_type);
        chsum += emos_popcount(hub_packet->flag);

        for (int i = 0 ; i < recv_len ; i++) 
        {
            chsum += emos_popcount(hub_packet->payload[i]);
        }
        
        if (cur_chsum != chsum)
        {
            uint8_t crc_buf[2];
            crc_buf[0] = cur_chsum;
            crc_buf[1] = chsum;
            UNUSED(crc_buf);
            com_vsys_send( COM_USER_EVENT, USER_EVENT_OWI_CRC_ERROR, NULL, 0);
            return;
        }
        
        msg_data[hub_header_size + recv_len + offset] = 0;  //clean CRC

        if (hub_packet->frame_type == RAK_HUB_FRM_TYPE_SENSORHUB)
        {
            com_vsys_mrsend(COM_TASK_RAK_PROTOCOL, COM_VSYS_RAK_PCL_COMMAND, (uint8_t *)&wakeup_byte, 1,(uint8_t *)hub_packet, recv_len + hub_header_size + 1/*CRC*/);
        }

        if (hub_packet->frame_type == RAK_HUB_FRM_TYPE_ATCMD)
        {
            com_source_id = COM_EVENT_RUIATREQ;
            switch (hub_packet->flag)
            {
                case RAK_HUB_FLG_REQ:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_CLI_COMMAND,hub_packet->payload,recv_len);
                    break;
                default:
                case RAK_HUB_FLG_RSP:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_WIRP_RUI_RECV,hub_packet->payload,recv_len);
                    break;
            }
            
        }

        if( msg_data_len > (hub_header_size + recv_len))
        {
            msg_data = msg_data + (hub_header_size + recv_len + 1/*CRC*/ + offset);
            msg_data_len -= (hub_header_size + recv_len + 1/*CRC*/ + offset);
            goto FUNC_RESET;
        }
        break;
    } while (0);
}


static void com_mdl_rak_wire_protocol_command(uint8_t *msg_data, uint16_t msg_data_len)
{
    uint8_t fm_type = 0;
    uint8_t pb_type = 0;
    rak_probe_wire_protocol_t *wire_packet = (rak_probe_wire_protocol_t *)(msg_data);

    com_if_vsys_mydev_t * mydev = com_mydev();
    if(wire_packet->header.rui.flag >= (uint16_t)RAK_PB_FRM_TYPE_MAX)
    {
        return;
    }

    if(wire_packet->header.prb.destlination != mydev->id)
    {
        com_if_rak_probe_param_t * probe_info;
        mdl_co_rak_chk_probe_cb( &probe_info, 
        {
            if ( probe_info->id == wire_packet->header.prb.destlination)
            {
                goto FUNC_NEXT;
            }
        });
        goto FUNC_EXIT;
    }
    goto FUNC_NEXT;
FUNC_NEXT:
    msg_data_len++; //add crc byte

    fm_type = wire_packet->header.prb.frame_type;
    pb_type = wire_packet->header.prb.payload_type;
    switch (wire_packet->header.rui.flag)
    {
        case RAK_HUB_FLG_REQ:
            recv_cmd_entry[fm_type].rxreq(msg_data,msg_data_len);
            
            keep_sequence = wire_packet->header.prb.sequence;
            break;
        case RAK_HUB_FLG_RSP:
            recv_cmd_entry[fm_type].rxrsp(msg_data,msg_data_len);
            mydev->sequence++;

            do
            {
                uint16_t rsp_type = SHORT(fm_type,pb_type);
                com_vsys_mrsend( COM_TASK_EVENT, COM_EVENT_WIRP_RSP, &rsp_type, sizeof(rsp_type),hdshk_box.msg,hdshk_box.len);
            } while (0);
            break;
        default:
            break;
    }
FUNC_EXIT:

    switch (wire_packet->header.prb.frame_type)
    {
        case RAK_PB_FRM_TYPE_YMDM:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_DFUMODE, NULL, 0);
            break;
        default:
            break;
    }
    
}

static uint8_t mdl_co_rak_rui_protocol_send(rak_rui_wire_protocol_t *src, 
                                            uint16_t msg_len, 
                                            uint8_t dir, 
                                            uint8_t *data,
                                            uint16_t data_len)
{
    
    volatile uint16_t tlen = sizeof(rak_rui_wire_protocol_t) + 1/*crc*/;
    rak_rui_wire_protocol_t *wp = src;
    uint8_t *tp = NULL;
    uint8_t dir_flag = 0;
    uint8_t fret = EMOS_OK;
    uint16_t pktlen;

    switch (dir)
    {
        case WIRE_SEND_REQ:
            dir_flag = RAK_HUB_FLG_REQ;
            break;
        case WIRE_SEND_RSP:
            dir_flag = RAK_HUB_FLG_RSP;
            break;
        default:
            fret = EMOS_FAIL;
            goto EXIT_FUNC;
    }
    
    tlen = tlen + data_len;
    pktlen = tlen;

    if (wp == NULL)
    {
        wp = com_malloc(tlen);
    }

    wp->header.wakeup     = 0xff;
    wp->header.rui.start_delimiter = DELIMTER;
    wp->header.rui.frame_type    = RAK_HUB_FRM_TYPE_ATCMD;
    wp->header.rui.flag          = dir_flag;
    uint16_t rui_len = pktlen - (sizeof(rak_hub_protocol_frame_t) + 1/* no wakeup byte*/) ;
    rui_len = rui_len -1 /* no crc byte*/;
    wp->header.rui.length.value  = rui_len;
    wp->header.rui.length.value  = SHORT_SWAP(wp->header.rui.length.msb,wp->header.rui.length.lsb);
    memcpy(wp->header.rui.payload, data, data_len);

    uint8_t chsum = 0;
    chsum += emos_popcount(wp->header.rui.frame_type);
    chsum += emos_popcount(wp->header.rui.flag);

    for (int i = 0 ; i < rui_len ; i++) 
    {
        chsum += emos_popcount(wp->header.rui.payload[i]);
    }
    
    tp = (uint8_t *)wp;
    tp[pktlen-1] = chsum;

    com_if_rak_probe_param_t * probe_info;
    mdl_co_rak_chk_probe_cb( &probe_info, 
    {
        com_vsys_send( COM_TASK_EVENT, COM_EVENT_WIRP_RECV, tp, pktlen);
    });
    
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_WIRP_TRANS, tp, pktlen);

EXIT_FUNC:
    com_free(tp);
    return fret;
}

static uint8_t mdl_co_rak_wire_protocol_send(rak_probe_wire_protocol_t *src, 
                                            uint16_t msg_len, 
                                            uint8_t dir, 
                                            uint8_t mid,
                                            uint8_t tid, 
                                            uint8_t sid, 
                                            uint8_t frm_type, 
                                            uint8_t pay_type, 
                                            uint8_t *data,
                                            uint16_t data_len)
{
    rak_probe_wire_protocol_t *wp = src;
    com_if_vsys_mydev_t * mydev = com_mydev();
    uint8_t *tp;
    uint8_t isc = EMOS_OK;
    uint8_t dir_flag = 0;
    uint8_t fret = EMOS_OK;
    uint16_t pktlen;
    
    switch (dir)
    {
        case WIRE_SEND_REQ:
            pktlen = send_cmd_entry[frm_type].txreq(&wp,msg_len,mid,sid,&pay_type,data,data_len,&isc);
            dir_flag = RAK_HUB_FLG_REQ;
            break;
        case WIRE_SEND_RSP:
            pktlen = send_cmd_entry[frm_type].txrsp(&wp,msg_len,mid,sid,&pay_type,data,data_len,&isc);
            dir_flag = RAK_HUB_FLG_RSP;
            break;
        default:
            fret = EMOS_FAIL;
            goto EXIT_FUNC;
    }
    
    if (isc != EMOS_OK)
    {
        fret = EMOS_FAIL;
        goto EXIT_FUNC;
    }
    
    wp->header.wakeup     = 0xff;
    wp->header.rui.start_delimiter = DELIMTER;
    wp->header.rui.frame_type    = RAK_HUB_FRM_TYPE_SENSORHUB;
    wp->header.rui.flag          = dir_flag;
    uint16_t rui_len = pktlen - (sizeof(rak_hub_protocol_frame_t) + 1/* no wakeup byte*/) ;
    rui_len = rui_len -1 /* no crc byte*/;
    wp->header.rui.length.value  = rui_len;
    wp->header.rui.length.value  = SHORT_SWAP(wp->header.rui.length.msb,wp->header.rui.length.lsb);
    wp->header.prb.destlination  = tid;
    wp->header.prb.source        = mid;
    wp->header.prb.sequence      = mydev->sequence;
    wp->header.prb.frame_type    = frm_type;
    wp->header.prb.payload_type  = pay_type;

    uint8_t chsum = 0;
    chsum += emos_popcount(wp->header.rui.frame_type);
    chsum += emos_popcount(wp->header.rui.flag);

    for (int i = 0 ; i < rui_len ; i++) 
    {
        chsum += emos_popcount(wp->header.rui.payload[i]);
    }

    tp = (uint8_t *)wp;
    tp[pktlen-1] = chsum;
        
    com_if_rak_probe_param_t * probe_info;
    mdl_co_rak_chk_probe_cb( &probe_info, 
    {
        if ( probe_info->id == tid || probe_info->id == mid)
        {
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_WIRP_RECV, tp, pktlen);
            goto EXIT_FUNC;
        }
    });

    com_vsys_send( COM_TASK_EVENT, COM_EVENT_WIRP_TRANS, tp, pktlen);
    // DBG_INFOA(tp,pktlen);
    // DBG_INFO("\r\n");
EXIT_FUNC:
    com_free(wp);
    return fret;
}


static const frame_command_entry_t recv_cmd_entry[] =
{
    [RAK_PB_FRM_TYPE_WAKUP]     = {.rxreq=this_recv_dummy     ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_PROVISION] = {.rxreq=recv_provision_req  ,.rxrsp=recv_provision_rsp},
    [RAK_PB_FRM_TYPE_PARAMSET]  = {.rxreq=recv_paramset_req   ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_SENDAT]    = {.rxreq=recv_sensordat_req  ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_CONTROL]   = {.rxreq=recv_control_req    ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_PARAMGET]  = {.rxreq=recv_paramget_req   ,.rxrsp=recv_paramget_rsp},
    [RAK_PB_FRM_TYPE_ALERT]     = {.rxreq=this_recv_dummy     ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_ERR]       = {.rxreq=this_recv_dummy     ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_ACK]       = {.rxreq=this_recv_dummy     ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_NACK]      = {.rxreq=this_recv_dummy     ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_YMDM]      = {.rxreq=recv_ymodem_req     ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_MODBUS]    = {.rxreq=this_recv_dummy     ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_IOC]       = {.rxreq=this_recv_dummy     ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_SCMD]      = {.rxreq=recv_scmd_req       ,.rxrsp=this_recv_dummy},
    [RAK_PB_FRM_TYPE_DISP]      = {.rxreq=recv_disp_req       ,.rxrsp=this_recv_dummy},
};

static const frame_send_entry_t send_cmd_entry[] =
{
    [RAK_PB_FRM_TYPE_WAKUP]     = {.txreq=this_send_dummy     ,.txrsp=this_send_dummy},
    [RAK_PB_FRM_TYPE_PROVISION] = {.txreq=send_provision_req  ,.txrsp=send_provision_rsp},
    [RAK_PB_FRM_TYPE_PARAMSET]  = {.txreq=send_paramset_req   ,.txrsp=send_paramset_rsp},
    [RAK_PB_FRM_TYPE_SENDAT]    = {.txreq=send_snsrdata_req   ,.txrsp=send_snsrdata_rsp},
    [RAK_PB_FRM_TYPE_CONTROL]   = {.txreq=send_control_req    ,.txrsp=send_control_rsp},
    [RAK_PB_FRM_TYPE_PARAMGET]  = {.txreq=send_paramget_req   ,.txrsp=send_paramget_rsp},
    [RAK_PB_FRM_TYPE_ALERT]     = {.txreq=this_send_dummy     ,.txrsp=this_send_dummy},
    [RAK_PB_FRM_TYPE_ERR]       = {.txreq=this_send_dummy     ,.txrsp=this_send_dummy},
    [RAK_PB_FRM_TYPE_ACK]       = {.txreq=this_send_dummy     ,.txrsp=this_send_dummy},
    [RAK_PB_FRM_TYPE_NACK]      = {.txreq=this_send_dummy     ,.txrsp=this_send_dummy},
    [RAK_PB_FRM_TYPE_YMDM]      = {.txreq=this_send_dummy     ,.txrsp=send_ymodem_rsp},
    [RAK_PB_FRM_TYPE_MODBUS]    = {.txreq=this_send_dummy     ,.txrsp=this_send_dummy},
    [RAK_PB_FRM_TYPE_IOC]       = {.txreq=this_send_dummy     ,.txrsp=this_send_dummy},
    [RAK_PB_FRM_TYPE_SCMD]      = {.txreq=send_scmd_req       ,.txrsp=send_scmd_rsp},
    [RAK_PB_FRM_TYPE_DISP]      = {.txreq=send_disp_req       ,.txrsp=send_disp_rsp},
    
};

emos_process( mdl_rak_wire_protocol, 
{
    /*
        Flow
        1. COM_VSYS_RAK_PCL_CHECK
        2. COM_VSYS_RAK_PCL_COMMAND
        3. SEND/RECV data
        loop 1
    */
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            break;
            
        case TASK_MODE_POLLING:
            /* sync probe config for hub, check time is ##SYNC_FAR_TIME##.*/
            break;
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                case COM_VSYS_RAK_PCL_CHECK:
                    com_mdl_rak_wire_protocol_check(msg_payload, msg_len);
                    break;

                case COM_VSYS_RAK_PCL_COMMAND:
                    com_mdl_rak_wire_protocol_command(msg_payload, msg_len);
                    break;
                    
                case COM_VSYS_RAK_PCL_SENDREQ:
                    do
                    {
                        rak_probe_wire_send_param_t * parm = (rak_probe_wire_send_param_t *)msg_payload;
                        if ( parm->data_len !=0 )
                        {
                            mdl_co_rak_wire_protocol_send_req_withinp(parm->mid, parm->pid,parm->sid,parm->frm_type,parm->pay_type,parm->data,parm->data_len);
                        }
                        else
                        {
                            mdl_co_rak_wire_protocol_send_req_bynew(parm->mid, parm->pid,parm->sid,parm->frm_type,parm->pay_type);
                        }
                        
                    } while (0);
                    break;

                case COM_VSYS_RAK_PCL_SENDRSP:
                    do
                    {
                        rak_probe_wire_send_param_t * parm = (rak_probe_wire_send_param_t *)msg_payload;
                        if ( parm->data_len !=0 )
                        {
                            mdl_co_rak_wire_protocol_send_rsp_withinp(parm->mid, parm->pid,parm->sid,parm->frm_type,parm->pay_type,parm->data,parm->data_len);
                        }
                        else
                        {
                            mdl_co_rak_wire_protocol_send_rsp_bynew(parm->mid, parm->pid,parm->sid,parm->frm_type,parm->pay_type);
                        }
                    } while (0);
                    break;
                case COM_VSYS_RUI_SENDREQ:
                    do
                    {
                        rak_probe_wire_send_param_t * parm = (rak_probe_wire_send_param_t *)msg_payload;
                        mdl_co_rak_rui_protocol_send(NULL,0,WIRE_SEND_REQ,parm->data,parm->data_len);
                    } while (0);
                    break;
                case COM_VSYS_RUI_SENDRSP:
                    do
                    {
                        rak_probe_wire_send_param_t * parm = (rak_probe_wire_send_param_t *)msg_payload;
                        mdl_co_rak_rui_protocol_send(NULL,0,WIRE_SEND_RSP,parm->data,parm->data_len);
                    } while (0);
                    break;
                default:
                    break;
            }
            break;
    }
})
