#include "../../kernel.h"

#ifdef EMOS_SUPPORT_HALL

#include "../inc/inc_co_hallsnsr.h"

emos_process( mdl_hallsnsr, 
{
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            emos.gpio->o_dir_input(GPIO_MASK(PIN_HALL_SNSNR),1);
            break;
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                case COM_TASK_HALLSNSNR_ACTIVE:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_HALLF_ACTIVE, NULL, 0);
                    break;
                default:
                    break;
            }
            break;
    }
})
#else

#ifdef BSP_NOT_SUPPORT_WEAK
void com_mdl_hallsnsr_process(emos_task_msg_t *task_msg)
{ 
}
#endif //BSP_NOT_SUPPORT_WEAK

#endif //EMOS_SUPPORT_HALL
