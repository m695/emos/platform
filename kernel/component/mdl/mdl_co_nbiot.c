#include "../../kernel.h"


#ifdef EMOS_SUPPORT_NBIOT

#include "../inc/inc_co_nbiot.h"

#define THIS_TASK_ID  COM_TASK_RAK_NBIOT

#define EXPAT "AT+EXP"
#define EXPLEN  "EXP+DATALEN"
#define DEFAULT_DELAY 10 

#define nbiot_sendprf(...) \
do { \
    uint8_t *this_com_evtmsg = emos_outext(__VA_ARGS__); \
    com.nbiot->enter.puts((uint8_t *)this_com_evtmsg, strlen((char *)this_com_evtmsg));\
} while (0)

static void com_nbiot_ack_control(uint8_t *data, uint16_t data_len);
static void com_nbiot_creg_control(uint8_t *data, uint16_t data_len);
static void com_nbiot_qiopen_control(uint8_t *data, uint16_t data_len);
static void com_nbiot_qmtopen_control(uint8_t *data, uint16_t data_len);
static void com_nbiot_upload_control(uint8_t *data, uint16_t data_len);
static void com_nbiot_sim_control(uint8_t *data, uint16_t data_len);
static void com_nbiot_qping_control(uint8_t *data, uint16_t data_len);
static void com_nbiot_connect_control(uint8_t *data, uint16_t data_len);

static void com_nbiot_http_read(uint8_t *data, uint16_t data_len);

static void com_nbiot_power_on_busy(uint8_t *payload, uint16_t length);
static void com_nbiot_power_on(uint8_t *data, uint16_t data_len);
static void com_nbiot_activate_context(void);
static void com_nbiot_goto_sleep(void);
static void com_nbiot_network_status_polling(void);
static void com_nbiot_power_off(uint8_t *data, uint16_t data_len);
static void com_nbiot_connected(uint8_t *data, uint16_t data_len);
static void com_nbiot_disconnect(uint8_t *data, uint16_t data_len);
static void com_nbiot_gps_fixed(uint8_t *data, uint16_t data_len);
static void com_mdl_rak_nbiot_power_check();

static com_if_rak_bank2_t this_eee;
#define EE_DATA  this_eee.nbiot_info
static uint16_t this_script_index = 0;
static COM_NBIOT_TIMER_ID_E this_timer_id;
static uint32_t this_sleep_time = 0;

typedef struct
{
    uint32_t addr;
    uint16_t cnt;
    uint8_t record_idx;
    uint8_t sta_intr;
    uint16_t script_idx;
    uint16_t data_len;
    uint8_t data[];
} __EMOS_PACKED com_nbiot_run_script_t;

typedef struct
{
    uint8_t sleep_status;
    uint8_t conn_status;
    uint8_t applying;
    uint8_t uploading;
    uint8_t check_timer;
    uint8_t upload_timer;
    uint32_t upload_len;
    union
    {
        uint16_t value;
        struct
        {
            uint16_t boot_set:1; 
            uint16_t mqtt_ssl_set:1; 
            uint16_t http_connect_method:2;
        };
    } nbcfg;
} __EMOS_PACKED com_nbiot_local_param_t;

static com_nbiot_local_param_t com_nbiot_local_param;


static void com_nbiot_set_timer(COM_NBIOT_TIMER_ID_E timer_id, uint32_t time)
{
    this_timer_id = timer_id;
    com_task_timeout(THIS_TASK_ID, time);
}

static void com_nbiot_set_sleep(COM_NBIOT_TIMER_ID_E timer_id, uint32_t time)
{
    this_sleep_time = emos_gettick() + time;
    com_task_sleep(THIS_TASK_ID);
}

static void com_nbiot_set_wakeup(void)
{
    com_task_wakeup(THIS_TASK_ID);
}

                    
static const com_nbiot_format_t this_recv_tbl[] =
{
    // {.delay=DEFAULT_DELAY,.cmd="APP RDY",.cmd_handle=com_nbiot_power_on},  // BG77 Power on
    {.delay=DEFAULT_DELAY,.cmd="RDY",.cmd_handle=com_nbiot_power_on_busy},  // BG77 Power on busy
    {.delay=0,.cmd="POWERED DOWN"     ,.cmd_handle=com_nbiot_power_off}, // BG77 Power down
    {.delay=0,.cmd="NORMAL POWER DOWN",.cmd_handle=com_nbiot_power_off}, // BG77 Power down
    {.delay=0,.cmd="+QIURC: \"pdpdeact\",1",.cmd_handle=com_nbiot_power_off}, // BG77 Power down
    {.delay=0,.cmd="PSM POWER DOWN",.cmd_handle=com_nbiot_power_off}, // BG77 goto PSM mode
    {.delay=0,.cmd="+CREG:",.cmd_handle=com_nbiot_creg_control},
    {.delay=0,.cmd="+QPING:",.cmd_handle=com_nbiot_qping_control},
    {.delay=0,.cmd="AT+QIACT=1",.cmd_handle=com_nbiot_goto_sleep},
    {.delay=0,.cmd="AT+QIDEACT=1",.cmd_handle=com_nbiot_disconnect}, 
    {.delay=0,.cmd="+QIURC: \"closed\",0",.cmd_handle=com_nbiot_disconnect}, // TCP disconnect
    {.delay=0,.cmd="+QMTSTAT: 0,1",.cmd_handle=com_nbiot_disconnect}, // MQTT disconnect
    {.delay=0,.cmd="+QIOPEN:"    ,.cmd_handle=com_nbiot_connected},
    {.delay=DEFAULT_DELAY,.cmd="+QMTOPEN:" ,.cmd_handle=com_nbiot_qmtopen_control},
    {.delay=DEFAULT_DELAY,.cmd="+QMTCONN: 0,0,0" ,.cmd_handle=com_nbiot_connected},
    {.delay=0,.cmd="+QMTPUB:" ,.cmd_handle=com_nbiot_upload_control},
    {.delay=0,.cmd="SEND OK" ,.cmd_handle=com_nbiot_upload_control},
    {.delay=0,.cmd="+QHTTPGET:" ,.cmd_handle=com_nbiot_http_read},
    {.delay=0,.cmd="+QHTTPPOST:",.cmd_handle=com_nbiot_http_read},
    {.delay=0,.cmd="+QHTTPREAD: 0",.cmd_handle=com_nbiot_goto_sleep},
    {.delay=0,.cmd="+CME ERROR: 516",.cmd_handle=com_nbiot_gps_fixed},
    {.delay=0,.cmd="+CME ERROR: 13",.cmd_handle=com_nbiot_sim_control},
    {.delay=0,.cmd="CONNECT",.cmd_handle=com_nbiot_connect_control},
    {.delay=0,.cmd="OK",.cmd_handle=com_nbiot_ack_control},  // Command ACK
    {.delay=0,.cmd="ERROR",.cmd_handle=com_nbiot_ack_control},  // Command ACK
    {.delay=0,.cmd=">",.cmd_handle=com_nbiot_ack_control},  // Command ACK
    {.delay=0,.cmd="+CME ERROR:",.cmd_handle=com_mdl_rak_nbiot_power_check},
};

static const com_nbiot_script_t this_run_boot_setting[] =
{
    {.delay=100,.cmd="at+qcfg=\"urc/ri/other\",\"pulse\",240,1\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=100,.cmd="at+qcfg=\"urc/delay\",1\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=100,.cmd="AT+COPS=0\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_power_check[] =
{
    {.delay=DEFAULT_DELAY,.cmd="AT+QPOWD=1\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_network_check[] =
{
    {.delay=100,.cmd="AT+CREG?\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_power_on[] =
{
    {.delay=100,.cmd="AT+QPSMS=0,,,\"01100001\",\"00000001\"\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=100,.cmd="AT+CREG?\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_sleep[] =
{
#ifdef EMOS_SUPPORT_NBIOT_PSM
    {.delay=100,.cmd="AT+COPS=0\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=100,.cmd="AT+QPSMS=0,,,\"01100001\",\"00000001\"\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=DEFAULT_DELAY,.cmd="AT+QPSMS=1,,,\"01100001\",\"00000001\"\r\n",.flag=0b11111,.p={"","","","",""}},
#endif
};

static const com_nbiot_script_t this_run_wakeup[] =
{
    {.delay=100,.cmd="AT+QPSMS=0,,,\"01100001\",\"00000001\"\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_activate_context[] =
{
    {.delay=100,.cmd="AT+QIDEACT=1\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=100,.cmd="AT+QIACT=1\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_tcp_connect[] =
{
    {.delay=DEFAULT_DELAY,.cmd="AT+QIOPEN=1,0,\"%s\",\"%s\",%d,0,0\r\n",.flag=0b11011,.p={EE_DATA.mode,EE_DATA.ip,&(EE_DATA.port),"",""}},
};

static const com_nbiot_script_t this_run_mqtt_open[] =
{
    {.delay=DEFAULT_DELAY,.cmd="AT+QMTOPEN=0,\"%s\",%d\r\n",.flag=0b11101,.p={EE_DATA.ip,&(EE_DATA.port),"","",""}},
};

static const com_nbiot_script_t this_run_mqtt_conn[] =
{
    {.delay=DEFAULT_DELAY,.cmd="AT+QMTCONN=0,\"%s\"\r\n"   ,.flag=0b11111,.p={EE_DATA.client_id,"","","",""}},
};

static const com_nbiot_script_t this_run_mqtt_conn_with_userauth[] =
{
    {.delay=DEFAULT_DELAY,.cmd="AT+QMTCONN=0,\"%s\",\"%s\",\"%s\"\r\n"   ,.flag=0b11111,.p={EE_DATA.client_id,EE_DATA.auth_username,EE_DATA.auth_password,"",""}},
};

static const com_nbiot_script_t this_run_mqtt_sub[] =
{
    {.delay=DEFAULT_DELAY,.cmd="AT+QMTSUB=0,1,\"%s\",0\r\n"   ,.flag=0b11111,.p={EE_DATA.sub_topic,"","","",""}},
};

static const com_nbiot_script_t this_run_tcp_upload[] =
{
    {.delay=500,.cmd="AT+QISEND=0,%d\r\n",.flag=0b11111,.p={EXPLEN,"","","",""}},
    {.delay=DEFAULT_DELAY  ,.cmd=EXPAT},
};

static const com_nbiot_script_t this_run_mqtt_upload[] =
{
    {.delay=500,.cmd="AT+QMTPUB=0,0,0,0,\"%s\"\r\n",.flag=0b11111,.p={EE_DATA.pub_topic,"","","",""}},
    {.delay=DEFAULT_DELAY  ,.cmd=EXPAT},
};

static const com_nbiot_script_t this_run_server_auth[] =
{
    {.delay=10,.cmd="AT+QMTCFG=\"ssl\",0,%d,2\r\n",.flag=0b11110,.p={&(EE_DATA.server_auth),"","","",""}},
    {.delay=10,.cmd="AT+QSSLCFG=\"seclevel\",2,2\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=10,.cmd="AT+QSSLCFG=\"sslversion\",2,4\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=10,.cmd="AT+QSSLCFG=\"ciphersuite\",2,0XFFFF\r\n\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=10,.cmd="AT+QSSLCFG=\"ignorelocaltime\",2,1\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_cacert[] =
{
    {.delay=50,.cmd="AT+QFDEL=\"cacert.pem\"\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=100,.cmd="AT+QFUPL=\"cacert.pem\",%d,10\r\n",.flag=0b11111,.p={EXPLEN,"","","",""}},
    {.delay=500,.cmd=EXPAT},
    {.delay=DEFAULT_DELAY,.cmd="AT+QSSLCFG=\"cacert\",2\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=DEFAULT_DELAY,.cmd="AT+QSSLCFG=\"cacert\",2,\"cacert.pem\"\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_client_cert[] =
{
    {.delay=50,.cmd="AT+QFDEL=\"clientcert.pem\"\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=100,.cmd="AT+QFUPL=\"clientcert.pem\",%d,10\r\n",.flag=0b11111,.p={EXPLEN,"","","",""}},
    {.delay=500,.cmd=EXPAT},
    {.delay=DEFAULT_DELAY,.cmd="AT+QSSLCFG=\"clientcert\",2\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=DEFAULT_DELAY,.cmd="AT+QSSLCFG=\"clientcert\",2,\"clientcert.pem\"\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_client_key[] =
{
    {.delay=50,.cmd="AT+QFDEL=\"clientkey.pem\"\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=100,.cmd="AT+QFUPL=\"clientkey.pem\",%d,10\r\n",.flag=0b11111,.p={EXPLEN,"","","",""}},
    {.delay=500 ,.cmd=EXPAT},
    {.delay=DEFAULT_DELAY,.cmd="AT+QSSLCFG=\"clientkey\",2\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=DEFAULT_DELAY,.cmd="AT+QSSLCFG=\"clientkey\",2,\"clientkey.pem\"\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_gps_enable[] =
{
    {.delay=2000,.cmd="AT+QGPS=1\r\n",.flag=0b11111,.p={"","","","",""}},
    {.delay=DEFAULT_DELAY,.cmd="AT+QGPSLOC=2\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_gps_data[] =
{
    {.delay=DEFAULT_DELAY,.cmd="AT+QGPSLOC?\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_http_url_request[] =
{
    {.delay=100,.cmd="AT+QHTTPURL=%d,25\r\n",.flag=0b11110,.p={&(EE_DATA.http_url_len),"","","",""}},
};

static const com_nbiot_script_t this_run_http_get_request[] =
{
    {.delay=1000,.cmd="%s\r\n",.flag=0b11111,.p={EE_DATA.http_url,"","","",""}},
    {.delay=1000,.cmd="AT+QHTTPGET=80\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_http_post_request[] =
{
    {.delay=1000,.cmd="%s\r\n",.flag=0b11111,.p={EE_DATA.http_url,"","","",""}},
    {.delay=DEFAULT_DELAY,.cmd="AT+QHTTPPOST=%d,25,25\r\n",.flag=0b11110,.p={&(com_nbiot_local_param.upload_len),"","","",""}},
};

static const com_nbiot_script_t this_run_http_read_request[] =
{
    {.delay=1000,.cmd="AT+QHTTPREAD=80\r\n",.flag=0b11111,.p={"","","","",""}},
};

static const com_nbiot_script_t this_run_http_post_data[] =
{
    {.delay=DEFAULT_DELAY ,.cmd=EXPAT},
};

static uint8_t this_run_sample_cmd[64];
static const com_nbiot_script_t this_run_sample[] =
{
    {.delay=3000,.cmd=(const char *)this_run_sample_cmd,.flag=0b11111,.p={"","","","",""}},
};

static void com_nbiot_run_usercmd( uint8_t *payload, uint16_t length )
{
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));
    data.sta_intr = IENABLE;
    data.addr = &this_run_sample[0];
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_sample)/sizeof(com_nbiot_script_t);
    memcpy(this_run_sample_cmd, payload, length);
    com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
}

static void com_nbiot_run_loop( uint8_t *payload, uint16_t length )
{
    com_nbiot_run_script_t * script = payload;
    com_nbiot_script_t *this_script = script->addr;
    com_nbiot_script_t *this_run_script = NULL;
   
    static uint16_t reg_index = 0;
    static uint32_t reg_script = 0;
    uint16_t cnt = script->cnt;

    uint32_t *param[NBIOT_SCRIPT_PARAM_CNT];

    if (reg_script != 0 )
    {
        if (reg_index != script->script_idx)
        {
            com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, payload, length);
            return;
        }
    }

    if ( com.nbiot->ctrl->retry == IENABLE )
    {
        #if 0
        DBG_INFO("[%02x]\r\n",script->record_idx);
        script->record_idx--;
        #endif
    }
    
    reg_script = this_script;
    reg_index = script->script_idx;

    for (uint8_t k = script->record_idx; k < cnt; k++)
    {
        this_run_script = &this_script[k];
        for (uint8_t i = 0; i < NBIOT_SCRIPT_PARAM_CNT; i++)
        {
            
            if (emos_strcmp(this_run_script->p[i], EXPLEN, 0) == 0)
            {
                param[i] = script->data_len;
            }
            else
            {
                param[i] = ((this_run_script->flag>>i)&0x1)?this_run_script->p[i]:*this_run_script->p[i];
            }
        }
        
        uint8_t *this_msg;
        uint8_t *pp_msg;
        if (emos_strcmp((char *)this_run_script->cmd, EXPAT, 0) == 0)
        {
            
            this_msg = emos_outext("%s\r\n",script->data);
        }
        else
        {
            this_msg = emos_outext(this_run_script->cmd,param[0],param[1],param[2],param[3],param[4]);
        }
        
        pp_msg = com_malloc(strlen(this_msg));
        memcpy(pp_msg,this_msg,strlen(this_msg));

        if (script->sta_intr == IENABLE)
        {
            com.nbiot->ctrl->intr = IENABLE;
        }
        
        com_vsys_send( COM_TASK_EVENT, COM_EVENT_NBIOT_TRANS, pp_msg, strlen(pp_msg));
        com_free(pp_msg);

        uint32_t this_delay = DEFAULT_DELAY;
        if (this_run_script->delay != 0)
        {
            this_delay = this_run_script->delay;
        }
        com_nbiot_set_sleep(COM_NBIOT_TIMER_SCRIPT_DELAY, this_delay);
        
        script->record_idx = ++k;
        com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, payload, length);
        return;
    }
    reg_script = 0;
    reg_index = 0;
}


static void com_nbiot_apply_connection_end( void )
{
    com_nbiot_local_param.applying = 0;

    com_flash_bank2_read((uint8_t *)&this_eee, sizeof(com_if_rak_bank2_t));
    
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));
    if (emos_strcmp((char *)this_eee.nbiot_info.mode, "TCP", 0) == 0 ||
        emos_strcmp((char *)this_eee.nbiot_info.mode, "UDP", 0) == 0 ) 
    {
        data.addr = this_run_tcp_connect;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_tcp_connect)/sizeof(com_nbiot_script_t);
        com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
    }

    if (emos_strcmp((char *)this_eee.nbiot_info.mode, "MQTTTCP", 0) == 0 ||
        emos_strcmp((char *)this_eee.nbiot_info.mode, "MQTTAWS", 0) == 0) 
    {
        // MQTT SLL Setting once
        if(com_nbiot_local_param.nbcfg.mqtt_ssl_set != 1)
        {
            data.addr = this_run_server_auth;
            data.script_idx = this_script_index++;
            data.cnt = sizeof(this_run_server_auth)/sizeof(com_nbiot_script_t);
            com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
            com_nbiot_local_param.nbcfg.mqtt_ssl_set = 1;
        }

        // MQTT OPEN
        memset(&data, 0 ,sizeof(com_nbiot_run_script_t));
        data.addr = this_run_mqtt_open;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_mqtt_open)/sizeof(com_nbiot_script_t);
        com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
    }

    if (emos_strcmp((char *)this_eee.nbiot_info.mode, "HTTP", 0) == 0) 
    {
        com_nbiot_local_param.nbcfg.http_connect_method = COM_NBIOT_HTTP_URL;
        data.addr = this_run_http_url_request;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_http_url_request)/sizeof(com_nbiot_script_t);
        com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
    }
    
}

static void com_nbiot_apply_connection_start(uint8_t value)
{
    com_nbiot_activate_context();

    com_flash_bank2_read((uint8_t *)&this_eee, sizeof(com_if_rak_bank2_t));

    com_nbiot_local_param.applying = value;
    EE_DATA.apply = value;
    com.nbiot->ctrl->nbt_apply = value;
    com_flash_bank2_write((uint8_t *)&this_eee, sizeof(com_if_rak_bank2_t));

    
    if (value)
    {
        if(com.nbiot->ctrl->nbt_state == 0)
        {

            com.nbiot->power(1);
        }
        else
        {
            com_nbiot_apply_connection_end();
        }
    }
    com_nbiot_set_timer(COM_NBIOT_TIMER_CREG_CHECK, 5000);
}

static void com_nbiot_tcp_upload_end(uint8_t *payload, uint16_t length)
{
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));
    if (emos_strcmp((char *)this_eee.nbiot_info.mode, "HTTP", 0) == 0) 
    {
        if(com.nbiot->ctrl->nbt_http_method == COM_NBIOT_HTTP_POST)
{
            data.addr = this_run_http_post_data;
            data.script_idx = this_script_index++;
            data.cnt = sizeof(this_run_http_post_data)/sizeof(com_nbiot_script_t);
            com_vsys_mrsend( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data), payload, length);
        }
        com_nbiot_local_param.uploading = 0;
        return;
    }

    #define DATA_SD  5
    uint8_t *p_data = (payload+1);
    uint8_t p_data_len = (length-1);
    uint8_t probe_cnt = com.rak_probe(0).probe_count();

    if(probe_cnt > 1)
    {
        com_if_rak_probe_param_t * probe_info;
        com.rak_probe(payload[0]).probe_get(&probe_info);
        uint8_t prefix[DATA_SD] = {0x00, 0x7e, 0,0,0,};
        memcpy(&prefix[2], (uint8_t *)probe_info->ssn.value, 3);
        p_data = emos.units->memcat(prefix, DATA_SD, (payload+1), (length-1));
        p_data_len = DATA_SD + (length-1);

    }

    if (emos_strcmp((char *)this_eee.nbiot_info.mode, "TCP", 0) == 0 ||
        emos_strcmp((char *)this_eee.nbiot_info.mode, "UDP", 0) == 0 ) 
    {
        data.addr = this_run_tcp_upload;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_tcp_upload)/sizeof(com_nbiot_script_t);
    }

    if (emos_strcmp((char *)this_eee.nbiot_info.mode, "MQTTTCP", 0) == 0 ||
        emos_strcmp((char *)this_eee.nbiot_info.mode, "MQTTAWS", 0) == 0) 
    {
        data.addr = this_run_mqtt_upload;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_mqtt_upload)/sizeof(com_nbiot_script_t);
    }

    if (emos_strcmp((char *)this_eee.nbiot_info.mode, "HTTP", 0) == 0) 
    {
        com_nbiot_local_param.nbcfg.http_connect_method = COM_NBIOT_HTTP_URL;
        data.addr = this_run_http_url_request;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_http_url_request)/sizeof(com_nbiot_script_t);
    }

    if (com.nbiot->ctrl->nbt_data_format == 0) //RAW data
    {
                    
        com_flash_bank2_read((uint8_t *)&this_eee, sizeof(com_if_rak_bank2_t));
        
        uint8_t send_len = (p_data_len*2) + 1;
        uint8_t send_buf[send_len];
        memset(send_buf, 0, send_len);
        for (int i=0; i<p_data_len; i++)
        {
            snprintf(send_buf + (i*2), sizeof(send_buf), "%02x", p_data[i]);
        }
        send_buf[send_len - 1] = '\032';

        data.data_len = send_len - 1;
        com_vsys_mrsend( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data),send_buf, send_len);
    }
    else if (com.nbiot->ctrl->nbt_data_format == 1) // JSON 
    {
        uint8_t json_data[256];
        memset(json_data, 0, 256);
        strcat(json_data, "{"); // "add {"
        #define TMP_SIZE 50
        for(int i=1; i<p_data_len; i+=2) // add KEY value
        {
            uint8_t temp_data[TMP_SIZE];
            memset(temp_data, 0, TMP_SIZE);
            switch (p_data[i])
            {
                case 0x01: //Digital-Output 1 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]; 
                        snprintf(temp_data, TMP_SIZE, "\"Digital-Output\":%d,", (snsr_data));
                        strcat(json_data, temp_data);
                        i+=1;
                    } while(0);
                    break;
                case 0x02: //Analog-Input 2 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]<<8 | p_data[i+2]; 
                        snprintf(temp_data, TMP_SIZE, "\"Analog-Input\":%.2f,", ((float)snsr_data/100));
                        strcat(json_data, temp_data);
                        i+=2;
                    } while(0);
                    break;
                case 0x7E: //SSN 3 Bytes
                    do {
                        snprintf(temp_data, TMP_SIZE, "\"SSN\":%02X%02X%02X,", p_data[i+1], p_data[i+2], p_data[i+3]);
                        strcat(json_data, temp_data);
                        i+=3;
                    } while(0);
                    break;
                case 0x67: //temperature 2 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]<<8 | p_data[i+2]; 
                        snprintf(temp_data, TMP_SIZE, "\"Temperature\":%.1f,", ((float)snsr_data/10));
                        strcat(json_data, temp_data);
                        i+=2;
                    } while(0);
                    break;
                case 0x68: //humidity 1 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]; 
                        snprintf(temp_data, TMP_SIZE, "\"Humidity\":%d,", (snsr_data));
                        strcat(json_data, temp_data);
                        i+=1;
                    } while(0);
                    break;
                case 0x70: //High-Precise-Humidity 2 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]<<8 | p_data[i+2]; 
                        snprintf(temp_data, TMP_SIZE, "\"High-Precise-Humidity\":%.1f,", ((float)snsr_data/10));
                        strcat(json_data, temp_data);
                        i+=2;
                    } while(0);
                    break;
                case 0x73: //pressure 2 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]<<8 | p_data[i+2]; 
                        snprintf(temp_data, TMP_SIZE, "\"Barmoeter\":%.1f,", ((float)snsr_data/10));
                        strcat(json_data, temp_data);
                        i+=2;
                    } while(0);
                    break;
                case 0xBE: //Wind Speed 2 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]<<8 | p_data[i+2]; 
                        snprintf(temp_data, TMP_SIZE, "\"Wind-Speed\":%.2f,", ((float)snsr_data/100));
                        strcat(json_data, temp_data);
                        i+=2;
                    } while(0);
                    break;
                case 0xBF: //Wind deiection 2 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]<<8 | p_data[i+2]; 
                        snprintf(temp_data, TMP_SIZE, "\"Wind-Direction\":%d,", (snsr_data));
                        strcat(json_data, temp_data);
                        i+=2;
                    } while(0);
                    break;
                case 0xC1: //“High-Precise-pH 2 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]<<8 | p_data[i+2]; 
                        snprintf(temp_data, 30, "\"High-Precise-pH\":%.2f,", ((float)snsr_data/100));
                        strcat(json_data, temp_data);
                        i+=2;
                    } while(0);
                    break;
                case 0xC2: //pH 2 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]<<8 | p_data[i+2]; 
                        snprintf(temp_data, 20, "\"pH\":%.1f,", ((float)snsr_data/10));
                        strcat(json_data, temp_data);
                        i+=2;
                    } while(0);
                    break;
                case 0xC3: //Pyranometer 2 Bytes
                    do {
                        uint16_t snsr_data = p_data[i+1]<<8 | p_data[i+2]; 
                        snprintf(temp_data, 20, "\"Pyranometer\":%d,", (snsr_data));
                        strcat(json_data, temp_data);
                        i+=2;
                    } while(0);
                    break;
                default:
                    break; 
            }
        }
        json_data[(strlen(json_data) - 1)] = '\0' ;//Remove the "," of last json key
        strcat(json_data, "}\r\n\032"); // add "}"
    
        //emos_printf("Final upload = %s\r\n", json_data); // Json data debug
        com_vsys_mrsend( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data),json_data, sizeof(json_data));
    }
    com_nbiot_local_param.uploading = 0;

    com_nbiot_goto_sleep();

    // Count the upload times
    // Continuously send 20 uploads wihtout PSM will forced to powered down
    if (com_nbiot_local_param.upload_timer >= 20)
    {
        com_nbiot_local_param.upload_timer = 0;
        com.nbiot->power_check();
    }
    com_nbiot_local_param.upload_timer++;
}

static void com_nbiot_tcp_upload_start(uint8_t *payload, uint16_t length)
{
    uint32_t now_time = emos_gettick();
    static uint32_t last_time = 0;//emos.dly->o_gettick();
    static uint8_t wait_timer = 0;
    com_nbiot_local_param.upload_len = length;

    if (com.nbiot->ctrl->nbt_connection_busy)
    {
        if((now_time - last_time ) > 20000) //20 seconds timeout
        {
            last_time = now_time;
//            com.nbiot->network_check();
            // emos.uart(2).flush();
            wait_timer++;
            if (wait_timer > 5)
            {
                com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_RECOVER,NULL,0);
                com.nbiot->power(0);
                wait_timer = 0;
                com.nbiot->ctrl->nbt_connection_busy = 0;
            }
        }

        com.nbiot->upload(payload, length);
        return;
    }

    com_nbiot_local_param.uploading = 1;
    if(com.nbiot->ctrl->nbt_state == 0) // When BG77 in power off state
    {
        emos_delay(500); //Wait BG77 to powered down
        com.nbiot->ctrl->nbt_connection_busy = 1;
        com.nbiot->power(1);
        com.nbiot->upload(payload, length);
    }
    else if (com_nbiot_local_param.conn_status == 1) // BG77 is ready to upload
    {
        if (com.nbiot->ctrl->nbt_upload_end == 1) // Last upload is finished
        {
            com.nbiot->ctrl->nbt_upload_end = 0;
            com.nbiot->ctrl->conned = IDISABLE;
            com_nbiot_tcp_upload_end(payload , length);
        }
        else
        {
            if((now_time - last_time ) > 30000) // 30 second timeout
            {
                last_time = now_time;
                com.nbiot->ctrl->nbt_state = 0;
                com_nbiot_local_param.conn_status = 0;
                com.nbiot->ctrl->nbt_upload_end = 1;
                // emos.uart(2).flush();
                com.nbiot->power(0);
            }
            com.nbiot->upload(payload, length);
            return;
        }  
    }
    else // BG77 is not connect to service
    {
        com.nbiot->ctrl->nbt_connection_busy = 1;
        com_nbiot_apply_connection_end();
        com.nbiot->upload(payload, length);
    }

    wait_timer = 0;
    last_time = now_time;
}

static void com_nbiot_recv_command(uint8_t *payload, uint8_t length)
{
    com_nbiot_set_wakeup();

    uint8_t ret = com.nbiot->ctrl->intr;
    
        do
        {
            if (com.nbiot->ctrl->intr == IENABLE)
            {
                if(strcmp(payload, "OK") == 0 )
                {
                    this_sleep_time = 0;
                    ret = IDISABLE;
                    com.nbiot->ctrl->intr = ret;
                }

                if(strcmp(payload, "ERROR") == 0 )
                {
                    this_sleep_time = 0;
                    ret = IDISABLE;
                    com.nbiot->ctrl->intr = ret;
                }
                SYS_INFO("%s\r\n",payload);
                break;
            }
        if (com.nbiot->ctrl->nbt_test_mode != 1)
        {
            uint16_t tbl_len = sizeof(this_recv_tbl)/sizeof(com_nbiot_format_t);
            uint8_t scmd = 0;

            for (uint16_t i = 0; i < tbl_len; i++)
            {
                if(strstr((char *)payload, this_recv_tbl[i].cmd)) 
                {
                    this_recv_tbl[i].cmd_handle(payload, length);
                    if (this_recv_tbl[i].delay != 0)
                    {
                        emos_delay(this_recv_tbl[i].delay);
                    }
                }
            }
        emos.vsys->ctrl->psm_def = IENABLE;
        }
        } while (0);
        
    }

static void com_nbiot_ack_control(uint8_t *data, uint16_t data_len)
{
    if(strcmp(data, "ERROR") == 0 )
    {
        com.nbiot->ctrl->retry = IENABLE;
    }
}

static void com_nbiot_creg_control(uint8_t *data, uint16_t data_len)
{
    // LPWAN mode check
    if (com.radio->ctrl->lpwan_mode == 1)
    {
        com.nbiot->power_check();
    }

    char **pp;
    pp = (char *)emos_split((char *)data, ",", NULL);

    uint8_t value = pp[1][0];
    
    switch ( value )
    {
        case '0': // Not registered
        case '2': // Not registered
        case '3': // Registration denied
        case '4': // Unknown
            com_nbiot_network_status_polling();
            break; 
        case '1': // Registered, home network
        case '5': // Registered, roaming
            com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_ACTIVE,NULL,0);
            com_nbiot_activate_context();
            break;
        default:
            return;
    }

}

static void com_nbiot_qiopen_control(uint8_t *data, uint16_t data_len)
{
    char **pp;
    pp = (char *)emos_split((char *)data, ",", NULL);

    uint8_t value = pp[1][0];
    
    switch ( value )
    {
        case '0': // Successfully
            com_nbiot_connected(NULL, 1);
            break; 
        default:
            com_nbiot_disconnect(NULL, 1);
            return;
    }

}

static void com_nbiot_qmtopen_control(uint8_t *data, uint16_t data_len)
{
    char **pp;
    pp = (char *)emos_split((char *)data, ",", NULL);

    uint8_t value = pp[1][0];
    
    com_nbiot_run_script_t script;
    switch ( value )
    {
        case '0': // Successfully
        case '2': // Already connected
            com_flash_bank2_read((uint8_t *)&this_eee, sizeof(com_if_rak_bank2_t));
            memset(&script, 0 ,sizeof(com_nbiot_run_script_t));
    
            if (EE_DATA.user_auth == 1)
            {
                script.script_idx = this_script_index++;
                script.addr = this_run_mqtt_conn_with_userauth;
                script.cnt = sizeof(this_run_mqtt_conn_with_userauth)/sizeof(com_nbiot_script_t);
            }
            else
            {
                script.script_idx = this_script_index++;
                script.addr = this_run_mqtt_conn;
                script.cnt = sizeof(this_run_mqtt_conn)/sizeof(com_nbiot_script_t);
            }
            com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &script, sizeof(script));
            break; 
        default:
            return;
    }

}

static void com_nbiot_upload_control(uint8_t *data, uint16_t data_len)
{
    char **pp;
    uint16_t num;
    pp = (char *)emos_split((char *)data, ",", &num);

    if (num == 1)
    {
        com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_SENDED,NULL,0);
        return;
    }

    uint8_t value = pp[2][0];
    
    com_nbiot_run_script_t script;
    switch ( value )
    {
        case '0': // Successfully
            com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_SENDED,NULL,0);
            com.nbiot->ctrl->conned = IENABLE;
            com.nbiot->ctrl->nbt_upload_end = 1;
            break;
        case '1': // Retry
            com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_SENDTRY,NULL,0);
            break; 
        case '2': // Fail
            com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_SENDFAIL,NULL,0);
            break; 
        default:
            return;
    }
}

static void com_nbiot_sim_control(uint8_t *data, uint16_t data_len)
{
    
    if (com.nbiot->ctrl->pwrhold == 1 )
    {
        return;
    }
    com.nbiot->power_check();
    com.nbiot->apply(0);

    com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_NOSIMCARD,NULL,0);
}

static void com_nbiot_connect_control(uint8_t *data, uint16_t data_len)
{
    com_nbiot_run_script_t script;
    memset(&script, 0 ,sizeof(com_nbiot_run_script_t));

    switch(com_nbiot_local_param.nbcfg.http_connect_method)
    {
        case COM_NBIOT_HTTP_GET:
            com_nbiot_connected(NULL, 1);
            break;
        //POST Message
        case COM_NBIOT_HTTP_POST:
            com_nbiot_connected(NULL, 1);
            break;
        //Do Nothing
        case COM_NBIOT_HTTP_READ:
            break;
        //Send URL 
        case COM_NBIOT_HTTP_URL:
            //GET
            if(com.nbiot->ctrl->nbt_http_method == 0)
            {
                script.addr = this_run_http_get_request;
                script.script_idx = this_script_index++;
                script.cnt = sizeof(this_run_http_get_request)/sizeof(com_nbiot_script_t);
                com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &script, sizeof(script));
                com_nbiot_local_param.nbcfg.http_connect_method = COM_NBIOT_HTTP_GET;
            }
            //POST
            else 
            {
                script.addr = this_run_http_post_request;
                script.script_idx = this_script_index++;
                script.cnt = sizeof(this_run_http_post_request)/sizeof(com_nbiot_script_t);
                com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &script, sizeof(script));
                com_nbiot_local_param.nbcfg.http_connect_method = COM_NBIOT_HTTP_POST;
            }
            break;
        default:
            break;
    }
    
}

static void com_nbiot_http_read(uint8_t *data, uint16_t data_len)
{
    char **pp; 
    pp = (char *)emos_split((char *)data, ",", NULL);

    uint16_t value;
    emos_atoi(pp[1], &value);

    switch ( value )
    {
        case 200:
            com_nbiot_connected(NULL, 1);

            com_nbiot_run_script_t script;
            memset(&script, 0 ,sizeof(com_nbiot_run_script_t));
            script.addr = this_run_http_read_request;
            script.script_idx = this_script_index++;
            script.cnt = sizeof(this_run_http_read_request)/sizeof(com_nbiot_script_t);
            com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &script, sizeof(script));
            break;
        default:
            com_nbiot_goto_sleep();
            DBG_INFO("HTTP_SEND_FAIL\r\n");
            break;
    }
}

static void com_nbiot_power_on(uint8_t *payload, uint16_t length)
{
    // LPWAN mode check
    if (com.radio->ctrl->lpwan_mode == 1)
    {
        com.nbiot->power_check();
        return;
    }

    com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_POWERON,NULL,0);

    // NBT status update
    com.nbiot->ctrl->nbt_state = 1;

    // Boot Setting once
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));
    if(com_nbiot_local_param.nbcfg.boot_set != 1)
    {
        data.addr = this_run_boot_setting;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_boot_setting)/sizeof(com_nbiot_script_t);
        com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
        com_nbiot_local_param.nbcfg.boot_set = 1;
    }


    // GOTO this_run_power_on 
    data.addr = this_run_power_on;
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_power_on)/sizeof(com_nbiot_script_t);
    com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
}

static void com_nbiot_power_on_busy(uint8_t *payload, uint16_t length)
{
    //GPIO Setting 
    emos.gpio->o_phigh((NBIOT_GPS_ENABLE/100), (NBIOT_GPS_ENABLE%100));

    com_nbiot_power_on(payload, length);
}

static void com_nbiot_qping_control(uint8_t *data, uint16_t data_len)
{
    DBG_INFO("%s\r\n", data);
}

static void com_nbiot_power_off(uint8_t *data, uint16_t data_len)
{
    // LPWAN mode check
    if (com.radio->ctrl->lpwan_mode == 1)
    {
        com.nbiot->ctrl->nbt_en = 0;
    }
    

    // GSP GPIO Setting 
    emos.gpio->o_plow((NBIOT_GPS_ENABLE/100), (NBIOT_GPS_ENABLE%100));

    // NBT status update
    com.nbiot->ctrl->nbt_connection_busy = 0;
    com.nbiot->ctrl->nbt_state = 0;
    com_nbiot_local_param.conn_status = 0;
    com.nbiot->ctrl->nbt_context = 0;
    com.nbiot->ctrl->nbt_upload_end = 1;

    // Reset upload timer
    com_nbiot_local_param.upload_timer = 0;

    com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_POWEROFF,NULL,0);

    // Power on recover while ble connected
    if (com.nbiot->ctrl->pwrhold == 1)
    {
        com.nbiot->power(1);
    }
}

static void com_nbiot_sleep(uint8_t value)
{
    if (value)
    {
        com.nbiot->ctrl->pwrhold = 0;

        com_nbiot_run_script_t data;
        memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

        com_nbiot_local_param.sleep_status = 1;

        data.addr = this_run_sleep;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_sleep)/sizeof(com_nbiot_script_t);
        com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
    }
    else
    {
        com.nbiot->ctrl->pwrhold = 1;

        emos.gpio->o_dir_output((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));

        emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
        emos.gpio->o_phigh((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
        emos_delay(500);
        emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));

        com_nbiot_run_script_t data;
        memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

        data.addr = this_run_wakeup;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_wakeup)/sizeof(com_nbiot_script_t);
        com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));

        com_nbiot_set_timer(COM_NBIOT_TIMER_CREG_CHECK, 5000);
    }

}

static void com_nbiot_goto_sleep(void)
{
    if (com_nbiot_local_param.applying || com_nbiot_local_param.uploading)
    {
        com_nbiot_apply_connection_end();
    }
    else
    {
        if (com.nbiot->ctrl->pwrhold == 1 )
        {
            return;
        }

        com_nbiot_run_script_t data;
        memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

        com_nbiot_local_param.sleep_status = 1;

        data.addr = this_run_sleep;
        data.script_idx = this_script_index++;
        data.cnt = sizeof(this_run_sleep)/sizeof(com_nbiot_script_t);
        com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
    }
}

static void com_nbiot_network_check(void)
{
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

    data.addr = this_run_network_check;
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_network_check)/sizeof(com_nbiot_script_t);
    com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
}

static void com_nbiot_network_status_polling(void)
{
    if (com_nbiot_local_param.check_timer > 40)
    {
        com.nbiot->power(0);
        com_nbiot_local_param.check_timer = 0;
        com.nbiot->ctrl->nbt_connection_busy = 0;
        com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_RECOVER,NULL,0);
    }

    com.nbiot->ctrl->nbt_context = 0;
    com_nbiot_set_timer(COM_NBIOT_TIMER_CREG_CHECK, 10000);
    
    com_nbiot_local_param.check_timer++;
}

static void com_nbiot_activate_context(void)
{
    com_nbiot_local_param.check_timer = 0;
    
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

    com.nbiot->ctrl->nbt_context = 1;

    data.addr = this_run_activate_context;
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_activate_context)/sizeof(com_nbiot_script_t);
    com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
}

static void com_nbiot_connected(uint8_t *data, uint16_t data_len)
{
    com_flash_bank2_read((uint8_t *)&this_eee, sizeof(com_if_rak_bank2_t));

    // NBT status update
    com_nbiot_local_param.conn_status = 1;
    com.nbiot->ctrl->nbt_apply = 1;
    com.nbiot->ctrl->nbt_connection_busy = 0;
    com.nbiot->ctrl->conned = IENABLE;

    com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_CONN,NULL,0);

    // GOTO this_run_mqtt_sub
    if ((emos_strcmp((char *)this_eee.nbiot_info.mode, "MQTTTCP", 0) == 0 ||
         emos_strcmp((char *)this_eee.nbiot_info.mode, "MQTTAWS", 0) == 0 ) &&
         strlen((char *)this_eee.nbiot_info.sub_topic) > 1)
    {
        com_nbiot_run_script_t script;
        memset(&script, 0 ,sizeof(com_nbiot_run_script_t));

        script.addr = this_run_mqtt_sub;
        script.script_idx = this_script_index++;
        script.cnt = sizeof(this_run_mqtt_sub)/sizeof(com_nbiot_script_t);
        com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &script, sizeof(script));
    }
}

static void com_nbiot_disconnect(uint8_t *data, uint16_t data_len)
{
//    com.nbiot->ctrl->nbt_connection_busy = 0;

    com_nbiot_local_param.conn_status = 0;
    com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_DISC,NULL,0);

}

static void com_nbiot_gps_fixed(uint8_t *data, uint16_t data_len)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    eee.nbiot_info.gps_signal = 0;
    com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    com_vsys_send(COM_TASK_EVENT,COM_EVENT_NBIOT_GPSERROR,NULL,0);
}

static void com_mdl_rak_nbiot_init( void )
{
    // GPIO setting
    emos.gpio->o_dir_output((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
    emos.gpio->o_dir_output((NBIOT_DISABLE/100), (NBIOT_DISABLE%100));
    emos.gpio->o_dir_output((NBIOT_TRIG/100), (NBIOT_TRIG%100));
    emos.gpio->o_dir_output((NBIOT_GPS_ENABLE/100), (NBIOT_GPS_ENABLE%100));
    emos.gpio->o_dir_input((NBIOT_RI/100), (NBIOT_RI%100), 1);
    
    emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
    emos.gpio->o_plow((NBIOT_DISABLE/100), (NBIOT_DISABLE%100));
    emos.gpio->o_plow((NBIOT_TRIG/100), (NBIOT_TRIG%100));
    emos.gpio->o_plow((NBIOT_GPS_ENABLE/100), (NBIOT_GPS_ENABLE%100));

    // Flahs Setting
    com_flash_bank2_read((uint8_t *)&this_eee, sizeof(com_if_rak_bank2_t));
    if (emos_strcmp((char *)this_eee.nbiot_info.mode, "TCP", 0)  != 0 &&
        emos_strcmp((char *)this_eee.nbiot_info.mode, "UDP", 0)  != 0 &&
        emos_strcmp((char *)this_eee.nbiot_info.mode, "NONE", 0) != 0 &&
        emos_strcmp((char *)this_eee.nbiot_info.mode, "MQTTAWS", 0) != 0 &&
        emos_strcmp((char *)this_eee.nbiot_info.mode, "MQTTTCP", 0) != 0 &&
        emos_strcmp((char *)this_eee.nbiot_info.mode, "HTTP", 0) != 0 ) 
    {
        memset(&this_eee, 0, sizeof(com_if_rak_bank2_t));
        this_eee.nbiot_info.port = 0;
        this_eee.nbiot_info.apply = 0;
        this_eee.nbiot_info.server_auth = 0;
        this_eee.nbiot_info.user_auth = 0;
        this_eee.nbiot_info.data_format = 0;
        this_eee.nbiot_info.http_method = 0;
        this_eee.nbiot_info.http_url_len = 0;

        memcpy((uint8_t *)&(this_eee.nbiot_info.mode), "NONE\0", strlen("NONE\0"));
        memcpy((uint8_t *)&(this_eee.nbiot_info.ip), "0.0.0.0\0", strlen("0.0.0.0\0"));
        memcpy((uint8_t *)&(this_eee.nbiot_info.auth_username), "username\0", strlen("username\0"));
        memcpy((uint8_t *)&(this_eee.nbiot_info.auth_password), "password\0", strlen("password\0"));
        memcpy((uint8_t *)&(this_eee.nbiot_info.client_id), "clientID\0", strlen("clientID\0"));
        memcpy((uint8_t *)&(this_eee.nbiot_info.sub_topic), "topic\0", strlen("topic\0"));
        memcpy((uint8_t *)&(this_eee.nbiot_info.pub_topic), "topic\0", strlen("topic\0"));
        memcpy((uint8_t *)&(this_eee.nbiot_info.http_url), "url\0", strlen("url\0"));

        com_nbiot_local_param.conn_status = 0;
        com.nbiot->ctrl->nbt_connection_busy = 0;
        com.nbiot->ctrl->conned = IDISABLE;

        com_flash_bank2_write((uint8_t *)&this_eee, sizeof(com_if_rak_bank2_t));
    }

    // RAM setting
    com.nbiot->ctrl->nbt_apply = EE_DATA.apply;
    com.nbiot->ctrl->nbt_data_format = EE_DATA.data_format;
    com.nbiot->ctrl->nbt_http_method = EE_DATA.http_method;
    com_if_rak_bank0_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank0_t));
    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

#ifdef EMOS_SUPPORT_NBIOT_DTM
    com.nbiot->ctrl->nbt_test_mode = 1; 
#endif
}

static void com_mdl_rak_nbiot_power_check(void)
{
    // GOTO this_run_power_check
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));
    data.addr = this_run_power_check;
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_power_check)/sizeof(com_nbiot_script_t);
    com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
    
   
}

static void com_mdl_rak_nbiot_server_auth(uint8_t type)
{
}

static void com_mdl_rak_nbiot_auth_cacert(uint8_t *payload, uint32_t length)
{
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

    data.addr = this_run_cacert;
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_cacert)/sizeof(com_nbiot_script_t);
    data.data_len = length;
    com_vsys_mrsend( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data), payload, length);
}

static void com_mdl_rak_nbiot_auth_client_cert(uint8_t *payload, uint32_t length)
{
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

    data.addr = this_run_client_cert;
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_client_cert)/sizeof(com_nbiot_script_t);
    data.data_len = length;
    com_vsys_mrsend( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data), payload, length);
}

static void com_mdl_rak_nbiot_auth_client_key(uint8_t *payload, uint32_t length)
{
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

    data.addr = this_run_client_key;
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_client_key)/sizeof(com_nbiot_script_t);
    data.data_len = length;
    com_vsys_mrsend( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data), payload, length);
}

static void com_mdl_rak_nbiot_gps_enable()
{
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

    data.addr = this_run_gps_enable;
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_gps_enable)/sizeof(com_nbiot_script_t);
    com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
}

static void com_mdl_rak_nbiot_gps_data()
{
    
    com_nbiot_run_script_t data;
    memset(&data, 0 ,sizeof(com_nbiot_run_script_t));

    data.addr = this_run_gps_data;
    data.script_idx = this_script_index++;
    data.cnt = sizeof(this_run_gps_data)/sizeof(com_nbiot_script_t);
    com_vsys_send( THIS_TASK_ID, COM_TASK_RAK_NBIOT_SCRIPT, &data, sizeof(data));
    
}

static void com_mdl_rak_nbiot_ble_lock(uint8_t lock)
{
    if (lock == 0)
    {
        com.nbiot->ctrl->pwrhold = 0;
        com.nbiot->sleep(1); // NBT sleep

    }
    else if (lock == 1)
    {
        com.nbiot->ctrl->pwrhold = 1;
        com.nbiot->sleep(0); //NBT wake up
    }
}

static void com_mdl_rak_nbiot_local_param_reset(void)
{
    com_nbiot_local_param.nbcfg.value = 0;
}

emos_process( mdl_rak_nbiot, 
{
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            com_mdl_rak_nbiot_init();
            break;
        case TASK_MODE_POLLING:
            do
            {
                static uint8_t keep_status = 0;
                static uint8_t rdycnt = 0;
                uint8_t taskcnt = com.vsys->mbox()->taskcnt(THIS_TASK_ID);
                if (taskcnt !=0 && keep_status == 0)
                {
                    // DBG_INFO("--->PWR_ON\r\n");
                    
                    rdycnt = 0;
                }
                
                if (taskcnt == 0 && keep_status != 0)
                {
                    rdycnt = 3;
                }

                switch (rdycnt)
                {
                    case 0:
                        break;
                    case 1:
                        //DBG_INFO("--->PWR_OFF\r\n");
                        if (com.radio->ctrl->lpwan_mode != 2 && com.nbiot->ctrl->nbt_state == 1)
                        {
                            nbiot_sendprf("AT+QPOWD=1\r\n\032\0");
                            com.nbiot->ctrl->nbt_state = 0;
                        }

                    default:
                        rdycnt--;
                        break;
                }

                keep_status = taskcnt;
            } while (0);
            
            break;

        case TASK_MODE_TIMEOUT:
            switch (this_timer_id)
            {
                case COM_NBIOT_TIMER_CREG_CHECK:
                    if(com.nbiot->ctrl->nbt_context == 0)
                    {
                        com.nbiot->network_check();
                    }
                    break;
                case COM_NBIOT_TIMER_SCRIPT_DELAY:
                    break;
                default:
                    break;
            }
            break;

        case TASK_MODE_SLEEP:
            do
            {
                uint32_t this_time = emos_gettick();
                if (this_time > this_sleep_time)
                {
                    if (com.nbiot->ctrl->intr == IENABLE)
                    {
                        com_vsys_send( COM_TASK_EVENT, COM_EVENT_NBIOT_CMDTIMEOUT, NULL, 0);
                        
                        this_sleep_time = 0;
                        com.nbiot->ctrl->intr = IDISABLE;
                    }
                    com_nbiot_set_wakeup();
                }
            } while (0);
            break;

        case TASK_MODE_WAKEUP:
            this_sleep_time = 0;
            break;
            
        case TASK_MODE_EVENT:
            if (com.radio->ctrl->lpwan_mode != 2)
            {
                return;
            }

            if (com.nbiot->ctrl->nbt_test_mode == 1)
            {
                switch(msg_type->msg_id)
                {
                case COM_TASK_RAK_NBIOT_POWER:

                    emos.gpio->o_dir_output((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                    if(task_msg->payload[0]) 
                    {
                        com.nbiot->ctrl->nbt_state = 1;
                        emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                        emos.gpio->o_phigh((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                        emos_delay(500);
                        emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                    } 
                    else if ((task_msg->payload[0] == 0)) 
                    {
                        emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                        emos.gpio->o_phigh((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                        emos_delay(1000);
                        emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                    }
                    break;
                case COM_TASK_RAK_NBIOT_SLEEP:
                    com_nbiot_sleep(task_msg->payload[0]);
                    break;
                case COM_TASK_RAK_NBIOT_RECV:
                    com_nbiot_recv_command(msg_payload, msg_len);
                    break;
                }
            }
            else
            {

                switch(msg_type->msg_id)
                {
                    case COM_TASK_RAK_NBIOT_INIT:
                        com_mdl_rak_nbiot_init();
                        break;
                    case COM_TASK_RAK_NBIOT_POWER:

                        emos.gpio->o_dir_output((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                        if(task_msg->payload[0]) 
                        {
                            com.nbiot->ctrl->nbt_state = 1;
                            emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                            emos.gpio->o_phigh((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                            emos_delay(500);
                            emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                        } 
                        else if ((task_msg->payload[0] == 0)) 
                        {
                            emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                            emos.gpio->o_phigh((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                            emos_delay(1000);
                            emos.gpio->o_plow((NBIOT_PWRKEY/100), (NBIOT_PWRKEY%100));
                        }
                        com_nbiot_set_timer(COM_NBIOT_TIMER_CREG_CHECK, 5000);
                        break;
                    case COM_TASK_RAK_NBIOT_POWER_CHECK:
                        com_mdl_rak_nbiot_power_check();
                        break;
                    case COM_TASK_RAK_NBIOT_NETWORK_CHECK:
                        com_nbiot_network_check();
                        break;
                    case COM_TASK_RAK_NBIOT_SLEEP:
                        com_nbiot_sleep(task_msg->payload[0]);
                        break;
                    case COM_TASK_RAK_NBIOT_CMD:
                        com_nbiot_run_usercmd(msg_payload, msg_len);
                        break;
                    case COM_TASK_RAK_NBIOT_RECV:
                        com_nbiot_recv_command(msg_payload, msg_len);
                        break;
                    case COM_TASK_RAK_NBIOT_SCRIPT:
                        com_nbiot_run_loop(msg_payload, msg_len);
                        break;
                    case COM_TASK_RAK_NBIOT_APPLY_CONNECTION:
                        com_nbiot_apply_connection_start(task_msg->payload[0]);
                        break;
                    case COM_TASK_RAK_NBIOT_TCP_UPLOAD:
                        com_nbiot_tcp_upload_start(msg_payload, msg_len);
                        break;
                    case COM_TASK_RAK_NBIOT_SERVER_AUTH:
                        com_mdl_rak_nbiot_server_auth(task_msg->payload[0]);
                        break;
                    case COM_TASK_RAK_NBIOT_AUTH_CACERT:
                        com_mdl_rak_nbiot_auth_cacert(msg_payload, msg_len);
                        break;
                    case COM_TASK_RAK_NBIOT_AUTH_CLIENT_CERT:
                        com_mdl_rak_nbiot_auth_client_cert(msg_payload, msg_len);
                        break;
                    case COM_TASK_RAK_NBIOT_AUTH_CLIENT_KEY:
                        com_mdl_rak_nbiot_auth_client_key(msg_payload, msg_len);
                        break;
                    case COM_TASK_RAK_NBIOT_GPS_ENABLE:
                        com_mdl_rak_nbiot_gps_enable();
                        break;
                    case COM_TASK_RAK_NBIOT_GPS_DATA:
                        com_mdl_rak_nbiot_gps_data();
                        break;
                    case COM_TASK_RAK_NBIOT_BLE_LOCK:
                        com_mdl_rak_nbiot_ble_lock(task_msg->payload[0]);
                    case COM_TASK_RAK_NBIOT_LOCAL_PARAM_RESET:
                        com_mdl_rak_nbiot_local_param_reset();
                        break;
                    case COM_TASK_RAK_NBIOT_DUMMY:
                        break;
                    default:
                        break;
                }
            }
            break;
    }
})

#else

#ifdef BSP_NOT_SUPPORT_WEAK

emos_process( mdl_rak_nbiot, {})

#endif //BSP_NOT_SUPPORT_WEAK

#endif //EMOS_SUPPORT_NBIOT
