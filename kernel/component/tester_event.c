#include "../kernel.h"

#ifdef EMOS_SUPPORT_TESTER

#include "inc/inc_co_service.h"
#include "inc/inc_co_rak_sensor.h"
#include "inc/inc_co_rak_wire_protocol.h"
#include "inc/inc_co_hallsnsr.h"

void com_event_role_init(uint16_t msg_id, uint8_t * msg_payload, uint16_t msg_len)
{
}

void com_event_role_process(uint16_t msg_id, uint8_t * msg_payload, uint16_t msg_len)
{
    com_if_vsys_mydev_t * mydev = com_mydev();
    switch (msg_id)
    {
        case COM_EVENT_NEXT_WAKEUP:
            com_vsys_send( COM_TASK_SERV, COM_TASK_SERV_AFTER_TIME_SHOT, NULL, 0);
            break;

        case COM_EVENT_SLEEP:
           break;

        case COM_EVENT_SNSR_CKSTART:
            com_vsys_send( COM_USER_EVENT, USER_EVENT_START_PREPARE, NULL, 0);
            com_vsys_send( COM_TASK_RAK_SENSOR, COM_VSYS_RAK_SNSR_START, NULL, 0);
            break;
        case COM_EVENT_SNSR_CKDONE:
            com_vsys_send( COM_USER_EVENT, USER_EVENT_SENSOR_CKDONE, NULL, 0);
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROVISION_START, NULL, 0);
            break;

        case COM_EVENT_PRI_TIMER_SHOT:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SNSR_SYNCSTART, NULL, 0);
            break;
        
        case COM_EVENT_SNSR_SYNCSTART:
            com_vsys_send( COM_TASK_RAK_SENSOR, COM_VSYS_RAK_SNSR_SYNC, NULL, 0);
            break;

        case COM_EVENT_PRI_TIMER_TRIG:
            break;

        case COM_EVENT_SNSR_SYNCDONE:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_NEXT_WAKEUP, NULL, 0);
            break;


        case COM_EVENT_PROVISION_START:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROVISION_DONE, NULL, 0);
            break;

        case COM_EVENT_PROVISION_DONE:
            if ( com_sprobe(0).loc_probe_get(&(com_iprobe)) == EMOS_OK )
            {
                SYS_INFO(EVT_MSG("ADD_PROBE:%d"),com_iprobe->id);
            }
            
            com_vsys_send( COM_TASK_RAK_SENSOR, COM_VSYS_RAK_SNSR_UPDATE, NULL, 0);
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_PRI_TIMER_SHOT, NULL, 0);
            break;

        case COM_EVENT_SNSR_DATREQ:
            goto GOTO_COM_EVENT_PROTOCOL_DATRDY;
            break;

        GOTO_COM_EVENT_PROTOCOL_DATRDY:
        case COM_EVENT_PROTOCOL_DATRDY:
            com_vsys_send( COM_TASK_SERV, COM_TASK_SERV_SEND, msg_payload, msg_len);
            break;

        case COM_EVENT_PROBE_ONLINE:
            SYS_INFO(EVT_MSG("ADD_PROBE:%d"),msg_payload[0]);
            break;
    }
}
#endif
