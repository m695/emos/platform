#include "../../kernel.h"
#include "../inc/inc_co_rak_ldo.h"


static void do_rak_ldo_v33 ( void );
static void do_rak_ldo_v120 ( void );
static void do_rak_ldo_v240 ( void );
static void do_rak_ldo_vccprb_rest ( void );

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static com_##X##_t hookp[1]; \
    if ( adr != NULL ) hookp[0] = adr; \
    return hookp[0]; \
}

do_install(rak_ldo_v33)
do_install(rak_ldo_v120)
do_install(rak_ldo_v240)

const com_if_rak_ldo_t com_if_rak_ldo = 
{                       
    .i.v120 = (void *)do_install_rak_ldo_v120,                   
    .i.v240 = (void *)do_install_rak_ldo_v240,  
    .i.v33  = (void *)do_install_rak_ldo_v33,
    .v33 = do_rak_ldo_v33,
    .v120 = do_rak_ldo_v120,
    .v240 = do_rak_ldo_v240,
    .vccprb_rest = do_rak_ldo_vccprb_rest,
};

static void do_rak_ldo_v33 ( void )
{
    com_rak_ldo_v33_t this_func = com.ldo->i.v33(NULL);

    ICHECK(this_func,);

    this_func();
}

static void do_rak_ldo_v120 ( void )
{
    com_rak_ldo_v120_t this_func = com.ldo->i.v120(NULL);

    ICHECK(this_func,);

    this_func();
}

static void do_rak_ldo_v240 ( void )
{
    com_rak_ldo_v240_t this_func = com.ldo->i.v240(NULL);

    ICHECK(this_func,);

    this_func();
}

static void do_rak_ldo_vccprb_rest ( void )
{
    com_vsys_send(COM_TASK_RAK_LDO, COM_TASK_EVT_RAK_LDO_VCCPROBE_RESET, NULL, 0);
}