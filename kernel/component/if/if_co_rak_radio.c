#include "../../kernel.h"

#include "../inc/inc_co_rak_radio.h"

static com_if_rak_radio_ctrl_t radio_ctrl;

static void do_rak_radio_lpwan ( uint8_t type );
static void do_rak_radio_swap_lora ( void );
static void do_rak_radio_swap_nbiot ( void );
static void do_rak_radio_enable ( uint8_t value );

const com_if_rak_radio_t com_if_rak_radio =
{
    .ctrl       = &radio_ctrl,
    .lpwan      = do_rak_radio_lpwan,
    .swap_lora  = do_rak_radio_swap_lora,
    .swap_nbiot = do_rak_radio_swap_nbiot,
    .enable     = do_rak_radio_enable,
};

static void do_rak_radio_lpwan ( uint8_t type )
{
    com_vsys_send( COM_TASK_RAK_RADIO, COM_TASK_RAK_RADIO_LPWAN_SWITCH, &type, 1);
}

static void do_rak_radio_swap_lora( void )
{
    com_vsys_send( COM_TASK_RAK_RADIO, COM_TASK_RAK_RADIO_SWAP_TO_LORA, NULL, 0);
}

static void do_rak_radio_swap_nbiot( void )
{
    com_vsys_send( COM_TASK_RAK_RADIO, COM_TASK_RAK_RADIO_SWAP_TO_NBIOT, NULL, 0);
}

static void do_rak_radio_enable (uint8_t value)
{
    com_vsys_send( COM_TASK_RAK_RADIO, COM_TASK_RAK_RADIO_ENABLE, &value, 1);
}
