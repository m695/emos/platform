#include "../../kernel.h"

#include "../inc/inc_co_nbiot.h"

#define THIS_BUFF_SIZE   NBIOT_BUFF_SIZE

static com_if_rak_nbiot_ctrl_t nbiot_ctrl;

static void do_nbiot_init ( void );
static void do_nbiot_power ( uint8_t power );
static void do_nbiot_power_check ( void );
static void do_nbiot_network_check ( void );
static void do_nbiot_sleep ( uint8_t sleep );
static void do_nbiot_send  ( uint8_t *payload, uint8_t length );
static void do_nbiot_apply_connection ( uint8_t value );
static void do_nbiot_upload ( uint8_t *payload, uint16_t length );
static void do_nbiot_server_auth (uint8_t type);
static void do_nbiot_auth_cacert ( uint8_t *payload, uint16_t length );
static void do_nbiot_auth_client_cert ( uint8_t *payload, uint16_t length );
static void do_nbiot_auth_client_key ( uint8_t *payload, uint16_t length );
static void do_nbiot_gps_enable ( uint8_t enable );
static void do_nbiot_gps_data ( void );
static void do_nbiot_http_request ( uint8_t *payload, uint16_t length );
static void do_nbiot_ble_lock (uint8_t lock);
static void do_nbiot_local_param_reset ( void );

static void do_rak_nbiot_key( uint8_t dir, uint8_t *data ,uint16_t len);

static void do_rak_nbiot_enter_key(uint8_t *data ,uint16_t len);
static void do_rak_nbiot_recv_key(uint8_t *data ,uint16_t len);
static void do_rak_nbiot_puts(uint8_t *cmd ,uint16_t cmd_len);
static void do_rak_nbiot_vprf(const char *fmt, va_list arg);
static void do_rak_nbiot_printf(char *fmt, ...);
// static void do_rak_nbiot_enter_command(uint8_t *cmd ,uint16_t cmd_len);
static void do_rak_nbiot_recv_command(uint8_t *cmd ,uint16_t cmd_len);

const com_if_rak_nbiot_t com_if_rak_nbiot = 
{
    .ctrl          = &nbiot_ctrl,
    .enter.key     = do_rak_nbiot_enter_key,
    .enter.printf  = do_rak_nbiot_printf,
    .enter.puts    = do_rak_nbiot_puts,
    .recv.key      = do_rak_nbiot_recv_key,
    .recv.command  = do_rak_nbiot_recv_command,
    .init          = do_nbiot_init,
    .power = do_nbiot_power,
    .power_check = do_nbiot_power_check,
    .network_check = do_nbiot_network_check,
    .sleep = do_nbiot_sleep,
    .send  = do_nbiot_send,
    .apply = do_nbiot_apply_connection,
    .upload = do_nbiot_upload,
    .server_auth = do_nbiot_server_auth,
    .auth_cacert = do_nbiot_auth_cacert,
    .auth_clientcert = do_nbiot_auth_client_cert,
    .auth_clientkey = do_nbiot_auth_client_key,
    .gps_enable = do_nbiot_gps_enable,
    .gps_data = do_nbiot_gps_data,
    .http_request = do_nbiot_http_request,
    .ble_lock = do_nbiot_ble_lock,
    .local_param_reset = do_nbiot_local_param_reset,
};

static void do_nbiot_init( void )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_INIT, NULL, 0);
}

static void do_nbiot_power( uint8_t power )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_POWER, &power, 1);
}

static void do_nbiot_power_check( void )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_POWER_CHECK, NULL, 0);
}

static void do_nbiot_network_check( void )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_NETWORK_CHECK, NULL, 0);
}

static void do_nbiot_sleep( uint8_t sleep )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_SLEEP, &sleep, 1);
}

static void do_nbiot_send( uint8_t *payload, uint8_t length)
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_SEND, payload, length);
}

static void do_nbiot_apply_connection( uint8_t value )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_APPLY_CONNECTION, &value, 1);
}

static void do_nbiot_upload ( uint8_t *payload, uint16_t length )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_TCP_UPLOAD, payload, length);
}

static void do_nbiot_server_auth (uint8_t type)
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_SERVER_AUTH, &type, 1);
}

static void do_nbiot_auth_cacert ( uint8_t *payload, uint16_t length )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_AUTH_CACERT, payload, length);
}

static void do_nbiot_auth_client_cert ( uint8_t *payload, uint16_t length )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_AUTH_CLIENT_CERT, payload, length);
}

static void do_nbiot_auth_client_key ( uint8_t *payload, uint16_t length )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_AUTH_CLIENT_KEY, payload, length);
}

static void do_nbiot_gps_enable ( uint8_t enable )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_GPS_ENABLE, &enable, 0);
}

static void do_nbiot_gps_data (void )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_GPS_DATA, NULL, 0);
}

static void do_nbiot_http_request ( uint8_t *payload, uint16_t length )
{
//    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_HTTP_REQUEST, payload, length);
}

static void do_nbiot_ble_lock ( uint8_t lock)
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_BLE_LOCK, &lock, 1);
}

static void do_nbiot_local_param_reset ( void )
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_LOCAL_PARAM_RESET, NULL, 0);
}

static void do_rak_nbiot_enter_key(uint8_t *data ,uint16_t len)
{
    do_rak_nbiot_key(COM_NBIOT_CMD_ENTER,data,len);
}

static void do_rak_nbiot_recv_key(uint8_t *data ,uint16_t len)
{
    do_rak_nbiot_key(COM_NBIOT_CMD_RECV,data,len);
}

static void do_rak_nbiot_key(uint8_t dir, uint8_t *data ,uint16_t len)
{
    static com_nbiot_buff_t this_buff[COM_NBIOT_CMD_DIR_MAX];
    uint8_t *tmp_stack = NULL;
    uint16_t i,j;

    com_nbiot_buff_t *nbiot_buff = &this_buff[dir];

    for( i=0 ; i<len; i++)
    {
        
        if( data[i] == 0x0d || data[i] == 0x0a)
        {
            nbiot_buff->buff[nbiot_buff->wp++] = '\0';
            
            uint16_t stack_size = 0;
            if( nbiot_buff->wp > nbiot_buff->rp )
            {
                stack_size = nbiot_buff->wp - nbiot_buff->rp;
            }
            else
            {
                stack_size = (THIS_BUFF_SIZE - nbiot_buff->rp) + nbiot_buff->wp;
            }

            tmp_stack = (uint8_t *)emos_malloc(stack_size);
            for( j=0 ; j<stack_size; j++)
            {   
                tmp_stack[j] =  nbiot_buff->buff[nbiot_buff->rp++];
                if( !(nbiot_buff->wp > nbiot_buff->rp) )
                {
                    if( !(nbiot_buff->rp < THIS_BUFF_SIZE))
                    {
                        nbiot_buff->rp = 0;
                    }
                }
            }

            stack_size -= 1;
            tmp_stack[stack_size] = 0;

            if(stack_size != 0)
            {
                do
                {
                    uint8_t dir_cmd = 0;
                
                    switch (dir)
                    {
                        case COM_NBIOT_CMD_ENTER:
                            dir_cmd = COM_EVENT_NBIOT_ENT_COMMAND;
                            break;
                        case COM_NBIOT_CMD_RECV:
                            dir_cmd = COM_EVENT_NBIOT_RECV_COMMAND;
                            break;
                        default:
                            break;
                    }

                    com_vsys_send( COM_TASK_EVENT, dir_cmd, tmp_stack, strlen((char *)tmp_stack) + 1);
                } while (0);
                
            }
            
            emos_free(tmp_stack);

            memset((void *)nbiot_buff, 0, sizeof(com_nbiot_buff_t));
        }
        else 
        {
            nbiot_buff->buff[nbiot_buff->wp++] = data[i];
            if( !(nbiot_buff->wp < THIS_BUFF_SIZE))
            {
               nbiot_buff->wp = 0;
            }
            if( data[i] == 0x7f || data[i] == 0x08)
            {
                nbiot_buff->wp--;
                if( nbiot_buff->wp > nbiot_buff->rp )
                {
                    nbiot_buff->wp--;
                }
            } else 
            {

            }
        }
    }
}

static void do_rak_nbiot_vprf(const char *fmt, va_list arg)
{
    com_rak_nbiot_puts_t this_puts = (com.nbiot->enter.puts);

    #define THIS_BUF_SIZE   EMOS_UART_RECV_BUFF

    char tmp_buf[THIS_BUF_SIZE];

    uint16_t tmp_len;
    do
    {
        tmp_len = vsnprintf( NULL, 0, fmt, arg) + 1; 
        memset(tmp_buf, 0, sizeof(tmp_buf));
        vsnprintf( tmp_buf, tmp_len, fmt, arg); 
    } while (0);

    this_puts((uint8_t *)tmp_buf , strlen(tmp_buf));
}

static void do_rak_nbiot_printf(char *fmt, ...)
{
    va_list arg; 
    va_start(arg,fmt); 
    do_rak_nbiot_vprf(fmt,arg);
    va_end(arg);
}

static void do_rak_nbiot_puts(uint8_t *cmd ,uint16_t cmd_len)
{
    // com_if_rak_tp_t tp_data;
    // tp_data.interface = TP_IF_NB;
    // com.tp->sndbtp(&tp_data, cmd, cmd_len);
    com_vsys_send( COM_TASK_EVENT, COM_EVENT_NBIOT_TRANS, cmd, cmd_len);
    return;
}

static void do_rak_nbiot_recv_command(uint8_t *cmd ,uint16_t cmd_len)
{
    com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_RECV, cmd, cmd_len);
}

