#include "../../kernel.h"
#include "../inc/inc_co_rak_sensor.h"

/***************************************************/

#define THIS_SNSR_NUM  RAK_SNSR_NUM

static uint8_t this_idx;
static com_if_rak_sensor_sta_t this_status[THIS_SNSR_NUM];

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static com_##X##_t hookp[THIS_SNSR_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(snsr_check);
do_install(snsr_sleep);
do_install(snsr_wakeup);
do_install(snsr_warmup);
do_install(snsr_read);
do_install(snsr_process);

static uint8_t dummy_func( void ){ return EMOS_ERROR; };
static uint8_t dummy_func0( void ){ return EMOS_OK; };
static uint8_t do_snsr_check( void );
static uint8_t do_snsr_sleep( void );
static uint8_t do_snsr_wakeup( void );
static uint16_t do_snsr_warmup( void );
static uint8_t do_snsr_process( void );
static uint8_t do_snsr_reset( void );
static uint16_t do_snsr_read( uint8_t *data );
static com_if_rak_sensor_sta_t * do_snsr_status ( void );

static const com_if_rak_sensor_t com_if_rak_sensor = 
{
    .i.check  = do_install_snsr_check,
    .i.sleep  = do_install_snsr_sleep,
    .i.wakeup = do_install_snsr_wakeup,
    .i.warmup = do_install_snsr_warmup,
    .i.read   = do_install_snsr_read,
    .i.process   = do_install_snsr_process,
    .check    = do_snsr_check,
    .sleep    = do_snsr_sleep,
    .wakeup   = do_snsr_wakeup,
    .read     = do_snsr_read,
    .reset    = do_snsr_reset,
    .process  = do_snsr_process,
    .warmup   = do_snsr_warmup,
    .status   = do_snsr_status,
};

com_if_rak_sensor_t com_if_rak_sensor_func(uint8_t idx)
{
    this_idx = idx;
    return com_if_rak_sensor;
}

static com_if_rak_sensor_sta_t * do_snsr_status ( void )
{
    uint8_t idx = this_idx;
    return &this_status[idx];
}

static uint8_t do_snsr_reset( void )
{
    uint8_t idx = this_idx;
    com_if_rak_sensor_t this_func = com.rak_sensor(idx);
    this_func.i.check(dummy_func);
    this_func.i.read(dummy_func);
    this_func.i.process(dummy_func);
    this_func.i.sleep(dummy_func);
    this_func.i.wakeup(dummy_func);
    this_func.i.warmup(dummy_func0);
    this_func.status()->value = 0;
    return EMOS_OK;
}

static uint8_t do_snsr_check( void )
{
    uint8_t idx = this_idx;
    uint8_t ret = EMOS_ERROR;
    com_snsr_check_t this_func = com.rak_sensor(idx).i.check(NULL);
    ret = this_func();
    this_status[idx].exist = 0;
    if( ret == EMOS_OK)
    {
        this_status[idx].exist = 1;
    }
    return ret;
}

static uint8_t do_snsr_sleep( void )
{
    uint8_t idx = this_idx;
    uint8_t ret = EMOS_ERROR;
    if(this_status[idx].exist == EMOS_OK)
    {
        return ret;
    }
    com_snsr_sleep_t this_func = com.rak_sensor(idx).i.sleep(NULL);
    this_func();
    ret = EMOS_OK;
    return ret;
}

static uint8_t do_snsr_wakeup( void )
{
    uint8_t idx = this_idx;
    uint8_t ret = EMOS_ERROR;
    if(this_status[idx].exist == EMOS_OK)
    {
        return ret;
    }
    com_snsr_wakeup_t this_func = com.rak_sensor(idx).i.wakeup(NULL);
    this_func();
    ret = EMOS_OK;
    return ret;
}

static uint16_t do_snsr_read( uint8_t *data )
{
    uint8_t idx = this_idx;
    uint8_t ret = EMOS_ERROR;
    if(this_status[idx].exist == EMOS_OK)
    {
        return ret;
    }
    com_snsr_read_t this_func = com.rak_sensor(idx).i.read(NULL);
    return this_func( data );
}

static uint8_t do_snsr_process( void )
{
    uint8_t idx = this_idx;
    uint8_t ret = EMOS_ERROR;
    if(this_status[idx].exist == EMOS_OK)
    {
        return ret;
    }
    com_snsr_process_t this_func = com.rak_sensor(idx).i.process(NULL);
    this_func();
    ret = EMOS_OK;

    return ret;
}

static uint16_t do_snsr_warmup( void )
{
    uint8_t idx = this_idx;
    uint8_t ret = EMOS_ERROR;
    if(this_status[idx].exist == EMOS_OK)
    {
        return 0;
    }
    com_snsr_warmup_t this_func = com.rak_sensor(idx).i.warmup(NULL);
    
    return this_func();
}
