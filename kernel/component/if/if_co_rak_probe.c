#include "../../kernel.h"
#include "../inc/inc_co_rak_probe.h"

static uint8_t this_idx;

static struct
{
    void *next; 
} this_param_root;

#ifdef EMOS_SUPPORT_TESTER
static struct
{
    void *next; 
} this_param_local_root;
#endif

#ifdef EMOS_SUPPORT_TESTER
#define THIS_ROOT(X)    &this_param_local_root
#else
#define THIS_ROOT(X)    X
#endif

static void do_new_probe( void );
static uint8_t do_probe_count( void );
static uint8_t do_probe_get( com_if_rak_probe_param_t **data);
static uint8_t do_probe_walk( com_if_rak_probe_param_t **data);
static void do_probe_del( void );


static uint8_t do_loc_probe_get( com_if_rak_probe_param_t **data);


static void do_new_sensor( uint8_t snsr_id ,uint8_t snsr_type);
static uint8_t do_snsr_count( void );
static uint8_t do_sensor_walk(uint8_t sensor_idx, com_if_rak_probe_sensor_t **data);
static uint8_t do_sensor_get(uint8_t sensor_idx, com_if_rak_probe_sensor_t **data);
static uint8_t do_sensor_save(uint8_t sensor_idx);

static void do_sensor_del( uint8_t sensor_idx );
static uint8_t do_probe_save( void );

static const com_if_rak_probe_t com_if_rak_probe = 
{
    .new_probe   = do_new_probe,
    .loc_probe_get = do_loc_probe_get,

    .probe_count = do_probe_count,
    .probe_get   = do_probe_get,
    .probe_walk  = do_probe_walk,
    .probe_del   = do_probe_del,
    .probe_save  = do_probe_save,

    .new_sensor = do_new_sensor,
    .snsr_count = do_snsr_count,
    .snsr_walk  = do_sensor_walk,
    .snsr_get   = do_sensor_get,
    .snsr_save  = do_sensor_save,
    .snsr_del   = do_sensor_del,
};

com_if_rak_probe_t com_if_rak_probe_func(uint8_t idx)
{
    this_idx = idx;
    return com_if_rak_probe;
}

static void do_new_probe( void )
{
    uint8_t idx = this_idx;

    com_if_rak_probe_node_t *new_probe;
    com_if_rak_bank0_t *eee;
    uint32_t adr = (uint32_t)&this_param_root;

    do_probe_del();
    new_probe = (com_if_rak_probe_node_t *)emos.llist->create( COM_PARAM_POOL_ID, (void **)adr, sizeof(com_if_rak_probe_node_t));
    new_probe->info.id = idx;
    new_probe->info.snsr_root.next = NULL;
    new_probe->info.sta.stop = IDISABLE;
    new_probe->info.sta.real = IDISABLE;
    new_probe->info.sta.nocheck = IDISABLE;
    
    new_probe->info.hw_version='X';
    new_probe->info.sw_version[0] = BUILD_MAJOR_VERSION;
    new_probe->info.sw_version[1] = BUILD_MINOR_VERSION;
    new_probe->info.sw_version[2] = BUILD_MICRO_VERSION;
    
    #define PROBE_DEF_MDNAME   "rak2560-default"
    #define PROBE_DEF_SN       "xxxxxxxxxxxxxxxxxx"
    #define PROBE_DEF_SSN      0x99
    memcpy(new_probe->info.model_name,PROBE_DEF_MDNAME,strlen(PROBE_DEF_MDNAME));
    memcpy(new_probe->info.sn.value,PROBE_DEF_SN,sizeof(new_probe->info.sn.value));
    memset(new_probe->info.ssn.value,PROBE_DEF_SSN,sizeof(new_probe->info.ssn.value));

    com_flash_bank0_readonly((uint8_t **)&eee, sizeof(com_if_rak_bank0_t));
    new_probe->info.intv = eee->probe_interval;
}

static uint8_t do_probe_count( void ) 
{
    uint32_t cnt = 0;
    uint32_t adr = (uint32_t)&this_param_root;
    cnt = emos.llist->count( COM_PARAM_POOL_ID, (void **)adr);
    return (uint8_t)cnt;
}

static uint8_t do_probe_get( com_if_rak_probe_param_t **data)
{
    uint8_t idx = this_idx;
    uint32_t cnt = 0;

    uint32_t adr = (uint32_t)&this_param_root;
    com_if_rak_probe_node_t *walk_probe;
    cnt = do_probe_count();
    for(uint8_t i=0; i< cnt; i++)
    {
        walk_probe = emos.llist->get( COM_PARAM_POOL_ID, (void **)adr, i);
        if(walk_probe->info.id == idx)
        {
            *data = &(walk_probe->info);
            return 0;
        }
    }
    return 1;
}

static uint8_t do_probe_walk( com_if_rak_probe_param_t **data)
{
    uint8_t idx = this_idx;
    uint32_t cnt = 0;

    uint32_t adr = (uint32_t)&this_param_root;
    com_if_rak_probe_node_t *walk_probe;
    cnt = do_probe_count();

    if( idx > cnt)
    {
        return 1;
    }

    walk_probe = emos.llist->get( COM_PARAM_POOL_ID, (void **)adr, idx);
    *data = &(walk_probe->info);
    return 0;
}

static uint8_t do_snsr_count( void ) 
{
    uint8_t idx = this_idx;
    uint8_t ret;
    uint32_t adr;
    uint32_t cnt = 0;
    UNUSED(idx);

    com_if_rak_probe_param_t * probe_info;
    ret = do_probe_get(&probe_info);
    if (ret != EMOS_OK)
    {
        return cnt;
    }
    
    adr = (uint32_t)&(probe_info->snsr_root);
    adr = THIS_ROOT(adr);
    cnt = emos.llist->count( COM_PARAM_POOL_ID, (void **)adr);
    return (uint8_t)cnt;
}

static void do_new_sensor( uint8_t snsr_id ,uint8_t snsr_type)
{
    uint8_t idx = this_idx;
    uint8_t ret;
    uint8_t snsr_cnt = 0;
    uint32_t adr;
    com_if_rak_probe_sensor_node_t *new_sensor;
    com_if_rak_probe_param_t * probe_info;
    ret = do_probe_get(&probe_info);
    if (ret)
    {
        return;
    }
    UNUSED(idx);

    do_sensor_del(snsr_id);
    snsr_cnt = do_snsr_count();
    adr = (uint32_t)&(probe_info->snsr_root);
    adr = THIS_ROOT(adr);
    new_sensor = (com_if_rak_probe_sensor_node_t *)emos.llist->create( COM_PARAM_POOL_ID, (void **)adr, sizeof(com_if_rak_probe_sensor_node_t));
    new_sensor->info.ctrl.id = snsr_id;
    new_sensor->info.ctrl.type = snsr_type;
    new_sensor->info.data.id = snsr_id;
    new_sensor->info.data.type = snsr_type;
    new_sensor->info.data.len = com_ipso_size(snsr_type);
    new_sensor->info.id = snsr_id;
    new_sensor->info.sta.init = 1;
    new_sensor->info.sta.stop = IDISABLE;
    new_sensor->info.sta.busy = 0;
    new_sensor->info.intv = 60;
    new_sensor->info.sta.newdat = 0;
    new_sensor->info.idx = snsr_cnt;

    do
    {
        if (probe_info->sta.real == IDISABLE)
        {
            break;
        }
        
        com_if_rak_bank1_page_t fff;
        uint8_t page_size = sizeof(com_if_rak_bank1_page_t);
        uint32_t page_idx = (page_size * (snsr_cnt));
        com_flash_bank1_read(page_idx,(uint8_t *)&fff, page_size);
        
        new_sensor->info.sta.init = 0;
        new_sensor->info.intv = fff.interval;
        new_sensor->info.ctrl.rule = fff.rule_flg;
        memcpy(new_sensor->info.thr.above, fff.hthr, 10);
        memcpy(new_sensor->info.thr.below, fff.lthr, 10);
    } while (0);
}

static uint8_t do_sensor_get(uint8_t sensor_idx, com_if_rak_probe_sensor_t **data)
{
    uint8_t idx = this_idx;
    uint32_t cnt = 0;
    uint8_t ret;
    uint32_t adr;
    UNUSED(idx);
    com_if_rak_probe_sensor_node_t *walk_sensor;
    com_if_rak_probe_param_t * probe_info;
    ret = do_probe_get(&probe_info);

    if (ret)
    {
        return ret;
    }
    
    adr = (uint32_t)&(probe_info->snsr_root);
    adr = THIS_ROOT(adr);
    cnt = do_snsr_count();
    for(uint8_t i=0; i< cnt; i++)
    {
        walk_sensor = emos.llist->get( COM_PARAM_POOL_ID, (void **)adr, i);
        if(walk_sensor->info.ctrl.id == sensor_idx)
        {
            *data = &(walk_sensor->info);
            return 0;
        }
    }
    return 1;
}

static uint8_t do_sensor_walk(uint8_t sensor_idx, com_if_rak_probe_sensor_t **data)
{
    uint8_t idx = this_idx;
    uint8_t ret;
    uint32_t adr;
    uint32_t cnt = 0;

    com_if_rak_probe_sensor_node_t *walk_sensor;
    com_if_rak_probe_param_t * probe_info;
    ret = do_probe_get(&probe_info);
    UNUSED(ret);
    UNUSED(idx);
    adr = (uint32_t)&(probe_info->snsr_root);
    adr = THIS_ROOT(adr);

    walk_sensor = emos.llist->get( COM_PARAM_POOL_ID, (void **)adr, sensor_idx);
    *data = (com_if_rak_probe_sensor_t *)&(walk_sensor);

    cnt = emos.llist->count( COM_PARAM_POOL_ID, (void **)adr);

    if( sensor_idx > cnt)
    {
        return 1;
    }

    // walk_probe = emos.llist->get( COM_PARAM_POOL_ID, (void **)adr, idx);
    *data = &(walk_sensor->info);
    return 0;
}

static void do_probe_del( void )
{
    uint8_t idx = this_idx;
    uint32_t cnt = 0;
    uint32_t snsr_cnt = 0;
    
    uint32_t adr = (uint32_t)&this_param_root;
    uint32_t snsr_addr;
    com_if_rak_probe_node_t *walk_probe;
    // com_if_rak_probe_sensor_node_t *walk_sensor;
    com_if_rak_probe_sensor_t * walk_sensor;
    cnt = do_probe_count();

    for(uint8_t i=0; i< cnt; i++)
    {
        walk_probe = emos.llist->get( COM_PARAM_POOL_ID, (void **)adr, i);
        if(walk_probe->info.id == idx)
        {
            snsr_addr = (uint32_t)&(walk_probe->info.snsr_root);
            snsr_addr = THIS_ROOT(snsr_addr);
            snsr_cnt = do_snsr_count();
            for (uint8_t j = 0; j < snsr_cnt; j++)
            {
                do_sensor_walk(j,&walk_sensor);
                emos.llist->remove(COM_PARAM_POOL_ID,(void **)snsr_addr,walk_sensor);
            }
            
            emos.llist->remove(COM_PARAM_POOL_ID,(void **)adr,walk_probe);
            return;
        }
    }
}

static void do_sensor_del( uint8_t sensor_idx )
{
    uint8_t idx = this_idx;

    UNUSED(idx);

    uint8_t ret;

    UNUSED(ret);
    
    uint32_t adr;
    uint32_t snsr_cnt = 0;

    com_if_rak_probe_sensor_node_t *walk_sensor;
    com_if_rak_probe_param_t * probe_info;
    ret = do_probe_get(&probe_info);
    adr = (uint32_t)&(probe_info->snsr_root);
    adr = THIS_ROOT(adr);

    snsr_cnt = do_snsr_count();
    for (uint8_t j = 0; j < snsr_cnt; j++)
    {
        do_sensor_walk(j,(com_if_rak_probe_sensor_t **)&walk_sensor);
        if (walk_sensor->info.id == sensor_idx)
        {
            emos.llist->remove(COM_PARAM_POOL_ID,(void **)adr,walk_sensor);
        }
        
    }
}


static void do_probe_write_flash( void )
{
    com_if_rak_bank0_t eee;
    uint8_t page_size = sizeof(com_if_rak_bank0_t);
    
    UNUSED(page_size);

    com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
    com_if_vsys_mydev_t * mydev = com_mydev();
    
    com_if_rak_probe_param_t * probe_info = NULL;
    uint8_t gret =do_probe_get(&probe_info);

    do
    {
        uint8_t ret = EMOS_OK;
        
        if ( gret == EMOS_OK)
        {
            if (probe_info->intv != eee.probe_interval)
            {
                eee.probe_interval = probe_info->intv;
                ret = EMOS_ERROR;
            }
        }
        
        if (mydev->sampling_interval != eee.sampling_interval)
        {
            eee.sampling_interval = mydev->sampling_interval;
            ret = EMOS_ERROR;
        }

        if (mydev->last_id != eee.keep_id )
        {
            eee.keep_id = mydev->last_id;
            ret = EMOS_ERROR;
        }
        
        if (ret == EMOS_OK)
        {
            break;
        }
        
        com_flash_bank0_write((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
    } while (0);
}

static void do_sensor_write_flash(com_if_rak_probe_sensor_t *snsr_info, uint8_t snsr_idx)
{
    com_if_rak_bank1_page_t fff;
    uint8_t page_size = sizeof(com_if_rak_bank1_page_t);
    uint32_t page_idx = (page_size * (snsr_idx));
    com_flash_bank1_read(page_idx,(uint8_t *)&fff, page_size);
    
    do
    {
        uint8_t ret = EMOS_OK;
        if (fff.interval != (snsr_info->intv))
        {
            fff.interval = (snsr_info->intv);
            ret = EMOS_ERROR;
        }
        
        if (fff.rule_flg != (snsr_info->ctrl.rule))
        {
            fff.rule_flg = (snsr_info->ctrl.rule);
            ret = EMOS_ERROR;
        }

        if (memcmp(fff.hthr,snsr_info->thr.above, sizeof(fff.hthr)) != EMOS_OK)
        {
            memcpy(fff.hthr,snsr_info->thr.above, sizeof(fff.hthr));
            ret = EMOS_ERROR;
        }

        if (memcmp(fff.lthr,snsr_info->thr.below, sizeof(fff.lthr)) != EMOS_OK)
        {
            memcpy(fff.lthr,snsr_info->thr.below, sizeof(fff.lthr));
            ret = EMOS_ERROR;
        }

        if (ret == EMOS_OK)
        {
            break;
        }
        
        com_flash_bank1_write(page_idx,(uint8_t *)&fff, page_size);
    } while (0);
}

static uint8_t do_probe_save( void )
{
    uint8_t idx = this_idx;
    
    UNUSED(idx);

    do_probe_write_flash();
    
    return 1;
}

static uint8_t do_sensor_save(uint8_t sensor_idx)
{
    uint8_t idx = this_idx;
    uint32_t cnt = 0;
    uint8_t ret;
    uint32_t adr;
    UNUSED(idx);
    com_if_rak_probe_sensor_node_t *walk_sensor;
    com_if_rak_probe_param_t * probe_info;
    ret = do_probe_get(&probe_info);

    if (ret)
    {
        return ret;
    }
    
    adr = (uint32_t)&(probe_info->snsr_root);
    adr = THIS_ROOT(adr);
    cnt = do_snsr_count();
    for(uint8_t i=0; i< cnt; i++)
    {
        walk_sensor = emos.llist->get( COM_PARAM_POOL_ID, (void **)adr, i);
        if(walk_sensor->info.ctrl.id == sensor_idx)
        {
            do_sensor_write_flash(&(walk_sensor->info),i);
            break;
        }
    }
    
    return 1;
}

static uint8_t do_loc_probe_get( com_if_rak_probe_param_t **data)
{
    uint8_t idx = this_idx;
    uint32_t cnt = 0;
    uint32_t fcnt = 0;

    uint32_t adr = (uint32_t)&this_param_root;
    com_if_rak_probe_node_t *walk_probe;
    cnt = do_probe_count();
    for(uint8_t i=0; i< cnt; i++)
    {
        walk_probe = emos.llist->get( COM_PARAM_POOL_ID, (void **)adr, i);
        if(walk_probe->info.sta.real == IENABLE)
        {
            if(fcnt == idx)
            {
                *data = &(walk_probe->info);
                return 0;
            }
            fcnt++;
        }
    }
    return 1;
}
