#include "../../kernel.h"
#include "../inc/inc_co_rak_wire_protocol.h"

/***************************************************/
static com_if_rak_protocol_ctrl_t this_ctrl;

static uint8_t ifunc_get_snsr_cnt(uint8_t pid);
static uint8_t ifunc_get_snsr_info(uint8_t pid ,uint8_t sid);
static uint8_t ifunc_get_snsr_intv(uint8_t pid ,uint8_t sid);
static uint8_t ifunc_get_snsr_rule(uint8_t pid ,uint8_t sid);
static uint8_t ifunc_get_snsr_data(uint8_t pid, uint8_t sid);
static uint8_t ifunc_get_snsr_hthr(uint8_t pid, uint8_t sid);
static uint8_t ifunc_get_snsr_lthr(uint8_t pid, uint8_t sid);
static uint8_t ifunc_snd_provision( void );
static uint8_t ifunc_snd_snsr_data(uint8_t pid, uint8_t *data, uint16_t data_len );
static uint8_t ifunc_set_snsr_rule(uint8_t pid, uint8_t sid, uint16_t new_rule );
static uint8_t ifunc_set_snsr_hthr(uint8_t pid, uint8_t sid, uint8_t *new_thr, uint8_t len );
static uint8_t ifunc_set_snsr_lthr(uint8_t pid, uint8_t sid, uint8_t *new_thr, uint8_t len );
static uint8_t ifunc_set_snsr_intv(uint8_t pid, uint8_t sid, uint32_t new_intv );
static uint8_t ifunc_set_probe_reboot( uint8_t pid );
static uint8_t ifunc_set_probe_dfu( uint8_t pid );
static uint8_t ifunc_set_probe_restore( uint8_t pid );
static uint8_t ifunc_set_probe_delete( uint8_t pid );

static uint8_t ifunc_set_prb_intv(uint8_t pid ,uint32_t new_intv );
static uint8_t ifunc_get_prb_intv(uint8_t pid);


static uint8_t ifunc_set_prb_smplintv(uint8_t pid ,uint32_t new_intv );
static uint8_t ifunc_get_prb_smplintv(uint8_t pid);


static uint8_t ifunc_set_scmd_add(uint8_t pid, uint8_t *cmd, uint8_t len);
static uint8_t ifunc_set_scmd_del(uint8_t pid );
static uint8_t ifunc_set_scmd_rest(uint8_t pid );
static uint8_t ifunc_get_scmd_list(uint8_t pid );
static uint8_t ifunc_get_scmd_build(uint8_t pid );

static void ifunc_req_puts( uint8_t *data, uint16_t len);
static void ifunc_rsp_puts( uint8_t *data, uint16_t len);


static uint8_t ifunc_snd_prb_disp( uint8_t mid,uint8_t *data, uint16_t data_len );
static uint8_t ifunc_snd_hello(void);

const com_if_rak_protocol_t com_if_rak_protocol = 
{
    .ctrl         = &this_ctrl,
    .req_puts     = ifunc_req_puts,
    .rsp_puts     = ifunc_rsp_puts,
    .get_snsrcnt  = ifunc_get_snsr_cnt,
    .get_snsrintv = ifunc_get_snsr_intv,
    .get_snsrrule = ifunc_get_snsr_rule,
    .get_snsrdata = ifunc_get_snsr_data,
    .get_snsrhthr = ifunc_get_snsr_hthr,
    .get_snsrlthr = ifunc_get_snsr_lthr,

    .set_snsrrule = ifunc_set_snsr_rule,
    .set_snsrintv = ifunc_set_snsr_intv,
    .set_snsrhthr = ifunc_set_snsr_hthr,
    .set_snsrlthr = ifunc_set_snsr_lthr,
    
    .get_prbsmplintv = ifunc_get_prb_smplintv,
    .set_prbsmplintv = ifunc_set_prb_smplintv,
    .get_prbintv = ifunc_get_prb_intv,
    .set_prbintv = ifunc_set_prb_intv,
    .set_prbrest = ifunc_set_probe_restore,
    .set_prbdfu  = ifunc_set_probe_dfu,
    .set_prbdel  = ifunc_set_probe_delete,
    .set_prbreboot = ifunc_set_probe_reboot,
    .snd_provision = ifunc_snd_provision,
    .snd_snsrdata = ifunc_snd_snsr_data,

    .set_scmd_add = ifunc_set_scmd_add,
    .set_scmd_rest = ifunc_set_scmd_rest,
    .set_scmd_del = ifunc_set_scmd_del,
    .get_scmd_list = ifunc_get_scmd_list,
    .snd_scmd_build = ifunc_get_scmd_build,

    .get_snsrinfo = ifunc_get_snsr_info,

    .snd_prbdisp = ifunc_snd_prb_disp,
    .snd_hello   = ifunc_snd_hello,
};

/******************************************************************************/

static uint8_t get_mid( void )
{
    com_if_vsys_mydev_t * mydev = com_mydev();
    return mydev->id;
}

static uint8_t ifunc_snd_hello(void)
{
#ifdef EMOS_SUPPORT_HOST
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = 0;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_PROVISION;
    param.pay_type = RAK_PB_PAY_TPYE_PRV_HELLO;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
#endif
    return EMOS_OK;
}

static uint8_t ifunc_get_snsr_info(uint8_t pid, uint8_t sid)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMGET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_INFO;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static void ifunc_req_puts( uint8_t *data, uint16_t len)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = 0;
    param.sid = 0;
    param.frm_type = 0;
    param.pay_type = 0;
    param.data_len = len;
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RUI_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),data,param.data_len);
}

static void ifunc_rsp_puts( uint8_t *data, uint16_t len)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = 0;
    param.sid = 0;
    param.frm_type = 0;
    param.pay_type = 0;
    param.data_len = len;
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RUI_SENDRSP,&param,sizeof(rak_probe_wire_send_param_t),data,param.data_len);
}

static uint8_t ifunc_set_prb_intv(uint8_t pid ,uint32_t new_intv )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMSET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_PRB_INTV;
    param.data_len = sizeof(new_intv);
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),&new_intv,param.data_len);
    return EMOS_OK;
}

static uint8_t ifunc_get_prb_intv(uint8_t pid)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMGET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_PRB_INTV;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_set_prb_smplintv(uint8_t pid ,uint32_t new_intv )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMSET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SMPL_INTV;
    param.data_len = sizeof(new_intv);
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),&new_intv,param.data_len);
    return EMOS_OK;
}

static uint8_t ifunc_get_prb_smplintv(uint8_t pid)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMGET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SMPL_INTV;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_set_probe_dfu( uint8_t pid )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_CONTROL;
    param.pay_type = RAK_PB_PAY_TYPE_CONTROL_DFU;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_set_probe_restore( uint8_t pid )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_CONTROL;
    param.pay_type = RAK_PB_PAY_TYPE_CONTROL_RESTORE;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_set_probe_delete( uint8_t pid )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_CONTROL;
    param.pay_type = RAK_PB_PAY_TYPE_CONTROL_REMOVE;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_set_probe_reboot( uint8_t pid )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_CONTROL;
    param.pay_type = RAK_PB_PAY_TYPE_CONTROL_REBOOT;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_set_snsr_rule(uint8_t pid, uint8_t sid, uint16_t new_rule )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMSET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_RULE;
    param.data_len = sizeof(new_rule);
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),&new_rule,param.data_len);
    return EMOS_OK;
}

static uint8_t ifunc_set_snsr_hthr(uint8_t pid, uint8_t sid, uint8_t *new_thr, uint8_t len )
{
    rak_probe_wire_send_param_t param;
    uint8_t new_thr_buf[10];
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMSET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR;
    param.data_len = sizeof(new_thr_buf);
    len = (len>sizeof(new_thr_buf))?sizeof(new_thr_buf):len;
    memset(new_thr_buf, 0, sizeof(new_thr_buf));
    memcpy(new_thr_buf,new_thr,len);
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),new_thr_buf,param.data_len);
    return EMOS_OK;
}

static uint8_t ifunc_set_snsr_lthr(uint8_t pid, uint8_t sid, uint8_t *new_thr, uint8_t len )
{
    rak_probe_wire_send_param_t param;
    uint8_t new_thr_buf[10];
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMSET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR;
    param.data_len = sizeof(new_thr_buf);
    len = (len>sizeof(new_thr_buf))?sizeof(new_thr_buf):len;
    memset(new_thr_buf, 0, sizeof(new_thr_buf));
    memcpy(new_thr_buf,new_thr,len);
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),new_thr_buf,param.data_len);
    return EMOS_OK;
}

static uint8_t ifunc_set_snsr_intv(uint8_t pid, uint8_t sid, uint32_t new_intv )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMSET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_INTV;
    param.data_len = sizeof(new_intv);
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),&new_intv,param.data_len);
    return EMOS_OK;
}

static uint8_t ifunc_snd_provision( void )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = PROBE_ID_HUB;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_PROVISION;
    param.pay_type = RAK_PB_PAY_TPYE_PRV_VER3;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_snd_snsr_data( uint8_t mid,uint8_t *data, uint16_t data_len )
{
    rak_probe_wire_send_param_t param;
    param.mid = mid;
    param.pid = PROBE_ID_HUB;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_SENDAT;
    param.pay_type = RAK_PB_PAY_TPYE_SENDAT;
    param.data_len = data_len;
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),data,data_len);
    return EMOS_OK;
}

static uint8_t ifunc_snd_prb_disp( uint8_t mid,uint8_t *data, uint16_t data_len )
{
    rak_probe_wire_send_param_t param;
    param.mid = mid;
    param.pid = PROBE_ID_HUB;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_DISP;
    param.pay_type = 0;
    param.data_len = data_len;
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),data,data_len);
    return EMOS_OK;
}

static uint8_t ifunc_get_snsr_data(uint8_t pid, uint8_t sid)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMGET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_DATA;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_get_snsr_rule(uint8_t pid, uint8_t sid)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMGET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_RULE;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_get_snsr_intv(uint8_t pid, uint8_t sid)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMGET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_INTV;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_get_snsr_cnt(uint8_t pid)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMGET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_CNT;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_get_snsr_hthr(uint8_t pid, uint8_t sid)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMGET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_HTHR;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_get_snsr_lthr(uint8_t pid, uint8_t sid)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = sid;
    param.frm_type = RAK_PB_FRM_TYPE_PARAMGET;
    param.pay_type = RAK_PB_PAY_TYPE_PARAM_SNSR_LTHR;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_set_scmd_add(uint8_t pid, uint8_t *cmd, uint8_t len)
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_SCMD;
    param.pay_type = RAK_PB_PAY_TYPE_SCMD_ADD;
    param.data_len = len;
    
    com_vsys_mrsend(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t),cmd,param.data_len);
    return EMOS_OK;
}

static uint8_t ifunc_set_scmd_rest(uint8_t pid )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_SCMD;
    param.pay_type = RAK_PB_PAY_TYPE_SCMD_REST;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_set_scmd_del(uint8_t pid )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_SCMD;
    param.pay_type = RAK_PB_PAY_TYPE_SCMD_DEL;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_get_scmd_list(uint8_t pid )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_SCMD;
    param.pay_type = RAK_PB_PAY_TYPE_SCMD_LIST;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}

static uint8_t ifunc_get_scmd_build(uint8_t pid )
{
    rak_probe_wire_send_param_t param;
    param.mid = get_mid();
    param.pid = pid;
    param.sid = 0;
    param.frm_type = RAK_PB_FRM_TYPE_SCMD;
    param.pay_type = RAK_PB_PAY_TYPE_SCMD_BUILD;
    param.data_len = 0;
    com_vsys_send(COM_TASK_RAK_PROTOCOL,COM_VSYS_RAK_PCL_SENDREQ,&param,sizeof(rak_probe_wire_send_param_t));
    return EMOS_OK;
}
