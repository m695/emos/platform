#include "../../kernel.h"
#include "../inc/inc_co_flashmap.h"

/***************************************************/

#define  STM32_EEPROM_BANK0        0x0000 
#define  STM32_EEPROM_BANK1        0x0800 // sensor config
#define  STM32_EEPROM_BANK2        0x0200 // nbiot config
// #define  STM32_EEPROM_BANK3        0x0C00
#define  STM32_EEPROM_BANK4        0x0400 // snsr command

//+--------+--------------+-------------------------------------------------+
//| 0x0000 | 0x0000 (512B)| General information, e.g SN/SSN, probe config   |
//+--------+--------------+-------------------------------------------------+
//| 0x0200 | 0x0200 (512B)| NBIOT config                                    |
//+--------+--------------+-------------------------------------------------+
//| 0x0400 | 0x0400  (1KB)|                                                 |
//+--------+--------------+-------------------------------------------------+
//| 0x0800 | 0x0400  (1KB)| Sensor config, Sensor Part(64B) * Sensor Num(16)|
//+--------+--------------+-------------------------------------------------+
//| 0x0C00 |              |                                                 |
//+--------+--------------+-------------------------------------------------+


static void do_eeprom_con_bank0_write( uint8_t *data, uint16_t size );
static void do_eeprom_con_bank0_read( uint8_t *data, uint16_t size);
static void do_eeprom_con_bank0_readonly( uint8_t **data, uint16_t size);

static void do_eeprom_con_bank1_write( uint32_t addr ,uint8_t *data, uint16_t size );
static void do_eeprom_con_bank1_read( uint32_t addr ,uint8_t *data, uint16_t size);

static void do_eeprom_con_bank2_write( uint8_t *data, uint16_t size );
static void do_eeprom_con_bank2_read( uint8_t *data, uint16_t size);
static void do_eeprom_con_bank2_readonly( uint8_t **data, uint16_t size);


static void do_eeprom_con_bank4_write( uint32_t addr ,uint8_t *data, uint16_t size );
static void do_eeprom_con_bank4_read( uint8_t *data, uint16_t size);
static void do_eeprom_con_bank4_readonly( uint8_t **data, uint16_t size);

static void do_rest( void );
static void do_rest_rdy( void );
static void do_bootmode( void );
static void do_bootmodeok( void );


static uint8_t dfu_field_write( uint32_t idx,uint8_t *data, uint16_t size);
static uint8_t dfu_field_read( uint32_t idx,uint8_t **data);
static uint32_t dfu_size_get( void );
static uint8_t dfu_size_set( uint32_t dfufile_size );

const com_if_flashmap_t com_if_flashmap = 
{
    .bk0_con_read   = do_eeprom_con_bank0_read,
    .bk0_con_write  = do_eeprom_con_bank0_write,
    .bk0_con_readonly = do_eeprom_con_bank0_readonly,

    .bk1_con_read   = do_eeprom_con_bank1_read,
    .bk1_con_write  = do_eeprom_con_bank1_write,

    .bk2_con_read   = do_eeprom_con_bank2_read,
    .bk2_con_write  = do_eeprom_con_bank2_write,
    .bk2_con_readonly = do_eeprom_con_bank2_readonly, 

    .bk4_con_read   = do_eeprom_con_bank4_read,
    .bk4_con_write  = do_eeprom_con_bank4_write,
    .bk4_con_readonly = do_eeprom_con_bank4_readonly,

    .rest_rdy       = do_rest_rdy,
    .rest           = do_rest,
    .bootmode       = do_bootmode,
    .bootmodeok     = do_bootmodeok,

    .dfu_field_write = dfu_field_write,
    .dfu_field_read  = dfu_field_read,
    .dfu_size_set    = dfu_size_set,
    .dfu_size_get    = dfu_size_get,
};

/******************************************************************************/

static void emos_eeprom_write( uint32_t addr,uint8_t *data, uint16_t size)
{
    // emos.eeprom->o_write(addr,data,size);
    emos.flash->eeprom_write(addr,data,size);
}

static void emos_eeprom_read( uint32_t addr,uint8_t *data, uint16_t size)
{
    // emos.eeprom->o_read(addr,data,size);
    emos.flash->eeprom_read(addr,data,size);
}

static void emos_eeprom_readonly( uint32_t addr,uint8_t **data, uint16_t size)
{
    // emos.eeprom->o_read(addr,data,size);
    emos.flash->eeprom_readonly(addr,data,size);
}

static uint8_t dfu_field_write( uint32_t idx,uint8_t *data, uint16_t size)
{
#ifdef EMOS_SUPPORT_TFILE
    if (idx == 0)
    {
        return EMOS_PARAM_ERROR;
    }

    emos.flash->field_write(idx*1024,data,size);
    return EMOS_OK;
#else
    return EMOS_NOT_SUPPORT;
#endif
}

static uint8_t dfu_field_read( uint32_t idx,uint8_t **data )
{
#ifdef EMOS_SUPPORT_TFILE
    if (idx == 0)
    {
        return EMOS_PARAM_ERROR;
    }

    emos.flash->field_read(idx*1024,data,0);
    return EMOS_OK;
#else
    return EMOS_NOT_SUPPORT;
#endif
}
        
static uint8_t dfu_size_set( uint32_t dfufile_size )
{
#ifdef EMOS_SUPPORT_TFILE
    emos.flash->field_write( 0, (uint8_t *)&dfufile_size, sizeof(uint32_t));
    return EMOS_OK;
#else
    return EMOS_NOT_SUPPORT;
#endif
}

static uint32_t dfu_size_get( void )
{
#ifdef EMOS_SUPPORT_TFILE
    uint32_t *dfufile_size = 0;
    emos.flash->field_read( 0, (uint8_t **)&dfufile_size, sizeof(uint32_t));
    return *dfufile_size;
#else
    return EMOS_NOT_SUPPORT;
#endif
}


static void do_eeprom_con_bank0_write( uint8_t *data, uint16_t size )
{
    // static uint8_t tmp[256];
    uint8_t *tmp;
    do
    {
        emos_eeprom_write(STM32_EEPROM_BANK0,data,size);
        emos_eeprom_readonly(STM32_EEPROM_BANK0,&tmp,size);
        if (memcmp(data,tmp,size) == 0)
        {
            break;
        }
    } while (1);
    
}

static void do_eeprom_con_bank0_read( uint8_t *data, uint16_t size)
{
    emos_eeprom_read(STM32_EEPROM_BANK0, data, size);
}

static void do_eeprom_con_bank0_readonly( uint8_t **data, uint16_t size)
{
    emos_eeprom_readonly(STM32_EEPROM_BANK0, data, size);
}

static void do_eeprom_con_bank4_write( uint32_t addr ,uint8_t *data, uint16_t size )
{
    // static uint8_t tmp[256];
    uint8_t *tmp;
    do
    {
        emos_eeprom_write(STM32_EEPROM_BANK4 + addr,data,size);
        emos_eeprom_readonly(STM32_EEPROM_BANK4 + addr,&tmp,size);
        if (memcmp(data,tmp,size) == 0)
        {
            break;
        }
    } while (1);
    
}

static void do_eeprom_con_bank4_read( uint8_t *data, uint16_t size)
{
    emos_eeprom_read(STM32_EEPROM_BANK4, data, size);
}

static void do_eeprom_con_bank4_readonly( uint8_t **data, uint16_t size)
{
    emos_eeprom_readonly(STM32_EEPROM_BANK4, data, size);
}

static void do_eeprom_con_bank1_write( uint32_t addr ,uint8_t *data, uint16_t size )
{
    emos_eeprom_write(STM32_EEPROM_BANK1 + addr,data,size);
}

static void do_eeprom_con_bank1_read( uint32_t addr ,uint8_t *data, uint16_t size)
{
    emos_eeprom_read(STM32_EEPROM_BANK1 + addr, data, size);
}

static void do_eeprom_con_bank2_write( uint8_t *data, uint16_t size )
{
    emos_eeprom_write(STM32_EEPROM_BANK2, data, size);
}

static void do_eeprom_con_bank2_read( uint8_t *data, uint16_t size)
{
    emos_eeprom_read(STM32_EEPROM_BANK2 , data, size);
}

static void do_eeprom_con_bank2_readonly( uint8_t **data, uint16_t size)
{
    emos_eeprom_readonly(STM32_EEPROM_BANK2, data, size);
}

static void do_rest_rdy( void )
{
    com_if_rak_bank0_t eee;
    do_eeprom_con_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
    eee.check.bit0 = BSP_EARSE_BITFALG;
    do_eeprom_con_bank0_write((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
}

static void do_rest( void )
{
    com_if_rak_bank0_t eee;
    do_eeprom_con_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
    eee.check.bit0 = !BSP_EARSE_BITFALG;
    eee.sampling_interval = DEF_INTERVAL;    
    eee.probe_interval = DEF_INTERVAL;    
    eee.lora_autojoin = 0;
    eee.tag_id = 0xFF;
    eee.keep_id = 0xFF;
    do_eeprom_con_bank0_write((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    com_if_rak_bank1_page_t fff;
    uint8_t page_size = sizeof(com_if_rak_bank1_page_t);
    memset((uint8_t *)&fff, 0, page_size);
    
    fff.interval = DEF_INTERVAL;
    fff.rule_flg = 0x00;
    // fff.sensor_type = 0x00;
    memset( fff.hthr, 0, sizeof(fff.hthr));
    memset( fff.lthr, 0, sizeof(fff.lthr));
    
    for (size_t i = 0; i < RAK_SNSR_NODE_NUM; i++)
    {
        do_eeprom_con_bank1_write(page_size * i,(uint8_t *)&fff, page_size);
    }
}

static void do_bootmode( void )
{
#ifdef EMOS_SUPPORT_SLAVE
    struct 
    {
        uint8_t boot_mode[8];
        uint8_t hub_id;
        uint8_t probe_id;
        uint8_t n1;
        uint8_t n2;
    } control;
    
    emos.flash->spi_read(0x08002FF0,(uint8_t *)&control,sizeof(control));
    if (control.boot_mode[0] == 0x5b)
    {
        return;
    }
    
    emos.flash->spi_earse(0x08002FF0, 1);
    emos_delay(50);
    control.boot_mode[0] = 0x5b;
    control.boot_mode[1] = 0x5a;
    control.boot_mode[2] = 0x5a;
    control.boot_mode[3] = 0x5a;
    control.hub_id = 0x00;
    control.probe_id = com_mydev()->id;
    emos.flash->spi_write(0x08002FF0,(uint32_t *)&control,sizeof(control));
    emos_delay(50);
    com.flashmap->bootmode();
#endif
}

static void do_bootmodeok( void )
{
#ifdef EMOS_SUPPORT_SLAVE
    struct 
    {
        uint8_t boot_mode[8];
        uint8_t hub_id;
        uint8_t probe_id;
        uint8_t n1;
        uint8_t n2;
    } control;
    emos.flash->spi_read(0x08002FF0,(uint8_t *)&control,sizeof(control));
    if (control.boot_mode[0] == 0x5a)
    {
        return;
    }
    emos.flash->spi_earse(0x08002FF0, 1);
    emos_delay(50);
    control.boot_mode[0] = 0x5a;
    control.boot_mode[1] = 0x5a;
    control.boot_mode[2] = 0x5a;
    control.boot_mode[3] = 0x5a;
    control.hub_id = 0x00;
    control.probe_id = com_mydev()->id;
    emos.flash->spi_write(0x08002FF0,(uint32_t *)&control,sizeof(control));
    emos_delay(50);
    com.flashmap->bootmodeok();
#endif
}

