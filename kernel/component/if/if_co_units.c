#include "../../kernel.h"
#include "../inc/inc_co_units.h"

static uint8_t do_ipso_get_size( uint8_t type );
static void do_tlv_decode(uint8_t *src, uint16_t src_size, uint8_t len_size, uint8_t type_size, tlv_decode_cb func_cb );

typedef struct
{
    uint8_t              type;
    uint8_t              size;
} __EMOS_PACKED rak_ipso_def_t;

static const rak_ipso_def_t ipso_table[] =
{
    { RAK_IPSO_DIGITAL_INPUT   ,1 },
    { RAK_IPSO_DIGITAL_OUTPUT  ,1 },
    { RAK_IPSO_ANALOG_INPUT    ,2 },
    { RAK_IPSO_ANALOG_INPUT_VOL,2 },
    { RAK_IPSO_ILLUM_SENSOR    ,2 },
    { RAK_IPSO_PRESENCE_SENSOR ,1 },
    { RAK_IPSO_TEMP_SENSOR     ,2 },
    { RAK_IPSO_HUMIDITY_SENSOR ,1 },
    { RAK_IPSO_HUMIDITY_PRO    ,2 },    
    { RAK_IPSO_ACCELEROMETER   ,6 },
    { RAK_IPSO_BAROMETER       ,2 },
    { RAK_IPSO_GYROMETER       ,6 },
    { RAK_IPSO_GPS_LOCAL       ,9 },
    { RAK_IPSO_WIND            ,2 },
    { RAK_IPSO_WIND_DIR        ,2 },
    { RAK_IPSO_EC	           ,2 },
    { RAK_IPSO_PH	           ,2 },
    { RAK_IPSO_PYRANOMETER     ,2 },
    { RAK_IPSO_MODBUS          ,64 },
    { RAK_IPSO_SDI12           ,64 },
    { RAK_IPSO_RS232           ,64 },
    { RAK_IPSO_DEPTH           ,4 },
    { RAK_IPSO_POWER           ,2 },
    { RAK_IPSO_TILT            ,2 },
    { RAK_IPSO_DISTANCE        ,2 },
    { RAK_IPSO_DIRECTION       ,2 },
    { RAK_IPSO_RATE            ,2 },
    { RAK_IPSO_PUSHBTN         ,2 },
};


const com_if_units_t com_if_units = 
{
    .ipso_size = do_ipso_get_size,
    .tlv_decode = do_tlv_decode,
};

/******************************************************************************/



static void do_tlv_decode(uint8_t *src, uint16_t src_size, uint8_t len_size, uint8_t type_size, tlv_decode_cb func_cb )
{
    uint8_t *this_type = &src[0];
    uint8_t *this_len  = &src[type_size];
    uint8_t *this_data = &src[len_size + type_size];
    uint16_t this_tlv_size = len_size + type_size + *this_len;
    uint16_t next_size = (this_tlv_size > src_size)?0:(src_size - this_tlv_size);
    uint8_t ret = EMOS_OK;

    if (src[0] == 0)
    {
        return;
    }
    
    ret = func_cb( *this_len, this_type, this_data);
    switch(ret)
    {
        case EMOS_OK:
            break;
        default:
            return;
    }
    
    if( next_size == 0)
    {
        return;
    }

    do_tlv_decode(&(this_data[*this_len]), next_size, len_size, type_size, func_cb);
}

static uint8_t do_ipso_get_size( uint8_t type )
{
    uint8_t tbl_size = COUNT_OF(ipso_table);
    uint8_t snsr_type_size = 0;
    for(uint8_t i=0; i<tbl_size; i++)
    {
        if(ipso_table[i].type == type)
        {
            snsr_type_size = ipso_table[i].size;
            break;
        }
    }
    return snsr_type_size;
}


