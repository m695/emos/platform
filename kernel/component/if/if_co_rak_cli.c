#include "../../kernel.h"
#include "../inc/inc_co_rak_cli.h"

#define THIS_BUFF_SIZE   CLI_BUFF_SIZE

#define SYS_CLI_INFO(...)   if(com.cli->ctrl->ate_en == 1) DBG_INFO(__VA_ARGS__)


static com_cli_buff_t cli_buff;
static com_if_rak_cli_ctrl_t hook_cli_ctrl;

static const com_cli_format_t format_list[] = 
{ 
    {.header="AT?"    ,.pcmd=COM_TASK_RAK_ATCMD_AT},
    {.header="ATZ"    ,.pcmd=COM_TASK_RAK_ATCMD_AT},
    {.header="ATZ!"   ,.pcmd=COM_TASK_RAK_ATCMD_AT},
    {.header="ATR"    ,.pcmd=COM_TASK_RAK_ATCMD_AT},
    {.header="ATE"    ,.pcmd=COM_TASK_RAK_ATCMD_AT},
    {.header="AT+%s"  ,.pcmd=COM_TASK_RAK_ATCMD_AT},
    {.header="ATC+%s" ,.pcmd=COM_TASK_RAK_ATCMD_ATC},
    {.header="TSC+%s" ,.pcmd=COM_TASK_RAK_ATCMD_TSC},
    {.header="TS+%s"  ,.pcmd=COM_TASK_RAK_ATCMD_TS},
    {.header="TP+%s"  ,.pcmd=COM_TASK_RAK_ATCMD_TP},
    
#ifdef EMOS_SUPPORT_RUI3_ATCELL
    {.header="ATCELL+%s"  ,.pcmd=COM_TASK_RAK_ATCMD_ATCELL},
    {.header="ATCELL=%s"  ,.pcmd=COM_TASK_RAK_ATCMD_ATCELL},
#endif
};

static void do_rak_cli_key(uint8_t *data ,uint16_t len);
static void do_rak_cli_command(uint8_t *atcmd ,uint16_t atcmd_len);

const com_if_rak_cli_t com_if_rak_cli = 
{
    .ctrl = &hook_cli_ctrl,
    .key = do_rak_cli_key,
    .command = do_rak_cli_command,
};

static void do_rak_cli_command(uint8_t *atcmd ,uint16_t atcmd_len)
{
    uint8_t ret;
    uint32_t command;
    bool cmd_check = false;
    bool cmd_busy = false;

    for (uint8_t i = 0; i < COUNT_OF(format_list) ; i++)
    {
        ret = emos.units->format((char *)atcmd,format_list[i].header);
        if (ret == 0)
        {
            cmd_check = true;
            command = format_list[i].pcmd;

            // com_vsys_send( TASK_API_EVENT, USER_EVENT_ATCMD, atcmd, atcmd_len);
            com_vsys_send( COM_TASK_RAK_CLI, command, atcmd, atcmd_len);
        }
    }

    if (cmd_busy)
    {
        SYS_INFO(RET_MSG("%s"),emos.errcode[EMOS_BUSY_ERROR]);
    }

    if (!cmd_check) 
    {
        SYS_INFO(RET_MSG("%s"),emos.errcode[EMOS_CMD_NOT_FOUND]);
    }

    return;
}

static void do_rak_cli_key(uint8_t *data ,uint16_t len)
{
    static uint8_t spic_key = 0;
    uint8_t *tmp_stack = NULL;
    uint16_t i,j;
    #define CTRL_C   0x03

    for( i=0 ; i<len; i++)
    {
        
        if ( com.cli->ctrl->input_cnt > 0)
        {
            do
            {
                uint16_t idx = com.cli->ctrl->input_data_len - com.cli->ctrl->input_cnt;
                com.cli->ctrl->input_data[idx] = data[i];
            } while (0);
            
            #if 0 // it's will handicap DFU
            if (data[i] == CTRL_C)
            {
                com.cli->ctrl->input_cnt = 0;
                goto CLI_CANCEL;
            }
            #endif
            if( (--com.cli->ctrl->input_cnt) == 0)
            {
                com_vsys_send( COM_TASK_EVENT, COM_EVENT_CLI_COMMAND, com.cli->ctrl->input_cmd, com.cli->ctrl->input_cmd_len);
            }
            continue;
        }
        
        if( data[i] == '\n' || data[i] == '\r')
        {
            SYS_CLI_INFO("\r\n");
            
            if(cli_buff.wp <= 0)
            {
                break;
            }
            
            cli_buff.buff[cli_buff.wp++] = '\0';
            
            uint16_t stack_size = 0;
            if( cli_buff.wp > cli_buff.rp )
            {
                stack_size = cli_buff.wp - cli_buff.rp;
            }
            else
            {
                stack_size = (THIS_BUFF_SIZE - cli_buff.rp) + cli_buff.wp;
            }

            tmp_stack = (uint8_t *)com_malloc(stack_size);
            for( j=0 ; j<stack_size; j++)
            {   
                tmp_stack[j] =  cli_buff.buff[cli_buff.rp++];
                if( !(cli_buff.wp > cli_buff.rp) )
                {
                    if( !(cli_buff.rp < THIS_BUFF_SIZE))
                    {
                        cli_buff.rp = 0;
                    }
                }
            }

            stack_size -= 1;
            tmp_stack[stack_size] = 0;

            if(stack_size != 0)
            {
                com_vsys_send( COM_TASK_EVENT, COM_EVENT_CLI_COMMAND, tmp_stack, strlen((char *)tmp_stack) + 1);
            }
            
            com_free(tmp_stack);

            memset((void *)&cli_buff, 0, sizeof(com_cli_buff_t));

        }
        else if( data[i] == '\\' )
        {
            spic_key = IENABLE;
        }
        else if(spic_key == IENABLE)
        {
            spic_key = IDISABLE;
            if (data[i] == 'n')
            {
                cli_buff.buff[cli_buff.wp++] = '\n';
            }
        }
        else 
        {
            cli_buff.buff[cli_buff.wp++] = data[i];

            if( !(cli_buff.wp < THIS_BUFF_SIZE))
            {
               cli_buff.wp = 0;
            }

            if( data[i] == 0x7f || data[i] == 0x08)
            {
                cli_buff.wp--;
                if( cli_buff.wp > cli_buff.rp )
                {
                    cli_buff.wp--;
                    SYS_CLI_INFO("%c%c%c",0x08,0x20,0x08);
                }
            }
            else if ( data[i] == 0x03)
            {
            CLI_CANCEL:
                cli_buff.wp = 0;
                SYS_CLI_INFO("%c%c%c\r\n",0x08,0x20,0x08);
            }
            else 
            {
                SYS_CLI_INFO("%c",data[i]);
            }
        }
    }
}


