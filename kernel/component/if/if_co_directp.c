#include "../../kernel.h"
#include "../inc/inc_co_directp.h"

/******************************************************************************/

#define THIS_BUFF_SIZE  EMOS_UART_TRANS_BUFF

typedef enum
{
    COM_DIRTP_SYSTEM,
    COM_DIRTP_TARGET,
    COM_DIRTP_SELECT,
} COM_DIRTP_E;

static void do_log_select_puts(uint32_t dir, const char *str , uint16_t len);
static void do_log_target_puts(const char *str, uint16_t len);
static void do_log_target_printf(char *fmt, ...);
static void do_log_system_puts( const char *str , uint16_t len);
static void do_log_system_printf(char *fmt, ...);
static void do_log_rui_printf(char *fmt, ...);

const com_if_directp_t com_if_directp = 
{
    .selputs  = do_log_select_puts,
    .tarputs  = do_log_target_puts,
    .tarprnf  = do_log_target_printf,
    .sysputs  = do_log_system_puts,
    .sysprnf  = do_log_system_printf,
    .ruiprnf  = do_log_rui_printf,
};

/******************************************************************************/

static void do_log_select_puts(uint32_t dir, const char *str , uint16_t len)
{
    switch (dir)
    {
        case COM_EVENT_RUIATCMD:
            com.rpcl->req_puts( (void *)str, len);
            break;

        case COM_EVENT_RUIATREQ:
            com.rpcl->rsp_puts( (void *)str, len);
            break;

        default:
            emos.log->puts(dir, (void *)str, len);
            break;
    }
}

static void do_log_target_puts( const char *str , uint16_t len)
{
    uint32_t dir = com_source_id;
    do_log_select_puts(dir, str, len);
}

static void do_log_system_puts( const char *str , uint16_t len)
{
    emos.log->puts(EMOS_EVENT_BLE_RECV, str, len);
    emos.log->puts(EMOS_EVENT_DBG_RECV, str, len);
}

static void do_log_vprintf(COM_DIRTP_E dire, uint32_t dirt,const char *fmt, va_list arg)
{
    uint16_t tmp_len;
    char *tmp_buf = (char *)emos.log->ctrl->tmp_buf;
    tmp_len = vsnprintf( NULL, 0, fmt, arg) + 1; 
    memset(tmp_buf, 0, THIS_BUFF_SIZE);
    vsnprintf( tmp_buf, tmp_len, fmt, arg); 

    switch (dire)
    {
        default:
        case COM_DIRTP_SYSTEM:
            do_log_system_puts( tmp_buf , strlen(tmp_buf));
            break;
        case COM_DIRTP_TARGET:
            do_log_target_puts( tmp_buf , strlen(tmp_buf));
            break;
        case COM_DIRTP_SELECT:
            do_log_select_puts( dirt ,tmp_buf , strlen(tmp_buf));
            break;
    }
}

static void do_log_target_printf(char *fmt, ...)
{
    va_list arg; 
    va_start(arg,fmt); 
    do_log_vprintf(COM_DIRTP_TARGET, 0, fmt,arg);
    va_end(arg);
}

static void do_log_system_printf(char *fmt, ...)
{
    va_list arg; 
    va_start(arg,fmt); 
    do_log_vprintf(COM_DIRTP_SYSTEM, 0,fmt,arg);
    va_end(arg);
}

static void do_log_rui_printf(char *fmt, ...)
{
    va_list arg; 
    va_start(arg,fmt); 
    do_log_vprintf(COM_DIRTP_SELECT, COM_EVENT_RUIATCMD,fmt,arg);
    va_end(arg);
}
