#include "../kernel.h"


#define THIS_POOL_ID      COM_POOL_ID
#define THIS_POOL_SIZE    COM_POOL_SIZE

#define SECD_POOL_ID      COM_PARAM_POOL_ID
#define SECD_POOL_SIZE    COM_PARAM_POOL_SIZE

/******************************************************************************/
extern void com_hc_event_process(emos_task_msg_t *task_msg);
static void task_api_event_process(emos_task_msg_t *task_msg);

static com_if_vsys_mydev_t * do_com_vsys_mydev( void );
static void do_com_vsys_init( void );
static uint8_t do_com_vsys_run( void );
static emos_if_msgbox_t * do_com_vsys_mbox( void );
static emos_if_malloc_t * do_com_vsys_mem( void );

const com_if_vsys_t com_if_vsys = 
{
    .init   = do_com_vsys_init,
    .run    = do_com_vsys_run,
    .mem    = do_com_vsys_mem,
    .mbox   = do_com_vsys_mbox,
    .mydev  = do_com_vsys_mydev,
};

entry_install(com_mdl_rak_sensor_process)
entry_install(com_mdl_rak_wire_protocol_process)
entry_install(com_mdl_rak_cli_process)
entry_install(com_mdl_service_process)
entry_install(com_mdl_hallsnsr_process)
entry_install(com_mdl_rak_nbiot_process)
entry_install(com_mdl_rak_sensor_advc_process)
entry_install(com_mdl_rak_ldo_process)
entry_install(com_mdl_rak_radio_process)

#define ENTRY_ACTION_ALL  (TASK_MODE_INIT|TASK_MODE_EVENT|TASK_MODE_POLLING)
#define ENTRY_ACTION_DEF  (TASK_MODE_INIT|TASK_MODE_EVENT)
const emos_task_entry_t com_task_entry_tbl[] =
{ 
    [COM_USER_EVENT]            = { .priority=EMOS_TASK_PRIORITY_HIGH    ,.mode=ENTRY_ACTION_DEF,.task_entry=task_api_event_process},
    [COM_TASK_RAK_LDO]          = { .priority=EMOS_TASK_PRIORITY_HIGH    ,.mode=ENTRY_ACTION_DEF,.task_entry=com_mdl_rak_ldo_process},
    [COM_TASK_SERV]             = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=com_mdl_service_process},
    [COM_TASK_RAK_PROTOCOL]     = { .priority=EMOS_TASK_PRIORITY_MID     ,.mode=ENTRY_ACTION_DEF,.task_entry=com_mdl_rak_wire_protocol_process},
    [COM_TASK_RAK_SENSOR]       = { .priority=EMOS_TASK_PRIORITY_MID     ,.mode=ENTRY_ACTION_DEF,.task_entry=com_mdl_rak_sensor_process},
    [COM_TASK_RAK_CLI]          = { .priority=EMOS_TASK_PRIORITY_MID     ,.mode=ENTRY_ACTION_ALL,.task_entry=com_mdl_rak_cli_process},
    [COM_TASK_HALLSNSNR]        = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=com_mdl_hallsnsr_process},
    [COM_TASK_RAK_SENSOR_ADVC]  = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=com_mdl_rak_sensor_advc_process},
    [COM_TASK_EVENT]            = { .priority=EMOS_TASK_PRIORITY_HIGH    ,.mode=ENTRY_ACTION_DEF,.task_entry=com_event_process},
    [COM_TASK_RAK_NBIOT]        = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_ALL,.task_entry=com_mdl_rak_nbiot_process},
    [COM_TASK_RAK_RADIO]        = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_ALL,.task_entry=com_mdl_rak_radio_process},
};

static emos_if_malloc_t * do_com_vsys_mem( void )
{
    return emos.mem(SECD_POOL_ID);
}

static emos_if_msgbox_t * do_com_vsys_mbox( void )
{
    return emos.msgbox(THIS_POOL_ID);
}

static com_if_vsys_mydev_t * do_com_vsys_mydev( void )
{
    static com_if_vsys_mydev_t this_mydev;
    return &this_mydev;
}

static void do_com_vsys_init( void )
{
    com_if_vsys_t *this_vsys = com.vsys;
    static uint8_t msgbox_pool[THIS_POOL_SIZE];
    this_vsys->mbox()->init( msgbox_pool, sizeof(msgbox_pool));

    static uint8_t param_pool[SECD_POOL_SIZE];
    emos.mem(SECD_POOL_ID)->init(param_pool,sizeof(param_pool));
    
    uint8_t task_cnt = COUNT_OF(com_task_entry_tbl);
    for(uint8_t i=0;i<task_cnt;i++)
    {
        this_vsys->mbox()->task_add(( emos_task_entry_t * )&com_task_entry_tbl[i]);
    }

    uint8_t ret_cnt;
    ret_cnt = 0;
    do
    {
        ret_cnt = this_vsys->mbox()->polling();
    } while ( ret_cnt != 0 );
    
}

static uint8_t do_com_vsys_run(void)
{
    return com.vsys->mbox()->polling();
}

static void task_api_event_process(emos_task_msg_t *task_msg)
{
    emos_msg_type_t *msg_type = (emos_msg_type_t *)&task_msg->msg_type;
    
    init_handle_t this_init_handle = *(init_handle_t *)(emos.vsys->reg_init_handle);
    loop_handle_t this_loop_handle = *(loop_handle_t *)(emos.vsys->reg_loop_handle);
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            this_init_handle();
            break;
        case TASK_MODE_EVENT:
            this_loop_handle(msg_type->msg_id,task_msg->payload,task_msg->payload_len);
            break;
    }
}


