#ifndef __COM_HOOK_H__
#define __COM_HOOK_H__

#ifdef __cplusplus
extern "C" {
#endif

extern uint8_t HOOK_NAME(com_vsys_send) (uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len);
extern uint8_t HOOK_NAME(com_vsys_mrsend)( uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len, uint8_t * more, uint32_t more_len);

extern void * HOOK_NAME(com_malloc) ( uint32_t len);
extern void HOOK_NAME(com_free) ( void *p );
extern void * HOOK_NAME(com_copy) ( uint32_t new_len, void *src, uint32_t src_len );
extern com_if_rak_probe_t HOOK_NAME(com_sprobe)(uint8_t idx);
extern com_if_vsys_mydev_t *HOOK_NAME(com_mydev) ( void );


extern uint8_t HOOK_NAME(com_get_snsr) ( uint8_t pid, uint8_t sid, com_if_rak_probe_sensor_t **info );
extern uint8_t HOOK_NAME(com_get_probe) ( uint8_t pid, com_if_rak_probe_param_t **info );

/******************************************************************************/
/*                            HUB API                                         */
/******************************************************************************/
extern uint8_t HOOK_NAME(com_nbiot_power) (uint8_t value);  
extern uint8_t HOOK_NAME(com_nbiotapp) (const char *mode);  
extern uint8_t HOOK_NAME(com_nbiot_upload) (const char *data);  
extern uint8_t HOOK_NAME(com_http_request) (uint8_t method, const char *url);  
extern uint8_t HOOK_NAME(com_nbiot_apply) (uint8_t apply);  

extern uint8_t HOOK_NAME(com_cli_cmd) (const char  *cmd);

// extern com_if_rak_probe_param_t *HOOK_NAME(com_probe) ( void );
// extern void HOOK_NAME(com_printf_arry)  ( uint8_t level, const char *data, uint16_t len);

#ifdef __cplusplus
}
#endif

#endif
