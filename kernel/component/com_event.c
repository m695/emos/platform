#include "../kernel.h"

#include "inc/inc_co_rak_wire_protocol.h"
#include "inc/inc_co_rak_sensor_advc.h"

#ifndef BSP_NOT_SUPPORT_WEAK

__RAKLINK_WEAK void com_event_role_init(uint16_t msg_id, uint8_t * msg_payload, uint16_t msg_len)
{
    /*
        week function
    */
}

__RAKLINK_WEAK void com_event_role_process(uint16_t msg_id, uint8_t * msg_payload, uint16_t msg_len)
{
    /*
        week function
    */
}
#endif

static uint8_t flag_wire_protect = IENABLE;

static void com_hc_event_init(uint16_t msg_id, uint8_t * msg_payload, uint16_t msg_len )
{
    com_if_rak_bank0_t *eee;
    com_if_vsys_mydev_t * mydev = com_mydev();

    com_flash_bank0_readonly((void *)&eee, sizeof(com_if_rak_bank0_t));
    com.flashmap->bootmodeok();

    if ( eee->check.bit0 == BSP_EARSE_BITFALG)
    {
        com.flashmap->rest();
        com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_REBOOT, NULL, 0);
        SYS_INFO(DO_MSG("SYSTEM REST!!"));
    }

    mydev->id = PROBE_ID_SEL;
    mydev->last_id = eee->keep_id;

    memcpy(mydev->sn.value, &(eee->int_srnum), sizeof(mydev->sn.value));
    memcpy(mydev->ssn.value, &(eee->ext_srnum), 3);
    memcpy(mydev->model_name, S_MODEL_NAME, strlen(S_MODEL_NAME));

    mydev->sampling_interval = (eee->sampling_interval == 0)?DEF_INTV_VALUE:eee->sampling_interval;
    mydev->current_intveral = mydev->sampling_interval;

    mydev->sw_version[0] = BUILD_MAJOR_VERSION;
    mydev->sw_version[1] = BUILD_MINOR_VERSION;
    mydev->sw_version[2] = BUILD_MICRO_VERSION;

    memcpy(&(mydev->type_id.current.value), &(eee->type_id.value), sizeof(uint32_t));

    mydev->hw_version = eee->hw_version;
    mydev->tag_id     = eee->tag_id;

    emos.vsys->ctrl->psm_def = DEF_PSM_VALUE;
    emos.vsys->ctrl->psm_en = emos.vsys->ctrl->psm_def;
}

emos_process( event, 
{
    #define BACKTO_EMOS() emos_vsys_send( EMOS_TASK_EVENT, msg_type->msg_id, msg_payload, msg_len)
    com_if_vsys_mydev_t * mydev = com_mydev();  
    static uint32_t keep_time = 0;

    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            com_hc_event_init(msg_type->msg_id,msg_payload,msg_len);
            com_vsys_send( COM_USER_EVENT, USER_EVENT_INIT_DONE, NULL, 0);
            break;

        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {
                /* test mode result */
                case COM_EVENT_TEST_MSG:
                    DBG_ALL("%s",msg_payload);
                    break;

                // case COM_EVENT_TRANSF:
                //     do
                //     {
                //         struct 
                //         {
                //             uint32_t dir;
                //             uint8_t data[];
                //         } *ptrf;
                //         ptrf = msg_payload;

                //         emos.log->puts(ptrf->dir,ptrf->data, (msg_len - sizeof(ptrf->dir)));
                //     } while (0);
                //     break;
                case COM_EVENT_BSP2ATCMD:
                    emos_vsys_tgsend(EMOS_TASK_EVENT, EMOS_EVENT_BSP2ATCMD, com_source_id, msg_payload, msg_len);
                    break;

                /* power saving */
                case COM_EVENT_WAKEUP:
                    goto ROLE_CASE;

                case COM_EVENT_SLEEP:
                    flag_wire_protect = IDISABLE;
                    com_event_role_process(msg_type->msg_id,msg_payload,msg_len);
                    emos_vsys_send(EMOS_TASK_EVENT,EMOS_EVENT_SLEEP,msg_payload,msg_len);
                    break;
                
                /* system action */
                case COM_EVENT_SYS_REBOOT:
                    emos.vsys->ctrl->do_reboot = IENABLE;
                    break;

                case COM_EVENT_SYS_REST:
                    com.flashmap->rest_rdy();
                    goto ROLE_CASE;

                case COM_EVENT_SYS_DFU:
                    goto ROLE_CASE;
                
                case COM_EVENT_SYS_SCMD_DUMP:
                    com_vsys_send( COM_TASK_RAK_SENSOR_ADVC, COM_TASK_EVT_RAK_SNSR_ADVC_DUMPMAP, NULL,0);
                    goto ROLE_CASE;

                case COM_EVENT_SYS_PROBE_PWRON:
                    BACKTO_EMOS();
                    goto ROLE_CASE;

                case COM_EVENT_PROBE_PARAM_SAVE:
                    do
                    {
                        uint8_t pid = msg_payload[0];
                        com_sprobe(pid).probe_save();
                    } while (0);
                    
                    goto ROLE_CASE;

                case COM_EVENT_SENSOR_PARAM_SAVE:
                    do
                    {
                        uint8_t pid = msg_payload[0];
                        uint8_t sid = msg_payload[1];
                        com_sprobe(pid).snsr_save(sid);
                    } while (0);
                    goto ROLE_CASE;

                case COM_EVENT_SYS_INTV:
                    memcpy(&(mydev->sampling_interval),msg_payload,msg_len);
                    
                    com_sprobe(0).probe_save();
                    break;

                case COM_EVENT_SYS_SNSR_REST:
                    do
                    { 
                        com_if_rak_probe_sensor_t * snsr_info;
                        
                        uint8_t snsr_cnt = com_sprobe(com_iprobe->id).snsr_count();
                        for( size_t i=0; i<snsr_cnt; i++ )
                        {
                            com_sprobe(com_iprobe->id).snsr_walk(i,&snsr_info);
                            snsr_info->sta.stop = IENABLE;
                        }
                    } while (0);
                    goto ROLE_CASE;

                case COM_EVENT_SYS_TARGET_REBOOT:
                    goto ROLE_CASE;

                case COM_EVENT_SYS_TARGET_DFU:
                    goto ROLE_CASE;
                    
                case COM_EVENT_SYS_TARGET_REST:
                    goto ROLE_CASE;
                    
                case COM_EVENT_SYS_TARGET_KILL:
                    goto ROLE_CASE;

                case COM_EVENT_SYS_INFO:
                    com.dirp->sysputs( (void *) msg_payload,msg_len);
                    break;

                case COM_EVENT_100MS:
                    break;

                /* wire protocol */

                case COM_EVENT_WIRP_RUI_TRANS:
                    com.rpcl->req_puts( (void *) msg_payload,msg_len);
                    break;

                case COM_EVENT_WIRP_RUI_RECV:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_INFO, msg_payload, msg_len);
                    break;

                case COM_EVENT_WIRP_TRANS:
                    BACKTO_EMOS();
                    break;

                case COM_EVENT_WIRP_RECV:
                    if ( flag_wire_protect == IENABLE )
                    {
                        break;
                    }
                    DBG_WIREA((void *)msg_payload, msg_len);
                    com_vsys_send( COM_TASK_RAK_PROTOCOL, COM_VSYS_RAK_PCL_CHECK, msg_payload, msg_len);
                    break;

                case COM_EVENT_WIRP_RSP:
                    do
                    {
                        uint16_t rsp_type;
                        memcpy(&rsp_type , msg_payload, msg_len);
                        uint8_t *box;
                        box = msg_payload + sizeof(rsp_type);
                        msg_len -= sizeof(rsp_type);
                        com.cli->ctrl->waitcur.value = rsp_type;
                        com.cli->ctrl->waitlog_len = msg_len;
                        memcpy(com.cli->ctrl->waitlog, &box[0], msg_len);
                    } while (0);
                    
                    break;
                /* BLE */
            #ifdef EMOS_SUPPORT_BLE
                case COM_EVENT_BLE_CONN:
                    goto ROLE_CASE;

                case COM_EVENT_BLE_DISC:
                    goto ROLE_CASE;
            #endif

                /* CLI*/
                case COM_EVENT_CLI_KEY:
                    com.cli->key(msg_payload, msg_len);
                    break;
                    
                case COM_EVENT_CLI_COMMAND:
                    com.cli->command(msg_payload, msg_len);
                    break;

                /* Hall */
            #ifdef EMOS_SUPPORT_HALL
                case COM_EVENT_HALLF_ACTIVE:
                #ifdef EMOS_SUPPORT_BLE
                    emos.ble->adv_name(mydev->model_name,strlen(mydev->model_name));
                    emos.ble->adv_start();
                #endif
                    goto ROLE_CASE;
            #endif

                /* system timer */
                case COM_EVENT_PRI_TIMER_SHOT_NEW:
                    do
                    {
                        uint32_t new_time;
                        memcpy(&new_time , msg_payload, msg_len);
                        com_pri_timer_stop();
                        com_pri_timer_shot(new_time);
                        com_vsys_send( COM_USER_EVENT, USER_EVENT_TIMER_SHOT, NULL, 0);
                    } while (0);
                    break;

                case COM_EVENT_PRI_TIMER_TRIG:
                    // com_vsys_send( COM_USER_EVENT, USER_EVENT_TIMER0_TRIG, NULL, 0);
                    goto ROLE_CASE;


                case COM_EVENT_PRI_TIMER_EARLY:
                    do
                    {
                        uint32_t early_time;
                        memcpy(&early_time , msg_payload, msg_len);

                        keep_time += early_time;
                        if ( keep_time < mydev->current_intveral)
                        {
                            uint32_t new_time = MSEC((mydev->current_intveral - keep_time));
                            com_pri_timer_shot(new_time);
                        }
                        else
                        {
                            msg_type->msg_id = COM_EVENT_PRI_TIMER_SHOT;
                            goto GOTO_COM_EVENT_PRI_TIMER_SHOT;
                        }

                    } while (0);
                    break;
                GOTO_COM_EVENT_PRI_TIMER_SHOT:
                case COM_EVENT_PRI_TIMER_SHOT:
                    keep_time = 0;
                    goto ROLE_CASE;


                case COM_EVENT_SEC_TIMER_TRIG:
                    // com_vsys_send( COM_USER_EVENT, USER_EVENT_TIMER1_TRIG, NULL, 0);
                    goto ROLE_CASE;


                case COM_EVENT_SEC_TIMER_SHOT_NEW:
                    do
                    {
                        uint32_t new_time;
                        memcpy(&new_time , msg_payload, msg_len);
                        com_sec_timer_stop();
                        com_sec_timer_shot(new_time);
                    } while (0);
                    break;
                
                case COM_EVENT_SEC_TIMER_EARLY:
                    goto ROLE_CASE;


                case COM_EVENT_SEC_TIMER_SHOT:
                    goto ROLE_CASE;

                /* NBIOT */
            #ifdef EMOS_SUPPORT_NBIOT
                case COM_EVENT_NBIOT_CMD:
                    goto ROLE_CASE;

                case COM_EVENT_NBIOT_TRANS:
                    BACKTO_EMOS();
                    break;

                case COM_EVENT_NBIOT_RECV:
                    goto ROLE_CASE;
                    
                case COM_EVENT_NBIOT_ENT_KEY:
                    goto ROLE_CASE;

                case COM_EVENT_NBIOT_ENT_COMMAND:
                    goto ROLE_CASE;

                case COM_EVENT_NBIOT_RECV_KEY:
                    goto ROLE_CASE;

                case COM_EVENT_NBIOT_RECV_COMMAND:
                    goto ROLE_CASE;
            #endif

                /* Lora*/
                case COM_EVENT_LORA_TRANSPARENT:
                    goto ROLE_CASE;

                case COM_EVENT_LORA_RECV:
                    goto ROLE_CASE;

                case COM_EVENT_LORAWAN_HEAP:
                    goto ROLE_CASE;

                case COM_EVENT_LORAWAN_TITLE:
                    goto ROLE_CASE;

                case COM_EVENT_LORAWAN_SEND:
                    goto ROLE_CASE;
                
                case COM_EVENT_LORAWAN_ERR:
                    goto ROLE_CASE;

                case COM_EVENT_LORA_JOINED:
                    goto ROLE_CASE;
                    
                /* GPIO*/
                case COM_EVENT_GPIO_PULLDN:
                case COM_EVENT_GPIO_PULLUP:
                    do
                    {
                        emos_evet_gpio_t *p_gpio;
                        p_gpio = (emos_evet_gpio_t *)msg_payload;
                        uint8_t group = p_gpio->group;
                        uint8_t pins  = p_gpio->pins;
                        uint16_t sel = (group * 100) + pins;
                        switch(sel)
                        {
                            case 24:
                                break;                           
                            default:
                                break;

                        }
                    } while (0);
                    goto ROLE_CASE;

                
                /* case flow */
                case COM_EVENT_SYS_INIT_DONE:
                    com_event_role_init(msg_type->msg_id,msg_payload,msg_len);
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SNSR_CKSTART, NULL, 0);
                    break;
                
                case COM_EVENT_PROTOCOL_DATRDY:
                    goto ROLE_CASE;

                case COM_EVENT_DFUMODE:
                    goto ROLE_CASE;

                ROLE_CASE:
                default:
                    #ifndef DUT_MODE
                    com_event_role_process(msg_type->msg_id,msg_payload,msg_len);
                    #endif
                    break;
            }
            break;
    }
})





