#include "../kernel.h"
#include "com.h"

uint8_t HOOK_NAME(com_vsys_send) (uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len)
{
    return com.vsys->mbox()->send(task_id, dir_id, msg_type, msg, len);
}

uint8_t HOOK_NAME(com_vsys_mrsend) (uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len, uint8_t * more, uint32_t more_len)
{
    return com.vsys->mbox()->mrsend(task_id, dir_id, msg_type, msg, len, more, more_len);
}

void * HOOK_NAME(com_malloc) ( uint32_t len)
{
    return com.vsys->mem()->malloc(len);
}

void HOOK_NAME(com_free) ( void *p )
{
    com.vsys->mem()->free(p);
}

void *HOOK_NAME(com_copy) ( uint32_t new_len, void *src, uint32_t src_len )
{
    return com.vsys->mem()->copy(new_len,src,src_len);
}

com_if_rak_probe_t HOOK_NAME(com_sprobe)(uint8_t idx)
{
    return com.rak_probe(idx);
}

com_if_vsys_mydev_t *HOOK_NAME(com_mydev) ( void )
{
    return (void *)(com.vsys->mydev);
}

uint8_t HOOK_NAME(com_get_snsr) ( uint8_t pid, uint8_t sid, com_if_rak_probe_sensor_t **info )
{
    return com.rak_probe(pid).snsr_get(sid,info);
}

uint8_t HOOK_NAME(com_get_probe) ( uint8_t pid, com_if_rak_probe_param_t **info )
{
    return com_sprobe(pid).probe_get(info);
}

/******************************************************************************/
/*                            HUB API                                         */
/******************************************************************************/

uint8_t HOOK_NAME(com_nbiot_power) (uint8_t value)
{
    com.nbiot->power(value);
    return EMOS_OK;
}

uint8_t HOOK_NAME(com_nbiot_upload) (const char *data)
{
    com.nbiot->apply(1);
    com.nbiot->upload((uint8_t *)data, strlen(data));

    return EMOS_OK;
}

uint8_t HOOK_NAME(com_nbiotapp) (const char *mode)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));
    memset((uint8_t *)&(eee.nbiot_info.mode), 0, sizeof(eee.nbiot_info.mode));
    memcpy((uint8_t *)&(eee.nbiot_info.mode), (uint8_t *)mode, strlen(mode));
    com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));

    return EMOS_OK;
}

uint8_t HOOK_NAME(com_http_request) (uint8_t method, const char *url)
{
    com_if_rak_bank2_t eee;
    memset(&eee, 0, sizeof(com_if_rak_bank2_t));
    com_flash_bank2_read((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));


    eee.nbiot_info.http_method = method;
    com.nbiot->ctrl->nbt_http_method = method;

    memset((uint8_t *)&(eee.nbiot_info.http_url), 0, sizeof(eee.nbiot_info.http_url));
    memcpy((void *)&(eee.nbiot_info.http_url), url, strlen((void *)url));
    eee.nbiot_info.http_url_len = strlen((void *)url);

    com_flash_bank2_write((uint8_t *)&eee, sizeof(com_if_rak_bank2_t));


    return EMOS_OK;
}

uint8_t HOOK_NAME(com_nbiot_apply) (uint8_t apply)
{
    com.nbiot->apply(apply);
    return EMOS_OK;
}

uint8_t HOOK_NAME(com_cli_cmd) (const char *cmd)
{
    com.cli->command(cmd ,strlen(cmd));

    return EMOS_OK;
}

// com_if_rak_probe_param_t *HOOK_NAME(com_probe) ( void )
// {
//     return com_mydev()->iprobe;
// }

/*

*/

// void HOOK_NAME(com_printf_arry)  ( uint8_t level, const char *data, uint16_t len)
// {
//     emos.log->debugA(level,data,len);
// }

// void HOOK_NAME(com_printf)   ( uint8_t level,char *, ... )
// {
//     va_list arg; 
//     va_start(arg,fmt); 
//     do_log_vprintf(dir,fmt,arg);
//     va_end(arg);
// }



