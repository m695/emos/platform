#include "../kernel.h"

#ifdef EMOS_SUPPORT_HOST

#include "inc/inc_co_service.h"
#include "inc/inc_co_rak_sensor.h"
#include "inc/inc_co_rak_wire_protocol.h"
#include "inc/inc_co_hallsnsr.h"
#include "inc/inc_co_rak_ldo.h"
#include "inc/inc_co_nbiot.h"

void com_event_role_init(uint16_t msg_id, uint8_t * msg_payload, uint16_t msg_len)
{
    com_if_rak_bank0_t *eee;
    com_if_vsys_mydev_t * mydev = com_mydev();
    com_flash_bank0_readonly((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));

    // com_if_rak_bank0_t *sss;
    
    // emos((uint8_t *)&sss, sizeof(com_if_rak_bank0_t));

#ifdef EMOS_SUPPORT_LORA
    emos.lorawan->ctrl->auto_join.value = eee->lora_autojoin;
    emos.lorawan->ctrl->auto_join.cnt = eee->lora_joincnt;
    emos.lorawan->ctrl->auto_join.cnt_def = eee->lora_joincnt;
    emos.lorawan->ctrl->auto_join.delay_time = eee->lora_joindly;

    emos.lorawan->autojoin();
    
#endif
    

#ifdef EMOS_SUPPORT_NBIOT

    if (com.nbiot->ctrl->nbt_en == 1 && eee->lpwan_mode== 2 )
    {
#ifndef EMOS_SUPPORT_NBIOT_DTM
    com.nbiot->power(1);
#endif
    }
#endif
    
    com_vsys_send(COM_TASK_RAK_LDO, COM_TASK_EVT_RAK_LDO_VCCPROBE_INIT, NULL, 0);
}

void com_event_role_process(uint16_t msg_id, uint8_t * msg_payload, uint16_t msg_len)
{
    com_if_vsys_mydev_t * mydev = com_mydev();
    switch (msg_id)
    {
        case COM_EVENT_NEXT_WAKEUP:
            com_vsys_send( COM_TASK_SERV, COM_TASK_SERV_AFTER_TIME_SHOT, NULL, 0);
            break;

        
        case COM_EVENT_BLE_CONN:
            SYS_INFO(EVT_MSG("BLE Connected"));
#ifdef EMOS_SUPPORT_NBIOT
            com.nbiot->ble_lock(1);
#endif
            break;

        case COM_EVENT_BLE_DISC:
            SYS_INFO(EVT_MSG("BLE Disconnect"));
#ifdef EMOS_SUPPORT_NBIOT
            com.nbiot->ble_lock(0);
#endif
            break;

        case COM_EVENT_SLEEP:
           break;

        case COM_EVENT_SNSR_CKSTART:
            com_vsys_send( COM_USER_EVENT, USER_EVENT_START_PREPARE, NULL, 0);
            com_vsys_send( COM_TASK_RAK_SENSOR, COM_VSYS_RAK_SNSR_START, NULL, 0);
            break;
        case COM_EVENT_SNSR_CKDONE:
            com_vsys_send( COM_USER_EVENT, USER_EVENT_SENSOR_CKDONE, NULL, 0);
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROVISION_START, NULL, 0);
            break;

        case COM_EVENT_PRI_TIMER_SHOT:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SNSR_SYNCSTART, NULL, 0);
            break;
        
        case COM_EVENT_SNSR_SYNCSTART:
            com_vsys_send( COM_TASK_RAK_SENSOR, COM_VSYS_RAK_SNSR_SYNC, NULL, 0);
            break;

        case COM_EVENT_PRI_TIMER_TRIG:
#ifdef EMOS_SUPPORT_NBIOT
            if(com.nbiot->ctrl->nbt_context == 0)
            {
                com.nbiot->network_check();
                break;
            }
#endif
            break;

        case COM_EVENT_SNSR_SYNCDONE:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_NEXT_WAKEUP, NULL, 0);
            break;


        case COM_EVENT_PROVISION_START:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROVISION_DONE, NULL, 0);
            break;

        case COM_EVENT_PROVISION_DONE:
            if ( com_sprobe(0).loc_probe_get(&(com_iprobe)) == EMOS_OK )
            {
                SYS_INFO(EVT_MSG("ADD_PROBE:%d"),com_iprobe->id);
            }
            
            com_vsys_send( COM_TASK_RAK_SENSOR, COM_VSYS_RAK_SNSR_UPDATE, NULL, 0);
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_PRI_TIMER_SHOT, NULL, 0);
            break;

        case COM_EVENT_SNSR_DATREQ:
            goto GOTO_COM_EVENT_PROTOCOL_DATRDY;
            break;

        GOTO_COM_EVENT_PROTOCOL_DATRDY:
        case COM_EVENT_PROTOCOL_DATRDY:
            com_vsys_send( COM_TASK_SERV, COM_TASK_SERV_SEND, msg_payload, msg_len);
            break;

        case COM_EVENT_PROBE_ONLINE:
            SYS_INFO(EVT_MSG("ADD_PROBE:%d"),msg_payload[0]);
            break;

        case COM_EVENT_PARAM_UPDATE:
            break;

        case COM_EVENT_SYS_REBOOT:
            emos_reboot();
            break;

        case COM_EVENT_SYS_REST:
            break;

        case COM_EVENT_PROBE_DFU_PUSH:
            SYS_INFO(EVT_MSG("DFU_PROBE:%d"),msg_payload[0]);
            break;

        case COM_EVENT_PROBE_UPDATE_DONE:
            SYS_INFO(EVT_MSG("UPD_PROBE:%d"),msg_payload[0]);
            break;

        case COM_EVENT_SENSOR_UPDATE_DONE:
            SYS_INFO(EVT_MSG("UPD_SENSR:%d:%d"),msg_payload[0],msg_payload[1]);
            break;

        case COM_EVENT_PROBE_DELETE_DONE:
            do
            {
                uint8_t pid = msg_payload[0];
                com_if_rak_probe_param_t *probe_info;
                com_sprobe(pid).probe_get(&probe_info);
                if (probe_info->sta.real != IENABLE)
                {
                    probe_info->sta.stop = IENABLE;
                }
                
                SYS_INFO(EVT_MSG("DEL_PROBE:%d"),pid);
            } while (0);
            break;

        case COM_EVENT_PROBE_RESET_DONE:    
            SYS_INFO(EVT_MSG("REST_PROBE:%d"),msg_payload[0]);
            break;

        case COM_EVENT_PROBE_REBOOT_DONE:
            SYS_INFO(EVT_MSG("REBOOT_PROBE:%d"),msg_payload[0]);
            break;

        case COM_EVENT_NBIOT_ENT_KEY:
            com.nbiot->enter.key(msg_payload,msg_len);
            break;

        case COM_EVENT_NBIOT_ENT_COMMAND:
            com.nbiot->enter.puts(msg_payload,msg_len);
            break;

        case COM_EVENT_NBIOT_RECV_KEY:
            com_task_wakeup(COM_TASK_RAK_NBIOT);
            com.nbiot->recv.key(msg_payload,msg_len);
            break;
        
        case COM_EVENT_NBIOT_CMD:
            com_task_wakeup(COM_TASK_RAK_NBIOT);
            com_vsys_send( COM_TASK_RAK_NBIOT, COM_TASK_RAK_NBIOT_CMD, msg_payload, msg_len);
            break;

        case COM_EVENT_NBIOT_RECV_COMMAND:
            com_task_wakeup(COM_TASK_RAK_NBIOT);
            com.nbiot->recv.command(msg_payload,msg_len);
            com_vsys_send( COM_USER_EVENT, USER_EVENT_NBIOT_RECV, msg_payload, msg_len);
            break;
#ifdef EMOS_SUPPORT_LORA
        case COM_EVENT_LORAWAN_HEAP:
            emos.lorawan->heap(msg_payload, msg_len);
            break;

        case COM_EVENT_LORAWAN_TITLE:
            emos.lorawan->title(msg_payload, msg_len);
            break;

        case COM_EVENT_LORAWAN_ERR:
            SYS_INFO(EVT_MSG("LORA_ERR:%d"),msg_payload[0]);
            break;

        case COM_EVENT_LORA_JOINED:
            SYS_INFO(EVT_MSG("LORA_JOINED"));
            break;
        
        case COM_EVENT_LORA_RECV:
            SYS_INFO(EVT_MSG("LORA_RECV"));
            DBG_INFOA(msg_payload,msg_len);
            break;
            
	    case COM_EVENT_LORA_TRANSPARENT:
	        emos.lora->uplink(msg_payload[0],(char *)&msg_payload[1],msg_len - 1);
	        break;

        case COM_EVENT_LORAWAN_SEND:
            emos.lorawan->fport(msg_payload[0]);
            emos.lorawan->send();
	        break;
#endif
        case COM_EVENT_SNSR_ALERT:
            do
            {
                uint8_t probe_id = msg_payload[0];
                uint8_t snsr_id = msg_payload[1];
                SYS_INFO(EVT_MSG("ALERT_SENSR:%d:%d"),probe_id,snsr_id);
            } while (0);
            break;
        
        case COM_EVENT_GPIO_PULLDN:
        case COM_EVENT_GPIO_PULLUP:
            do
            {
                emos_evet_gpio_t *p_gpio;
                p_gpio = (emos_evet_gpio_t *)msg_payload;
                uint8_t group = p_gpio->group;
                uint8_t pins  = p_gpio->pins;
                uint16_t sel = (group * 100) + pins;
                switch(sel)
                {
                    case PIN_HALL_SNSNR:
                        com_vsys_send(COM_TASK_HALLSNSNR,COM_TASK_HALLSNSNR_ACTIVE,NULL,0);
                        break;
                    default:
                        break;

                }
            } while (0);
            break;

        case COM_EVENT_SYS_TARGET_REBOOT:
            SYS_INFO(DO_MSG("REBOOT_PROBE:%d"),msg_payload[0]);
            break;

        case COM_EVENT_SYS_TARGET_DFU:
            SYS_INFO(DO_MSG("DFU_PROBE:%d"),msg_payload[0]);
            break;

        case COM_EVENT_SYS_TARGET_REST:
            SYS_INFO(DO_MSG("REST_PROBE:%d"),msg_payload[0]);
            break;
            
        case COM_EVENT_SYS_TARGET_KILL:
            SYS_INFO(DO_MSG("DEL_PROBE:%d"),msg_payload[0]);
            break;

        case COM_EVENT_SYS_TARGET_INTV:
            SYS_INFO(DO_MSG("PROBE_INTV:%d"),msg_payload[0]);
            break;
        
        case COM_EVENT_HALLF_ACTIVE:
            SYS_INFO(EVT_MSG("HALL ACTIVE"));
            break;
            
        case COM_EVENT_SYS_PROBE_PWRON:
            com.rpcl->snd_hello();
            SYS_INFO(EVT_MSG("Hello! SensorHub."));
            break;

            
        case COM_EVENT_NBIOT_CONN:
            //SYS_INFO(EVT_MSG("NB_CONNECT"));
            break;
        case COM_EVENT_NBIOT_DISC:
            //SYS_INFO(EVT_MSG("NB_DISCONNECT"));
            break;
        case COM_EVENT_NBIOT_RECOVER:
            //SYS_INFO(EVT_MSG("NB_RECOVER"));
            break;
        case COM_EVENT_NBIOT_ACTIVE:
            //SYS_INFO(EVT_MSG("NB_ACTIVATE"));
            break;
        case COM_EVENT_NBIOT_SENDED:
            SYS_INFO(EVT_MSG("NB_SEND_OK"));
            break;
        case COM_EVENT_NBIOT_SENDFAIL:
            SYS_INFO(EVT_MSG("NB_SEND_FAILED"));
            break;
        case COM_EVENT_NBIOT_SENDTRY:
            SYS_INFO(EVT_MSG("NB_SEND_RETRY"));
            break;
        case COM_EVENT_NBIOT_NOSIMCARD:
            SYS_INFO(EVT_MSG("NB_NO_SIM_CARD"));
            break;
        case COM_EVENT_NBIOT_POWERON:
            //SYS_INFO(EVT_MSG("NB_POWER_ON"));
            break;
        case COM_EVENT_NBIOT_POWEROFF:
            //SYS_INFO(EVT_MSG("NB_POWER_OFF"));
            break;
        case COM_EVENT_NBIOT_GPSERROR:
            //SYS_INFO(EVT_MSG("GPS_NOT_FIXED"));
            break;
        case COM_EVENT_NBIOT_CMDTIMEOUT:
            SYS_INFO(EVT_MSG("ATCELL_TIEMOUT"));
            break;
    }
}
#endif
