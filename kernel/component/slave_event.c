#include "../kernel.h"

#ifdef EMOS_SUPPORT_SLAVE

#include "inc/inc_co_service.h"
#include "inc/inc_co_rak_sensor.h"
#include "inc/inc_co_rak_ldo.h"


void com_event_role_init(uint16_t msg_id, uint8_t * msg_payload, uint16_t msg_len)
{
    // com_if_rak_bank0_t eee;
    // com_if_vsys_mydev_t * mydev = com_mydev;
    // com_flash_bank0_read((uint8_t *)&eee, sizeof(com_if_rak_bank0_t));
    
    /* create virtual probe */
    do
    {
        com_sprobe(PROBE_ID_SEL).new_probe();
        com_sprobe(PROBE_ID_SEL).probe_get(&(com_iprobe));
        com_iprobe->sta.real = IENABLE;
    } while (0);

    com_vsys_send(COM_TASK_RAK_LDO, COM_TASK_EVT_RAK_LDO_V12_INIT, NULL, 0);
}

void com_event_role_process(uint16_t msg_id, uint8_t * msg_payload, uint16_t msg_len)
{
    /*
        Flow
        1. COM_EVENT_SNSR_CKSTART
        2. COM_EVENT_SNSR_CKDONE
        3. COM_EVENT_PROVISION_START
        4. COM_EVENT_PROVISION_DONE
        5. COM_EVENT_SNSR_SYNCSTART
        6 .COM_EVENT_SNSR_SYNCDONE
        7. COM_EVENT_NEXT_WAKEUP
        loop 5
    */

    com_if_vsys_mydev_t * mydev = com_mydev();
    switch (msg_id)
    {
        case COM_EVENT_NEXT_WAKEUP:
            com_vsys_send( COM_TASK_SERV, COM_TASK_SERV_AFTER_TIME_SHOT, NULL, 0);
            break;

        case COM_EVENT_SLEEP:
            break;

        case COM_EVENT_SNSR_CKSTART:
            com_vsys_send( COM_USER_EVENT, USER_EVENT_START_PREPARE, NULL, 0);
            com_vsys_send( COM_TASK_RAK_SENSOR, COM_VSYS_RAK_SNSR_START, NULL, 0);

       
            break;
        case COM_EVENT_SNSR_CKDONE:
            com_vsys_send( COM_USER_EVENT, USER_EVENT_SENSOR_CKDONE, NULL, 0);
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_PROVISION_START, NULL, 0);
            break;

        case COM_EVENT_PROVISION_START:
            com_pri_timer_shot( MSEC(DELAY_STARTUP) );
            break;
        
        case COM_EVENT_PRI_TIMER_SHOT:
            switch (mydev->id)
            {
                case PROBE_ID_DEF:
                    com.rpcl->snd_provision();// provision request
                    break;
                
                default:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SNSR_SYNCSTART, NULL, 0);
                    break;
            }
            break;

        case COM_EVENT_PRI_TIMER_TRIG:
            break;

        case COM_EVENT_PROVISION_DONE:
            SYS_INFO(EVT_MSG("INIT_PROBE:%d"),mydev->id);
            com_vsys_send( COM_TASK_RAK_SENSOR, COM_VSYS_RAK_SNSR_UPDATE, NULL, 0);
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_PRI_TIMER_SHOT, NULL, 0);
            break;

        case COM_EVENT_SNSR_SYNCSTART:
            com_vsys_send( COM_TASK_RAK_SENSOR, COM_VSYS_RAK_SNSR_SYNC, NULL, 0);
            break;

        case COM_EVENT_SNSR_SYNCDONE:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_NEXT_WAKEUP, NULL, 0);
            break;

        case COM_EVENT_PROTOCOL_DATRDY:
            com_vsys_send( COM_TASK_SERV, COM_TASK_SERV_SEND, msg_payload, msg_len);
            break;

        case COM_EVENT_SYS_TARGET_REST:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_REST, NULL, 0);
            break;

        case COM_EVENT_SYS_TARGET_KILL:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_REBOOT, NULL, 0);
            break;

        case COM_EVENT_SYS_TARGET_REBOOT:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_REBOOT, NULL, 0);
            break;

        case COM_EVENT_SYS_TARGET_DFU:
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_DFU, NULL, 0);
            break;

        case COM_EVENT_SYS_DFU:
            com.flashmap->bootmode();
            com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_REBOOT, NULL, 0);
            break;

        case COM_EVENT_DFUMODE:
            do
            {
                uint32_t new_nxtime = MSEC(10);
                com_vsys_send( COM_TASK_EVENT, COM_EVENT_PRI_TIMER_SHOT_NEW, &new_nxtime, sizeof(new_nxtime));
            } while (0);
        
            break;

        case COM_EVENT_SYS_TARGET_INTV:
            do
            {
                uint8_t probe_id = msg_payload[0];
                UNUSED(probe_id);
                uint8_t *new_data = &(msg_payload[1]);
                com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_INTV, new_data, (msg_len - sizeof(probe_id)));
            } while (0);
            
            break;
            
        default:
            break;
    
    }
}
#endif
