#ifndef __KERNEL_H__
#define __KERNEL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../raklink.h"

/******************************************************************************/
typedef union
{
    uint8_t value;
    struct
    {
        uint8_t bit0:1;
        uint8_t bit1:1;
        uint8_t bit2:1;
        uint8_t bit3:1;
        uint8_t bit4:1;
        uint8_t bit5:1;
        uint8_t bit6:1;
        uint8_t bit7:1;
    };
} __EMOS_PACKED byte_t;


typedef struct 
{
    union 
    {
        uint32_t value;
        uint8_t  bytes[4];
    } ;
} __EMOS_PACKED emos_uin32t;

/******************************************************************************/

#define EMOS_TASK_NUM        EMOS_DEF_TASK_NUM

#define HOOK_NAME(X)         X##_hook

#include "emos/emos.h"
#include "emos/emos_vsys.h"
#include "emos/emos_event.h"
#include "emos/emos_hook.h"

#include "component/com.h"
#include "component/com_vsys.h"
#include "component/com_event.h"
#include "component/com_hook.h"

#ifndef BSP_NOT_SUPPORT_WEAK
#define entry_install(X)     __EMOS_WEAK void X(emos_task_msg_t *msg){}
#else
#define entry_install(X)     extern void X(emos_task_msg_t *msg);
#endif


#ifdef __cplusplus
}
#endif


#endif
