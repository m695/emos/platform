#include "../kernel.h"

#define THIS_POOL_ID      EMOS_POOL_ID
#define THIS_POOL_SIZE    EMOS_POOL_SIZE

#define SECD_POOL_ID      EMOS_GLOBAL_POOL_ID
#define SECD_POOL_SIZE    EMOS_GLOBAL_POOL_SIZE

/******************************************************************************/

static emos_if_vsys_ctrl_t this_ctrl;
static loop_handle_t this_loop_handle;
static init_handle_t this_init_handle;

static void vsystem_init(void);
static void vsystem_run(void);
static emos_if_msgbox_t * do_emos_vsys_mbox( void );
static emos_if_malloc_t * do_emos_vsys_mem( void );

const emos_if_vsys_t emos_if_vsys = 
{
    .init   = vsystem_init,
    .run    = vsystem_run,
    .mem    = do_emos_vsys_mem,
    .mbox   = do_emos_vsys_mbox,
    .reg_init_handle = (init_handle_t)&this_init_handle,
    .reg_loop_handle = (loop_handle_t)&this_loop_handle,
    .ctrl  = &this_ctrl,
};

/******************************************************************************/

entry_install(emos_hal_wdog_process)
entry_install(emos_hal_uart_process)
entry_install(emos_hal_gpio_process)
entry_install(emos_hal_i2c_process)
entry_install(emos_hal_nfc_process)
entry_install(emos_hal_ble_process)
entry_install(emos_hal_lora_process)
entry_install(emos_hal_tim_process)
entry_install(emos_hal_flash_process)
entry_install(emos_hal_rs485_process)
entry_install(emos_hal_sdi12_process)
entry_install(emos_hal_rs232_process)
entry_install(emos_hal_adc_process)
entry_install(emos_hal_powersaving_process)
entry_install(emos_hal_lorawan_process)
entry_install(emos_hal_radio_process)
entry_install(emos_hc_event_process)
entry_install(emos_hal_bsprc_process)
entry_install(emos_hal_digital_process)

#define ENTRY_ACTION_ALL  (TASK_MODE_INIT|TASK_MODE_EVENT|TASK_MODE_POLLING)
#define ENTRY_ACTION_DEF  (TASK_MODE_INIT|TASK_MODE_EVENT)
const emos_task_entry_t emos_task_entry_tbl[] =
{ 
    [EMOS_TASK_WDOG]          = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_ALL,.task_entry=emos_hal_wdog_process},
    [EMOS_TASK_UART]          = { .priority=EMOS_TASK_PRIORITY_HIGH    ,.mode=ENTRY_ACTION_ALL,.task_entry=emos_hal_uart_process},
    [EMOS_TASK_GPIO]          = { .priority=EMOS_TASK_PRIORITY_MID     ,.mode=ENTRY_ACTION_ALL,.task_entry=emos_hal_gpio_process},
    [EMOS_TASK_TIM]           = { .priority=EMOS_TASK_PRIORITY_HIGH    ,.mode=ENTRY_ACTION_ALL,.task_entry=emos_hal_tim_process},
    [EMOS_TASK_I2C]           = { .priority=EMOS_TASK_PRIORITY_MID     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_i2c_process},
    [EMOS_TASK_NFC]           = { .priority=EMOS_TASK_PRIORITY_MID     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_nfc_process},
    [EMOS_TASK_BLE]           = { .priority=EMOS_TASK_PRIORITY_MID     ,.mode=ENTRY_ACTION_ALL,.task_entry=emos_hal_ble_process},
    [EMOS_TASK_LORA]          = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_lora_process},
    [EMOS_TASK_LORAWAN]       = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_lorawan_process},
    [EMOS_TASK_RADIO]         = { .priority=EMOS_TASK_PRIORITY_MID     ,.mode=ENTRY_ACTION_ALL,.task_entry=emos_hal_radio_process},
    [EMOS_TASK_FLASH]         = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_flash_process},
    [EMOS_TASK_RS485]         = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_rs485_process},
    [EMOS_TASK_SDI12]         = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_sdi12_process},
    [EMOS_TASK_RS232]         = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_rs232_process},
    [EMOS_TASK_ADC]           = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_adc_process},
    [EMOS_TASK_DIGITAL]       = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_digital_process},
    [EMOS_TASK_BSPRC]         = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_bsprc_process},
    [EMOS_TASK_EVENT]         = { .priority=EMOS_TASK_PRIORITY_LOW     ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hc_event_process},
    [EMOS_TASK_PSM]           = { .priority=EMOS_TASK_PRIORITY_LASTEST ,.mode=ENTRY_ACTION_DEF,.task_entry=emos_hal_powersaving_process},
};

static emos_if_msgbox_t * do_emos_vsys_mbox( void )
{
    return emos.msgbox(THIS_POOL_ID);
}

static emos_if_malloc_t * do_emos_vsys_mem( void )
{
    return emos.mem(THIS_POOL_ID);
}

static void vsystem_init( void )
{
    static uint8_t msgbox_pool[THIS_POOL_SIZE];
    memset(msgbox_pool, 0, THIS_POOL_SIZE);
    emos.vsys->mbox()->init( msgbox_pool, sizeof(msgbox_pool));
    
    static uint8_t global_pool[SECD_POOL_SIZE];
    memset(global_pool, 0, SECD_POOL_SIZE);
    emos.mem(SECD_POOL_ID)->init(global_pool,sizeof(global_pool));

    /* enable debug message*/
    emos.log->ctrl->level = EMOS_LOG_ALL;
    /* *************************** */

    uint8_t task_cnt = COUNT_OF(emos_task_entry_tbl);
    for(uint8_t i=0;i<task_cnt;i++)
    {
        emos.vsys->mbox()->task_add(( emos_task_entry_t * )&emos_task_entry_tbl[i]);
    }
}

static void vsystem_run(void)
{
    static uint8_t run_init = IENABLE;
    static uint16_t ret = 0;
    static uint32_t this_time = 0;
    uint32_t cur_time = 0;
    static uint8_t sys100ms = 0;
    static uint8_t sys500ms = 0;

    if (run_init == IENABLE)
    {
        ret = EMOS_MSGBOX_NO_TASK;
        ret = emos.vsys->mbox()->polling();
        if ( ret == EMOS_MSGBOX_NO_TASK)
        {
            emos_vsys_send( EMOS_TASK_EVENT, EMOS_EVENT_INIT_DONE, NULL, 0);
            run_init = IDISABLE;
        }
        return;
    }
    
    uint32_t this_tick = emos.units->gettick();
    emos.vsys->ctrl->gettick = this_tick;
    emos.msgbox(0)->ctrl->time_tick = this_tick;
    cur_time = emos_gettick();

    if((cur_time - this_time) > 5)
    {
        sys100ms++;
        if(sys100ms == 20)
        {
            sys100ms = 0;
            sys500ms++;
            if(sys500ms == 5)
            {
                sys500ms = 0;
            }
            emos_vsys_send( EMOS_TASK_EVENT, EMOS_EVENT_100MS, NULL, 0);
        }
        
        this_time = cur_time;
        
        // emos.wdog->inc();
    }

    ret = emos.vsys->mbox()->polling();
    if (ret == EMOS_MSGBOX_CONTINUE)
    {
        return;
    }
    
    emos_vsys_send( EMOS_TASK_EVENT, EMOS_EVENT_IDLE, &ret, sizeof(ret));
    
}



