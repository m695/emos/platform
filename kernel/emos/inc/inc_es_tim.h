#ifndef __EMOS_INC_TIM_H__
#define __EMOS_INC_TIM_H__

#ifdef __cplusplus
extern "C" {
#endif

#define THIS_TIM_NUM        EMOS_TIM_NUM

typedef void ( *emos_tim_start_t ) ( uint32_t);
typedef void ( *emos_tim_shot_t ) ( uint32_t);
typedef void ( *emos_tim_istart_t ) ( uint32_t, uint32_t );
typedef void ( *emos_tim_ishot_t ) ( uint32_t, uint32_t);
typedef void ( *emos_tim_stop_t ) ( void );
typedef uint32_t ( *emos_tim_pass_t ) ( uint32_t );

typedef enum
{
    EMOS_TASK_EVENT_TIM0_KICK,
    EMOS_TASK_EVENT_TIM1_KICK,
    EMOS_TASK_EVENT_TIM0_UPDATE,
} EMOS_TASK_EVENT_TIM_ID_E;

extern void emos_hal_tim_process(emos_task_msg_t *emos_task_msg_t);

extern emos_if_tim_t emos_if_tim_func(uint8_t idx);

#ifdef __cplusplus
}
#endif

#endif

