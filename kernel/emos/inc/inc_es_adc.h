#ifndef __EMOS_INC_ADC_H__
#define __EMOS_INC_ADC_H__

#ifdef __cplusplus
extern "C" {
#endif

#define THIS_ADC_NUM        4
typedef uint32_t ( *emos_adc_read_t ) ( void );


extern void emos_hal_adc_process(emos_task_msg_t *task_msg);

extern emos_if_adc_t  emos_if_adc_func(uint8_t idx);

#ifdef __cplusplus
}
#endif

#endif
