#ifndef __EMOS_INC_SDI12_H__
#define __EMOS_INC_SDI12_H__

#ifdef __cplusplus
extern "C" {
#endif

#define THIS_SDI12_NUM        1
typedef uint8_t ( *emos_sdi12_conf_t ) ( uint32_t , uint8_t , uint8_t , int8_t );
typedef uint8_t ( *emos_sdi12_deconf_t ) ( void );
typedef uint8_t ( *emos_sdi12_write_t ) (  uint8_t *, int );
typedef uint8_t ( *emos_sdi12_read_t ) ( uint8_t *, int, uint16_t );


extern void emos_hal_sdi12_process(emos_task_msg_t *task_msg);

extern emos_if_sdi12_t * emos_if_sdi12_func(uint8_t idx);

#ifdef __cplusplus
}
#endif

#endif
