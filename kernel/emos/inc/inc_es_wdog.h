#ifndef __EMOS_INC_WDOG_H__
#define __EMOS_INC_WDOG_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifdef EMOS_SUPPORT_WDOG

typedef enum
{
    EMOS_TASK_EVENT_WDOG_FEED,
    EMOS_TASK_EVENT_WDOG_INC,
} EMOS_TASK_EVENT_WDOG_ID_E;

extern void emos_hal_wdog_process(emos_task_msg_t *);

extern const emos_if_wdog_t emos_if_wdog;

#endif

#ifdef __cplusplus
}
#endif

#endif

