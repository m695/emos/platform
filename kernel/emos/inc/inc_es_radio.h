#ifndef __EMOS_INC_RADIO_H__
#define __EMOS_INC_RADIO_H__

#ifdef __cplusplus
extern "C" {
#endif

extern void emos_hal_radio_process(emos_task_msg_t *emos_task_msg_t);

#ifdef __cplusplus
}
#endif

#endif
