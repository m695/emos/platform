#ifndef __EMOS_INC_LORAWAN_H__
#define __EMOS_INC_LORAWAN_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    uint16_t             payload_size;
} __EMOS_PACKED rak_lora_region_t;

typedef enum
{
    US915_UPL_SF10BW125 = 0,
    US915_UPL_SF09BW125,
    US915_UPL_SF08BW125,
    US915_UPL_SF07BW125,
    US915_UPL_SF08BW500,
    US915_DEF_CR13BW162,
    US915_DEF_CR23BW325,
    US915_DEF_RFU,
    US915_DWL_SF12BW500,
    US915_DWL_SF11BW500,
    US915_DWL_SF10BW500,
    US915_DWL_SF09BW500,
    US915_DWL_SF08BW500,
    US915_DWL_SF07BW500,
} us915_dr_table_e;

typedef enum
{
    EU868_UPL_SF12BW125 = 0,
    EU868_UPL_SF11BW125,
    EU868_UPL_SF10BW125,
    EU868_UPL_SF9BW125,
    EU868_UPL_SF8BW125,
    EU868_UPL_SF7BW125,
    EU868_UPL_SF7BW250,
    EU868_FSK_50K,
    EU868_DWL_CR13BW137,
    EU868_DWL_CR23BW137,
    EU868_DWL_CR13BW336,
    EU868_DWL_CR23BW336,
    EU868_DEF_RFU,
} eu868_dr_table_e;

typedef enum
{
    EMOS_TASK_EVENT_LORAWAN_DATA2HEAP,
    EMOS_TASK_EVENT_LORAWAN_DATA2TITLE,
    EMOS_TASK_EVENT_LORAWAN_DATA2FPORT,
    EMOS_TASK_EVENT_LORAWAN_DATA2SEND,
    EMOS_TASK_EVENT_LORAWAN_DATA2CONTINUE,
    EMOS_TASK_EVENT_LORAWAN_NOJOINCASE,
    EMOS_TASK_EVENT_LORAWAN_DOAUTOJOIN,
    EMOS_TASK_EVENT_LORAWAN_NONOE,
} EMOS_TASK_EVENT_LORAWAN_ID_E;

extern void emos_hal_lorawan_process(emos_task_msg_t *emos_task_msg_t);

extern const emos_if_lorawan_t emos_if_lorawan;

#ifdef __cplusplus
}
#endif

#endif

