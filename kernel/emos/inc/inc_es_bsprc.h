#ifndef __EMOS_INC_BSPRC_H__
#define __EMOS_INC_BSPRC_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
    EMOS_TASK_EVENT_BSPRC_ATCMD,
} EMOS_TASK_EVENT_BSPRC_ID_E;

extern void emos_hal_bsprc_process(emos_task_msg_t *emos_task_msg_t);

#ifdef __cplusplus
}
#endif

#endif

