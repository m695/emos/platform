#ifndef __EMOS_INC_UART_H__
#define __EMOS_INC_UART_H__

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(EMOS_UART_NUM)
    #define EMOS_UART_NUM        1
#endif

#define THIS_UART_NUM        EMOS_UART_NUM

typedef void (*emos_uart_putc_t ) ( uint8_t );
typedef void (*emos_uart_puts_t ) ( const char * , uint16_t);
typedef void (*emos_uart_recv_t ) ( char * , uint16_t, uint32_t );
typedef void (*emos_uart_flush_t ) ( void );
typedef void (*emos_uart_vprintf_t ) ( const char *, va_list );
typedef void (*emos_uart_printf_t ) ( char *, ... );
typedef void (*emos_uart_disable_t ) ( uint8_t );

typedef enum
{
    EMOS_TASK_EVENT_UART0_RECV,
    EMOS_TASK_EVENT_UART1_RECV,
    EMOS_TASK_EVENT_UART2_RECV,
    EMOS_TASK_EVENT_UART3_RECV,
    EMOS_TASK_EVENT_UART4_RECV,
    EMOS_TASK_EVENT_USBUART_RECV,
    EMOS_TASK_EVENT_LPUART1_RECV,
    EMOS_TASK_EVENT_ONWIRE_RECV,
    EMOS_TASK_EVENT_UART0_WK,
    EMOS_TASK_EVENT_UART1_WK,
    EMOS_TASK_EVENT_UART2_WK,
    EMOS_TASK_EVENT_UART_QTX,
    EMOS_TASK_EVENT_LPUART1_WK,
    VSYS_RS485_UART1_WK,
} EMOS_TASK_EVENT_UART_ID_E;

extern void emos_hal_uart_process(emos_task_msg_t *emos_task_msg_t);

extern emos_if_uart_t emos_if_tuart_func(uint8_t idx);

#ifdef __cplusplus
}
#endif

#endif

