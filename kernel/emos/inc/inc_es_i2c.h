#ifndef __EMOS_INC_I2C_H__
#define __EMOS_INC_I2C_H__

#ifdef __cplusplus
extern "C" {
#endif

#define THIS_I2C_NUM        EMOS_I2C_NUM

typedef enum
{
    EMOS_TASK_EVENT_I2C1_RECV,
    EMOS_TASK_EVENT_I2C2_RECV,
} EMOS_TASK_EVENT_I2C_ID_E;

typedef uint8_t ( *emos_i2c_write_t ) ( uint16_t, uint16_t, uint8_t *, uint16_t );
typedef uint8_t ( *emos_i2c_read_t ) ( uint16_t, uint16_t, uint8_t *, uint16_t );
typedef uint8_t ( *emos_i2c_swrite_t ) ( uint16_t, uint8_t *, uint16_t );
typedef uint8_t ( *emos_i2c_sread_t ) ( uint16_t, uint8_t *, uint16_t );
typedef uint8_t ( *emos_i2c_con_write_t ) ( uint16_t, uint16_t, uint8_t *, uint16_t );
typedef uint8_t ( *emos_i2c_con_read_t ) ( uint16_t, uint16_t, uint8_t *, uint16_t );


extern emos_if_i2c_t * emos_if_i2c_func(uint8_t idx);

extern void emos_hal_i2c_process(emos_task_msg_t *emos_task_msg_t);


#ifdef __cplusplus
}
#endif

#endif