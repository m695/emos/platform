#ifndef __EMOS_INC_RS485_H__
#define __EMOS_INC_RS485_H__

#ifdef __cplusplus
extern "C" {
#endif
#define THIS_RS485_NUM        1

typedef uint8_t ( *emos_rs485_conf_t ) ( uint32_t , uint8_t , uint8_t , int8_t );
typedef uint8_t ( *emos_rs485_deconf_t ) ( void );
typedef uint8_t ( *emos_rs485_write_t ) (  uint8_t *, int );
typedef uint8_t ( *emos_rs485_read_t ) ( uint8_t *, int, uint16_t );


extern void emos_hal_rs485_process(emos_task_msg_t *task_msg);

extern emos_if_rs485_t * emos_if_rs485_func(uint8_t idx);

#ifdef __cplusplus
}
#endif

#endif