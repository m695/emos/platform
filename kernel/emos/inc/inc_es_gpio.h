#ifndef __EMOS_INC_GPIO_H__
#define __EMOS_INC_GPIO_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*emos_gpio_pull_t ) ( uint8_t ,uint8_t, uint8_t);
typedef void (*emos_gpio_dir_t ) ( uint8_t ,uint8_t, uint8_t);
typedef void (*emos_gpio_set_t ) ( uint8_t ,uint8_t, uint8_t);
typedef uint8_t (*emos_gpio_get_t ) ( uint8_t ,uint8_t );

extern const emos_if_gpio_t emos_if_gpio;

extern void emos_hal_gpiote_process(emos_task_msg_t *emos_task_msg_t);

#ifdef __cplusplus
}
#endif

#endif

