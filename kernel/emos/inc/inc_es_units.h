#ifndef __EMOS_INC_UNITS_H__
#define __EMOS_INC_UNITS_H__

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(EMOS_RAND_SEED_NUM)
    #define EMOS_RAND_SEED_NUM   1
#endif

#define RAND_SEED_NUM    EMOS_RAND_SEED_NUM   

typedef void ( *emos_delay_ms_t ) ( uint32_t );
typedef void ( *emos_reboot_t ) ( void );
typedef uint32_t ( *emos_gettick_t ) ( void );


typedef void ( *emos_radio_enable_t ) ( uint8_t );
typedef void ( *emos_radio_switch_t ) ( uint8_t );

extern const emos_if_units_t emos_if_units;

#ifdef __cplusplus
}
#endif

#endif

