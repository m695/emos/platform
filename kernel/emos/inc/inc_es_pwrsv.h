#ifndef __EMOS_INC_POWERSAVING_H__
#define __EMOS_INC_POWERSAVING_H__

#ifdef __cplusplus
extern "C" {
#endif


typedef enum
{
    EMOS_TASK_EVENT_POWERSAVING_ACT,
    EMOS_TASK_EVENT_POWERSAVING_COUNT,
    EMOS_TASK_EVENT_POWERSAVING_HOLD,
    EMOS_TASK_EVENT_POWERSAVING_REST,
} EMOS_TASK_EVENT_POWERSAVING_ID_E;

extern void emos_hal_powersaving_process(emos_task_msg_t *emos_task_msg_t);

#ifdef __cplusplus
}
#endif

#endif