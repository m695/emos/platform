#ifndef __EMOS_INC_LINKLIST_H__
#define __EMOS_INC_LINKLIST_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    void *                  next;
    uint8_t                 __dummy[];
} __EMOS_PACKED emos_linklist_t;

extern const emos_if_linklist_t emos_if_linklist;

#ifdef __cplusplus
}
#endif

#endif





