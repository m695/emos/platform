#ifndef __EMOS_INC_NFC_H__
#define __EMOS_INC_NFC_H__

#ifdef __cplusplus
extern "C" {
#endif


typedef enum
{
    EMOS_TASK_EVENT_NFC_RECV,
    EMOS_TASK_EVENT_NFC_READ,
    EMOS_TASK_EVENT_NFC_ONOFF,
    EMOS_TASK_EVENT_NFC_TEST,
    EMOS_TASK_EVENT_NFC_TEST_CK,
} EMOS_TASK_EVENT_NFC_ID_E;

extern void emos_hal_nfc_process(emos_task_msg_t *emos_task_msg_t);

extern const emos_if_nfc_t emos_if_nfc;


#ifdef __cplusplus
}
#endif

#endif

