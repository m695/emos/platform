#ifndef __EMOS_INC_LORA_H__
#define __EMOS_INC_LORA_H__

#ifdef __cplusplus
extern "C" {
#endif


#define THIS_LORA_NUM        1

typedef enum
{
    LORA_CLASS_A = 0,
    LORA_CLASS_B,
    LORA_CLASS_C,
} rak_lora_class_e;


typedef uint8_t ( *emos_lora_set_band_t ) ( uint8_t );
typedef int16_t ( *emos_lora_get_band_t ) ( void );
typedef uint8_t ( *emos_lora_set_nwm_t ) ( uint8_t );
typedef int16_t ( *emos_lora_get_nwm_t ) ( void );
typedef uint8_t ( *emos_lora_set_cfm_t ) ( uint8_t );
typedef uint8_t ( *emos_lora_get_cfm_t ) ( void );
typedef uint8_t ( *emos_lora_set_class_t ) ( uint8_t );
typedef int16_t ( *emos_lora_get_class_t ) ( void );
typedef uint8_t ( *emos_lora_njs_t ) (void);
typedef uint8_t ( *emos_lora_get_dr_t ) (void);
typedef uint8_t ( *emos_lora_set_dr_t ) ( uint8_t );
typedef uint8_t ( *emos_lora_get_adr_t ) (void);
typedef uint8_t ( *emos_lora_set_adr_t ) ( uint8_t );
typedef uint8_t ( *emos_lora_set_deveui_t ) ( uint8_t * );
typedef uint8_t ( *emos_lora_get_deveui_t ) ( uint8_t * );
typedef uint8_t ( *emos_lora_set_appeui_t ) ( uint8_t * );
typedef uint8_t ( *emos_lora_get_appeui_t ) ( uint8_t * );
typedef uint8_t ( *emos_lora_set_appkey_t ) ( uint8_t * );
typedef uint8_t ( *emos_lora_get_appkey_t ) ( uint8_t * );

typedef enum
{
    EMOS_TASK_EVENT_LORA_DOWNLINK,
    EMOS_TASK_EVENT_LORA_KRESET,
    EMOS_TASK_EVENT_LORA_JOIN,
    EMOS_TASK_EVENT_LORA_JOIN_END,
    EMOS_TASK_EVENT_LORA_TEST,
    EMOS_TASK_EVENT_LORA_TEST_CK,
    EMOS_TASK_EVENT_LORA_UPLINK,
    EMOS_TASK_EVENT_LORA_RESET,
    EMOS_TASK_EVENT_LORAWAN_WAKEUP,
} EMOS_TASK_EVENT_LORA_ID_E;

extern void emos_hal_lora_process(emos_task_msg_t *emos_task_msg_t);

extern const emos_if_lora_t emos_if_lora;

#ifdef __cplusplus
}
#endif

#endif

