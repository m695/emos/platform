#ifndef __EMOS_INC_FLASH_H__
#define __EMOS_INC_FLASH_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef uint32_t ( *emos_field_write_t) ( uint32_t, uint8_t *, uint16_t );
typedef void     ( *emos_field_read_t)  ( uint32_t, uint8_t **, uint16_t );

typedef uint32_t ( *emos_eeprom_write_t) ( uint32_t, uint8_t *, uint16_t );
typedef void     ( *emos_eeprom_read_t)  ( uint32_t, uint8_t *, uint16_t );
typedef void     ( *emos_eeprom_readonly_t)  ( uint32_t, uint8_t **, uint16_t );

typedef uint32_t ( *emos_flash_write_t)  ( uint32_t, uint32_t *, uint32_t );
typedef void     ( *emos_flash_read_t)   ( uint32_t, uint8_t *, uint32_t );
typedef uint32_t ( *emos_flash_earse_t)  ( uint32_t, uint32_t );

extern const emos_if_flash_t emos_if_flash;

extern void emos_hal_flash_process(emos_task_msg_t *emos_task_msg_t);

#ifdef __cplusplus
}
#endif

#endif

