#ifndef __EMOS_INC_RS232_H__
#define __EMOS_INC_RS232_H__

#ifdef __cplusplus
extern "C" {
#endif

#define THIS_RS232_NUM        1
typedef uint8_t ( *emos_rs232_conf_t ) ( uint32_t , uint8_t , uint8_t , int8_t );
typedef uint8_t ( *emos_rs232_deconf_t ) ( void );
typedef uint8_t ( *emos_rs232_write_t ) (  uint8_t *, int );
typedef uint8_t ( *emos_rs232_read_t ) ( uint8_t *, int, uint16_t );


extern void emos_hal_rs232_process(emos_task_msg_t *task_msg);

extern emos_if_rs232_t * emos_if_rs232_func(uint8_t idx);

#ifdef __cplusplus
}
#endif

#endif
