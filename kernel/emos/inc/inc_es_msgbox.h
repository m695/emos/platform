#ifndef __EMOS_INC_MSGBOX_H__
#define __EMOS_INC_MSGBOX_H__

#ifdef __cplusplus
extern "C" {
#endif

#define THIS_MSGBOX_NUM      EMOS_MSGBOX_NUM
#define MSGBOX_TASK_NUM      EMOS_DEF_TASK_NUM
#define TASK_PRIORITY_NUM    EMOS_TASK_PRIORITY_NUM


extern emos_if_msgbox_t *emos_if_msgbox_func(uint8_t idx);

#ifdef __cplusplus
}
#endif

#endif
