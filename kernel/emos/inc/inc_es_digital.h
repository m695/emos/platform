#ifndef __EMOS_INC_DIGITAL_H__
#define __EMOS_INC_DIGITAL_H__

#ifdef __cplusplus
extern "C" {
#endif

#define THIS_DIGITAL_NUM        4

typedef uint8_t ( *emos_if_digital_read_t ) ( void );
typedef void ( *emos_if_digital_write_t ) ( uint8_t );

extern void emos_hal_digital_process(emos_task_msg_t *task_msg);

extern emos_if_digital_t emos_if_digital_func(uint8_t);

#ifdef __cplusplus
}
#endif

#endif
