#ifndef __EMOS_INC_MALLOC_H__
#define __EMOS_INC_MALLOC_H__

#ifdef __cplusplus
extern "C" {
#endif

#define __MAX_ALIGNMENT__   4

#if !defined(EMOS_MALLOC_POOL_NUM)
    #define EMOS_MALLOC_POOL_NUM   20
#endif

typedef enum
{
    POOL_NUM_DEFAULT      = 0,
    POOL_NUM_MAX          = EMOS_MALLOC_POOL_NUM,
} POOL_NUM_E;

extern emos_if_malloc_t * emos_if_malloc_func(uint8_t idx);

#ifdef __cplusplus
}
#endif

#endif

