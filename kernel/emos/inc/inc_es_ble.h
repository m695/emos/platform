#ifndef __EMOS_INC_BLE_H__
#define __EMOS_INC_BLE_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
    EMOS_TASK_EVENT_BLE_ADV_START,
    EMOS_TASK_EVENT_BLE_ADV_NAME,
    EMOS_TASK_EVENT_BLE_STOP,
    EMOS_TASK_EVENT_BLE_DISCONNECT,
    EMOS_TASK_EVENT_BLE_UART_SEND,
    EMOS_TASK_EVENT_BLE_UART_RECV,
    EMOS_TASK_EVENT_BLE_TEST,
    EMOS_TASK_EVENT_BLE_TEST_CK,
    EMOS_TASK_EVENT_BLE_SCAN_RECV,
} EMOS_TASK_EVENT_BLE_ID_E;

#define THIS_BLE_NUM        1

typedef void (*emos_ble_uart_putc_t ) ( uint8_t );
typedef void (*emos_ble_uart_puts_t ) ( const char * , uint16_t);
typedef void (*emos_ble_uart_printf_t ) ( char *, ... );
typedef void (*emos_ble_uart_vprintf_t ) ( char * , va_list );
typedef void (*emos_ble_uart_puth_t ) ( const char *,uint16_t len, const char * );
typedef uint8_t * (*emos_ble_getmac_t ) ( void );

extern const emos_if_ble_t emos_if_ble;

extern void emos_hal_ble_process(emos_task_msg_t *emos_task_msg_t);



#ifdef __cplusplus
}
#endif

#endif

