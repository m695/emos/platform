#include "../../kernel.h"
#include "../inc/inc_es_lorawan.h"

static emos_if_lorawan_ctrl_t this_ctrl;
static void do_lorawan_heap( uint8_t * data, uint16_t len);
static void do_lorawan_title( uint8_t * data, uint16_t len);
static void do_lorawan_send( void );
static void do_lorawan_joined( void );
static void do_lorawan_fport( uint8_t data);
static void do_lorawan_autojoin( void );

/******************************************************************************/

const emos_if_lorawan_t emos_if_lorawan = 
{
    .ctrl = &this_ctrl,
    .heap = do_lorawan_heap,
    .title = do_lorawan_title,
    .fport = do_lorawan_fport,
    .send = do_lorawan_send,
    .joined = do_lorawan_joined,
    .autojoin = do_lorawan_autojoin,
};

    
/******************************************************************************/

static void do_lorawan_heap( uint8_t * data, uint16_t len)
{
    emos_vsys_send(EMOS_TASK_LORAWAN,EMOS_TASK_EVENT_LORAWAN_DATA2HEAP,data,len);
}

static void do_lorawan_title( uint8_t * data, uint16_t len)
{
    emos_vsys_send(EMOS_TASK_LORAWAN,EMOS_TASK_EVENT_LORAWAN_DATA2TITLE,data,len);
}

static void do_lorawan_fport( uint8_t data)
{
    emos_vsys_send(EMOS_TASK_LORAWAN,EMOS_TASK_EVENT_LORAWAN_DATA2FPORT,&data,1);
}

static void do_lorawan_send( void )
{
    this_ctrl.send_cnt++;
    emos_vsys_send(EMOS_TASK_LORAWAN,EMOS_TASK_EVENT_LORAWAN_DATA2SEND,NULL,0);
}

static void do_lorawan_joined( void )
{
    emos.lorawan->ctrl->auto_join.cnt = 0;
}

static void do_lorawan_autojoin( void )
{
    emos_vsys_send(EMOS_TASK_LORAWAN,EMOS_TASK_EVENT_LORAWAN_DOAUTOJOIN,NULL,0);
}