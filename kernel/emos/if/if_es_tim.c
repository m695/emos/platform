#include "../../kernel.h"
#include "../inc/inc_es_tim.h"

/******************************************************************************/
static emos_if_tim_ctrl_t this_ctrl;
static uint32_t hook_tim_clock[THIS_TIM_NUM];
static uint8_t this_idx;

static void do_tim_install_kclk(uint32_t khz);
static void do_tim_start ( uint32_t periodic );
static void do_tim_shot ( uint32_t wtime);
static void do_tim_stop ( );

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_TIM_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(tim_shot)
do_install(tim_start)
do_install(tim_stop)

static const emos_if_tim_t emos_if_tim = 
{                                                   
    .i.khz_clk = (void(*)(uint32_t))do_tim_install_kclk,                       
    .i.start = (void *)do_install_tim_start,                   
    .i.shot = (void *)do_install_tim_shot,  
    .i.stop  = (void *)do_install_tim_stop,
    .start = (emos_tim_start_t)do_tim_start,  
    .stop  = (emos_tim_stop_t)do_tim_stop,   
    .shot  = (emos_tim_shot_t)do_tim_shot,
    .ctrl  = &this_ctrl,
};

emos_if_tim_t emos_if_tim_func(uint8_t idx)
{
    this_idx = idx;
    return emos_if_tim;
}

/******************************************************************************/

static void do_tim_install_kclk(uint32_t khz)
{
    uint8_t idx = this_idx;
    hook_tim_clock[idx] = khz;
}

static void do_tim_start ( uint32_t periodic )
{
    uint8_t idx = this_idx;
    emos_tim_istart_t this_tim_start = emos.tim(idx).i.start(NULL);
    uint32_t clock_khz = hook_tim_clock[idx];
    if (periodic == 0 )
    {
        periodic = 1024;
    }
    
    ICHECK(this_tim_start,);

    this_tim_start(clock_khz, periodic);
}

static void do_tim_stop ( void )
{
    uint8_t idx = this_idx;
    emos_tim_stop_t this_tim_stop = emos.tim(idx).i.stop(NULL);
    
    ICHECK(this_tim_stop,);

    this_tim_stop();
}

static void do_tim_shot ( uint32_t wtime)
{
    uint8_t idx = this_idx;
    emos_tim_ishot_t this_tim_shot = emos.tim(idx).i.shot(NULL);
    uint32_t clock_khz = hook_tim_clock[idx];
    if (wtime == 0 )
    {
        wtime = 1024;
    }

    
    ICHECK(this_tim_shot,);

    do_tim_stop();
    this_tim_shot(clock_khz, wtime);

    this_ctrl.cfg_time = wtime;
}




