#include "../../kernel.h"
#include "../inc/inc_es_malloc.h"
/******************************************************************************/

#define __FUNC_DISABLE_IRQ__    //__disable_irq();
#define __FUNC_ENABLE_IRQ__    //__enable_irq();


/* Current heap pointer */
static char *_last_heap_object[POOL_NUM_MAX];
static char *_heap_of_memory[POOL_NUM_MAX];
static char *_top_of_heap[POOL_NUM_MAX];
static uint8_t this_active[POOL_NUM_MAX];
static uint8_t this_idx;

static void init_mempool(uint8_t *heap, uint16_t len);
static void *imalloc(uint32_t n);
static void ifree(void *ptr);
static uint8_t active_mempool( void );
static void* icopy(uint32_t new, void *src, uint32_t src_len);

static const emos_if_malloc_t emos_if_malloc = 
{
    .malloc = imalloc,
    .free   = ifree,
    .init   = init_mempool,
    .active = active_mempool,
    .copy   = icopy,
};

emos_if_malloc_t *emos_if_malloc_func(uint8_t idx)
{
    this_idx = idx;
    return (emos_if_malloc_t *)&emos_if_malloc;
}

/******************************************************************************/

typedef struct {
    // The 4 MSB of 'val' is indicate the busy.
    char isBusy : 4;
    // The 4 LSB's of 'val' indicate memory pool index, in 8-bit bytes.
    char index : 4;
} __attribute__((packed)) MemHdrFlag_t;

/* The header for each allocated region on the heap */
typedef struct
{
    MemHdrFlag_t flag;
    char *next;
} __attribute__((aligned(4))) _m_header;


static uint8_t active_mempool( void )
{
    uint8_t index = this_idx;
    return this_active[index];
}

//#define   _heap_of_memory     memory_pool
//#define   _top_of_heap        (memory_pool+sizeof(memory_pool)-1)
//#define _heap_of_memory ((char * )__segment_begin("XDATA_HEAP"))
//#define _top_of_heap    ((char * )__segment_end("XDATA_HEAP"))
static void init_mempool(uint8_t *heap, uint16_t len)
{
    uint8_t index = this_idx;
    memset( heap , 0, len);
    _last_heap_object[index] = 0;
    _heap_of_memory[index] = (char *)heap;
    _top_of_heap[index] = (char *)(heap + (len - 1));
    this_active[index] = 1;
}
/*----------------------------------------------------------------------*/
/* If there are free bytes at location pointed to by head_p in quantity */
/* bigger then block header creates free block.                         */
/*----------------------------------------------------------------------*/
static void _make_new_mem_hole_xdata(_m_header *head_p, char *ptr)
{
    char *p;

    if (head_p->next - ptr > sizeof(_m_header))
    {
        p = head_p->next;
        head_p->next = ptr;
        head_p = (_m_header *) ptr;
        head_p->flag.isBusy = 0;
        head_p->next = p;
    }
}

/*----------------------------------------------------------------------*/
/* Tries to allocate n bytes of memory.   First looks for memory after  */
/* last busy block and if there is enough space allocates them there.   */
/* If there is not it searches the blocks from begining to find big     */
/* enough free block. First fit is accepted.  Returns pointer to the    */
/* first byte after block header if request for memory satisfied or     */
/* NULL otherwise.                                                      */
/*----------------------------------------------------------------------*/

static void *imalloc(uint32_t n)
{
    uint8_t index = this_idx;
    #ifdef EMOS_SUPPORT_CMALLOC
    void *ptr=malloc(nnn);
    memset(ptr, 0, nnn);
    return ptr;
    #else    
    __FUNC_DISABLE_IRQ__
    char *ptr;
    char *ret_ptr;
    char *result_ptr;
    _m_header *head_p;
    /*==========================================*/
    /* If alignment required round up           */
    /* n to nearest __MAX_ALIGNMENT__ multiple  */
    /*==========================================*/
    result_ptr = NULL;

    if(n == 0)
    {
        goto FUNC_EXIT;
    }

    n += __MAX_ALIGNMENT__ - 1;
    n &= ~(__MAX_ALIGNMENT__ - 1);

    if(_last_heap_object[index] == 0)
    {
        char * p = (void *)_heap_of_memory[index];
        _last_heap_object[index] = p;
    }

    //enter_critical_section();

    if ((n + sizeof(_m_header)) <= (_top_of_heap[index] - _last_heap_object[index]))
    {
        /*=================================*/
        /* There is still space at the top */
        /*=================================*/
        head_p = (_m_header *) _last_heap_object[index];
        head_p->flag.isBusy = 1;           /* Set busy */
        head_p->flag.index = index;        /* Set memory pool index */
        _last_heap_object[index] += n + sizeof(_m_header);
        head_p->next = _last_heap_object[index];
                                         /* Calculate address of next free block */
        result_ptr = _last_heap_object[index] - n;
    }
    else
    {
        /*=================*/
        /* Look for a hole */
        /*=================*/
        ptr = _heap_of_memory[index];
        while (ptr < _last_heap_object[index])
        {
            head_p = (_m_header *) ptr;
            if (   ! head_p->flag.isBusy
                && (head_p->next - ptr) - sizeof(_m_header) >= n )
            {
                /*========================*/
                /* Found suitable hole... */
                /*========================*/
                ret_ptr = ptr + sizeof(_m_header);
                head_p->flag.isBusy = 1;
                head_p->flag.index = index;        /* Set memory pool index */
                /*===================================*/
                /* If there is space left - leave    */
                /* it as a hole so create new header */
                /*===================================*/
                _make_new_mem_hole_xdata(head_p, ret_ptr + n);
                result_ptr = ret_ptr;
                goto FUNC_EXIT;
            }
            ptr = head_p->next;
        }
        /*=======================*/
        /* Sorry, no memory left */
        /*=======================*/
        result_ptr = 0;
    }

    if ( result_ptr != 0)
    {
        memset(result_ptr,0,n);
    }
    
FUNC_EXIT:
    //exit_critical_section();
    if(!result_ptr)
    {
        longjmp(*(emos.svpnt), !0);
    }

    __FUNC_ENABLE_IRQ__
    
    // DBG_INFO("%02x",result_ptr);
    return result_ptr;
    #endif
}

/*----------------------------------------------------------------------*/
/* Releases block of memory starting at location given as argument and  */
/* if it creates two or three free blocks in the row make them one.     */
/* If there is no block begining at this location does nothing.         */
/*----------------------------------------------------------------------*/
static void ifree( void *ptr)
{
    #ifdef EMOS_SUPPORT_CMALLOC
    free(ptr);
    ptr=NULL;
    #else    
    uint8_t index = this_idx;
    __FUNC_DISABLE_IRQ__
    char *prev = _heap_of_memory[index];
    char *curr = _heap_of_memory[index];
    _m_header *prev_h;
    _m_header *curr_h;
    _m_header *next_h;
    _m_header *now_h;
    (void)now_h;

    /* No action for null pointer  */
    if (ptr == 0)
    {
        goto FUNC_EXIT;
    }

    if (ptr == NULL)
    {
        goto FUNC_EXIT;
    }
    
    now_h = (_m_header *) ptr;

    while (curr < _last_heap_object[index])
    {
        if (ptr == curr + sizeof(_m_header))        /* Block found */
        {
            curr_h = (_m_header *) curr;
            if (curr_h->flag.isBusy)                  /* Block busy */
            {
                prev_h = (_m_header *) prev;
                                                /* Header of previous block */
                next_h = curr_h->next < _last_heap_object[index] ?
                (_m_header *) curr_h->next : curr_h;
                                                /* If no next exists make next
                                                   equal current */
                                                /* Header of next block */
                if (!prev_h->flag.isBusy)               /* If previous block free */
                {
                    prev_h->next = next_h->flag.isBusy ? curr_h->next : next_h->next;
                    curr_h = prev_h;
                }
                else
                {
                    if (!next_h->flag.isBusy)
                    {
                        curr_h->next = next_h->next;
                    }
                    curr_h->flag.isBusy = 0;
                }

                if (curr_h->next == _last_heap_object[index])
                {
                                                /* Update _last_heap_object pointer */
                    _last_heap_object[index] = (char *) curr_h;
                }
            }
            goto FUNC_EXIT;
        }
        prev = curr;
        curr = ((_m_header *) curr)->next;
    }

FUNC_EXIT:
    __FUNC_ENABLE_IRQ__
    return;
    #endif
}

static void* icopy(uint32_t new, void *src, uint32_t src_len)
{
    void *p = imalloc(new);
    memcpy(p,src,src_len);
    
    emos_delay(1); //wait cpu busy
    
    return p;
}




