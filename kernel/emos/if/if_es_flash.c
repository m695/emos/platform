#include "../../kernel.h"
#include "../inc/inc_es_flash.h"

/***************************************************/
static uint32_t do_eeprom_write(uint32_t addr, uint8_t *data, uint32_t size );
static void do_eeprom_read(uint32_t addr , uint8_t *data, uint32_t size);
static uint32_t do_flash_write(uint32_t addr, uint32_t *data, uint32_t size );
static void do_flash_read(uint32_t addr , uint8_t *data, uint32_t size);
static uint32_t do_flash_earse(uint32_t addr, uint32_t page);
static uint32_t do_field_write(uint32_t addr, uint8_t *data, uint32_t size );
static void do_field_read(uint32_t addr , uint8_t **data, uint32_t size);
static void do_eeprom_readonly(uint32_t addr , uint8_t **data, uint32_t size);

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[1]; \
    if ( adr != NULL ) hookp[0] = adr; \
    return hookp[0]; \
}

do_install(field_write)
do_install(field_read)
do_install(eeprom_write)
do_install(eeprom_read)
do_install(eeprom_readonly)
do_install(flash_write)
do_install(flash_read)
do_install(flash_earse)

const emos_if_flash_t emos_if_flash = 
{
    .i.field_read = do_install_field_read,
    .i.field_write = do_install_field_write,
    .i.eeprom_read = do_install_eeprom_read,
    .i.eeprom_write = do_install_eeprom_write,
    .i.eeprom_readonly = do_install_eeprom_readonly,
    .i.spi_read = do_install_flash_read,
    .i.spi_write = do_install_flash_write,
    .i.spi_earse = do_install_flash_earse,

    .field_read = do_field_read,
    .field_write = do_field_write,
    .eeprom_read = do_eeprom_read,
    .eeprom_write = do_eeprom_write,
    .eeprom_readonly = do_eeprom_readonly,
    .spi_earse = do_flash_earse,
    .spi_write = do_flash_write,
    .spi_read = do_flash_read,
};

/******************************************************************************/

static uint32_t do_field_write(uint32_t addr, uint8_t *data, uint32_t size )
{
    emos_field_write_t this_func = emos.flash->i.field_write(NULL);

    if ( this_func == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }
    
    return this_func( addr, data ,size );
}

static void do_field_read(uint32_t addr , uint8_t **data, uint32_t size)
{
    emos_field_read_t this_func = emos.flash->i.field_read(NULL);

    if ( this_func == NULL )
    {
        return;
    }

    this_func( addr, data ,size );
}

static uint32_t do_eeprom_write(uint32_t addr, uint8_t *data, uint32_t size )
{
    emos_eeprom_write_t this_func = emos.flash->i.eeprom_write(NULL);

    if ( this_func == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }
    
    return this_func( addr, data ,size );
}

static void do_eeprom_readonly(uint32_t addr , uint8_t **data, uint32_t size)
{
    emos_eeprom_readonly_t this_func = do_install_eeprom_readonly(NULL);

    if ( this_func == NULL )
    {
        return;
    }

    this_func( addr, data ,size );
}

static void do_eeprom_read(uint32_t addr , uint8_t *data, uint32_t size)
{
    emos_eeprom_read_t this_func = emos.flash->i.eeprom_read(NULL);

    if ( this_func == NULL )
    {
        return UNUSED(EMOS_ERROR_NOT_INIT);
    }

    this_func( addr, data ,size );
}

static uint32_t do_flash_earse(uint32_t addr, uint32_t page)
{
    emos_flash_earse_t this_func = emos.flash->i.spi_earse(NULL);
    
    if ( this_func == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }

    return this_func( addr, page );
}

static uint32_t do_flash_write(uint32_t addr, uint32_t *data, uint32_t size )
{
    emos_flash_write_t this_func = emos.flash->i.spi_write(NULL);
    
    if ( this_func == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }

    return this_func( addr,  data ,size );
}

static void do_flash_read(uint32_t addr , uint8_t *data, uint32_t size)
{
    emos_flash_read_t this_func = emos.flash->i.spi_read(NULL);
    
    if ( this_func == NULL )
    {
        return UNUSED(EMOS_ERROR_NOT_INIT);
    }
    
    this_func( addr,  data , size );
}

/*******************************************/


