#include "../../kernel.h"
#include "../inc/inc_es_units.h"

/******************************************************************************/

static char ** do_split(char *src, const char *sep, uint16_t limit, uint16_t *num);
static uint8_t do_format( char *src, const char *format) ;
static uint16_t do_strcmp( char *src, const char *cmpstr, uint8_t case_sens) ;
static uint8_t do_atoi(char* str, uint32_t *val);
static unsigned builtin_popcount (unsigned u);
static uint8_t* do_memcat(const char *s1, uint8_t s1len, const char *s2, uint8_t s2len);
static uint8_t do_atoh(char *pbDest, char *pbSrc, uint32_t len);
static uint32_t do_rand_roll(void);
static void do_rand_seed(uint8_t num, unsigned long seed);
static char do_toupper(char ch);
static void do_if_delay( uint32_t ms );
static uint32_t do_if_gettick( void );
static void do_if_reboot( void );
static void do_if_radio_enable( uint8_t enable );
static void do_if_radio_switch( uint8_t swich );
static uint32_t builtin_crc32(const void *buf, size_t size);
static uint16_t builtin_crc16(const void *buf, size_t size);
static uint8_t  do_memswap( uint8_t *s1, uint8_t slen);

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[1]; \
    if ( adr != NULL ) hookp[0] = adr; \
    return hookp[0]; \
}

do_install(delay_ms)
do_install(reboot)
do_install(gettick)
do_install(radio_enable)
do_install(radio_switch)

const emos_if_units_t emos_if_units = 
{
    .split   = do_split,
    .format  = do_format,
    .strcmp  = do_strcmp,
    .atoi    = do_atoi,
    .atoh    = do_atoh,
    .memcat  = do_memcat,
    .memswap = do_memswap,
    .rand    = do_rand_roll,
    .toupper = do_toupper,
    .dly   = do_if_delay,
    .reboot  = do_if_reboot,
    .gettick = do_if_gettick,
    .radio.enable = do_if_radio_enable,
    .radio.swich = do_if_radio_switch,
    .i.rand_seed = do_rand_seed,
    .i.gettick   = do_install_gettick,
    .i.dly     = do_install_delay_ms,
    .i.reboot    = do_install_reboot,

    .radio.i.enable     = do_install_radio_enable,
    .radio.i.swich     = do_install_radio_switch,
    .builtin.popcount   = builtin_popcount,
    .builtin.crc32     = builtin_crc32,
    .builtin.crc16     = builtin_crc16,
};

/******************************************************************************/

static unsigned long gseed[RAND_SEED_NUM];


static void do_if_delay( uint32_t ms )
{
    emos_delay_ms_t this_func = emos.units->i.dly(NULL);

    if (this_func == NULL)
    {
        return;
    }
    this_func(ms);
}

static uint32_t do_if_gettick( void )
{
    emos_gettick_t this_func = emos.units->i.gettick(NULL);

    if (this_func == NULL)
    {
        return 0;
    }
    return this_func();
}

static void do_if_reboot( void )
{
    emos_reboot_t this_func = emos.units->i.reboot(NULL);

    if (this_func == NULL)
    {
        return;
    }
    
    this_func();
}

static void do_if_radio_enable( uint8_t enable )
{
    emos_radio_enable_t this_func = emos.units->radio.i.enable(NULL);

    if (this_func == NULL)
    {
        return;
    }
    
    this_func(enable);
}

static void do_if_radio_switch( uint8_t swich )
{
    emos_radio_switch_t this_func = emos.units->radio.i.swich(NULL);

    if (this_func == NULL)
    {
        return;
    }
    
    this_func(swich);
}

static void do_rand_seed(uint8_t num, unsigned long seed)
{
    gseed[num] = seed;
}

static uint32_t do_rand_roll(void)
{
    unsigned long out = 0;
    for(uint8_t i=0; i<RAND_SEED_NUM ; i++ )
    {
        gseed[i] = (gseed[i] * 214013L) + 2531011L;
        out += (gseed[i] >> 16) & 0x7fff;
    }
    
    return (out/RAND_SEED_NUM);
}


static uint8_t  do_memswap( uint8_t *s1, uint8_t slen)
{
    uint8_t *s2 = emos_malloc(slen);
    memcpy(s2,s1,slen);

    for(uint8_t i=0, j=(slen-1); i<slen ;i++,j--)
    {
        s1[i] = s2[j];
    }
    emos_free(s2);
    return EMOS_OK;
}

static uint8_t* do_memcat(const char *s1, uint8_t s1len, const char *s2, uint8_t s2len)
{
    static uint8_t tmp[256];

    if ((s1len+s2len)>sizeof(tmp))
    {
        return NULL;
    }
    
    memset( tmp, 0, sizeof(tmp));
    memcpy( tmp+0, s1, s1len);
    if( s2 != NULL )
    {
        memcpy( tmp+s1len, s2, s2len);
    }
    
    return tmp;
}

static char do_toupper(char ch)
{
    if(ch >= 'a' && ch <= 'x')
    {
        ch += ('A' - 'a');
    }
    return ch;
}

static const uint32_t crc32_tab[] = {
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
	0xe963a535, 0x9e6495a3,	0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
	0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
	0xf3b97148, 0x84be41de,	0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,	0x14015c4f, 0x63066cd9,
	0xfa0f3d63, 0x8d080df5,	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
	0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,	0x35b5a8fa, 0x42b2986c,
	0xdbbbc9d6, 0xacbcf940,	0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
	0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
	0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,	0x76dc4190, 0x01db7106,
	0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
	0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
	0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
	0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
	0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
	0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
	0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
	0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
	0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
	0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
	0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
	0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
	0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
	0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
	0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
	0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
	0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
	0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
	0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
	0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
	0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
	0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
	0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
	0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
	0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
	0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
	0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

static uint32_t builtin_crc32(const void *buf, size_t size)
{
    const uint8_t *p = buf;
    uint32_t crc;

    crc = ~0U;
    while (size--)
    {
        crc = crc32_tab[(crc ^ *p++) & 0xFF] ^ (crc >> 8);
    }

    return crc ^ ~0U;
}

static uint16_t crc_update(uint16_t crc_in, int incr)
{
    uint16_t xr = crc_in >> 15;
    uint16_t out = crc_in << 1;

    if (incr)
        out++;

    if (xr)
        out ^= 0x1021;

    return out;
}

static uint16_t builtin_crc16(const void *buf, size_t size)
{
    const uint8_t *p = buf;
    uint16_t crc , i;

    for (crc = 0; size > 0; size--, p++)
        for (i = 0x80; i; i >>= 1)
            crc = crc_update(crc, *p & i);

    for (i = 0; i < 16; i++)
        crc = crc_update(crc, 0);

    return crc;
}

static unsigned builtin_popcount (unsigned u)
{
    u = (u & 0x55555555) + ((u >> 1) & 0x55555555);
    u = (u & 0x33333333) + ((u >> 2) & 0x33333333);
    u = (u & 0x0F0F0F0F) + ((u >> 4) & 0x0F0F0F0F);
    u = (u & 0x00FF00FF) + ((u >> 8) & 0x00FF00FF);
    u = (u & 0x0000FFFF) + ((u >> 16) & 0x0000FFFF);
    return u;
}

static uint8_t do_atoh(char *pbDest, char *pbSrc, uint32_t len)
{
    uint8_t h1,h2;
    uint8_t s1,s2;
    uint8_t ret,ret1,ret2;
    int i;

    ret = EMOS_OK;
    for (i=0; i<len; i++)
    {
        h1 = pbSrc[2*i];
        h2 = pbSrc[2*i+1];

        h1 = do_toupper(h1);
        if((h1 >= '0' && h1 <= '9') || (h1 >= 'A' && h1 <= 'F'))
        {
            ret1 = EMOS_OK;
            s1 = h1 - 0x30;
            if (s1 > 9)
            {
                s1 -= 7;
            }
        }
        else
        {
            ret1 = EMOS_ERROR;
            s1 = 0;
        }
        

        h2 = do_toupper(h2);
        if((h2 >= '0' && h2 <= '9') || (h2 >= 'A' && h2 <= 'F'))
        {
            ret2 = EMOS_OK;
            s2 = h2 - 0x30;
            if (s2 > 9)
            {
                s2 -= 7;
            }
        }
        else
        {
            ret2 = EMOS_ERROR;
            s2 = 0;
        }
        
        if ( ret1 && ret2) 
        {
            ret = EMOS_ERROR;
        }
        
        pbDest[i] = (s1<<4) + s2;
    }
 
    return ret;
}

static uint8_t do_atoi(char* str, uint32_t *val)
{
    uint8_t ret = 0;
    uint32_t i = 0;
    *val = 0;

    if(str[0] == '-' || str[0] == '+')
    {
        i = 1;
    }

    for (; str[i] != '\0'; ++i)
    {
        if(str[i] >= '0' && str[i] <= '9')
        {
            *val = *val * 10 + str[i] - '0';
        }
        else
        {
            ret = 1;
            break;
        }
    }
 
    if(str[0] == '-')
    {
        *val = ~*val;
        *val += 1;
    }

    return ret;
}

static char * this_strpbrk(const char * cs,const char * ct)
{
    const char *sc1,*sc2;

    for( sc1 = cs; *sc1 != '\0'; ++sc1) 
    {
        for( sc2 = ct; *sc2 != '\0'; ++sc2) 
        {
            if (*sc1 == *sc2)
            {
                return (char *) sc1;
            }
        }
    }
    return NULL;
}

static char * this_strsep(char **s, const char *ct)
{
    char *sbegin = *s, *end;

    if (sbegin == NULL)
    {
        return NULL;
    }

    end = this_strpbrk(sbegin, ct);
    if (end)
    *end++ = '\0';
    *s = end;

    return sbegin;
}

static char ** do_split(char *src, const char *sep, uint16_t limit, uint16_t *num) 
{
    static char *tmp[64] = {0};
    memset(tmp , 0, sizeof(tmp));
    char ** pp = (char **)&tmp;
    char *pNext;

    static char *tmp_buf = NULL;
    if (tmp_buf == NULL)
    {
        tmp_buf = emos_malloc(EMOS_UART_TRANS_BUFF);
    }

    static char *s = NULL;

    uint16_t count = 0;
    if( num != NULL )
    {
        *num = count;
    }

    if (sep == NULL || src == NULL)
    {
        return pp;
    }

    if(strlen(src) == 0 || strlen(sep) == 0)
    {
        return pp;
    }
    
    s = (char *)tmp_buf;
    uint16_t s_len = strlen(src);
    memset(s, 0, s_len+2);
    memcpy(s, src, s_len);

    pNext = this_strsep(&s, sep);
    while(pNext != NULL) 
    {
        tmp[count++] = pNext;
        
        if (limit != 0)
        {
            if (count >= (limit -1) )
            {
                tmp[count++] = pNext + strlen(pNext) + 1;
                break;
            }
        }
        
        pNext = (char *)this_strsep(&s, sep); 
    }
    
    if( num != NULL )
    {
        *num = count;
    }

    return pp;
} 

static uint16_t do_strcmp( char *src, const char *cmpstr, uint8_t case_sens) 
{
    uint16_t len1 = strlen(src);
    uint16_t len2 = strlen(cmpstr);
    // uint16_t len = (len1 < len2)?len1:len2;
    (void) len1;
    uint16_t len = len2;
    if( case_sens == 0 )
    {
        for(uint8_t i=0; i<len; i++)
        {
            char case_src;
            char case_cmpstr;

            case_src = src[i];
            if (case_src >= 'a' && case_src <= 'z')
            {
                case_src = case_src - 32;
            }

            case_cmpstr = cmpstr[i];
            if (case_cmpstr >= 'a' && case_cmpstr <= 'z')
            {
                case_cmpstr = case_cmpstr - 32;
            }
            
            if (case_src != case_cmpstr)
            {
                return i + 1;
            }
        }
        return 0;
    }
    else
    {
        return strcmp(src,cmpstr);
    }

}

static uint8_t do_format( char *src, const char *format) 
{
    uint8_t ret;
    char **pp;
    uint32_t tmp_len;
    char *tmp_buf;
    
    pp = do_split( src, " +=/:;[](){}", 0, NULL);

    tmp_len = snprintf(NULL, 0, format, pp[1], pp[2], pp[3], pp[4], pp[5], pp[6], pp[7], pp[8], pp[9], pp[10], pp[12], pp[13], pp[14], pp[15]) + 1;
    tmp_buf = emos_malloc(tmp_len);
    snprintf(tmp_buf, tmp_len, format, pp[1], pp[2], pp[3], pp[4], pp[5], pp[6], pp[7], pp[8], pp[9], pp[10], pp[12], pp[13], pp[14], pp[15]);
    
    ret = do_strcmp(src,tmp_buf, 0);

    emos_free(tmp_buf);
    return ret;
}


