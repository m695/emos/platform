#include "../../kernel.h"
#include "../inc/inc_es_sdi12.h"

/******************************************************************************/

static uint8_t this_idx;
static uint8_t do_sdi12_conf( uint32_t baudrate, uint8_t databit, uint8_t stopbit, int8_t parity );
static uint8_t do_sdi12_deconf(void);
static uint8_t do_sdi12_write( uint8_t * mcmd, int mlen);
static uint8_t do_sdi12_read( uint8_t *pbuf, int blen, uint16_t timeout );

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_SDI12_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(sdi12_conf)
do_install(sdi12_deconf)
do_install(sdi12_write)
do_install(sdi12_read)

static const emos_if_sdi12_t emos_if_sdi12 = 
{
    .i.conf      = do_install_sdi12_conf,
    .i.deconf    = do_install_sdi12_deconf,
    .i.write     = do_install_sdi12_write, 
    .i.read      = do_install_sdi12_read,
    .conf        = (emos_sdi12_conf_t)do_sdi12_conf,
    .deconf      = (emos_sdi12_deconf_t)do_sdi12_deconf,
    .write       = (emos_sdi12_write_t)do_sdi12_write, 
    .read        = (emos_sdi12_read_t)do_sdi12_read,
};

emos_if_sdi12_t * emos_if_sdi12_func(uint8_t idx)
{
    this_idx = idx;
    return (emos_if_sdi12_t *)&emos_if_sdi12;
}

/******************************************************************************/
static uint8_t do_sdi12_conf(uint32_t baudrate, uint8_t databit, uint8_t stopbit, int8_t parity)
{
    uint8_t idx = this_idx;
    emos_sdi12_conf_t this_sdi12_conf = emos.sdi12(idx)->i.conf(NULL);
    this_sdi12_conf(baudrate, databit, stopbit, parity);
    return EMOS_OK;
}

static uint8_t do_sdi12_deconf()
{
    uint8_t idx = this_idx;
    emos_sdi12_deconf_t this_sdi12_deconf = emos.sdi12(idx)->i.deconf(NULL);
    return this_sdi12_deconf();
}

static uint8_t do_sdi12_write(uint8_t * mcmd, int mlen)
{
    uint8_t idx = this_idx;
    emos_sdi12_write_t this_sdi12_write = emos.sdi12(idx)->i.write(NULL);
    return this_sdi12_write(mcmd, mlen);
}

static uint8_t do_sdi12_read( uint8_t *pbuf, int blen, uint16_t timeout )
{
    uint8_t idx = this_idx;
    emos_sdi12_read_t this_sdi12_read = emos.sdi12(idx)->i.read(NULL);
    return this_sdi12_read(pbuf,blen,timeout);
}

