#include "../../kernel.h"
#include "../inc/inc_es_wdog.h"

/******************************************************************************/
static void do_feed (void);
static void do_inc (void);

static emos_if_wdog_parm_t feed_time; 


const emos_if_wdog_t emos_if_wdog = 
{
    .param     = &feed_time,
    .feed      = do_feed,
    .inc       = do_inc,
};

/******************************************************************************/

static void do_feed (void) 
{
    feed_time.time = 0;
}

static void do_inc (void)
{
    feed_time.time++;
#ifdef EMOS_SUPPORT_WDOG
    if (feed_time.time > WDOG_FEED_TIME)
    {
        emos.units->reboot();
    }
#endif
}



