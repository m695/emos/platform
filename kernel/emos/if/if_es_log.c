#include "../../kernel.h"
#include "../inc/inc_es_log.h"

/******************************************************************************/

#define THIS_BUFF_SIZE  EMOS_UART_TRANS_BUFF

typedef emos_def_puts_t emos_log_puts_t;
static emos_log_puts_t hook_debug_puts = NULL;
static emos_if_log_ctrl_t hook_if_log_ctrl;

static void do_debug_arrary( uint8_t level, const char *str, uint16_t len);
static void do_log_puts( uint32_t dir, const char *str , uint16_t len);
static void do_log_printf(uint32_t dir, char *fmt, ...);
static void do_log_debug( uint8_t level ,char *fmt, ...);
static uint8_t * do_log_outext(char *fmt, ...);

const emos_if_log_t emos_if_log = 
{
    .i.debug = (void *)&hook_debug_puts,
    .debugA  = do_debug_arrary,
    .debug   = do_log_debug,
    .puts    = do_log_puts,
    .outext  = do_log_outext,
    .printf  = do_log_printf,
    .ctrl    = &hook_if_log_ctrl,
};

/******************************************************************************/
static void * do_log_finduart(uint32_t dir)
{
    for(size_t i=0; i<EMOS_UART_NUM ;i++)
    {
        if (emos.uart(i).sta()->register_event == dir)
        {
            return emos.uart(i).puts;
        }
    }
    return NULL;
}

static void do_log_puts( uint32_t dir, const char *str , uint16_t len)
{
    emos_log_puts_t this_puts = NULL;

    switch(dir)
    {
        case EMOS_EVENT_USBURT_RECV:
        case EMOS_EVENT_URT0_RECV:
        case EMOS_EVENT_URT1_RECV:
        case EMOS_EVENT_URT2_RECV:
        case EMOS_EVENT_URT3_RECV:
        case EMOS_EVENT_URT4_RECV:
        case EMOS_EVENT_LPUART1_RECV:
            this_puts = do_log_finduart(dir);
            break;
        case EMOS_EVENT_BLE_RECV:
            emos_vsys_send(EMOS_TASK_EVENT, EMOS_EVENT_BLE_SEND, str, len);
            return;
        case EMOS_EVENT_DBG_RECV:
            this_puts = *(emos_log_puts_t *)(emos.log->i.debug);
            break;
        // case EMOS_EVENT_LORA_RECV:
        //     break;
        // case EMOS_EVENT_NFC_RECV:
        //     break;
        default:
            break;
    }

    if (this_puts == NULL)
    {
        return;
    }

    this_puts( str , len);
}

static void do_log_vprintf(uint32_t dir, const char *fmt, va_list arg)
{
    uint16_t tmp_len;
    char *tmp_buf = (char *)hook_if_log_ctrl.tmp_buf;
    tmp_len = vsnprintf( NULL, 0, fmt, arg) + 1; 
    memset(tmp_buf, 0, THIS_BUFF_SIZE);
    vsnprintf( tmp_buf, tmp_len, fmt, arg); 

    do_log_puts( dir, tmp_buf , strlen(tmp_buf));
}

static void do_log_printf(uint32_t dir, char *fmt, ...)
{
    va_list arg; 
    va_start(arg,fmt); 
    do_log_vprintf(dir,fmt,arg);
    va_end(arg);
}

static uint8_t * do_log_outext(char *fmt, ...)
{
    uint16_t tmp_len;
    char *tmp_buf = (char *)hook_if_log_ctrl.tmp_buf;
    va_list arg; 
    va_start(arg,fmt); 
    tmp_len = vsnprintf( NULL, 0, fmt, arg) + 1; 
    memset(tmp_buf, 0, THIS_BUFF_SIZE);
    vsnprintf( tmp_buf, tmp_len, fmt, arg); 
    va_end(arg);

    return (uint8_t *)tmp_buf;
}

static void do_log_debug( uint8_t level, char *fmt, ...)
{
    uint16_t tmp_len;
    char *tmp_buf = (char *)hook_if_log_ctrl.tmp_buf;
    emos_log_puts_t this_puts = *(emos_log_puts_t *)(emos.log->i.debug);

    if (this_puts == NULL)
    {
        return;
    }

    if (hook_if_log_ctrl.level < level)
    {
        return;
    }

    va_list arg; 
    va_start(arg,fmt); 
    tmp_len = vsnprintf( NULL, 0, fmt, arg) + 1; 
    memset(tmp_buf, 0, THIS_BUFF_SIZE);
    vsnprintf( tmp_buf, tmp_len, fmt, arg); 

    this_puts(tmp_buf , strlen(tmp_buf));

    va_end(arg);
}

static void do_debug_arrary( uint8_t level, const char *str, uint16_t len)
{
    uint16_t i = 0;
    
    if ( hook_if_log_ctrl.level != EMOS_LOG_ALL )
    {
        do
        {
            if (hook_if_log_ctrl.level == level)
            {
                break;
            }
            
            return;
        } while (0);
    }

    if( len != 0 )
    {
        for(i=0; i<len - 1;i++)
        {
            do_log_debug(level ,"%02x,", str[i]);
        }
        do_log_debug(level, "%02x\r\n", str[i]);
    }
}
