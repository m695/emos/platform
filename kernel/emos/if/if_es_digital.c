#include "../../kernel.h"
#include "../inc/inc_es_digital.h"

/******************************************************************************/

static uint8_t this_idx;

static uint8_t do_digital_read( void );
static void do_digital_write( uint8_t val );

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_DIGITAL_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(if_digital_read)
do_install(if_digital_write)

static const emos_if_digital_t emos_if_digital = 
{
    .i.read      = do_install_if_digital_read,
    .i.write     = do_install_if_digital_write,
    .read        = (emos_if_digital_read_t)do_digital_read,
    .write       = (emos_if_digital_write_t)do_digital_write,
};

emos_if_digital_t emos_if_digital_func(uint8_t idx)
{
    this_idx = idx;
    return emos_if_digital;
}

/******************************************************************************/

static uint8_t do_digital_read( void )
{
    uint8_t idx = this_idx;
    emos_if_digital_read_t this_func = emos.digital(idx).i.read(NULL);
    ICHECK(this_func,0);
    return this_func();
}

static void do_digital_write( uint8_t val )
{
    uint8_t idx = this_idx;
    emos_if_digital_write_t this_func = emos.digital(idx).i.write(NULL);
    ICHECK(this_func,);
    return this_func(val);
}

