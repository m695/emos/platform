#include "../../kernel.h"
#include "../inc/inc_es_adc.h"

/******************************************************************************/

static uint8_t this_idx;

static uint32_t do_adc_read( void );

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_ADC_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(adc_read)

static const emos_if_adc_t emos_if_adc = 
{
    .i.read      = do_install_adc_read,
    .read        = (emos_adc_read_t)do_adc_read,
};

emos_if_adc_t emos_if_adc_func(uint8_t idx)
{
    this_idx = idx;
    return emos_if_adc;
}

/******************************************************************************/

static uint32_t do_adc_read( void )
{
    uint8_t idx = this_idx;
    emos_adc_read_t this_adc_read = emos.adc(idx).i.read(NULL);
    ICHECK(this_adc_read,0);
    return this_adc_read();
}

