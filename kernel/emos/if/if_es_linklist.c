#include "../../kernel.h"
#include "../inc/inc_es_linklist.h"

/******************************************************************************/

static void * linklist_new      ( uint8_t pool_id, void ** source, uint16_t new_datalen);
static void linklist_remove     ( uint8_t pool_id, void ** source, void *   kill_target);
static uint32_t linklist_count  ( uint8_t pool_id, void ** source);
static void * linklist_get      ( uint8_t pool_id, void ** source, uint32_t index);

const emos_if_linklist_t emos_if_linklist = 
{
    .create   = linklist_new,
    .remove   = linklist_remove,
    .count    = linklist_count,
    .get      = linklist_get,
};

/******************************************************************************/

static void * linklist_new(uint8_t pool_id,
                         void ** source,
                         uint16_t new_datalen)
{

    emos_linklist_t *new_tmp = NULL;
    emos_linklist_t *last_tmp = (emos_linklist_t*)*source;

    do
    {
        if(last_tmp == NULL || last_tmp->next == NULL)
        {
            break;
        }

        last_tmp = last_tmp->next;
    }
    while(last_tmp != NULL && last_tmp->next != NULL);

    new_tmp = (emos_linklist_t *)emos.mem(pool_id)->malloc(new_datalen);
    if( new_tmp == 0 )
    {
        return NULL;
    }
    
    if(new_tmp != NULL)
    {
        new_tmp->next           = NULL;
        if(last_tmp == NULL)
        {
            *source = new_tmp;
        }
        else
        {
            last_tmp->next = new_tmp;
        }
    }
    
    return new_tmp;
}

static void linklist_remove(uint8_t pool_id,
                            void ** source,
                            void * kill_target)
{
    emos_linklist_t *cur_tmp = (emos_linklist_t*)*source;
    emos_linklist_t *pre_tmp = NULL;

    while(cur_tmp != NULL)
    {
        if(cur_tmp == kill_target)
        {
            break;
        }

        pre_tmp = cur_tmp;
        cur_tmp = cur_tmp->next;
    }

    if(cur_tmp == NULL)
    {
        return;
    }

    if(pre_tmp != NULL)
    {
        pre_tmp->next = cur_tmp->next;
        cur_tmp->next = NULL;
    }
    else
    {
        if(cur_tmp->next != NULL)
        {
            *source = cur_tmp->next;
        }
        else
        {
            *source = NULL;
        }
    }
    
    emos.mem(pool_id)->free(cur_tmp);
    return;
}

static uint32_t linklist_count( uint8_t pool_id, void ** source)
{
    uint32_t func_count = 0;
    emos_linklist_t *last_tmp = (emos_linklist_t*)*source;

    if(last_tmp != NULL)
    {
        func_count++;
    }
    
    do
    {
        if(last_tmp == NULL || last_tmp->next == NULL)
        {
            break;
        }

        last_tmp = last_tmp->next;
        func_count++;
    }
    while(last_tmp != NULL && last_tmp->next != NULL);
    
    return func_count;
}


static void * linklist_get(uint8_t pool_id,
                           void ** source,
                           uint32_t index)
{
    uint32_t func_count = 0;
    emos_linklist_t *last_tmp = (emos_linklist_t*)*source;

    if(func_count == index)
    {
        goto FUNC_EXIT;
    }
    
    do
    {
        if(last_tmp == NULL || last_tmp->next == NULL)
        {
            break;
        }

        last_tmp = last_tmp->next;
        func_count++;
        
        if(func_count == index)
        {
            goto FUNC_EXIT;
        }
    }
    while(last_tmp != NULL && last_tmp->next != NULL);
    
    return NULL;
FUNC_EXIT:
    return last_tmp;
}




