#include "../../kernel.h"
#include "../inc/inc_es_ble.h"

/******************************************************************************/

static void do_ble_adv_start( void );
static void do_ble_stop( void );
static void do_ble_disconnect( void );
static void do_ble_uart_putc(uint8_t ch);
static void do_ble_uart_puts(const char *str , uint16_t len);
static uint8_t * do_ble_getmac( void );
static void do_ble_adv_name( char * name , uint8_t name_len );
static void do_ble_test( void );
static void do_ble_test_ck( void );
static uint8_t do_ble_ts_en(uint8_t en);
static void emos_dummy(void) {};

static const uint8_t this_idx = 0;

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_BLE_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(ble_getmac)
do_install(ble_uart_putc)
do_install(ble_uart_puts)


const emos_if_ble_t emos_if_ble = 
{
    .i.getmac    = do_install_ble_getmac,
    .i.putc      = do_install_ble_uart_putc,
    .i.puts      = do_install_ble_uart_puts,
    .adv_start   = do_ble_adv_start,
    .adv_name    = do_ble_adv_name,
    .stop        = do_ble_stop,
    .disconnect  = do_ble_disconnect,
    .send        = (void (*)(uint8_t *, uint16_t))emos_dummy,
    .uart.putc   = (emos_ble_uart_putc_t)do_ble_uart_putc, 
    .uart.puts   = (emos_ble_uart_puts_t)do_ble_uart_puts, 
    .getmac      = (emos_ble_getmac_t)do_ble_getmac,
    .test        = do_ble_test,
    .test_ck     = do_ble_test_ck,
    .ts_en       = do_ble_ts_en,
};

/******************************************************************************/


static uint8_t do_ble_ts_en(uint8_t en)
{
    static uint8_t this_en = 0;
    if( en != 0xff)
    {
        this_en = en;
    }
    return this_en;
}

static void do_ble_uart_putc(uint8_t ch)
{
    emos_ble_uart_putc_t this_putc = emos.ble->i.putc(NULL);
    
    ICHECK(this_putc, );

    this_putc(ch);
}

static void do_ble_uart_puts(const char *str , uint16_t len)
{
    emos_ble_uart_putc_t this_putc = emos.ble->i.putc(NULL);
    emos_ble_uart_puts_t this_puts = emos.ble->i.puts(NULL);

    ICHECK(this_puts, );
    ICHECK(this_putc, );

    this_puts(str, len);
    return;
}

static void do_ble_test_ck( void )
{
    emos_vsys_send(EMOS_TASK_BLE, EMOS_TASK_EVENT_BLE_TEST_CK, NULL, 0);
}

static void do_ble_adv_name( char * name , uint8_t name_len)
{
    emos_vsys_send(EMOS_TASK_BLE, EMOS_TASK_EVENT_BLE_ADV_NAME, (uint8_t *)name, name_len);
}

static void do_ble_adv_start( void )
{
    emos_vsys_send(EMOS_TASK_BLE, EMOS_TASK_EVENT_BLE_ADV_START, NULL, 0);
}

static void do_ble_stop( void )
{
    emos_vsys_send(EMOS_TASK_BLE, EMOS_TASK_EVENT_BLE_STOP, NULL, 0);
}

static void do_ble_disconnect( void )
{
    emos_vsys_send(EMOS_TASK_BLE, EMOS_TASK_EVENT_BLE_DISCONNECT, NULL, 0);
}

static void do_ble_test( void )
{
    emos_vsys_send(EMOS_TASK_BLE, EMOS_TASK_EVENT_BLE_TEST, NULL, 0);
}

static uint8_t * do_ble_getmac( void )
{
    emos_ble_getmac_t this_getmac = emos.ble->i.getmac(NULL);

    ICHECK(this_getmac, (uint8_t *)"");

    return this_getmac();
}


