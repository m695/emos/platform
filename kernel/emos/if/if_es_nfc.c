#include "../../kernel.h"
#include "../inc/inc_es_nfc.h"

static void do_nfc_test( void );
static void do_nfc_test_ck( void );
static uint8_t do_nfc_ts_en(uint8_t en);

/******************************************************************************/

const emos_if_nfc_t emos_if_nfc = 
{
    .test = do_nfc_test,
    .ts_en = do_nfc_ts_en,
    .test_ck = do_nfc_test_ck,
};

/******************************************************************************/

static void do_nfc_test( void )
{
    emos_vsys_send(EMOS_TASK_NFC,EMOS_TASK_EVENT_NFC_TEST, NULL, 0);
}

static void do_nfc_test_ck( void )
{
    emos_vsys_send(EMOS_TASK_NFC, EMOS_TASK_EVENT_NFC_TEST_CK, NULL, 0);
}

static uint8_t do_nfc_ts_en(uint8_t en)
{
    static uint8_t this_en = 0;
    if( en != 0xff)
    {
        this_en = en;
    }
    return this_en;
}




