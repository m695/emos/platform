#include "../../kernel.h"
#include "../inc/inc_es_uart.h"

/******************************************************************************/

static emos_if_uart_status_t hook_uart_status[THIS_UART_NUM];
static uint8_t this_idx;


static void do_uart_putc( uint8_t ch);
static void do_uart_puts(const char *str , uint16_t len);
static void do_uart_recv( char *buff , uint16_t len, uint32_t timeout );
static void do_uart_flush( void );
static void do_uart_qputs(const char *str , uint16_t len);
static emos_if_uart_status_t * do_uart_status(void);

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_UART_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(uart_putc)
do_install(uart_puts)
do_install(uart_recv)
do_install(uart_flush)

static const emos_if_uart_t emos_if_uart = 
{
    .i.putc = do_install_uart_putc,
    .i.puts = do_install_uart_puts,
    .i.recv = do_install_uart_recv,
    .i.flush = do_install_uart_flush,
    .putc = do_uart_putc,
    .puts = do_uart_puts,
    .recv = do_uart_recv,
    .flush = do_uart_flush,
    .qputs = do_uart_qputs,
    .sta  = do_uart_status,
};

emos_if_uart_t emos_if_uart_func(uint8_t idx)
{
    this_idx = idx;
    return emos_if_uart;
}

/******************************************************************************/
static emos_if_uart_status_t * do_uart_status(void)
{
    uint8_t idx = this_idx;
    return &hook_uart_status[idx];
}

static void do_uart_putc( uint8_t ch)
{
    uint8_t idx = this_idx;
    emos_uart_putc_t this_putc = emos.uart(idx).i.putc(NULL);

    ICHECK(this_putc,);
    
    this_putc(ch);
}

static void do_uart_qputs(const char *str , uint16_t len)
{
    uint8_t idx = this_idx;

    emos_vsys_mrsend( EMOS_TASK_UART, EMOS_TASK_EVENT_UART_QTX, &idx, 1,(uint8_t *)str, len);
}

static void do_uart_puts(const char *str , uint16_t len)
{
    uint8_t idx = this_idx;
    // emos_uart_putc_t this_putc = emos.uart(idx).i.putc(NULL);
    emos_uart_puts_t this_puts = emos.uart(idx).i.puts(NULL);

    ICHECK(this_puts,);
    this_puts(str, len);
}

static void do_uart_recv( char *buff , uint16_t len, uint32_t timeout )
{
    uint8_t idx = this_idx;
    emos_uart_recv_t this_func = emos.uart(idx).i.recv(NULL);

    ICHECK(this_func,);
    this_func(buff, len, timeout);
}

static void do_uart_flush( void )
{
    uint8_t idx = this_idx;
    emos_uart_flush_t this_flush = emos.uart(idx).i.flush(NULL);

    ICHECK(this_flush,);

    this_flush();
}

