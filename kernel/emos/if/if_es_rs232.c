#include "../../kernel.h"
#include "../inc/inc_es_rs232.h"

/******************************************************************************/
static uint8_t this_idx;
static uint8_t do_rs232_conf( uint32_t baudrate, uint8_t databit, uint8_t stopbit, int8_t parity );
static uint8_t do_rs232_deconf( void );
static uint8_t do_rs232_write( uint8_t * mcmd, int mlen);
static uint8_t do_rs232_read( uint8_t *pbuf, int blen, uint16_t timeout );

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_RS232_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(rs232_conf)
do_install(rs232_deconf)
do_install(rs232_write)
do_install(rs232_read)

static const emos_if_rs232_t emos_if_rs232 = 
{
    .i.conf      = do_install_rs232_conf,
    .i.deconf    = do_install_rs232_deconf,
    .i.write     = do_install_rs232_write, 
    .i.read      = do_install_rs232_read,
    .conf        = (emos_rs232_conf_t)do_rs232_conf, 
    .deconf      = (emos_rs232_deconf_t)do_rs232_deconf,
    .write       = (emos_rs232_write_t)do_rs232_write, 
    .read        = (emos_rs232_read_t)do_rs232_read,
};

emos_if_rs232_t * emos_if_rs232_func(uint8_t idx)
{
    this_idx = idx;
    return (emos_if_rs232_t *)&emos_if_rs232;
}

/******************************************************************************/
static uint8_t do_rs232_conf(uint32_t baudrate, uint8_t databit, uint8_t stopbit, int8_t parity)
{
    uint8_t idx = this_idx;
    emos_rs232_conf_t this_rs232_conf = emos.rs232(idx)->i.conf(NULL);
    ICHECK(this_rs232_conf, 0);
    this_rs232_conf(baudrate, databit, stopbit, parity);
    return EMOS_OK;
}

static uint8_t do_rs232_deconf( void )
{
    uint8_t idx = this_idx;
    emos_rs232_deconf_t this_rs232_deconf = emos.rs232(idx)->i.deconf(NULL);
    ICHECK(this_rs232_deconf, 0);
    return this_rs232_deconf();
}

static uint8_t do_rs232_write(uint8_t * mcmd, int mlen)
{
    uint8_t idx = this_idx;
    emos_rs232_write_t this_rs232_write = emos.rs232(idx)->i.write(NULL);
    ICHECK(this_rs232_write, 0);
    return this_rs232_write(mcmd, mlen);
}

static uint8_t do_rs232_read( uint8_t *pbuf, int blen, uint16_t timeout )
{
    uint8_t idx = this_idx;
    emos_rs232_read_t this_rs232_read = emos.rs232(idx)->i.read(NULL);
    ICHECK(this_rs232_read, 0);
    return this_rs232_read(pbuf,blen,timeout);
}
