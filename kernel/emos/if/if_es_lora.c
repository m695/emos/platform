#include "../../kernel.h"
#include "../inc/inc_es_lora.h"

/******************************************************************************/

static emos_hal_lora_ctrl_t this_ctrl;

static void do_lora_join( void );
static void do_lora_uplink(uint8_t fport, const char *str , uint16_t len);
static void do_lora_test( uint32_t );
static uint8_t do_lora_ts_en(uint8_t en);
static void do_lora_test_ck( void );
static int16_t do_lora_get_band ( void );
static uint8_t do_lora_set_band ( uint8_t band);
static int16_t do_lora_get_nwm ( void );
static uint8_t do_lora_set_nwm ( uint8_t nwm);
static int16_t do_lora_get_class ( void );
static uint8_t do_lora_set_class ( uint8_t nwm);
static uint8_t do_lora_get_datarate ( void );
static uint8_t do_lora_set_datarate ( uint8_t datarate );
static uint8_t do_lora_njs ( void );



static int16_t do_lora_get_cfm ( void );
static uint8_t do_lora_set_cfm ( uint8_t cfm);

static uint8_t do_lora_get_autodatarate ( void );
static uint8_t do_lora_set_autodatarate ( uint8_t enable );

static uint8_t do_lora_get_deveui ( uint8_t *eui);
static uint8_t do_lora_set_deveui ( uint8_t *eui);
static uint8_t do_lora_get_appeui ( uint8_t *eui);
static uint8_t do_lora_set_appeui ( uint8_t *eui);
static uint8_t do_lora_get_appkey ( uint8_t *eui);
static uint8_t do_lora_set_appkey ( uint8_t *eui);
static void do_lora_reset( void );
static void do_lora_kreset( void );

static const uint8_t this_idx = 0;

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_LORA_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(lora_get_band)
do_install(lora_set_band)
do_install(lora_get_nwm)
do_install(lora_set_nwm)
do_install(lora_get_cfm)
do_install(lora_set_cfm)
do_install(lora_get_dr)
do_install(lora_set_dr)
do_install(lora_get_adr)
do_install(lora_set_adr)
do_install(lora_njs)
do_install(lora_get_class)
do_install(lora_set_class)

do_install(lora_set_deveui)
do_install(lora_get_deveui)
do_install(lora_set_appeui)
do_install(lora_get_appeui)
do_install(lora_set_appkey)
do_install(lora_get_appkey)

const emos_if_lora_t emos_if_lora = 
{
    .ctrl = &this_ctrl,
    
    .i.get_njs = do_install_lora_njs,
    .i.get_band = do_install_lora_get_band,
    .i.set_band = do_install_lora_set_band,
    .i.get_adr = do_install_lora_get_adr,
    .i.set_adr = do_install_lora_set_adr,
    .i.get_dr = do_install_lora_get_dr,
    .i.set_dr = do_install_lora_set_dr,
    .i.get_nwm = do_install_lora_get_nwm,
    .i.set_nwm = do_install_lora_set_nwm,
    .i.get_cfm = do_install_lora_get_cfm,
    .i.set_cfm = do_install_lora_set_cfm,
    .i.get_class = do_install_lora_get_class,
    .i.set_class = do_install_lora_set_class,
    .i.get_appeui = do_install_lora_get_appeui,
    .i.set_appeui = do_install_lora_set_appeui,
    .i.get_deveui = do_install_lora_get_deveui,
    .i.set_deveui = do_install_lora_set_deveui,
    .i.get_appkey = do_install_lora_get_appkey,
    .i.set_appkey = do_install_lora_set_appkey,
    .get_cfm = do_lora_get_cfm,
    .set_cfm = do_lora_set_cfm,
    .get_appeui = do_lora_get_appeui,
    .set_appeui = do_lora_set_appeui,
    .get_deveui = do_lora_get_deveui,
    .set_deveui = do_lora_set_deveui,
    .get_appkey = do_lora_get_appkey,
    .set_appkey = do_lora_set_appkey,
    .get_band = do_lora_get_band,
    .set_band = do_lora_set_band,
    .get_nwm = do_lora_get_nwm,
    .set_nwm = do_lora_set_nwm,
    .get_class = do_lora_get_class,
    .set_class = do_lora_set_class,
    .get_dr = do_lora_get_datarate,
    .set_dr = do_lora_set_datarate,
    .get_adr = do_lora_get_autodatarate,
    .set_adr = do_lora_set_autodatarate,
    .get_njs = do_lora_njs,

    .kreset = do_lora_kreset,
    .reset = do_lora_reset,
    .uplink = do_lora_uplink,
    .join = do_lora_join,
    .test = do_lora_test,
    .ts_en = do_lora_ts_en,
    .test_ck = do_lora_test_ck,
};

/******************************************************************************/

static void do_lora_test_ck( void )
{
    emos_vsys_send(EMOS_TASK_LORA, EMOS_TASK_EVENT_LORA_TEST_CK, NULL, 0);
}

static uint8_t do_lora_ts_en(uint8_t en)
{
    static uint8_t this_en = 0;
    if( en != 0xff)
    {
        this_en = en;
    }
    return this_en;
}

static uint8_t do_lora_get_deveui ( uint8_t *eui)
{
    emos_lora_get_deveui_t this_func = emos.lora->i.get_deveui(NULL);
    ICHECK(this_func,0);
    return this_func(eui);
}

static uint8_t do_lora_set_deveui ( uint8_t *eui)
{
    emos_lora_set_deveui_t this_func = emos.lora->i.set_deveui(NULL);
    ICHECK(this_func,0);
    return this_func(eui);
}

static uint8_t do_lora_get_appeui ( uint8_t *eui)
{
    emos_lora_get_appeui_t this_func = emos.lora->i.get_appeui(NULL);
    ICHECK(this_func,0);
    return this_func(eui);
}

static uint8_t do_lora_set_appeui ( uint8_t *eui)
{
    emos_lora_set_appeui_t this_func = emos.lora->i.set_appeui(NULL);
    ICHECK(this_func,0);
    return this_func(eui);
}

static uint8_t do_lora_get_appkey ( uint8_t *key)
{
    emos_lora_get_appeui_t this_func = emos.lora->i.get_appkey(NULL);
    ICHECK(this_func,0);
    return this_func(key);
}

static uint8_t do_lora_set_appkey ( uint8_t *key)
{
    emos_lora_set_appeui_t this_func = emos.lora->i.set_appkey(NULL);
    ICHECK(this_func,0);
    return this_func(key);
}

static int16_t do_lora_get_band ( void )
{
    emos_lora_get_band_t this_get_band = emos.lora->i.get_band(NULL);
    ICHECK(this_get_band,0);
    return this_get_band();
}

static uint8_t do_lora_set_band ( uint8_t band)
{
    emos_lora_set_band_t this_set_band = emos.lora->i.set_band(NULL);
    ICHECK(this_set_band,0);
    return this_set_band(band);
}

static int16_t do_lora_get_nwm ( void )
{
    emos_lora_get_nwm_t this_get_nwm = emos.lora->i.get_nwm(NULL);
    ICHECK(this_get_nwm,0);
    return this_get_nwm();
}

static uint8_t do_lora_set_nwm ( uint8_t nwm)
{
    emos_lora_set_nwm_t this_set_nwm = emos.lora->i.set_nwm(NULL);
    ICHECK(this_set_nwm,0);
    return this_set_nwm(nwm);
}

static int16_t do_lora_get_cfm ( void )
{
    emos_lora_get_cfm_t this_get_cfm = emos.lora->i.get_cfm(NULL);
    ICHECK(this_get_cfm,0);
    return this_get_cfm();
}

static uint8_t do_lora_set_cfm ( uint8_t cfm)
{
    emos_lora_set_cfm_t this_set_cfm = emos.lora->i.set_cfm(NULL);
    ICHECK(this_set_cfm,0);
    return this_set_cfm(cfm);
}

static int16_t do_lora_get_class ( void )
{
    emos_lora_get_class_t this_class = emos.lora->i.get_class(NULL);
    ICHECK(this_class,0);
    return this_class();
}

static uint8_t do_lora_set_class ( uint8_t in_class)
{
    emos_lora_set_class_t this_class = emos.lora->i.set_class(NULL);
    ICHECK(this_class,0);
    return this_class(in_class);
}

static uint8_t do_lora_get_autodatarate ( void )
{
    emos_lora_get_adr_t this_get_adr = emos.lora->i.get_adr(NULL);
    ICHECK(this_get_adr,0);
    return this_get_adr();
}

static uint8_t do_lora_set_autodatarate ( uint8_t enable )
{
    emos_lora_set_adr_t this_set_adr = emos.lora->i.set_adr(NULL);
    ICHECK(this_set_adr,0);
    return this_set_adr(enable);
}

static uint8_t do_lora_get_datarate ( void )
{
    emos_lora_get_dr_t this_get_dr = emos.lora->i.get_dr(NULL);
    ICHECK(this_get_dr,0);
    return this_get_dr();
}

static uint8_t do_lora_set_datarate ( uint8_t datarate )
{
    emos_lora_set_dr_t this_set_dr = emos.lora->i.set_dr(NULL);
    ICHECK(this_set_dr,0);
    return this_set_dr(datarate);
}

static uint8_t do_lora_njs ( void )
{
    emos_lora_njs_t this_get_njs = emos.lora->i.get_njs(NULL);
    ICHECK(this_get_njs,0);
    return this_get_njs();
}

static void do_lora_uplink(uint8_t fport, const char *str , uint16_t len)
{
    emos_vsys_mrsend( EMOS_TASK_LORA ,EMOS_TASK_EVENT_LORA_UPLINK, &fport,sizeof(fport), str, len);
}

static void do_lora_kreset( void )
{
    emos_vsys_send(EMOS_TASK_LORA,EMOS_TASK_EVENT_LORA_KRESET, NULL, 0);
}

static void do_lora_reset( void )
{
    emos_vsys_send(EMOS_TASK_LORA,EMOS_TASK_EVENT_LORA_RESET, NULL, 0);
}

static void do_lora_join( void )
{
    emos_vsys_send(EMOS_TASK_LORA,EMOS_TASK_EVENT_LORA_JOIN, NULL, 0);
}

static void do_lora_test( uint32_t config )
{
    emos_vsys_send(EMOS_TASK_LORA,EMOS_TASK_EVENT_LORA_TEST, &config, sizeof(config));
}


