#include "../../kernel.h"
#include "../inc/inc_es_rs485.h"

/******************************************************************************/

static uint8_t this_idx;

static uint8_t do_rs485_conf( uint32_t baudrate, uint8_t databit, uint8_t stopbit, int8_t parity );
static uint8_t do_rs485_deconf(void);
static uint8_t do_rs485_write( uint8_t * mcmd, int mlen);
static uint8_t do_rs485_read( uint8_t *pbuf, int blen, uint16_t timeout );

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_RS485_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(rs485_conf)
do_install(rs485_deconf)
do_install(rs485_write)
do_install(rs485_read)


static const emos_if_rs485_t emos_if_rs485 = 
{
    .i.conf      = do_install_rs485_conf,
    .i.deconf    = do_install_rs485_deconf,
    .i.write     = do_install_rs485_write, 
    .i.read      = do_install_rs485_read,
    .conf        = (emos_rs485_conf_t)do_rs485_conf, 
    .deconf      = (emos_rs485_deconf_t)do_rs485_deconf,
    .write       = (emos_rs485_write_t)do_rs485_write, 
    .read        = (emos_rs485_read_t)do_rs485_read,
};

emos_if_rs485_t * emos_if_rs485_func(uint8_t idx)
{
    this_idx = idx;
    return (emos_if_rs485_t *)&emos_if_rs485;
}

/******************************************************************************/
static uint8_t do_rs485_conf(uint32_t baudrate, uint8_t databit, uint8_t stopbit, int8_t parity)
{
    uint8_t idx = this_idx;
    emos_rs485_conf_t this_rs485_conf = emos.rs485(idx)->i.conf(NULL);
    this_rs485_conf(baudrate, databit, stopbit, parity);
    ICHECK(this_rs485_conf, 0);
    return EMOS_OK;
}

static uint8_t do_rs485_deconf(void)
{
    uint8_t idx = this_idx;
    emos_rs485_deconf_t this_rs485_deconf = emos.rs485(idx)->i.deconf(NULL);
    ICHECK(this_rs485_deconf, 0);
    return this_rs485_deconf();
}

static uint8_t do_rs485_write(uint8_t * mcmd, int mlen)
{
    uint8_t idx = this_idx;
    emos_rs485_write_t this_rs485_write = emos.rs485(idx)->i.write(NULL);
    ICHECK(this_rs485_write, 0);
    return this_rs485_write(mcmd, mlen);
}

static uint8_t do_rs485_read( uint8_t *pbuf, int blen, uint16_t timeout )
{
    uint8_t idx = this_idx;
    emos_rs485_read_t this_rs485_read = emos.rs485(idx)->i.read(NULL);
    ICHECK(this_rs485_read, 0);
    return this_rs485_read(pbuf,blen,timeout);
}

