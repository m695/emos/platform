#include "../../kernel.h"
#include "../inc/inc_es_gpio.h"

/******************************************************************************/


static emos_gpio_dir_t hook_gpio_dir;
static emos_gpio_set_t hook_gpio_set;
static emos_gpio_get_t hook_gpio_get;
static emos_gpio_pull_t hook_gpio_pull;

static struct
{
    void *next; 
} this_param_root;

static void do_gpio_dir_input(uint8_t group, uint8_t pin, uint8_t intr);
static void do_gpio_dir_output(uint8_t group, uint8_t pin);
static void do_gpio_set_high(uint8_t group, uint8_t pin);
static void do_gpio_set_low(uint8_t group, uint8_t pin);
static uint8_t do_gpio_get(uint8_t group, uint8_t pin );
static void do_gpio_set_toggle(uint8_t group, uint8_t pin);
static void do_gpio_input_cfg(uint8_t group, uint8_t pin, uint8_t pull);

const emos_if_gpio_t emos_if_gpio = 
{
    .addr  = (uint32_t *)&this_param_root,
    .i_dir = (emos_gpio_dir_t)&hook_gpio_dir,
    .i_get = (emos_gpio_get_t)&hook_gpio_get,
    .i_set = (emos_gpio_set_t)&hook_gpio_set,
    .i_cfg_pull = (emos_gpio_pull_t)&hook_gpio_pull,
    .o_dir_input = do_gpio_dir_input,
    .o_dir_output = do_gpio_dir_output,
    .o_get = do_gpio_get,
    .o_phigh = do_gpio_set_high,
    .o_plow = do_gpio_set_low,
    .o_toggle = do_gpio_set_toggle,
    .o_cfg_pull = do_gpio_input_cfg,
};

/******************************************************************************/


static uint8_t func_gpio_count( void ) 
{
    uint32_t cnt = 0;
    uint32_t adr = (uint32_t)&this_param_root;
    cnt = emos.llist->count( 0, (void **)adr);
    return (uint8_t)cnt;
}

static uint8_t func_gpio_get( emos_if_gpio_param_t **data, uint8_t group, uint8_t pin)
{
    uint32_t cnt = 0;

    uint32_t adr = (uint32_t)&this_param_root;
    emos_if_gpio_note_t *walk_gpio;
    cnt = func_gpio_count();
    for(uint8_t i=0; i< cnt; i++)
    {
        walk_gpio = emos.llist->get( 0, (void **)adr, i);
        if(walk_gpio->info.group == group && walk_gpio->info.pin == pin)
        {
            *data = &(walk_gpio->info);
            return EMOS_OK;
        }
    }
    return EMOS_ERROR;
}

static emos_if_gpio_note_t* func_gpio_fget( uint8_t group, uint8_t pin)
{
    uint8_t ret = EMOS_ERROR;
    emos_if_gpio_note_t *this_gpio;
    uint32_t adr = (uint32_t)&this_param_root;
    ret = func_gpio_get((emos_if_gpio_param_t **)&this_gpio,group,pin);
    if (ret != EMOS_OK)
    {
        this_gpio = (emos_if_gpio_note_t *)emos.llist->create( COM_PARAM_POOL_ID, (void **)adr, sizeof(emos_if_gpio_note_t));
        memset(this_gpio,0,sizeof(emos_if_gpio_note_t));
        this_gpio->info.group = group;
        this_gpio->info.pin = pin;
    }
    return this_gpio;
}

static void do_gpio_dir_cfg(uint8_t group, uint8_t pin, uint8_t dir)
{
    emos_gpio_dir_t this_dir = *(emos_gpio_dir_t *)(emos.gpio->i_dir);

    if(this_dir == NULL)
    {
        return;
    }
    
    if ((group + pin) == 0 )
    {
        return;
    }
    this_dir(group,pin,dir);
}

static void do_gpio_dir_output(uint8_t group, uint8_t pin)
{
    emos_if_gpio_note_t *find_gpio;
    find_gpio = func_gpio_fget(group,pin);
    find_gpio->info.dir = 1;

    do_gpio_dir_cfg(group,pin,1);
}

static void do_gpio_dir_input(uint8_t group, uint8_t pin, uint8_t intr)
{
    emos_if_gpio_note_t *find_gpio;
    find_gpio = func_gpio_fget(group,pin);
    find_gpio->info.dir = 0;
    find_gpio->info.intr = (intr>0)?1:0;

    do_gpio_dir_cfg(group,pin,0);
}

static void do_gpio_set_cfg(uint8_t group, uint8_t pin, uint8_t set)
{
    emos_gpio_set_t this_set = *(emos_gpio_set_t *)(emos.gpio->i_set);

    if(this_set == NULL)
    {
        return;
    }

    if ((group + pin) == 0 )
    {
        return;
    }
    

    this_set(group,pin,set);
}

static void do_gpio_set_high(uint8_t group, uint8_t pin)
{
    do_gpio_set_cfg(group,pin,1);
}

static void do_gpio_set_low(uint8_t group, uint8_t pin)
{
    do_gpio_set_cfg(group,pin,0);
}

static uint8_t do_gpio_get(uint8_t group, uint8_t pin )
{
    emos_gpio_get_t this_get = *(emos_gpio_get_t *)(emos.gpio->i_get);
    // emos_if_gpio_note_t *find_gpio;

    if(this_get == NULL)
    {
        return 0;
    }

    uint8_t ret = this_get(group,pin);
    return ret;
}

static void do_gpio_set_toggle(uint8_t group, uint8_t pin)
{
    uint8_t ss = do_gpio_get(group,pin);
    ss = !ss;
    do_gpio_set_cfg(group,pin,ss);
}

static void do_gpio_input_cfg(uint8_t group, uint8_t pin, uint8_t pull)
{
    emos_if_gpio_note_t *find_gpio;
    emos_gpio_dir_t this_dir = *(emos_gpio_pull_t *)(emos.gpio->i_cfg_pull);
    find_gpio = func_gpio_fget(group,pin);
    find_gpio->info.dir = 0;
    find_gpio->info.intr = 0;

    if(this_dir == NULL)
    {
        return;
    }

    this_dir(group,pin,pull);
}