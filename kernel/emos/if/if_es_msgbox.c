#include "../../kernel.h"
#include "../inc/inc_es_msgbox.h"

typedef struct
{
    void *next;  
    emos_task_msg_t            msg;  
} __EMOS_PACKED msgbox_list_t;

typedef struct
{
    msgbox_list_t * msgbox_list[TASK_PRIORITY_NUM];
} __EMOS_PACKED msgbox_group_t;


/******************************************************************************/

static msgbox_group_t msgbox_group[THIS_MSGBOX_NUM];
static emos_task_entry_t  task_tbl[THIS_MSGBOX_NUM][MSGBOX_TASK_NUM];
static uint8_t this_task_count[THIS_MSGBOX_NUM];
static emos_if_msgbox_ctrl_t emos_if_msgbox_ctrl;

static uint8_t this_idx;
static uint8_t this_reset_flag = 0;

/******************************************************************************/

static void msgbox_init( uint8_t *pool_addr, uint16_t pool_size);
static uint8_t msgbox_polling( void );
static uint8_t msgbox_send(uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len);
static void msgbox_task_add( emos_task_entry_t * new_task);
static uint8_t msgbox_send_more( uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len, uint8_t * more, uint32_t more_len);
static uint8_t msgbox_count( void );
static void msgbox_sleep(uint8_t task_id, uint8_t onoff);
static void msgbox_destroy( uint8_t sel_task_id );
static void msgbox_timeout(uint8_t task_id, uint32_t timeout);
static uint16_t msgbox_task_count( uint8_t sel_task_id );


static const emos_if_msgbox_t emos_if_msgbox = 
{            
    .ctrl      = &emos_if_msgbox_ctrl,                  
    .init      = msgbox_init,     
    .polling   = msgbox_polling,  
    .send      = msgbox_send,     
    .mrsend    = msgbox_send_more,  
    .task_add  = msgbox_task_add,     
    .count     = msgbox_count,     
    .sleep     = msgbox_sleep,
    .destroy   = msgbox_destroy,
    .timeout   = msgbox_timeout,
    .taskcnt   = msgbox_task_count,
};

emos_if_msgbox_t * emos_if_msgbox_func(uint8_t idx)
{
    this_idx = idx;
    return (emos_if_msgbox_t *)&emos_if_msgbox;
}

/******************************************************************************/

static void msgbox_task_add(emos_task_entry_t * new_task)
{
    uint8_t pool_id = this_idx;
    uint8_t *task_count = &this_task_count[pool_id];
    if(! (*task_count < MSGBOX_TASK_NUM))
    {
        return;
    }
    
    task_tbl[pool_id][*task_count].mode = new_task->mode;
    task_tbl[pool_id][*task_count].timeout = 0;
    task_tbl[pool_id][*task_count].priority = new_task->priority;
    task_tbl[pool_id][*task_count].task_entry = new_task->task_entry;
    *task_count = *task_count + 1;
}

static void msgbox_init(uint8_t *pool_addr, uint16_t pool_size)
{
    uint8_t pool_id = this_idx;
    uint8_t *task_count = &this_task_count[pool_id];
    *task_count = 0;

    emos.mem(pool_id)->init( pool_addr, pool_size);
    memset(&task_tbl[pool_id],0, sizeof(task_tbl[pool_id]));
    memset(&msgbox_group[pool_id],0, sizeof(msgbox_group[pool_id]));
    
    msg_type_t msg_type;
    emos_msg_type_t *p_msg_type = (emos_msg_type_t *)&msg_type;
    p_msg_type->msg_id = 0x00;
    p_msg_type->sys_id = TASK_MODE_INIT;
    for( uint8_t task_id=0; task_id<MSGBOX_TASK_NUM; task_id++)
    {
        msgbox_send( task_id, 0, msg_type, NULL, 0);
    }
}

static void msgbox_timeout(uint8_t task_id, uint32_t timeout)
{
    uint8_t pool_id = this_idx;
    
    task_tbl[pool_id][task_id].timeout = timeout;
    uint32_t in = -1;
    msgbox_send(task_id, 0, in, NULL, 0);
        
}

static void msgbox_sleep(uint8_t task_id, uint8_t onoff)
{
    uint8_t pool_id = this_idx;
    
    switch (onoff)
    {
        case IENABLE:
            task_tbl[pool_id][task_id].mode |= TASK_MODE_SLEEP;
            
            do
            {
                uint32_t in = -1;
                msgbox_send(task_id, 0, in, NULL, 0);
            } while (0);
            
            break;
        default:
        case IDISABLE:
            task_tbl[pool_id][task_id].mode &= ~TASK_MODE_SLEEP;
            this_reset_flag = 1;
            do
            {
                emos_task_msg_t sub_msg;
                emos_task_entry_t *this_task;
                emos_msg_type_t *p_msg_type;
                p_msg_type = (emos_msg_type_t *)&(sub_msg.msg_type);
                p_msg_type->sys_id = TASK_MODE_WAKEUP;
                
                this_task = &task_tbl[pool_id][task_id];
                this_task->task_entry(&sub_msg);
            }while(0);

            break;
    }

    if ((task_tbl[pool_id][task_id].mode & TASK_MODE_SLEEP) == TASK_MODE_SLEEP)
    {
        /* message */
    }
}

static inline uint8_t msgbox_send_more(uint8_t task_id, uint8_t src_id, msg_type_t msg_type, uint8_t * msg, uint32_t len, uint8_t * more, uint32_t more_len)
{
    uint8_t pool_id = this_idx;
    static uint8_t this_lock = 0;
    if( this_lock == 1)
    {
        return EMOS_ERROR;
    }

    this_lock = 1;
    uint8_t task_priority = TASK_PRIORITY_NUM;
    uint8_t task_mode = 0;
    msgbox_list_t * new_task;
    emos_msg_type_t *p_msg_type = (emos_msg_type_t *)&msg_type;

    task_priority = task_tbl[pool_id][task_id].priority;
    task_mode = task_tbl[pool_id][task_id].mode;
    UNUSED(task_mode);
    
    if(! (task_id < MSGBOX_TASK_NUM))
    {
        goto FUNC_EXIT;
    }

    uint32_t adr = (uint32_t)&msgbox_group[pool_id].msgbox_list[task_priority];
    new_task = emos.llist->create(pool_id,
                                   (void **)adr,
                                   (sizeof(msgbox_list_t) + len + more_len ));
    if( new_task == NULL )
    {
        return EMOS_ERROR;
    }

    new_task->msg.src_id = src_id;
    new_task->msg.task_id = task_id;      
    if (p_msg_type->sys_id == 0)
    {
        p_msg_type->sys_id = TASK_MODE_EVENT;
    }

    new_task->msg.msg_type = p_msg_type->value;
    new_task->msg.payload_len = len + more_len;
    
    if( (msg != NULL) && (len != 0))
    {
        memcpy(new_task->msg.payload, msg, len);
    }

    if( (more != NULL) && (more_len != 0))
    {
        memcpy(new_task->msg.payload + len, more, more_len);
    }
FUNC_EXIT:
    this_lock = 0;
    return EMOS_OK;
}

static inline uint8_t msgbox_send(uint8_t task_id, uint8_t src_id, msg_type_t msg_type, uint8_t * msg, uint32_t len)
{
    return msgbox_send_more( task_id, src_id, msg_type, msg, len, NULL, 0);
}

static uint8_t msgbox_count( void )
{
    uint8_t pool_id = this_idx;
    uint8_t task_priority = 0;
    uint8_t msg_cnt = 0;
    do
    {
        uint32_t adr = (uint32_t)&msgbox_group[pool_id].msgbox_list[task_priority];
        msg_cnt += emos.llist->count(pool_id,(void **)adr);
        task_priority++;
    }while(task_priority < TASK_PRIORITY_NUM );
    return msg_cnt;
}

static void msgbox_destroy( uint8_t sel_task_id )
{
}

static uint16_t msgbox_task_count( uint8_t sel_task_id )
{
    uint8_t pool_id = this_idx;
    uint8_t task_priority = 0;
    uint8_t msg_cnt = 0;
    emos_task_entry_t *this_task;
    msgbox_list_t *p;
    do
    {
        uint32_t adr = (uint32_t)&msgbox_group[pool_id].msgbox_list[task_priority];
        uint8_t listcnt = emos.llist->count(pool_id,(void **)adr);
        
        for (size_t i = 0; i < listcnt; i++)
        {
            p = (msgbox_list_t *)emos.llist->get(pool_id,(void **)adr,i);
            if (p->msg.task_id == sel_task_id)
            {
                msg_cnt++;
            }
        }
        
        task_priority++;
    }while(task_priority < TASK_PRIORITY_NUM );

    return msg_cnt;
}

static uint8_t msgbox_polling( void )
{
    emos_task_msg_t sub_msg;
    uint8_t pool_id = this_idx;
    uint8_t task_priority = 0;
    uint8_t this_task_id = 0;
    uint8_t ret = EMOS_MSGBOX_NO_TASK;
    uint8_t *task_count = &this_task_count[pool_id];
    emos_task_entry_t *this_task;
    emos_msg_type_t *p_msg_type;
    static uint32_t time_last[4];

    sub_msg.payload_len = 0;
    do
    {
        while(msgbox_group[pool_id].msgbox_list[task_priority] != NULL)
        {
            msgbox_list_t * msgbox_mission = NULL;
            msgbox_mission = msgbox_group[pool_id].msgbox_list[task_priority];
            
            do
            {
                this_task_id = msgbox_mission->msg.task_id;
                
                /* sleep field */
                if ((task_tbl[pool_id][this_task_id].mode & TASK_MODE_SLEEP) == TASK_MODE_SLEEP)
                {
                    p_msg_type = (emos_msg_type_t *)&(sub_msg.msg_type);
                    p_msg_type->sys_id = TASK_MODE_SLEEP;
                    
                    this_task = &task_tbl[pool_id][this_task_id];
                    this_task->task_entry(&sub_msg);


                    /* run next task*/
                    msgbox_mission = msgbox_mission->next;
                    if( msgbox_mission == NULL)
                    {
                        ret = EMOS_MSGBOX_SLEEP_AND_NO_TASK;
                        goto EXIT_LOOP;
                    }

                    if (this_reset_flag == 1)
                    {
                        this_reset_flag = 0;
                        ret = EMOS_MSGBOX_CONTINUE;
                        goto EXIT_EVENT;
                    }
                    
                    continue;
                }
                break;
            } while (1);
            
            p_msg_type = (emos_msg_type_t *)&msgbox_mission->msg.msg_type;
            
            this_task = &task_tbl[pool_id][this_task_id];
            
            /* event field */
            if(this_task->task_entry != NULL)
            {
                emos_task_msg_t *p_msg = &msgbox_mission->msg;
                if (setjmp(*(emos.svpnt)) == 0)
                {
                    if ((this_task->mode & TASK_MODE_EVENT) == TASK_MODE_EVENT)
                    {
                        this_task->task_entry(p_msg);
                    }
                }
                else
                {
                    DBG_ALL("+EVT:NO MEMORY:%02x,%02x\r\n",p_msg->task_id,p_msg->msg_type);
                    /* no memory leave task */
                }
            }
            
            uint32_t adr = (uint32_t)&msgbox_group[pool_id].msgbox_list[task_priority];
            
            emos.llist->remove(pool_id,
                              (void **)adr,
                              msgbox_mission);   
            ret = EMOS_MSGBOX_CONTINUE;
            if ((p_msg_type->sys_id & TASK_MODE_INIT) == TASK_MODE_INIT)
            {
                goto EXIT_FUNC;
            }
            else
            {
                goto EXIT_EVENT;
            }
        }
EXIT_LOOP:
        task_priority++;
    }while(task_priority < TASK_PRIORITY_NUM );
EXIT_EVENT:

    p_msg_type = (emos_msg_type_t *)&(sub_msg.msg_type);
    /* timeout field */
    p_msg_type->sys_id = TASK_MODE_TIMEOUT;
    
    if (emos_if_msgbox_ctrl.time_tick > time_last[this_idx])
    {
        do
        {
            uint32_t less_time = emos_if_msgbox_ctrl.time_tick - time_last[this_idx];

            for( uint8_t task_id=0; task_id<*task_count; task_id++)
            {
                this_task = &task_tbl[this_idx][task_id];
                
                do
                {
                    if ( this_task->task_entry == NULL )
                    {
                        break;
                    }

                    if ( this_task->timeout == 0 )
                    {
                        break;
                    }

                    if ( less_time >= this_task->timeout )
                    {
                        this_task->timeout = 0;
                        this_task->task_entry(&sub_msg);
                        emos.vsys->ctrl->psm_do = IDISABLE;
                        ret = EMOS_MSGBOX_WAIT_AND_NO_TASK;
                        break;
                    }
                    
                    this_task->timeout -= less_time;
                    ret = EMOS_MSGBOX_WAIT_AND_NO_TASK;
                    emos_delay(1); //difference timestemp
                    //DBG_INFO("<%d %d>",this_task->timeout, less_time);
                } while (0);
            }
        } while (0);
    }
    time_last[this_idx] = emos_if_msgbox_ctrl.time_tick;

    /* polling field */
    p_msg_type->sys_id = TASK_MODE_POLLING;
    for( uint8_t task_id=0; task_id<*task_count; task_id++)
    {
        this_task = &task_tbl[this_idx][task_id];
        if ((this_task->mode & TASK_MODE_POLLING) == TASK_MODE_POLLING)
        {
            if(this_task->task_entry!=NULL )
            {
                this_task->task_entry(&sub_msg);
            }
        }
    }
EXIT_FUNC:
    return ret;
}













