#include "../../kernel.h"
#include "../inc/inc_es_i2c.h"

/******************************************************************************/

static uint8_t this_idx;

static uint8_t do_i2c_write( uint16_t caddr, uint16_t raddr, uint8_t *data, uint16_t size );
static uint8_t do_i2c_con_write( uint16_t caddr, uint16_t raddr, uint8_t *data, uint16_t size );
static uint8_t do_i2c_read( uint16_t caddr, uint16_t raddr, uint8_t *data, uint16_t size );
static uint8_t do_i2c_con_read( uint16_t caddr, uint16_t raddr, uint8_t *data, uint16_t size );
static uint8_t do_i2c_swrite( uint16_t caddr, uint8_t *data, uint16_t size );
static uint8_t do_i2c_sread( uint16_t caddr, uint8_t *data, uint16_t size );

#define do_install(X)    \
__EMOS_NOINLINE static void* do_install_##X(void *adr) \
{ \
    static emos_##X##_t hookp[THIS_I2C_NUM]; \
    if ( adr != NULL ) hookp[this_idx] = adr; \
    return hookp[this_idx]; \
}

do_install(i2c_write)
do_install(i2c_con_write)
do_install(i2c_read)
do_install(i2c_con_read)
do_install(i2c_swrite)
do_install(i2c_sread)

static const emos_if_i2c_t emos_if_i2c = 
{
    .i.write     = do_install_i2c_write, 
    .i.con_write = do_install_i2c_con_write,
    .i.read      = do_install_i2c_read,
    .i.con_read  = do_install_i2c_con_read,
    .i.swrite     = do_install_i2c_swrite, 
    .i.sread      = do_install_i2c_sread,
    .swrite       = (emos_i2c_swrite_t)do_i2c_swrite, 
    .sread        = (emos_i2c_sread_t)do_i2c_sread,
    .write       = (emos_i2c_write_t)do_i2c_write, 
    .read        = (emos_i2c_read_t)do_i2c_read,
    .con_write   = (emos_i2c_con_write_t)do_i2c_con_write, 
    .con_read    = (emos_i2c_con_read_t)do_i2c_con_read,
};

emos_if_i2c_t * emos_if_i2c_func(uint8_t idx)
{
    this_idx = idx;
    return (emos_if_i2c_t *)&emos_if_i2c;
}

/******************************************************************************/
static uint8_t do_i2c_swrite(uint16_t caddr, uint8_t *data, uint16_t size )
{
    uint8_t idx = this_idx;
    emos_i2c_swrite_t this_func = emos.iic(idx)->i.swrite(NULL);

    if ( this_func == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }

    return this_func(caddr,data,size);
}

static uint8_t do_i2c_sread( uint16_t caddr, uint8_t *data, uint16_t size )
{
    uint8_t idx = this_idx;
    emos_i2c_sread_t this_func = emos.iic(idx)->i.sread(NULL);

    if ( this_func == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }

    return this_func(caddr,data,size);
}

static uint8_t do_i2c_write(uint16_t caddr, uint16_t raddr, uint8_t *data, uint16_t size )
{
    uint8_t idx = this_idx;
    emos_i2c_write_t this_func = emos.iic(idx)->i.write(NULL);

    if ( this_func == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }

    return this_func(caddr,raddr,data,size);
}

static uint8_t do_i2c_con_write(uint16_t caddr, uint16_t raddr, uint8_t *data, uint16_t size )
{
    uint8_t idx = this_idx;
    emos_i2c_con_write_t this_func = emos.iic(idx)->i.con_write(NULL);
    return this_func(caddr,raddr,data,size);
}

static uint8_t do_i2c_read( uint16_t caddr, uint16_t raddr, uint8_t *data, uint16_t size )
{
    uint8_t idx = this_idx;
    emos_i2c_read_t this_func_read = emos.iic(idx)->i.read(NULL);
    emos_i2c_write_t this_func_write = emos.iic(idx)->i.write(NULL);

    if ( this_func_write == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }

    this_func_write(caddr,raddr,NULL,0);

    if ( this_func_read == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }

    return this_func_read(caddr,raddr,data,size);
}

static uint8_t do_i2c_con_read(uint16_t caddr, uint16_t raddr, uint8_t *data, uint16_t size )
{
    uint8_t idx = this_idx;
    emos_i2c_con_read_t this_func = emos.iic(idx)->i.con_read(NULL);

    if ( this_func == NULL )
    {
        return EMOS_ERROR_NOT_INIT;
    }

    return this_func(caddr,raddr,data,size);
}
