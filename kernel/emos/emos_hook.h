#ifndef __EMOS_HOOK_H__
#define __EMOS_HOOK_H__

#ifdef __cplusplus
extern "C" {
#endif

extern uint8_t HOOK_NAME(emos_vsys_send) (uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len);
extern uint8_t HOOK_NAME(emos_vsys_mrsend)( uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len, uint8_t * more, uint32_t more_len);
extern void * HOOK_NAME(emos_malloc) ( uint32_t len);
extern void HOOK_NAME(emos_free) ( void *p );
extern uint32_t HOOK_NAME(emos_gettick) ( void );
extern unsigned HOOK_NAME(emos_builtin_popcount) ( unsigned u );
extern uint8_t HOOK_NAME(emos_atoi) ( char *str, uint32_t *num );

#ifdef __cplusplus
}
#endif

#endif