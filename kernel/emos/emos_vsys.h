#ifndef __EMOS_VSYS_H__
#define __EMOS_VSYS_H__

#ifdef __cplusplus
extern "C" {
#endif



typedef enum
{
    EMOS_TASK_WDOG,
    EMOS_TASK_GPIO,
    EMOS_TASK_UART,
    EMOS_TASK_I2C,
    EMOS_TASK_TIM,
    EMOS_TASK_NFC,
    EMOS_TASK_BLE,
    EMOS_TASK_LORA,
    EMOS_TASK_FLASH,
    EMOS_TASK_EVENT,
    EMOS_TASK_PSM,
    EMOS_TASK_LORAWAN,
    EMOS_TASK_RADIO,
    EMOS_TASK_RS485,
    EMOS_TASK_SDI12,
    EMOS_TASK_RS232,
    EMOS_TASK_ADC,
    EMOS_TASK_DIGITAL,
    EMOS_TASK_BSPRC,
    EMOS_TASK_MAX,
} EMOS_TASK_ID_E;


#ifdef __cplusplus
}
#endif

#endif

