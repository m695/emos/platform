#include "../kernel.h"
#include "inc/inc_es_pwrsv.h"
#include "inc/inc_es_bsprc.h"

extern void emos_board_event_process(uint16_t id, uint8_t * pd, uint16_t len);

#ifndef BSP_NOT_SUPPORT_WEAK
__RAKLINK_WEAK void emos_board_event_process(uint16_t id, uint8_t *pd , uint16_t len)
{
    /*
        week function
    */
}
#endif

static void emos_hc_event_idle( uint8_t emos_ret )
{
    uint8_t ret = com.vsys->run();
    if (ret == EMOS_MSGBOX_CONTINUE)
    {
        return;
    }

    ret = (ret + emos_ret);
    com_vsys_send(COM_TASK_EVENT, COM_EVENT_SLEEP, &ret, sizeof(ret));
}

void emos_hc_event_process(emos_task_msg_t *task_msg)
{
    emos_msg_type_t *msg_type = (emos_msg_type_t *)&task_msg->msg_type;
    uint8_t *msg_payload = (uint8_t *)task_msg->payload;
    uint16_t msg_len = task_msg->payload_len;
    
    com_source_id = msg_type->msg_id;
    
    switch( msg_type->sys_id )
    {
        case TASK_MODE_INIT:
            emos.vsys->ctrl->do_reboot = IDISABLE;
            com.vsys->init();
            break;
        case TASK_MODE_EVENT:
            switch(msg_type->msg_id)
            {   
                case EMOS_EVENT_TEST_MSG:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_TEST_MSG, msg_payload, msg_len);
                    break;
                    
                case EMOS_EVENT_INIT_DONE:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SYS_INIT_DONE, NULL, 0);
                    break;

                case EMOS_EVENT_IDLE:
                    emos_hc_event_idle( msg_payload[0]);
                    break;

                case EMOS_EVENT_BSP2ATCMD:
                    emos_vsys_tgsend( EMOS_TASK_BSPRC, EMOS_TASK_EVENT_BSPRC_ATCMD, task_msg->src_id,msg_payload, msg_len);
                    break;
                    
                case EMOS_EVENT_100MS:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_100MS, NULL, 0);
                    break;
                
                case EMOS_EVENT_WAKEUP:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_WAKEUP, NULL, 0);
                    break;

                case EMOS_EVENT_SLEEP:
                    do
                    {
                        static uint8_t sleep_cnt = 0;
                        uint8_t sys_ret = msg_payload[0];
                        uint8_t ret;
                        
                        ret = com.vsys->run();
                        if (ret == EMOS_MSGBOX_CONTINUE)
                        {
                            sleep_cnt = 0;
                            break;
                        }

                        sleep_cnt++;
                        if (sleep_cnt < EMOS_SLEEP_DELAYCNT)
                        {
                            break;
                        }
                        
                        sleep_cnt = 0;
                        if ( emos.vsys->ctrl->do_reboot == IENABLE)
                        {
                            emos.units->reboot();
                            return;
                        }
                        
                        if( emos.vsys->ctrl->psm_en == IDISABLE)
                        {
                            break;
                        }

                        if ( sys_ret != EMOS_MSGBOX_NO_TASK)
                        {
                            emos.vsys->ctrl->tsk_pulse = IENABLE;
                            emos_timer0_shot(MSEC(EMOS_PULSE_WAKUP_DEALY)); //set 1 second
                        }
                        
                        emos.vsys->ctrl->psm_do = emos.vsys->ctrl->psm_def;
                        emos_vsys_send( EMOS_TASK_PSM, EMOS_TASK_EVENT_POWERSAVING_ACT, NULL, 0);
                    } while (0);
                    break;

                
                case EMOS_EVENT_USBURT_RECV:
                    goto BOARDS_CASE;
                

                case EMOS_EVENT_URT0_RECV:
                    goto BOARDS_CASE;


                case EMOS_EVENT_URT1_RECV:
                    goto BOARDS_CASE;

                    
                case EMOS_EVENT_URT2_RECV:
                    goto BOARDS_CASE;


                case EMOS_EVENT_LPUART1_RECV:
                    goto BOARDS_CASE;


                case EMOS_EVENT_IIC_RECV:
                    goto BOARDS_CASE;


                case EMOS_EVENT_TIMER0_TRIG:
                    emos.vsys->ctrl->psm_do = IDISABLE;
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_PRI_TIMER_TRIG, msg_payload, msg_len);
                    goto BOARDS_CASE;


                case EMOS_EVENT_TIMER0_SHOT:
                    emos.vsys->ctrl->psm_do = IDISABLE;
                    do
                    {
                        uint32_t evt_action = COM_EVENT_PRI_TIMER_SHOT;
                        uint32_t timer_delay = 0;
                        if (emos.vsys->ctrl->tsk_pulse == IENABLE)
                        {
                            timer_delay = EMOS_PULSE_WAKUP_DEALY;
                            evt_action = COM_EVENT_PRI_TIMER_EARLY;
                            emos.vsys->ctrl->tsk_pulse = IDISABLE;
                        }
                        
                        com_vsys_send( COM_TASK_EVENT, evt_action, &timer_delay, sizeof(timer_delay));
                    } while (0);
                    
                    goto BOARDS_CASE;

                
                case EMOS_EVENT_TIMER0_EARLY:
                    // com_vsys_send( COM_TASK_EVENT, COM_EVENT_PRI_TIMER_EARLY, msg_payload, msg_len);
                    goto BOARDS_CASE;


                case EMOS_EVENT_TIMER1_TRIG:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_SEC_TIMER_TRIG, msg_payload, msg_len);
                    goto BOARDS_CASE;
                    

                case EMOS_EVENT_TIMER1_SHOT:
                    goto BOARDS_CASE;
                
                
                case EMOS_EVENT_TIMER1_EARLY:
                    // com_vsys_send( COM_TASK_EVENT, COM_EVENT_SEC_TIMER_EARLY, msg_payload, msg_len);
                    goto BOARDS_CASE;

                
                case EMOS_EVENT_BLE_CONN:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_BLE_CONN, msg_payload, msg_len);
                    goto BOARDS_CASE;

                    
                case EMOS_EVENT_BLE_DISCONN:
                    com_vsys_send( COM_TASK_EVENT, COM_EVENT_BLE_DISC, msg_payload, msg_len);
                    goto BOARDS_CASE;
                
                case EMOS_EVENT_BLE_SEND:
                    goto BOARDS_CASE;

                case EMOS_EVENT_BLE_RECV:
                    goto BOARDS_CASE;


                case EMOS_EVENT_NFC_RECV:
                    goto BOARDS_CASE;

                /* LoRa */
                case EMOS_EVENT_LORA_JOINED:
                    com_vsys_send(COM_TASK_EVENT,COM_EVENT_LORA_JOINED, NULL, 0);
                    emos.lorawan->joined();
                    break;

                case EMOS_EVENT_LORA_SEND:
                    goto BOARDS_CASE;


                case EMOS_EVENT_LORA_RECV:
                    com_vsys_send(COM_TASK_EVENT,COM_EVENT_LORA_RECV, msg_payload, msg_len);
                    goto BOARDS_CASE;

                    
                case EMOS_EVENT_LORAWAN_ERR:
                    com_vsys_send(COM_TASK_EVENT,COM_EVENT_LORAWAN_ERR, msg_payload, msg_len);
                    break;

                /*  */
                case EMOS_EVENT_GPIO_TRIG:
                    do
                    {
                        emos_evet_gpio_t *p_gpio;
                        p_gpio = (emos_evet_gpio_t *)msg_payload;
                        uint16_t val = p_gpio->sel;
                        const uint32_t this_tbl[2] = 
                        {
                            [0] = COM_EVENT_GPIO_PULLDN,
                            [1] = COM_EVENT_GPIO_PULLUP,
                        };

                        com_vsys_send(COM_TASK_EVENT,this_tbl[p_gpio->state],(uint8_t *)&val, sizeof(val));
     
                    } while (0);
                    break;


                BOARDS_CASE:
                default:
                    emos_board_event_process(msg_type->msg_id,msg_payload,msg_len);
                    break;
            }
            break;
    }
}


