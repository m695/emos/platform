#ifndef __EMOS_H__
#define __EMOS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "../kernel.h"

#define emos_vsys_send( TID, MSG, DAT, LEN )                       emos_vsys_send_hook(TID, 0, MSG, (uint8_t *)DAT, LEN)
#define emos_vsys_tgsend( TID, MSG, TAG, DAT, LEN )                emos_vsys_send_hook(TID, TAG, MSG, (uint8_t *)DAT, LEN)
#define emos_vsys_mrsend( TID, MSG, DAT1, LEN1, DAT2, LEN2 )       emos_vsys_mrsend_hook(TID, 0, MSG, DAT1, LEN1, (uint8_t *)DAT2, LEN2)
#define emos_vsys_sendend( TID, MSG, DAT, LEN )                    emos_vsys_mrsend(TID, MSG, DAT, LEN ,"\0",1)
#define emos_task_wakeup( TID )                                    emos.vsys->mbox()->sleep(TID, IDISABLE);
#define emos_task_sleep( TID )                                     emos.vsys->mbox()->sleep(TID, IENABLE);
#define emos_task_timeout( TID,TIMEOUT )                           emos.vsys->mbox()->timeout(TID, TIMEOUT);
#define emos_outext                                                emos.log->outext

#define emos_memcpy(DES,SRC,LEN)          do{for (size_t i = 0; i < LEN; i++) { ((uint8_t *)DES)[i]=((uint8_t *)SRC)[i]; } } while (0)

#define emos_gettick()       emos_gettick_hook()//emos.vsys->ctrl->gettick
#define emos_malloc          emos_malloc_hook//emos.vsys->mem()->malloc
#define emos_free            emos_free_hook//emos.vsys->mem()->free
#define emos_atoi            emos_atoi_hook//emos.units->atoi
#define emos_split(SRC,SEP,NUM)           emos.units->split(SRC,SEP,0,NUM)
#define emos_split_limit(SRC,SEP,NUM)     emos.units->split(SRC,SEP,3,NUM);
#define emos_popcount        emos_builtin_popcount_hook//emos.units->builtin.popcount
#define emos_strcmp          emos.units->strcmp
#define emos_memswap         emos.units->memswap
#define emos_atoh            emos.units->atoh
#define emos_reboot          emos.units->reboot
#define emos_delay           emos.units->dly
#define emos_timer0_shot     emos.tim(0).shot
#define emos_timer0_start    emos.tim(0).start
#define emos_timer0_stop     emos.tim(0).stop
#define emos_timer1_shot     emos.tim(1).shot
#define emos_timer1_start    emos.tim(1).start
#define emos_timer1_stop     emos.tim(1).stop

#define emos_tmsg(...) \
do { \
    uint8_t *this_emos_tmsg = emos_outext(__VA_ARGS__); \
    emos_vsys_sendend( EMOS_TASK_EVENT, EMOS_EVENT_TEST_MSG, (uint8_t *)this_emos_tmsg, strlen((char *)this_emos_tmsg)); \
} while (0)

/******************************************************************************/
/*                            UART                                            */
/******************************************************************************/

typedef struct 
{
    union
    {
        uint8_t value;
        struct
        {
            uint8_t disable:1;
        };
    };

    
    uint32_t register_event;
} emos_if_uart_status_t;

typedef struct
{
    struct 
    {
        void * (*putc) ( void * );
        void * (*puts) ( void * );
        void * (*flush) ( void * );
        void * (*recv) ( void * );
    } i;
    
    void (*putc)    ( uint8_t );
    void (*puts)    ( const char * , uint16_t);
    void (*recv)    (  char * , uint16_t, uint32_t);
    void (*flush)    ( void );
    void (*qputs)    ( const char * , uint16_t);
    emos_if_uart_status_t *(*sta)   ( void );
} __EMOS_PACKED emos_if_uart_t;

/******************************************************************************/
/*                            MSGBOX                                          */
/******************************************************************************/

#define TASK_MODE_INIT    0b00000001
#define TASK_MODE_EVENT   0b00000010
#define TASK_MODE_POLLING 0b00000100
#define TASK_MODE_SLEEP   0b00001000
#define TASK_MODE_WAKEUP  0b00010000
#define TASK_MODE_TIMEOUT 0b00100000

typedef uint32_t msg_type_t;

typedef enum
{
    EMOS_MSGBOX_NO_TASK = 0,
    EMOS_MSGBOX_CONTINUE,
    EMOS_MSGBOX_SLEEP_AND_NO_TASK,
    EMOS_MSGBOX_WAIT_AND_NO_TASK,
} EMOS_MSGBOX_E;

typedef struct
{
    union 
    {
        msg_type_t value;
        struct 
        {
            msg_type_t   msg_id:24;
            msg_type_t   sys_id:8;
        };
    };
    
} __EMOS_PACKED emos_msg_type_t;

typedef struct 
{
    uint8_t    task_id;
    msg_type_t msg_type;
    msg_type_t src_id;
    uint32_t   payload_len;
    uint8_t    payload[];    
} __EMOS_PACKED emos_task_msg_t;

typedef struct 
{
    uint8_t priority;
    uint8_t mode;
    uint32_t timeout;
    void (*task_entry)(emos_task_msg_t *msg); 
} __attribute__((aligned(4))) emos_task_entry_t;

typedef struct
{
    uint32_t time_tick;
} __EMOS_PACKED emos_if_msgbox_ctrl_t;

typedef struct
{
    emos_if_msgbox_ctrl_t *ctrl;
    void (*init)       ( uint8_t *, uint16_t  );
    uint8_t (*polling) ( void );
    uint8_t (*send)       ( uint8_t, uint8_t, msg_type_t, uint8_t *, uint32_t );
    uint8_t (*mrsend)     ( uint8_t, uint8_t, msg_type_t, uint8_t *, uint32_t,uint8_t *, uint32_t );
    void (*task_add)      ( emos_task_entry_t * );
    void (*sleep)         ( uint8_t , uint8_t );
    void (*timeout)       ( uint8_t , uint32_t );
    void (*destroy)       ( uint8_t );
    uint8_t (*count)      ( void );
    uint16_t (*taskcnt)   ( uint8_t );
} __EMOS_PACKED emos_if_msgbox_t;

/******************************************************************************/
/*                            NBIOT                                            */
/******************************************************************************/

typedef struct
{
    struct 
    {
        void (*power)    ( void );
    } i;
    void (*power)    ( void );
    
} __EMOS_PACKED emos_if_nbiot_t;


/******************************************************************************/
/*                            NFC                                            */
/******************************************************************************/

typedef struct
{
    void (*test)          ( void );
    uint8_t (*ts_en)      ( uint8_t );
    void ( *test_ck )     ( void );
    
} __EMOS_PACKED emos_if_nfc_t;

/******************************************************************************/
/*                            WDOG                                            */
/******************************************************************************/

typedef struct
{
    uint32_t time;
} emos_if_wdog_parm_t;

typedef struct
{
    emos_if_wdog_parm_t *param;
    void (*feed)     ( void );
    void (*inc)     ( void );
} __EMOS_PACKED emos_if_wdog_t;

/******************************************************************************/
/*                            LOG                                            */
/******************************************************************************/

typedef enum
{
    EMOS_LOG_DISABLE = 0,
    EMOS_LOG_INFO,
    EMOS_LOG_EVT0, //WIRE
    EMOS_LOG_EVT1, //BLE
    EMOS_LOG_EVT2, //NBIOT
    EMOS_LOG_ALL,
} EMOS_LOG_E;

typedef struct
{
    uint8_t level;
    uint8_t tmp_buf[EMOS_UART_TRANS_BUFF];
} __EMOS_PACKED emos_if_log_ctrl_t;

typedef struct
{
    emos_if_log_ctrl_t *ctrl;
    struct 
    {
        void (*debug)    ( const char * , uint16_t);
    } i; 
    
    void (*debug)   ( uint8_t ,char *, ... );
    void (*debugA)  ( uint8_t ,const char *, uint16_t );
    void (*info)    ( char *, ... );
    void (*puts)    ( uint32_t, const char * , uint16_t);
    void (*printf)  ( uint32_t, char *, ... );
    uint8_t * (*outext)  ( char *, ... );
} __EMOS_PACKED emos_if_log_t;

/******************************************************************************/
/*                            GPIOTE                                             */
/******************************************************************************/

typedef struct
{
    uint8_t group;
    uint8_t pin;
    union 
    {
        uint8_t status;
        struct 
        {
            uint8_t dir:1;
            uint8_t cur_val:1;
            uint8_t lst_val:1;
            uint8_t intr:1;
        };
    };
    
} __EMOS_PACKED emos_if_gpio_param_t;

typedef struct
{  
    void *next;
    emos_if_gpio_param_t info;
} __EMOS_PACKED emos_if_gpio_note_t;

typedef struct 
{
    union 
    {
        uint16_t sel;
        struct
        {
            uint8_t group;
            uint8_t pins;
        };
    };
    uint8_t state;
} __EMOS_PACKED emos_evet_gpio_t;


typedef struct
{
    uint32_t               *addr;
    void (*i_dir)          ( uint8_t, uint8_t, uint8_t);
    void (*i_set)          ( uint8_t, uint8_t, uint8_t);
    void (*i_cfg_pull)     ( uint8_t, uint8_t, uint8_t);
    uint8_t (*i_get)       ( uint8_t, uint8_t );
    void (*o_dir_output)   ( uint8_t, uint8_t);
    void (*o_dir_input)    ( uint8_t, uint8_t, uint8_t);
    void (*o_cfg_pull)     ( uint8_t, uint8_t, uint8_t);
    void (*o_phigh)        ( uint8_t, uint8_t);
    void (*o_plow)         ( uint8_t, uint8_t);
    uint8_t (*o_get)       ( uint8_t, uint8_t);
    void (*o_toggle)       ( uint8_t, uint8_t);
} __EMOS_PACKED emos_if_gpio_t;

/******************************************************************************/
/*                            TIM                                            */
/******************************************************************************/

typedef struct 
{
    uint32_t cfg_time;
    uint32_t offset_time;
} __EMOS_PACKED emos_if_tim_ctrl_t;


typedef struct
{
    struct 
    {
        void (*khz_clk)   ( uint32_t );
        void * (*stop) ( void * );
        void * (*start) ( void * );
        void * (*shot ) ( void * );
    } i;
    
    void (*start ) ( uint32_t );
    void (*shot ) ( uint32_t );
    void (*stop ) ( void );

    emos_if_tim_ctrl_t *ctrl;
} __EMOS_PACKED emos_if_tim_t;

/******************************************************************************/
/*                            MALLOC                                          */
/******************************************************************************/


typedef struct
{
    void *(*malloc) ( uint32_t);
    void  (*free)   ( void * );
    void  (*init)   ( uint8_t *, uint16_t);
    void *(*copy)   ( uint32_t, void *, uint32_t);
    uint8_t  (*active) ( void );
} __EMOS_PACKED emos_if_malloc_t;

/******************************************************************************/
/*                            LINKLIST                                        */
/******************************************************************************/

typedef struct
{
    void * (*create) ( uint8_t, void **, uint16_t);
    void (*remove)   ( uint8_t, void **, void * );
    uint32_t (*count)( uint8_t, void ** );
    void * (*get)    ( uint8_t, void **, uint32_t);
} __EMOS_PACKED emos_if_linklist_t;

/******************************************************************************/
/*                            VSYS                                            */
/******************************************************************************/


typedef void (*init_handle_t) ( void );
typedef void (*loop_handle_t) ( uint32_t , uint8_t * ,uint16_t);

typedef struct
{
    union
    {
        uint16_t value;
        struct
        {
            uint16_t psm_en:1;
            uint16_t psm_def:1;
            uint16_t psm_do:1;
            uint16_t tsk_pulse:1;
            uint16_t do_reboot:1;
        };
    };
    uint32_t gettick;
} __EMOS_PACKED emos_if_vsys_ctrl_t;


typedef struct
{
    void (*init)     ( void );
    void (*run)      ( void );
    emos_if_msgbox_t *  (*mbox )   ( void );
    emos_if_malloc_t *  (*mem )   ( void );
    void (*reg_init_handle) ( void );
    void (*reg_loop_handle) ( uint32_t , uint8_t *, uint16_t);
    emos_if_vsys_ctrl_t *ctrl;
} __EMOS_PACKED emos_if_vsys_t;

/******************************************************************************/
/*                            I2C                                             */
/******************************************************************************/

typedef struct
{
    struct 
    {
        void * (*write)     ( void *adr );
        void * (*read)      ( void *adr );
        void * (*con_write) ( void *adr );
        void * (*con_read)  ( void *adr );  
        void * (*swrite)     ( void *adr );
        void * (*sread)      ( void *adr );
    } i;
    uint8_t (*swrite)     ( uint16_t, uint8_t *, uint16_t );
    uint8_t (*sread)      ( uint16_t, uint8_t *, uint16_t );

    uint8_t (*write)     ( uint16_t, uint16_t, uint8_t *, uint16_t );
    uint8_t (*read)      ( uint16_t, uint16_t, uint8_t *, uint16_t );
    uint8_t (*con_write) ( uint16_t, uint16_t, uint8_t *, uint16_t );
    uint8_t (*con_read)  ( uint16_t, uint16_t, uint8_t *, uint16_t );
} __EMOS_PACKED emos_if_i2c_t;

/******************************************************************************/
/*                            RS485                                           */
/******************************************************************************/

typedef struct
{
    struct 
    {
        void * (*deconf)      ( void *adr );
        void * (*conf)      ( void *adr );
        void * (*write)     ( void *adr );
        void * (*read)      ( void *adr );
    } i;
    uint8_t (*conf)      ( uint32_t , uint8_t , uint8_t , int8_t );
    uint8_t (*write)     ( uint8_t *, int );
    uint8_t (*read)      ( uint8_t *, int, uint16_t );
    uint8_t (*deconf)    ( void );
} __EMOS_PACKED emos_if_rs485_t;

/******************************************************************************/
/*                            SDI12                                           */
/******************************************************************************/

typedef struct
{
    struct 
    {
        void * (*conf)      ( void *adr );
        void * (*deconf)    ( void *adr );
        void * (*write)     ( void *adr );
        void * (*read)      ( void *adr );
    } i;
    uint8_t (*conf)      ( uint32_t , uint8_t , uint8_t , int8_t );
    uint8_t (*deconf)    ( void );
    uint8_t (*write)     ( uint8_t *, int );
    uint8_t (*read)      ( uint8_t *, int, uint16_t );
} __EMOS_PACKED emos_if_sdi12_t;

/******************************************************************************/
/*                            RS232                                           */
/******************************************************************************/

typedef struct
{
    struct 
    {
        void * (*conf)      ( void *adr );
        void * (*deconf)    ( void *adr );
        void * (*write)     ( void *adr );
        void * (*read)      ( void *adr );
    } i;
    uint8_t (*conf)      ( uint32_t , uint8_t , uint8_t , int8_t );
    uint8_t (*deconf)    ( void );
    uint8_t (*write)     ( uint8_t *, int );
    uint8_t (*read)      ( uint8_t *, int, uint16_t );
} __EMOS_PACKED emos_if_rs232_t;

/******************************************************************************/
/*                            ADC                                           */
/******************************************************************************/

typedef struct
{
    struct 
    {
        void * (*read)      ( void *adr );
    } i;
    uint32_t (*read)      ( void );
} __EMOS_PACKED emos_if_adc_t;

/******************************************************************************/
/*                            DIGITAL                                           */
/******************************************************************************/

typedef struct
{
    struct 
    {
        void * (*read)      ( void *adr );
        void * (*write)      ( void *write );
    } i;
    uint8_t (*read)      ( void );
    void (*write)      ( uint8_t );
} __EMOS_PACKED emos_if_digital_t;

/******************************************************************************/
/*                            FLASH                                        */
/******************************************************************************/

typedef struct 
{
    struct 
    {
        void * (*spi_write)        ( void *adr );
        void * (*spi_read)         ( void *adr );
        void * (*spi_earse)        ( void *adr );
        void * (*eeprom_write)     ( void *adr );
        void * (*eeprom_read)      ( void *adr );
        void * (*eeprom_readonly)  ( void *adr );
        void * (*field_write)      ( void *adr );
        void * (*field_read)       ( void *adr );
    } i;
    
    uint32_t    (*spi_write)  ( uint32_t, uint32_t *, uint32_t );
    uint32_t    (*spi_earse)  ( uint32_t, uint32_t );
    void        (*spi_read)   ( uint32_t, uint8_t*, uint32_t );
    
    uint32_t    (*eeprom_write)  ( uint32_t, uint8_t*, uint32_t );
    void        (*eeprom_read)   ( uint32_t, uint8_t*, uint32_t );
    void        (*eeprom_readonly)   ( uint32_t, uint8_t**, uint32_t );

    uint32_t    (*field_write)  ( uint32_t, uint8_t*, uint32_t );
    void        (*field_read)   ( uint32_t, uint8_t**, uint32_t );


} __EMOS_PACKED emos_if_flash_t;

/******************************************************************************/
/*                            UNITS                                        */
/******************************************************************************/

typedef struct 
{
    char **  (*split)     ( char *, const char *, uint16_t, uint16_t *);
    uint8_t  (*format)    ( char *, const char *);
    uint16_t (*strcmp)    ( char *, const char *, uint8_t ) ;
    uint8_t  (*atoi)      ( char *, uint32_t * );
    uint8_t  (*atoh)      ( char *, char *, uint32_t);
    
    uint8_t* (*memcat)    ( const char *, uint8_t, const char *, uint8_t);
    uint8_t  (*memswap)   ( uint8_t *, uint8_t);
    uint32_t (*rand)      ( void );
    char     (*toupper)   ( char );

    
    void     (*dly)     ( uint32_t );
    void     (*reboot)    ( void );
    uint32_t (*gettick)   ( void );

    struct 
    {
        uint32_t (*crc32)      (const void *, size_t );
        uint16_t (*crc16)      (const void *, size_t );
        unsigned (*popcount)   ( unsigned );
    } builtin;

    struct 
    {
        void     (*enable)    ( uint8_t );
        void     (*swich)     ( uint8_t );
        struct 
        {
            void * (*enable)     ( void *adr );
            void * (*swich)       ( void *adr );
        } i;
    } radio;
    

    struct 
    {
        void (*rand_seed)    ( uint8_t, unsigned long);
        void * (*dly)         ( void *adr );
        void * (*reboot)     ( void *adr );
        void * (*gettick)    ( void *adr );
    }i;
    
} __EMOS_PACKED emos_if_units_t;

/******************************************************************************/
/*                            BLE                                        */
/******************************************************************************/

typedef union
{
    uint8_t value;
    struct 
    {
        uint8_t connect:1;
    };
} __EMOS_PACKED emos_hal_ble_status_t;

typedef struct 
{
    void (*adv_start)  ( void );
    void (*adv_name)   ( char * , uint8_t );
    void (*stop)       ( void );
    void (*disconnect) ( void ) ;

    
    struct 
    {
        void * (*getmac) ( void *);
        void * (*puts) ( void *);
        void * (*putc) ( void *);
    } i;

    struct 
    {
        void (*putc)    ( uint8_t );
        void (*puts)    ( const char * , uint16_t);
    } uart;
    
    uint8_t * (*getmac) (void);
    emos_hal_ble_status_t *status;
    void (*send)       ( uint8_t *, uint16_t );
    void ( *test )       ( void );
    void ( *test_ck )       ( void );
    uint8_t (*ts_en) ( uint8_t );
} __EMOS_PACKED emos_if_ble_t;

/******************************************************************************/
/*                            LORA                                        */
/******************************************************************************/

typedef enum
{
    LORA_BAND_EU433 = 0,
    LORA_BAND_CN470,
    LORA_BAND_RU864,
    LORA_BAND_IN865,
    LORA_BAND_EU868,
    LORA_BAND_US915,
    LORA_BAND_AU915,
    LORA_BAND_KR920,
    LORA_BAND_AS923,
} rak_lora_band_e;

typedef union
{
    uint8_t value;
    struct 
    {
        uint8_t busy:1;
    };
} __EMOS_PACKED emos_hal_lora_ctrl_t;

typedef struct 
{
    uint16_t join_cnt;
    uint16_t send_cnt;
} __EMOS_PACKED emos_hal_lora_param_t;


typedef struct 
{
    emos_hal_lora_ctrl_t *ctrl;
    struct 
    {
        void * (*set_dr) ( void *);
        void * (*get_dr) ( void *);
        void * (*set_adr) ( void *);
        void * (*get_adr) ( void *);
        void * (*set_cfm) ( void *);
        void * (*get_cfm) ( void *);
        void * (*set_band) ( void * );
        void * (*get_band) ( void * );
        void * (*get_njs) ( void *);
        void * (*set_nwm) ( void * );
        void * (*get_nwm) ( void * );
        void * (*set_class) ( void * );
        void * (*get_class) ( void * );
        void * (*set_deveui) ( void * );
        void * (*get_deveui) ( void * );
        void * (*set_appeui) ( void * );
        void * (*get_appeui) ( void * );
        void * (*set_appkey) ( void * );
        void * (*get_appkey) ( void * );
        void * (*reset) ( void * );
    } i;

    uint8_t (*get_njs)      ( void );
    uint8_t (*set_band) ( uint8_t );
    int16_t (*get_band) ( void );
    uint8_t (*set_dr) ( uint8_t );
    uint8_t (*get_dr) ( void );
    uint8_t (*set_adr) ( uint8_t );
    uint8_t (*get_adr) ( void );
    uint8_t (*set_nwm) ( uint8_t );
    int16_t (*get_nwm) ( void );
    uint8_t (*set_cfm) ( uint8_t );
    uint8_t (*get_cfm) ( void );
    uint8_t (*set_class) ( uint8_t );
    int16_t (*get_class) ( void );
    uint8_t (*set_deveui) ( uint8_t * );
    uint8_t (*get_deveui) ( uint8_t * );
    uint8_t (*set_appeui) ( uint8_t * );
    uint8_t (*get_appeui) ( uint8_t * );
    uint8_t (*set_appkey) ( uint8_t * );
    uint8_t (*get_appkey) ( uint8_t * );

    void (*uplink)      ( uint8_t , const char * , uint16_t);
    void (*join)          ( void );
    void (*kreset)          ( void );
    void (*reset)          ( void );
    void (*test)          ( uint32_t );
    uint8_t (*ts_en) ( uint8_t );
    void ( *test_ck )       ( void );
    
} __EMOS_PACKED emos_if_lora_t;

/******************************************************************************/
/*                            LORAWAN                                        */
/******************************************************************************/
typedef enum
{
    EMOS_LORAWAN_ERR_UNKONW = 0,
    EMOS_LORAWAN_ERR_NOJOINED,
    EMOS_LORAWAN_ERR_JOINFAIL,
} EMOS_LORAWAN_ERR_E;

typedef struct
{
    
    struct
    {
        union
        {
            uint8_t value;
            struct
            {
                uint8_t enable:1;
            };
        };
        
        uint8_t cnt_def;
        uint8_t cnt;
        uint32_t delay_time;
    } auto_join;
    
    uint32_t send_cnt;
    uint32_t curr_cnt;
} emos_if_lorawan_ctrl_t;

typedef struct 
{
    emos_if_lorawan_ctrl_t *ctrl;
    void ( *title )      ( uint8_t *, uint16_t );
    void ( *heap )       ( uint8_t *, uint16_t );
    void ( *fport )      ( uint8_t );
    void ( *send )       ( void );
    void ( *joined )     ( void );
    void ( *autojoin )     ( void );
    
} __EMOS_PACKED emos_if_lorawan_t;


/******************************************************************************/
/*                            STRING                                        */
/******************************************************************************/

typedef enum
{
    EMOS_OK,
    EMOS_ERROR,
    EMOS_FAIL = EMOS_ERROR,
    EMOS_PARAM_ERROR,
    EMOS_BUSY_ERROR,
    EMOS_TEST_PARAM_OVERFLOW,
    EMOS_NO_CLASSB_ENABLE,
    EMOS_NO_NETWORK_JOINED,
    EMOS_RX_ERROR,
    EMOS_MODE_NO_SUPPORT,
    EMOS_CMD_NOT_FOUND,
    EMOS_FORMAT_ERROR,
    EMOS_ERROR_NOT_INIT,
    EMOS_NONE_OK,
    EMOS_CLI_CONTINUE,
    EMOS_CLI_WAIT,
    EMOS_TIMEOUT_ERROR,
    EMOS_NOT_SUPPORT,
} EMOS_ERRCODE_E;

/******************************************************************************/

typedef void (*emos_def_puts_t ) ( const char * , uint16_t);

typedef struct
{
    jmp_buf * svpnt;
    char ** errcode;
    emos_if_malloc_t *      (*mem)(uint8_t);
    emos_if_linklist_t      *llist;
    emos_if_uart_t          (*uart)(uint8_t);
    emos_if_msgbox_t *      (*msgbox)(uint8_t);
    emos_if_vsys_t          *vsys;
    emos_if_i2c_t *         (*iic)(uint8_t);
    emos_if_rs485_t *       (*rs485)(uint8_t);
    emos_if_sdi12_t *       (*sdi12)(uint8_t);
    emos_if_rs232_t *       (*rs232)(uint8_t);
    emos_if_adc_t           (*adc)(uint8_t);
    emos_if_digital_t       (*digital)(uint8_t);
    emos_if_tim_t           (*tim)(uint8_t);
    emos_if_units_t         *units;
    emos_if_ble_t           *ble;
    emos_if_lora_t          *lora;
    emos_if_lorawan_t       *lorawan;
    emos_if_gpio_t          *gpio;
    emos_if_flash_t         *flash;
    emos_if_wdog_t          *wdog;
    emos_if_nfc_t           *nfc;
    emos_if_log_t           *log;
} __EMOS_PACKED emos_t;

extern const emos_t emos;

#ifdef __cplusplus
}
#endif
#endif
