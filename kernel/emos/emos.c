#include "../kernel.h"
#include "emos.h"

extern const emos_if_malloc_t    emos_if_malloc;
extern const emos_if_linklist_t  emos_if_linklist;
extern const emos_if_vsys_t      emos_if_vsys;
extern const emos_if_wdog_t      emos_if_wdog;
extern const emos_if_units_t     emos_if_units;
extern const emos_if_ble_t       emos_if_ble;
extern const emos_if_lora_t      emos_if_lora;
extern const emos_if_lorawan_t   emos_if_lorawan;
extern const emos_if_nfc_t       emos_if_nfc;
extern const emos_if_gpio_t      emos_if_gpio;
extern const emos_if_flash_t     emos_if_flash;
extern const emos_if_log_t       emos_if_log;


extern emos_if_uart_t emos_if_uart_func(uint8_t idx);
extern emos_if_malloc_t * emos_if_malloc_func(uint8_t idx);
extern emos_if_msgbox_t * emos_if_msgbox_func(uint8_t idx);
extern emos_if_i2c_t * emos_if_i2c_func(uint8_t idx);
extern emos_if_rs485_t * emos_if_rs485_func(uint8_t idx);
extern emos_if_sdi12_t * emos_if_sdi12_func(uint8_t idx);
extern emos_if_rs232_t * emos_if_rs232_func(uint8_t idx);
extern emos_if_adc_t emos_if_adc_func(uint8_t idx);
extern emos_if_tim_t emos_if_tim_func(uint8_t idx);
extern emos_if_digital_t emos_if_digital_func(uint8_t idx);


static const char emos_string_ok[]             = "OK";
static const char emos_string_error[]          = "AT_ERROR";
static const char emos_string_param_error[]    = "AT_PARAM_ERROR";
static const char emos_string_busy_error[]     = "AT_BUSY_ERROR";
static const char emos_string_test_param_overflow[]  = "AT_TEST_PARAM_OVERFLOW";
static const char emos_string_no_classb_enable[]     = "AT_NO_CLASSB_ENABLE";
static const char emos_string_no_network_joined[]    = "AT_NO_NETWORK_JOINED";
static const char emos_string_rx_error[]             = "AT_RX_ERROR";
static const char emos_string_mode_no_support[]   = "AT_MODE_NO_SUPPORT";
static const char emos_string_cmd_not_found[]  = "AT_COMMAND_NOT_FOUND";
static const char emos_string_format_error[]   = "AT_FORMAT_ERROR";
static const char emos_string_format_noneok[]  = "";
static const char emos_string_format_cli_continue[]  = ">";
static const char emos_string_timeout_error[]   = "AT_TIMEOUT_ERROR";
static jmp_buf jmpbuffer;
/******************************************************************************/

static const char * do_errcode_str[] =
{
    [EMOS_OK]     = emos_string_ok,
    [EMOS_ERROR]   = emos_string_error,
    [EMOS_PARAM_ERROR]   = emos_string_param_error,
    [EMOS_BUSY_ERROR]   = emos_string_busy_error,
    [EMOS_TEST_PARAM_OVERFLOW]   = emos_string_test_param_overflow,
    [EMOS_NO_CLASSB_ENABLE]   = emos_string_no_classb_enable,
    [EMOS_NO_NETWORK_JOINED]   = emos_string_no_network_joined,
    [EMOS_RX_ERROR]   = emos_string_rx_error,
    [EMOS_MODE_NO_SUPPORT]   = emos_string_mode_no_support,
    [EMOS_CMD_NOT_FOUND]   = emos_string_cmd_not_found,
    [EMOS_FORMAT_ERROR]  = emos_string_format_error,
    [EMOS_NONE_OK]       = emos_string_format_noneok,
    [EMOS_CLI_CONTINUE]  = emos_string_format_cli_continue,
    [EMOS_TIMEOUT_ERROR]  = emos_string_timeout_error,
};

const emos_t emos = 
{ 
    .svpnt     = &jmpbuffer,
    .errcode   = (char **)do_errcode_str,
    .mem      = emos_if_malloc_func,
    .llist      = (emos_if_linklist_t *)&emos_if_linklist,
    .uart      = emos_if_uart_func,
    .msgbox    = emos_if_msgbox_func,
    .vsys       = (emos_if_vsys_t *)&emos_if_vsys,
    .units      = (emos_if_units_t *)&emos_if_units,
    .tim        = emos_if_tim_func,
    .iic        = emos_if_i2c_func,
    .ble        = (emos_if_ble_t *)&emos_if_ble,
    .lora       = (emos_if_lora_t *)&emos_if_lora,
    .gpio       = (emos_if_gpio_t *)&emos_if_gpio,
    .flash      = (emos_if_flash_t *)&emos_if_flash,
    .wdog       = (emos_if_wdog_t *)&emos_if_wdog,
    .nfc        = (emos_if_nfc_t *)&emos_if_nfc,
    .lorawan    = (emos_if_lorawan_t *)&emos_if_lorawan,
    .log        = (emos_if_log_t *)&emos_if_log,
    .rs485      = emos_if_rs485_func,
    .sdi12      = emos_if_sdi12_func,
    .rs232      = emos_if_rs232_func,
    .adc        = emos_if_adc_func,
    .digital    = emos_if_digital_func,
};
