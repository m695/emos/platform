#include "../kernel.h"
#include "emos.h"

uint8_t HOOK_NAME(emos_vsys_send) (uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len)
{
    return emos.vsys->mbox()->send(task_id, dir_id, msg_type, msg, len);
}

uint8_t HOOK_NAME(emos_vsys_mrsend) (uint8_t task_id, uint8_t dir_id, msg_type_t msg_type, uint8_t * msg, uint32_t len, uint8_t * more, uint32_t more_len)
{
    return emos.vsys->mbox()->mrsend(task_id, dir_id, msg_type, msg, len, more, more_len);
}

uint32_t HOOK_NAME(emos_gettick) ( void )
{
    return emos.vsys->ctrl->gettick;
}

void * HOOK_NAME(emos_malloc) ( uint32_t len)
{
    return emos.vsys->mem()->malloc(len);
}

void HOOK_NAME(emos_free) ( void *p )
{
    emos.vsys->mem()->free(p);
}

unsigned HOOK_NAME(emos_builtin_popcount) ( unsigned u )
{
    return emos.units->builtin.popcount(u);
}

uint8_t HOOK_NAME(emos_atoi) ( char *str, uint32_t *num )
{
    return emos.units->atoi(str,num);
}

