#ifndef __START_H__
#define __START_H__

#ifdef __cplusplus
extern "C" {
#endif

extern void start(void);

#ifdef __cplusplus
}
#endif

#endif

