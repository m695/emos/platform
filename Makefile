######################################
# target
######################################

MAKEALL_CMD=

include boards/boards.mk

all:
	echo ${MAKEALL_CMD}
	make ${MAKEALL_CMD}

list:
	@echo ${MAKEALL_CMD}

# *** EOF ***
